proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	Descartes.Symbolizers.Descartes_Symbolizers_RequestSLD = {
	    'Point': {
	        graphicName: 'square',
	        fillColor: '#FFFF00',
	        pointRadius: 8.0
	    },
	    'Line': {
	        fillColor: '#FFFF00',
	        fillOpacity: 1,
	        strokeColor: '#FFFF00',
	        strokeWidth: 5.0,
	        strokeOpacity: 1
	    },
	    'Polygon': {
	        fillColor: '#FFFF00',
	        fillOpacity: 0.5,
	        strokeOpacity: 1,
	        strokeColor: '#FFFF00',
	        strokeWidth: 2.0
	    }
	};
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	// Ajout de couches au groupe
	contenuCarte.addItem(coucheStations);
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheNature);

	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});

	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes',{
		alwaysExportSelectionLayers:true,
        resultUiParams: {
            withCsvExport: true,
            withReturn: true
        }});
	var requeteStation = new Descartes.Request(coucheStations, "Filtrer les stations essence", Descartes.Layer.POINT_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Station Total","Station Total Le National", "Station Total - Garage Renault"],
					true
				);
	
	requeteStation.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteStation);
	
	var requeteNature = new Descartes.Request(coucheNature, "Filtrer les espaces naturels", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["La Durance","Canal EDF"],
					true
				);

	requeteNature.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteNature);
	
	var requeteEau = new Descartes.Request(coucheEau, "Filtrer les cours d'eau", Descartes.Layer.LINE_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Canal de Marseille","La Touloubre"],
					true
				);
	
	requeteEau.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteEau);
	
}
