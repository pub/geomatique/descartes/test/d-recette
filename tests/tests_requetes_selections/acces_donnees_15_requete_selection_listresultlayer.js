proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	Descartes.Layer.ResultLayer.config.displayMarker = true;
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	// Ajout de couches au groupe
	coucheStations.geometryType = "Point";
	coucheStations.attributes = {
       attributesAlias: [
           	{fieldName: 'name', label: 'Alias Nom &àôé'}
       ],
	};
	contenuCarte.addItem(coucheStations);
	coucheEau.geometryType = "Line";
	coucheEau.attributes = {
       attributesAlias: [
           	{fieldName: 'name', label: 'Alias Nom'}
       ],
	};
	coucheEau.activeToQuery=true;
	contenuCarte.addItem(coucheEau);
	coucheNature.geometryType = "Polygon";
	coucheNature.attributes = {
       attributesAlias: [
           	{fieldName: 'osm id', label: 'Alias Identifiant'}
       ],
	};
	coucheNature.activeToQuery=true;
	contenuCarte.addItem(coucheNature);
	coucheRoutes.geometryType = "Line";
	coucheRoutes.activeToQuery=true;
	coucheRoutes.queryable=true;
	contenuCarte.addItem(coucheRoutes);
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
        args: {
        	persist:true, 
        	infoRadius: true,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_BUFFER_HALO_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	configHalo: true,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.LINE_BUFFER_HALO_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	configHalo: true,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	
    var managerOptions = {
        uiOptions: {
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            }
        }
    };
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes',{
        resultUiParams: {
            withReturn: true,
            withUIExports: true,
            withAvancedView: true,
            withResultLayerExport: true,
            withFilterColumns: true,
            withListResultLayer: true
        }});
	var requeteStation = new Descartes.Request(coucheStations, "Filtrer les stations essence", Descartes.Layer.POINT_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Station Total","Station Total Le National", "Station Total - Garage Renault"],
					true
				);
	
	requeteStation.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteStation);
	
	/*var requeteNature = new Descartes.Request(coucheNature, "Filtrer les espaces naturels", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["La Durance","Canal EDF"],
					true
				);

	requeteNature.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteNature);
	
	var requeteEau = new Descartes.Request(coucheEau, "Filtrer les cours d'eau", Descartes.Layer.LINE_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Canal de Marseille","La Touloubre"],
					true
				);
	
	requeteEau.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteEau);*/
	
    //var toolTipLayers = [{layer: coucheStations, fields: ['name']}];
    //carte.addToolTip('ToolTip', toolTipLayers);
    
    carte.addOpenLayersInteractions([
                                     {type: Descartes.Map.OL_DRAG_PAN},
                                     {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}]);
}
