proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	coucheGeoserverGeobretagneDrealB = new Descartes.Layer.WMS(
			"(GS)couche Geobretagne", 
			[
				{
					serverUrl: "https://geobretagne.fr/geoserver/dreal_b/wms?",
					layerName: "EPCI_053",
					serverVersion: "1.1.1",
					featureServerUrl: "https://geobretagne.fr/geoserver/dreal_b/wfs?",
					featureName: "EPCI_053",
					//featureNameSpace:"dreal_b",
					featureServerVersion: "1.1.0",
					featureGeometryName: "the_geom"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: ["https://geobretagne.fr/geoserver/dreal_b/wms?" + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=EPCI_053"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.POLYGON_GEOMETRY
			}
	);
	contenuCarte.addItem(coucheGeoserverGeobretagneDrealB);

	coucheGeoserverCarto2Prod_point = new Descartes.Layer.WMS(
			"(GS)couche Point Carto2", 
			[
				{
					serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d/c_stations?",
					layerName: "c_stations",
					serverVersion: "1.1.1",
					//internalProjection: "EPSG:3857",
					featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d/c_stations?",
		            featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
					featureName: "c_stations",
					featureServerVersion: "1.1.0",
					featureGeometryName: "the_geom"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?" + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.POINT_GEOMETRY
			}
		);
	contenuCarte.addItem(coucheGeoserverCarto2Prod_point);
	
	coucheGeoserverCarto2Prod_l = new Descartes.Layer.WMS(
			"(GS)couche ligne Carto2", 
			[
				{
					serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d/c_waterways_Valeurs_type?",
					layerName: "c_waterways_Valeurs_type",
					serverVersion: "1.1.1",
					featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d/c_waterways_Valeurs_type?",
					featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
					featureName: "c_waterways_Valeurs_type",
					featureServerVersion: "1.1.0",
					featureGeometryName: "the_geom"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?" + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_waterways_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY
			}
		);
	contenuCarte.addItem(coucheGeoserverCarto2Prod_l);
	
	coucheGeoserverCarto2Prod = new Descartes.Layer.WMS(
			"(GS)couche Polygone Carto2", 
			[
				{
					serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d/c_natural_Valeurs_type?",
					layerName: "c_natural_Valeurs_type",
					serverVersion: "1.3.0",
					featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d/c_natural_Valeurs_type?",
					featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
					featureName: "c_natural_Valeurs_type",
					featureServerVersion: "1.1.0",
					featureGeometryName: "the_geom"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?" + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.POLYGON_GEOMETRY
			}
		);
	contenuCarte.addItem(coucheGeoserverCarto2Prod);
	
	coucheGeograndest = new Descartes.Layer.WMS(
			"(GS)[PB PKI]couche geograndest", 
			[
				{
					serverUrl: "https://www.geograndest.fr/geoserver/araa/wms",
					layerName: "ARAA_BDSols-Alsace_250000_SHP_L93",
					serverVersion: "1.1.1",
					featureServerUrl: "https://www.geograndest.fr/geoserver/araa/wfs",
					featureName: "ARAA_BDSols-Alsace_250000_SHP_L93",
					featureNameSpace: "araa",
					featureGeometryName: "the_geom",
					featureServerVersion: "1.1.0"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: false,
				queryable:true,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: ["https://www.geograndest.fr/geoserver/araa/wms?" + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=ARAA_BDSols-Alsace_250000_SHP_L93"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.POLYGON_GEOMETRY
			}
		);
	contenuCarte.addItem(coucheGeograndest);
	
	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?";
	
		coucheEauSelections = new Descartes.Layer.WMS(
			"(MS)Cours d'eau", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					featureServerUrl: serveur,
					featureName: "c_waterways_Valeurs_type",
					featureServerVersion: "1.1.0",
					serverVersion: "1.3.0"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				displayOrder:5,
				geometryType: "Line"
			}
		);
	
		coucheStationsSelections = new Descartes.Layer.WMS(
			"(MS)Stations essence", 
			[
				{
					serverUrl: serveur,
					layerName: "c_stations",
					featureServerUrl: serveur,
					featureName: "c_stations",
					featureServerVersion: "1.1.0",
					serverVersion: "1.3.0"
				}
				],
			{
				//maxScale: null,
				//minScale: 390000,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:true,
				opacity: 100,
				opacityMax: 100,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				displayOrder:7,
				geometryType: "Point"
			}
		);
	
		coucheNatureSelections = new Descartes.Layer.WMS(
			"(MS)Espaces naturels", 
			[
				{
					serverUrl: serveur,
					layerName: "c_natural_Valeurs_type",
					featureServerUrl: serveur,
					featureName: "c_natural_Valeurs_type",
					featureServerVersion: "1.1.0",
					serverVersion: "1.3.0"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				format: "image/png",
				attribution: "&#169;mon copyright",
				displayOrder:1,
				geometryType: "Polygon"
			}
		);
	
	contenuCarte.addItem(coucheStationsSelections);
	contenuCarte.addItem(coucheEauSelections);
	contenuCarte.addItem(coucheNatureSelections);

	var serveur2 = "https://preprod.descartes.din.developpement-durable.gouv.fr/qgisserver?";

		coucheEauSelections2 = new Descartes.Layer.WMS(
			"(QS)Cours d'eau", 
			[
				{
					serverUrl: serveur2,
					layerName: "c_waterways_Valeurs_type",
					featureServerUrl: serveur2,
					featureName: "c_waterways_Valeurs_type",
					featureServerVersion: "1.1.0",
					serverVersion: "1.3.0"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				displayOrder:5,
				geometryType: "Line"
			}
		);
	
		coucheStationsSelections2 = new Descartes.Layer.WMS(
			"(QS)Stations essence", 
			[
				{
					serverUrl: serveur2,
					layerName: "c_stations",
					featureServerUrl: serveur2,
					featureName: "c_stations",
					featureServerVersion: "1.1.0",
					serverVersion: "1.3.0"
				}
				],
			{
				//maxScale: null,
				//minScale: 390000,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:true,
				opacity: 100,
				opacityMax: 100,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				displayOrder:7,
				geometryType: "Point"
			}
		);
	
		coucheNatureSelections2 = new Descartes.Layer.WMS(
			"(QS)Espaces naturels", 
			[
				{
					serverUrl: serveur2,
					layerName: "c_natural_Valeurs_type",
					featureServerUrl: serveur2,
					featureName: "c_natural_Valeurs_type",
					featureServerVersion: "1.1.0",
					serverVersion: "1.3.0"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				format: "image/png",
				attribution: "&#169;mon copyright",
				displayOrder:1,
				geometryType: "Polygon"
			}
		);
	
	contenuCarte.addItem(coucheStationsSelections2);
	contenuCarte.addItem(coucheEauSelections2);
	contenuCarte.addItem(coucheNatureSelections2);
	
	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
        args: {
        	resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});

	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout du gestionnaire de requete
/*	
	var gestionnaireRequetes = carte.addRequestManager('Requetes',{
        resultUiParams: {
            withCsvExport: true,
            withReturn: true
        }});
	var requeteCoucheGeoserverCarto2Prod = new Descartes.Request(coucheGeoserverCarto2Prod, "Filtrer Carto2Prod Polygone", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["La Durance","Canal EDF"],
					true
				);
	
	requeteCoucheGeoserverCarto2Prod.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteCoucheGeoserverCarto2Prod);
	
	var requetecoucheGeoserverCarto2Prod_point = new Descartes.Request(coucheGeoserverCarto2Prod_point, "Filtrer Carto2Prod Point", Descartes.Layer.POINT_GEOMETRY);
	var critereType2 = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"~",
					[],
					true
				);
	
	requetecoucheGeoserverCarto2Prod_point.addMember(critereType2);
	gestionnaireRequetes.addRequest(requetecoucheGeoserverCarto2Prod_point);
	
	var requeteCoucheGeoserverCarto2Prod2 = new Descartes.Request(coucheGeoserverCarto2Prod, "Filtrer Carto2Prod Polygone", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType3 = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["La Durance","Jardin Japonais","Le lac Vert"],
					true
				);
	var critereType4 = new Descartes.RequestMember(
			"Sélectionner un type",
			"type",
			"==",
			["berge","parc","eau"],
			true
		);
	
	requeteCoucheGeoserverCarto2Prod2.addMember(critereType3);
	requeteCoucheGeoserverCarto2Prod2.addMember(critereType4);
	gestionnaireRequetes.addRequest(requeteCoucheGeoserverCarto2Prod2);

*/	
}
