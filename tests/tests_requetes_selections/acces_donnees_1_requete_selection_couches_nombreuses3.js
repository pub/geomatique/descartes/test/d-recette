proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/qgisserver?";

	var groupeStations = new Descartes.Group(
			"20 couches - (QS) Stations essence", 
			{opened : false}
		);
	contenuCarte.addItem(groupeStations);
	
	var	coucheStations = {
		title :"Stations essence", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_stations",
				featureServerUrl: serveur,
				featureName: "c_stations"
			}
		],
		options:{
			id:'coucheStations',
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};
	
	for (var i=0; i<20; i++) {
		var couche = new Descartes.Layer.WMS(coucheStations.title, coucheStations.definition, coucheStations.options);
		contenuCarte.addItem(couche,groupeStations);
	}
	
	var groupeEau = new Descartes.Group(
			"20 couches - (QS) Cours d'eau", 
			{opened : false}
		);
	contenuCarte.addItem(groupeEau);
	
	var coucheEau = {
			title :"Cours d'eau", 
			type: 0,
			definition:[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					featureServerUrl: serveur,
					featureName: "c_waterways_Valeurs_type"			}
			],
			options:{
				id:'coucheEau',
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		};
	
	for (var i=0; i<20; i++) {
		var couche = new Descartes.Layer.WMS(coucheEau.title, coucheEau.definition, coucheEau.options);
		contenuCarte.addItem(couche,groupeEau);
	}
	
	var groupeNature = new Descartes.Group(
			"20 couches - (QS) Espaces Naturels", 
			{opened : false}
		);
	contenuCarte.addItem(groupeNature);
	
	var coucheNature = {
			title : "Espaces naturels", 
			type: 0,
			definition: [
			  {
				serverUrl: serveur,
				layerName: "c_natural_Valeurs_type"	,
				featureServerUrl: serveur,
				featureName: "c_natural_Valeurs_type"
			  }
			],
			options:{
				id:'coucheNature',
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		};
	
	for (var i=0; i<20; i++) {
		var couche = new Descartes.Layer.WMS(coucheNature.title, coucheNature.definition, coucheNature.options);
		contenuCarte.addItem(couche,groupeNature);
	}

	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					displayExtendedOLExtent: true,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
        args: {
        	resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});

	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	//carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	//carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	//carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout du gestionnaire de requete
/*	var gestionnaireRequetes = carte.addRequestManager('Requetes',{
        resultUiParams: {
            withCsvExport: true,
            withReturn: true
        }});
	var requeteStation = new Descartes.Request(coucheStations, "Filtrer les stations essence", Descartes.Layer.POINT_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Station Total","Station Total Le National", "Station Total - Garage Renault"],
					true
				);
	
	requeteStation.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteStation);
	
	var requeteNature = new Descartes.Request(coucheNature, "Filtrer les espaces naturels", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["La Durance","Canal EDF"],
					true
				);

	requeteNature.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteNature);
	
	var requeteEau = new Descartes.Request(coucheEau, "Filtrer les cours d'eau", Descartes.Layer.LINE_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Canal de Marseille","La Touloubre"],
					true
				);
	
	requeteEau.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteEau);
*/	
}
