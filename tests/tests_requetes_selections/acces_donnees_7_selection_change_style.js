proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	// Ajout de couches au groupe
	contenuCarte.addItem(coucheStationsSelections);
	contenuCarte.addItem(coucheEauSelections);
	contenuCarte.addItem(coucheNatureSelections);

	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
		args: {
        	resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                symbolizers: {
        		    'Point': {
        		        graphicName: 'square',
        		        fillColor: '#00FF00',
        		        pointRadius: 8.0
        		    },
        		    'Line': {
        		        fillColor: '#FF0000',
        		        fillOpacity: 1,
        		        strokeColor: '#FF0000',
        		        strokeWidth: 5.0,
        		        strokeOpacity: 1
        		    },
        		    'Polygon': {
        		        fillColor: '#0000FF',
        		        fillOpacity: 0.5,
        		        strokeOpacity: 1,
        		        strokeColor: '#0000FF',
        		        strokeWidth: 2.0
        		    }
        		}
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                symbolizers: {
        		    'Point': {
        		        graphicName: 'square',
        		        fillColor: '#00FF00',
        		        pointRadius: 8.0
        		    },
        		    'Line': {
        		        fillColor: '#FF0000',
        		        fillOpacity: 1,
        		        strokeColor: '#FF0000',
        		        strokeWidth: 5.0,
        		        strokeOpacity: 1
        		    },
        		    'Polygon': {
        		        fillColor: '#0000FF',
        		        fillOpacity: 0.5,
        		        strokeOpacity: 1,
        		        strokeColor: '#0000FF',
        		        strokeWidth: 2.0
        		    }
        		}
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                symbolizers: {
        		    'Point': {
        		        graphicName: 'square',
        		        fillColor: '#00FF00',
        		        pointRadius: 8.0
        		    },
        		    'Line': {
        		        fillColor: '#FF0000',
        		        fillOpacity: 1,
        		        strokeColor: '#FF0000',
        		        strokeWidth: 5.0,
        		        strokeOpacity: 1
        		    },
        		    'Polygon': {
        		        fillColor: '#0000FF',
        		        fillOpacity: 0.5,
        		        strokeOpacity: 1,
        		        strokeColor: '#0000FF',
        		        strokeWidth: 2.0
        		    }
        		}
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                symbolizers: {
        		    'Point': {
        		        graphicName: 'square',
        		        fillColor: '#00FF00',
        		        pointRadius: 8.0
        		    },
        		    'Line': {
        		        fillColor: '#FF0000',
        		        fillOpacity: 1,
        		        strokeColor: '#FF0000',
        		        strokeWidth: 5.0,
        		        strokeOpacity: 1
        		    },
        		    'Polygon': {
        		        fillColor: '#0000FF',
        		        fillOpacity: 0.5,
        		        strokeOpacity: 1,
        		        strokeColor: '#0000FF',
        		        strokeWidth: 2.0
        		    }
        		}
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true
            },
            resultLayerParams: {
                display: true,
                symbolizers: {
        		    'Point': {
        		        graphicName: 'square',
        		        fillColor: '#00FF00',
        		        pointRadius: 8.0
        		    },
        		    'Line': {
        		        fillColor: '#FF0000',
        		        fillOpacity: 1,
        		        strokeColor: '#FF0000',
        		        strokeWidth: 5.0
        		    },
        		    'Polygon': {
        		        fillColor: '#0000FF',
        		        fillOpacity: 0.5,
        		        strokeOpacity: 1,
        		        strokeColor: '#0000FF',
        		        strokeWidth: 2.0
        		    }
        		}
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});

	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
}
