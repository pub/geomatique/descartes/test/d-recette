proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	//Test perf: carto2 4* plus rapide (plus de CPU/RAM)
	//Descartes.FEATUREINFO_SERVER = "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/v1/getFeatureInfo?";

	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	
	groupe2 = new Descartes.Group(
			"20 couches - Carto2 PROD L_PISTE_SKI_L_074", 
			{opened : false}
		);
	contenuCarte.addItem(groupe2);
	
	var couche2 = {
		title : "L_PISTE_SKI_L_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_PISTE_SKI_L_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				featureServerVersion: "1.1.0",
				layerName: "L_PISTE_SKI_L_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_PISTE_SKI_L_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
		}
	};
	
	for (var i=0; i<20; i++) {
		var couche = new Descartes.Layer.WMS(couche2.title, couche2.definition, couche2.options);
		contenuCarte.addItem(couche,groupe2);
	}
	
	
	groupe1 = new Descartes.Group(
			"20 couches - Carto2 PROD L_ENV_DOMAINE_SKIABLE_S_074", 
			{opened : false}
		);
	contenuCarte.addItem(groupe1);
	
	var couche1 = {
		title : "L_ENV_DOMAINE_SKIABLE_S_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_ENV_DOMAINE_SKIABLE_S_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				featureServerVersion: "1.1.0",
				layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_ENV_DOMAINE_SKIABLE_S_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
		}
	};
	
	for (var i=0; i<20; i++) {
		var couche = new Descartes.Layer.WMS(couche1.title, couche1.definition, couche1.options);
		contenuCarte.addItem(couche,groupe1);
	}

	

	
	
	var bounds = [683697, 5738429, 804314, 5809973];

    var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					displayExtendedOLExtent: true,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
        args: {
        	resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
        args: {
            resultUiParams: {
                withCsvExport: true,
                withReturn: true,
                withAvancedView: true
            },
            resultLayerParams: {
                display: true,
                exportAllSelectionLayers:true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});

	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	//carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	//carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	//carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout du gestionnaire de requete
/*	var gestionnaireRequetes = carte.addRequestManager('Requetes',{
        resultUiParams: {
            withCsvExport: true,
            withReturn: true
        }});
	var requeteStation = new Descartes.Request(coucheStations, "Filtrer les stations essence", Descartes.Layer.POINT_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Station Total","Station Total Le National", "Station Total - Garage Renault"],
					true
				);
	
	requeteStation.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteStation);
	
	var requeteNature = new Descartes.Request(coucheNature, "Filtrer les espaces naturels", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["La Durance","Canal EDF"],
					true
				);

	requeteNature.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteNature);
	
	var requeteEau = new Descartes.Request(coucheEau, "Filtrer les cours d'eau", Descartes.Layer.LINE_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le nom",
					"name",
					"==",
					["Canal de Marseille","La Touloubre"],
					true
				);
	
	requeteEau.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteEau);
*/	
}
