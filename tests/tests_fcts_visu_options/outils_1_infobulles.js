proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable:true});
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
			'map',
			contenuCarte,
			{
				projection: projection,
				initExtent: bounds,
				maxExtent: bounds,
				minScale:2150000,
				size: [600, 400]
			}
		);
	
	var tools = [
            {type : Descartes.Map.DRAG_PAN, 
             args :{
            	 title : 'Mon texte: DRAG_PAN'
             }
            },
			{type : Descartes.Map.ZOOM_IN, 
                args :{
               	 title : 'Mon texte: ZOOM_IN'
                }
            },
			{type : Descartes.Map.ZOOM_OUT, 
                    args :{
                   	 title : 'Mon texte: ZOOM_OUT'
                    }
            },
			{type : Descartes.Map.INITIAL_EXTENT, 
            	args : bounds, 
            	options:{
            		title : 'Mon texte: INITIAL_EXTENT'
				}
            },
			{type : Descartes.Map.MAXIMAL_EXTENT, 
	             args :{
	            	 title : 'Mon texte: MAXIMAL_EXTENT'
	             }
			},
			{type : Descartes.Map.CENTER_MAP, 
	             args :{
	            	 title : 'Mon texte: CENTER_MAP'
	             }
			},
			{type : Descartes.Map.COORDS_CENTER, 
	             args :{
	            	 title : 'Mon texte: COORDS_CENTER'
	             }
			},
			{type : Descartes.Map.NAV_HISTORY, 
				args :{
             	   previousOptions : {title: 'Mon texte: NAV_HISTORY1'},
             	   nextOptions : {title: 'Mon texte: NAV_HISTORY2'}
                }
			},
			{type : Descartes.Map.DISTANCE_MEASURE, 
	             args :{
	            	 title : 'Mon texte: DISTANCE_MEASURE'
	             }
			},
			{type : Descartes.Map.AREA_MEASURE, 
	             args :{
	            	 title : 'Mon texte: AREA_MEASURE'
	             }
			},
			{type : Descartes.Map.POINT_SELECTION, 
	             args :{
	            	 title : 'Mon texte: POINT_SELECTION'
	             }
			},
			{type : Descartes.Map.POLYGON_SELECTION, 
	             args :{
	            	 title : 'Mon texte: POLYGON_SELECTION'
	             }
			},
			{type : Descartes.Map.RECTANGLE_SELECTION, 
	             args :{
	            	 title : 'Mon texte: RECTANGLE_SELECTION'
	             }
			},
			{type : Descartes.Map.CIRCLE_SELECTION, 
	             args :{
	            	 title : 'Mon texte: CIRCLE_SELECTION'
	             }
			},
			{type : Descartes.Map.POINT_RADIUS_SELECTION, 
	             args :{
	            	 title : 'Mon texte: POINT_RADIUS_SELECTION'
	             }
			},
			{type : Descartes.Map.PNG_EXPORT, 
	             args :{
	            	 title : 'Mon texte: PNG_EXPORT'
	             }
			},
			{type : Descartes.Map.PDF_EXPORT, 
	             args :{
	            	 title : 'Mon texte: PDF_EXPORT'
	             }
			}
	];
	
	var toolsBar = carte.addNamedToolBar('toolBar',tools);
	
	carte.addContentManager('layersTree',
			[
				{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL, 
	                options :{
	                  	 title : 'Mon texte: ADD_GROUP_TOOL'
	                   }
				},
				{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL, 
					options :{
	                  	 title : 'Mon texte: ADD_LAYER_TOOL'
	                   }
				},
				{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL, 
					options :{
	                  	 title : 'Mon texte: REMOVE_GROUP_TOOL'
	                   }
				},
				{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL, 
					options :{
	                  	 title : 'Mon texte: REMOVE_LAYER_TOOL'
	                   }
				},
				{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL, 
					options :{
	                  	 title : 'Mon texte: ALTER_GROUP_TOOL'
	                   }
				},
				{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL, 
					options :{
	                  	 title : 'Mon texte: ALTER_LAYER_TOOL'
	                   }
				},
				{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL, 
					options :{
	                  	 title : 'Mon texte: ADD_WMS_LAYERS_TOOL'
	                   }
				}
			],
			{
				toolBarDiv: "managerToolBar"
			});
	
	// Affichage de la carte
	carte.show();
	
}
