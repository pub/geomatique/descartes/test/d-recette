var coucheNature, coucheBati, coucheRoutes, coucheFer, coucheEau, coucheToponymes, coucheParkings, coucheStations, groupeInfras;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeCouchesGroupesContext() {
	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?";

	coucheNature = {
		title : "Espaces naturels", 
		type: 0,
		definition: [
		  {
			serverUrl: serveur,
			layerName: "c_natural_Valeurs_type"	
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};

	coucheBati = {
		title :"Constructions", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_buildings",
				featureServerUrl: serveur,
				featureName: "c_buildings2",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
		],
		options:{
			maxScale: null,
			minScale: 39000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};

	coucheRoutes = {
		title :"Routes", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_roads"			}
		],
		options:{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_roads"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};

	coucheFer = {
		title :"Chemins de fer", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_railways"			}
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_railways"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};

	coucheEau = {
		title :"Cours d'eau", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_waterways_Valeurs_type"			}
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};

	coucheToponymes = {
		title :"Lieux", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_places_Etiquettes"			}
		],
		options:{
			maxScale: null,
			minScale: 190000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: "http://metadataURL.fr",
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};
	
	coucheStations = {
		title :"Stations essence", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_stations",
				featureServerUrl: serveur,
				featureName: "c_stations",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
		],
		options:{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};

	coucheParkings =  {
		title :"Parkings", 
		type: 0,
		definition:[
			{
				serverUrl: serveur,
				layerName: "c_parkings",
				featureServerUrl: serveur,
				featureName: "c_parkings",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
		],
		options:{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	};

	groupeInfras = {
		title: "Infrastructures", 
		options : {opened : false}
	};
}
