proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
    carte.addOpenLayersControls([
         {type: Descartes.Map.OL_ZOOM}
    ]);
    
    //Interactions openlayers
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_DRAG_PAN},
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	]); 

}
