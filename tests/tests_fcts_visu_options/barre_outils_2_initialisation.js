proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
			'map',
			contenuCarte,
			{
				projection: projection,
				initExtent: bounds,
				maxExtent: bounds,
				minScale:2150000,
				size: [600, 400]
			}
		);
	
	var tools = [
            {type : Descartes.Map.DRAG_PAN},
			{type : Descartes.Map.ZOOM_IN},
			{type : Descartes.Map.ZOOM_OUT},
			{type : Descartes.Map.INITIAL_EXTENT, args : bounds},
			{type : Descartes.Map.MAXIMAL_EXTENT},
			{type : Descartes.Map.CENTER_MAP},
			{type : Descartes.Map.COORDS_CENTER},
			{type : Descartes.Map.NAV_HISTORY},
			{type : Descartes.Map.DISTANCE_MEASURE},
			{type : Descartes.Map.AREA_MEASURE},
			{type : Descartes.Map.POINT_SELECTION},
			{type : Descartes.Map.POLYGON_SELECTION},
			{type : Descartes.Map.RECTANGLE_SELECTION},
			{type : Descartes.Map.CIRCLE_SELECTION},
			{type : Descartes.Map.POINT_RADIUS_SELECTION},
			{type : Descartes.Map.PNG_EXPORT},
			{type : Descartes.Map.PDF_EXPORT}
	];
	
	var toolsBar = carte.addNamedToolBar('toolBar',tools);
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
}
