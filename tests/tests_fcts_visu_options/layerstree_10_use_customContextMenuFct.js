proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({
		editable: true,
		editInitialItems: true,
		fctContextMenu:true});
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					maxScale:100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	
	carte.addContentManager(
		'layersTree',
		null,
		{
			uiOptions: {
				contextMenuFct: contextMenuFct
			}
		}
	);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});
	
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");
		
	// Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes');
	var requeteBati = new Descartes.Request(coucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);
	
	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	
	// Ajout du gestionnaire d'info-bulle
	carte.addToolTip('ToolTip', [
  		{layer: coucheParkings, fields: ['name']},
  		{layer: coucheStations, fields: ['name']}
  	]);
  	
  	/*carte.addToolTip('ToolTip', [ // pour intranet
   		{layer: coucheParkings, fields: ['name']},
   		{layer: coucheStations, fields: ['name']}
   	]);*/

	
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes');
	
	// Ajout du gestionnaire de localisation rapide
	carte.addDefaultGazetteer('Gazetteer', "93", Descartes.Action.DefaultGazetteer.DEPARTEMENT);
}

var contextMenuFct = function (itemTree) {
    var self = this;
    var position = itemTree.id.substring(self.CLASS_NAME.replace(/\./g, '').length);
    var descartesItem = null;
    if (position !== '') {
        descartesItem = self.model.getItemByIndex(position);
    }
    var contextMenuItems = {};
    if (descartesItem === null) {
    	var myItemRoot1 = {
            label: "Mon action 1 (pour noeud principal)",
			icon: 'fa fa-cogs',
			separator_before: false,
            separator_after: false,
            _disabled: false,
            action: function () {
                 alert("Traitement Action 1 pour le noeud principal");
            }
        };
    	contextMenuItems.myItem1 = myItemRoot1;
    } else if (descartesItem.CLASS_NAME === "Descartes.Group") {
    	var myItemGroup1 = {
            label: "Mon action 1 (pour un goupe)",
			icon: 'fa fa-cogs',
			separator_before: false,
            separator_after: false,
            _disabled: false,
            action: function () {
                 alert("Traitement Action 1 pour le goupe '" + self.model.getItemByIndex(self.selectedItem).title + "'");
            }
        };
    	contextMenuItems.myItem1 = myItemGroup1;
    	var myItemGroup2 = {
            label: "Mon action 2 (pour un goupe)",
			icon: 'fa fa-clone',
			separator_before: false,
            separator_after: false,
            _disabled: false,
            action: function () {
                 alert("Traitement Action 2 pour le goupe '" + self.model.getItemByIndex(self.selectedItem).title + "'");
            }
        };
    	contextMenuItems.myItem2 = myItemGroup2;
    } else {
    	var myItemLayer1 = {
            label: "Mon action 1 (pour une couche)",
			icon: 'fa fa-cogs',
			separator_before: false,
            separator_after: false,
            _disabled: false,
            action: function () {
                 alert("Traitement Action 1 pour la couche '" + self.model.getItemByIndex(self.selectedItem).title + "'");
            }
        };
    	contextMenuItems.myItem1 = myItemLayer1;
    	var myItemLayer2 = {
            label: "Mon action 2 (pour une couche)",
			icon: 'fa fa-clone',
			separator_before: false,
            separator_after: false,
            _disabled: false,
            action: function () {
                 alert("Traitement Action 2 pour la couche '" + self.model.getItemByIndex(self.selectedItem).title + "'");
            }
        };
    	contextMenuItems.myItem2 = myItemLayer2;
    	var myItemLayer3 = {
            label: "Mon action 3 (pour une couche)",
			icon: 'fa fa-plus',
			separator_before: false,
            separator_after: false,
            _disabled: false,
            action: function () {
                 alert("Traitement Action 3 pour la couche '" + self.model.getItemByIndex(self.selectedItem).title + "'");
            }
        };
    	contextMenuItems.myItem3 = myItemLayer3;

    }
    return contextMenuItems;
};
