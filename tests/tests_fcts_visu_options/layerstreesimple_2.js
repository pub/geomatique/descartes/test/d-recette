proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({
        editable: true,
        editInitialItems: true,
        fixedDisplayOrders: false,
        fctQueryable: true,
        fctVisibility: true,
        fctOpacity: true,
        displayIconLoading: true,
        fctContextMenu: true,
        fctDisplayLegend: true,
        fctMetadataLink: true,
        displayMoreInfos: true
    });
	
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfrasOpen);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfrasOpen);
	contenuCarte.addItem(coucheStations, groupeInfrasOpen);
	contenuCarte.addItem(coucheRoutes, groupeInfrasOpen);
	contenuCarte.addItem(coucheFer, groupeInfrasOpen);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
			'map',
			contenuCarte,
			{
				projection: projection,
				initExtent: bounds,
				maxExtent: bounds,
				minScale:2150000,
				size: [600, 400]
			}
		);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.DISPLAY_LAYERSTREE_SIMPLE});

	
    var contentTools = [
                        {type: "AddGroup", options: null},
                        {type: "AddLayer", options: null},
                        {type: "RemoveGroup", options: null},
                        {type: "RemoveLayer", options: null},
                        {type: "AlterGroup", options: null},
                        {type: "AlterLayer", options: null},
                        {type: "ChooseWmsLayers", options: null}
                    ];
    
    var managerOptions = {
            toolBarDiv: "managerToolBar",
            uiOptions: {
                moreInfosUiParams: {
    				fctOpacity:true,
    				fctDisplayLegend:true,
    				displayOnClickLayerName: true
    			}
            }
        };
	
	carte.addContentManager('layersTree',contentTools,managerOptions);
	
	// Affichage de la carte
	carte.show();
	
}
