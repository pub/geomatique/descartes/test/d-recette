var coucheNature, coucheBati, coucheRoutes, coucheFer, coucheEau, coucheToponymes, coucheParkings, coucheStations, groupeInfras, coucheWmsKO, coucheNatureReproj, coucheEauReproj, coucheAggregatWMS, coucheParkingsWmsWIthWfs, coucheStationsWmsWIthWfs;
var coucheEauWfs, coucheNatureWfs, coucheEauWfsPost, coucheNatureWfsPost, coucheWfsKO, coucheWfsKOPost, coucheEauWfsReproj, coucheNatureWfsReproj, coucheEauWfsReprojPost, coucheNatureWfsReprojPost, coucheEauWfsWithWms, coucheNatureWfsWithWms;
var coucheBdxBDorthoWmsc, coucheWmscKO;
var coucheBdxBDorthoTms, coucheTmsKO;
var coucheDepartementGeoJson, couchePaysGeoJson, couchePaysGeoJson2, coucheGeoJsonKO, couchePaysGeoJsonReproj;
var coucheReservesNaturellesRegionalesKML, couchePointsKML, coucheLinesKML, coucheKMLKO, couchePointsKMLWithStyleEmbarque, coucheReservesNaturellesRegionalesKMLWithStyleEmbarque, couchePointsKMLReproj;
var groupeFonds;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeCouchesGroupes() {
	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?";


	/************************************************************************
	 * 
	 * COUCHES WFS
	 * 
	 ***********************************************************************/
	
	coucheWfsKO = new Descartes.Layer.ClusterLayer.WFS(
			"Couche (CLUSTER WFS) KO", 
			[
				{
					serverUrl: "http://test",
					layerName: "c_waterways_Valeurs_type"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY
			}
		);	
	
	coucheEauWfs = new Descartes.Layer.ClusterLayer.WFS(
			"Couche de type ligne (CLUSTER WFS)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					featureGeometryName: "the_geom",
					featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d"
				}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY
			}
		);

	coucheEauWfsPost = new Descartes.Layer.ClusterLayer.WFS(
			"Couche de type ligne (CLUSTER WFS) - requête POST", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					featureGeometryName: "ms:geometry",
					featureNameSpace: "",
					featureLoaderMode: Descartes.Layer.POST_REQUEST_LOADER
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY
			}
		);
	
	coucheEauWfsStyle = new Descartes.Layer.ClusterLayer.WFS(
			"Couche de type ligne (CLUSTER WFS)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					featureGeometryName: "the_geom",
					featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d"			}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY,
				cluster:{
			        distance: 100,
			        scaleLimit: 1500000,
	    	        symbolizersClusterPoint: {
	    			    'Point': {
	    	                pointRadius: 20,
	    	                graphicName: 'circle',
	    	                fillColor: 'red',
	    	                fillOpacity: 1,
	    	                strokeWidth: 1,
	    	                strokeOpacity: 1,
	    	                strokeColor: '#fff'
	    			    }
	    		    }
				}
			}
		);

	
	coucheWfsStyle= new Descartes.Layer.ClusterLayer.WFS(
			"Couche de type polygone (CLUSTER WFS) - style particulier", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					featureGeometryName: "the_geom",
					featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY,
				cluster: {
			        distance: 50,
			        scaleLimit: 1000000,
	    	        symbolizersClusterPoint: {
	    			    'Point': {
	    			    	 pointRadius: 20,
	    	                 graphicName: 'square',
	    	                 points: 4,
	    	                 angle: Math.PI / 4,
	    	                 fillColor: '#A5F38D',
	    	                 fillOpacity: 1,
	    	                 strokeWidth: 1,
	    	                 strokeOpacity: 1,
	    	                 strokeColor: '#fff'
	    			    }
	    		    }
				},
				symbolizers:{
				  "Point": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#A5F38D" //"#ee9900"
				  },
				  "Line": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "Polygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  },
				  "MultiPoint": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#A5F38D" //"#ee9900"
				  },
				  "MultiLine": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "MultiPolygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  }
				}
			}
		);
	
	coucheEauWfsReproj = new Descartes.Layer.ClusterLayer.WFS(
			"Couche de type ligne (CLUSTER WFS) - Reprojecté (EPSG:2154 en EPSG:3857)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					featureGeometryName: "the_geom",
					featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
					internalProjection: "EPSG:2154"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY
			}
		);
	
	coucheEauWfsReprojPost = new Descartes.Layer.ClusterLayer.WFS(
			"Couche de type ligne (CLUSTER WFS) - Reprojecté (EPSG:2154 en EPSG:3857)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					internalProjection: "EPSG:2154",
					featureGeometryName: "ms:geometry",
					featureNameSpace: "",
					featureLoaderMode: Descartes.Layer.POST_REQUEST_LOADER
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright",
				geometryType: Descartes.Layer.LINE_GEOMETRY
			}
		);
    
	/************************************************************************
	 * 
	 * COUCHES GEOJSON
	 * 
	 ***********************************************************************/
    
    coucheDepartementGeoJson = new Descartes.Layer.ClusterLayer.GeoJSON(
    	    'Couche de type polygone (CLUSTER GeoJSON) - Style par défaut',
    	    [{
    	            serverUrl: './data/geojson/departements.geojson',
    	            internalPorjection: 'EPSG:4326'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
       	        geometryType: Descartes.Layer.POLYGON_GEOMETRY,
       	        cluster: {
       	        	distance:20,
       	        	scaleLimit: 4000000
       	        }
    	    }
    	);
    
    coucheGeoJsonKO = new Descartes.Layer.ClusterLayer.GeoJSON(
    	    'couche de type polygone (CLUSTER GeoJson) KO',
    	    [{
    	            serverUrl: './test.json'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.POLYGON_GEOMETRY
    	    }
    	);
    
 
    coucheDepartementGeoJsonStyle = new Descartes.Layer.ClusterLayer.GeoJSON(
    	    'Couche de type polygon (CLUSTER GeoJSON) - Style par défaut',
    	    [{
    	            serverUrl: './data/geojson/departements.geojson',
    	            internalPorjection: 'EPSG:4326'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
       	        geometryType: Descartes.Layer.POLYGON_GEOMETRY,
       	        cluster: {
       	        	distance:0,
       	        	scaleLimit: 4000000,
	    	        symbolizersClusterPoint: {
	    			    'Point': {
	    			    	 pointRadius: 20,
	    	                 graphicName: 'square',
	    	                 points: 4,
	    	                 angle: Math.PI / 4,
	    	                 fillColor: 'orange',
	    	                 fillOpacity: 1,
	    	                 strokeWidth: 1,
	    	                 strokeOpacity: 1,
	    	                 strokeColor: '#fff'
	    			    }
	    		    }
       	        },
    	        symbolizers:{
				  "Polygon": {
					strokeWidth: 2,
					strokeOpacity: 1, 
					strokeColor: "orange",
					fillColor: "orange", 
					fillOpacity: 0.3 
				  }
    	        }
    	    }
    	);
    
	/************************************************************************
	 * 
	 * COUCHES KML
	 * 
	 ***********************************************************************/

    coucheReservesNaturellesRegionalesKMLWithStyleEmbarque = new Descartes.Layer.ClusterLayer.KML(
	    'Couche de type polygon (CLUSTER KML) avec style embarque',
	    [{
	            serverUrl: './data/kml/RNR_2013.KML'
	        }],
	    {
	        visible: true,
	        opacity: 100,
	        keepInternalStyles:true,
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
	    }
	);
    
    coucheReservesNaturellesRegionalesKML = new Descartes.Layer.ClusterLayer.KML(
    	    'Couche de type polygon (CLUSTER KML)',
    	    [{
    	            serverUrl: './data/kml/RNR_2013.KML'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.POLYGON_GEOMETRY
    	    }
    	);
    
    coucheKMLKO = new Descartes.Layer.ClusterLayer.KML(
    	    'couche de type Point (CLUSTER KML) KO',
    	    [{
    	            serverUrl: './pasdefichier.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.POINT_GEOMETRY
    	    }
    	);
    
    couchePointsKMLWithStyleEmbarque = new Descartes.Layer.ClusterLayer.KML(
    	    'couche de type point (Cluster KML) avec le style embarqué',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:true,
				geometryType: Descartes.Layer.POINT_GEOMETRY
    	    }
    	);
    
    couchePointsKML = new Descartes.Layer.ClusterLayer.KML(
    	    'couche de type point (CLUSTER KML)',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.POINT_GEOMETRY
    	    }
    	);
    
    coucheLinesKML = new Descartes.Layer.ClusterLayer.KML(
    	    'couche de type ligne (CLUSTER KML)',
    	    [{
    	            serverUrl: './data/kml/lines.kml'/*,
    	            displayProjection: 'EPSG:2154',
    	            internalProjection: 'EPSG:4326'*/
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.LINE_GEOMETRY
    	    }
    	);
    
    coucheReservesNaturellesRegionalesKMLStyle = new Descartes.Layer.ClusterLayer.KML(
    	    'Couche de type polygon (CLUSTER KML)',
    	    [{
    	            serverUrl: './data/kml/RNR_2013.KML'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.POLYGON_GEOMETRY,
       	        cluster: {
	    	        symbolizersClusterPoint: {
	    			    'Point': {
	    	                pointRadius: 20,
	    	                graphicName: 'circle',
	    	                fillColor: 'red',
	    	                fillOpacity: 1,
	    	                strokeWidth: 1,
	    	                strokeOpacity: 1,
	    	                strokeColor: '#fff'
	    			    }
	    		    }
       	        }
    	    }
    	);
    
    couchePointsKMLStyle = new Descartes.Layer.ClusterLayer.KML(
    	    'couche de type point (CLUSTER KML) - style particulier',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.POINT_GEOMETRY,
       	        cluster: {
	    	        symbolizersClusterPoint: {
	    			    'Point': {
	    			    	 pointRadius: 20,
	    	                 graphicName: 'square',
	    	                 points: 4,
	    	                 angle: Math.PI / 4,
	    	                 fillColor: 'blue',
	    	                 fillOpacity: 1,
	    	                 strokeWidth: 1,
	    	                 strokeOpacity: 1,
	    	                 strokeColor: '#fff'
	    			    }
	    		    }
       	        },
    	        symbolizers:{
    	          "Point": {
					pointRadius: 4, //6
					graphicName: "circle",
					fillColor: "blue", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "blue" //"#ee9900"
				  },
				  "Line": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "Polygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					fillColor: "blue", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  },
				  "MultiPoint": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "blue", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "blue" //"#ee9900"
				  },
				  "MultiLine": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "MultiPolygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					fillColor: "blue", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  }
    	        }
    	    }
    	);
    
    couchePointsKMLReproj = new Descartes.Layer.ClusterLayer.KML(
    	    'couche de type point (CLUSTER KML) - Reprojeté (EPSG:4326 en EPSG:2154)',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				geometryType: Descartes.Layer.POINT_GEOMETRY
    	    }
    	);
    
	groupeFonds = {
			title: "Fonds cartographiques",
			options : {
				opened: true
			}
		};
}
