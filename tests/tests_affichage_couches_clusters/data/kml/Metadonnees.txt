Réserves naturelles régionales
	- contact :
	  Philippe Mathieu - Service Analyse Spatiale

	- organisme producteur :
	  Conseil R�gional PACA

	- projection :
	  Lambert II carto

	- �chelle de num�risation :
	  entre 1/2000e et 1/5000e

	- liste des sites :
	* RNR de Saint-Maurin
	* RNR des Partias
	* RNR de la Tour du Valat (6 polygones)
	* RNR de la Poitevine-Regarde-Venir
	* RNR de l'Ilon
	* RNR des Gorges de Daluis

	- description des champs attributaires :
	* NOM_ZONE (car, 40) : libell� de la RNR
	* CODE_ZONE (car, 10) : code interne de la RNR
	* DATE_CREATION (car, 10) : date de cr�ation de la RNR (correspond � la date de cr�ation de la couche par le service analyse spatiale, � l'exception des RNR de l'Ilon et des gorges de Daluis)
	* DATE_MAJ (car, 10) : date de mise � jour de la RNR ((correspond � la date de cr�ation de la couche par le service analyse spatiale, � l'exception des RNR de l'Ilon et des gorges de Daluis)
	* SOURCE (car, 50) : organisme ou service � l'origine de la donn�e
	* COMMENT (car, 100) : commentaire indiquant les moyens de transmission ou de cr�ation de la donn�e
