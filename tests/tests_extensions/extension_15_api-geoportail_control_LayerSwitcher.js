proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

var carte;

function chargementCarte() {
	
	
	chargeCouchesGroupes();
	

	
	var contenuCarte = new Descartes.MapContent({editable: true});
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	
	var coucheFond = new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options);
	contenuCarte.addItem(coucheFond,gpFonds);

	
	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = 'EPSG:2154';

	
    // Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					//minScale:2150000,
					size: [750, 500],
					displayExtendedOLExtent: true
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});

	//Widget de gestion d'empilement des couches API GEOPORTAIL
	var layerSwitcher = new ol.control.LayerSwitcher({
		 layers: [
		     {
		         layer : coucheFond.OL_layers[0],
		         config : {
		             title : "couche de fond",
		             description : "description couche de fond"
		         }
		     }
		 ]}
	);
	carte.OL_map.addControl(layerSwitcher);

}