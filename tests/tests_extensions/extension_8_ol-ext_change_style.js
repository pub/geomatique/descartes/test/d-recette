proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

var carte;

function chargementCarte() {
	
	chargeCouchesGroupes();
	
    coucheKML = new Descartes.Layer.KML(
    	    'couche vecteur',
    	    [{
    	            serverUrl: './data/kml/polygons.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:false
    	    }
    	);
	
	
	var contenuCarte = new Descartes.MapContent({editable: true});
    contenuCarte.addItem(coucheKML);
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

    var bounds = [-5.8, 42.3, 8.13, 52.1];
	//var bounds = [4.02, 42.60, 7.91, 45.37];
	var projection = 'EPSG:4326';
    // Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					//minScale:2150000,
					displayExtendedOLExtent: true,
					size: [600, 400]
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});


    
	var imgFile = './data/pattern.png';
	// AddChar patterns
	ol.style.FillPattern.addPattern ("copy (char pattern)", { char:"©" });
	ol.style.FillPattern.addPattern ("bug (fontawesome)", { char:'\uf188', size:12, font:"10px FontAwesome" });
	ol.style.FillPattern.addPattern ("smiley (width angle)", { char:'\uf118', size:20, angle:true, font:"15px FontAwesome" });

	// Popup
	$("#select").click(function(){
		$("#pselect").toggle();
	});

	var pat = "hatch"
	var spattern = $("#pselect");
	for (var i in ol.style.FillPattern.prototype.patterns) {
		var p = new ol.style.FillPattern({ pattern:i })
		$("<div>").attr('title',i)
			.css("background-image",'url("'+p.getImage().toDataURL()+'")')
			.click(function(){ pat = $(this).attr("title"); refresh(); })
			.appendTo(spattern);
	}
	/*var p = new ol.style.FillPattern({ image: new ol.style.Icon({ src : imgFile }) });
	$("<div>").attr('title','Image (PNG)')
			.css("background-image",'url("'+imgFile+'")')
			.click(function(){ pat = $(this).attr("title"); refresh(); })
			.appendTo(spattern);*/

	// Redraw map
	refresh = function() {
		$("#pselect").hide();
		coucheKML.OL_layers[0].setStyle(getStyle());
		if ( $.inArray(pat,["hatch","cross","dot","circle","square","tile"]) < 0 ){
			$("#size").prop("disabled",true);
			$("#spacing").prop("disabled",true);
			$("#angle").prop("disabled",true);
			$("#angle").next().text("");
		} else {
			$("#size").prop("disabled",false);
			$("#spacing").prop("disabled",false);
			$("#angle").prop("disabled",false);
			if (pat=="hatch") $("#angle").next().text("(deg)");
			else $("#angle").next().text("(bool)");
		}
		// Calculate image to be drawn outside the map
		var p = new ol.style.FillPattern(
					{	pattern: pat,
						image: (pat=='Image (PNG)') ? new ol.style.Icon({ src : imgFile }) : undefined,
						ratio: 2,
						color: "#000",
						size: Number($("#size").val()),
						spacing: Number($("#spacing").val()),
						angle: Number($("#angle").val())
					});
		$("#select").css('background-image', 'url('+p.getImage().toDataURL()+')');
	};

	refresh();
	

	function getStyle(feature) {
		var p = pat;
		return [ new ol.style.Style(
			{	fill: new ol.style.FillPattern(
					{	pattern: (p!='Image (PNG)') ? p : undefined,
						image: (p=='Image (PNG)') ? new ol.style.Icon({ src : imgFile }) : undefined,
						ratio: 1,
						icon: p=='Image (PNG)' ? new ol.style.Icon ({src:'data/target.png'}) : undefined,
						color: $("#color").val(),
						offset: Number($("#offset").val()),
						scale: Number($("#scale").val()),
						fill: new ol.style.Fill ({ color:$("#bg").val() }),
						size: Number($("#size").val()),
						spacing: Number($("#spacing").val()),
						angle: Number($("#angle").val())
					})
			})];
	}
	coucheKML.OL_layers[0].setStyle(getStyle());
	

}
