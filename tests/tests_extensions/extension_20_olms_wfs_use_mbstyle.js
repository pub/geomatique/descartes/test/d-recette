function chargementCarte() {
	chargeCouchesGroupes();
	
	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/geoserver/descartes/ows?";

	var featureNameSpace = "descartes";
	var serverVersion = "1.1.0";
	var attribution = "&#169;Serveur Descartes de tests";
	var internalProjection = "urn:ogc:def:crs:EPSG::4326";
	var featureReverseAxisOrientation = true;
	
	// couche de type "point"
	var layerNamePoints = "points4326";
	var featureGeometryNamePoints = "points_geom";
	var geometryTypeLayerPoints =  Descartes.Layer.POINT_GEOMETRY;
	
	// couche de type "ligne"
	var layerNameLignes = "lignes4326";
	var featureGeometryNameLignes = "lignes_geom";
	var geometryTypeLayerLignes =  Descartes.Layer.LINE_GEOMETRY;
	
	// couche de type "polygone"
	var layerNamePolygones = "polygones4326";
	var featureGeometryNamePolygones = "polygones_geom";
	var geometryTypeLayerPolygones =  Descartes.Layer.POLYGON_GEOMETRY;
	
	var couchePoints = {
		    title: "Ma couche WFS de points",
		    type: 10,
		    definition: [
		                 {	            	
		                	 serverUrl: serveur,
		                	 layerName: layerNamePoints,
		                	 featureServerUrl: serveur,
		                	 featureName: layerNamePoints,
		                	 featureNameSpace: featureNameSpace,
		                	 featureGeometryName: featureGeometryNamePoints,
		                	 serverVersion: serverVersion,
		                	 internalProjection: internalProjection,
		                	 featureReverseAxisOrientation: featureReverseAxisOrientation
		        }
		    ],
		    options: {
		        attributes: {
		            /*attributeId: {
		                fieldName: "d_attrib_1"
		            },*/
		            attributesEditable: [
		                {fieldName: 'd_attrib_2', label: 'Un attribut'},
		                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
		            ]
		        },
		        alwaysVisible: false,
		        visible: true,
		        queryable: true,
		        activeToQuery: false,
		        sheetable: true,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        format: "image/png",
		        displayOrder: 1,
		        geometryType: geometryTypeLayerPoints
		    }
		};
	
	var coucheLignes = {
		    title: "Ma couche WFS de lignes",
		    type: 10,
		    definition: [
		        {
		            	serverUrl: serveur,
						layerName: layerNameLignes,
						featureServerUrl: serveur,
						featureName: layerNameLignes,
						featureNameSpace: featureNameSpace,
						featureGeometryName: featureGeometryNameLignes,
						serverVersion: serverVersion,
	               	 internalProjection: internalProjection,
	            	 featureReverseAxisOrientation: featureReverseAxisOrientation
		        }
		    ],
		    options: {
		    	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'}
			           ]
			       },
		        alwaysVisible: false,
		        visible: true,
		        queryable: true,
		        activeToQuery: false,
		        sheetable: true,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        format: "image/png",
		        displayOrder: 1,
		        geometryType: geometryTypeLayerLignes
		    }
		};
		
		var couchePolygones = {
			   title: "Ma couche WFS de polygones",
			   type: 10,
			   definition: [
			                {
			           	  serverUrl: serveur,
			           	  layerName: layerNamePolygones,
			           	  featureServerUrl: serveur,
			           	  featureName: layerNamePolygones,
			           	  featureNameSpace: featureNameSpace,
			           	  featureGeometryName: featureGeometryNamePolygones,
			           	  serverVersion: serverVersion,
		                	 internalProjection: internalProjection,
		                	 featureReverseAxisOrientation: featureReverseAxisOrientation
			       }
			   ],
			   options: {
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPolygones
			   }
			};
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePoints.title, couchePoints.definition, couchePoints.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignes.title, coucheLignes.definition, coucheLignes.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygones.title, couchePolygones.definition, couchePolygones.options);
    
    contenuCarte.addItem(editionLayer1);
    contenuCarte.addItem(editionLayer2);
    contenuCarte.addItem(editionLayer3);

    const glStyle1 = "{"
		+"	    \"version\": 8,"
		+"		    \"name\": \"myMBStyle1\","
		+"		    \"layers\": ["
		+"		        {"
		+"                  \"id\": \"myStyle1\","	
		+"                  \"source\": \"mySource1\","
		+"                  \"paint\": {"
		+"                    \"circle-radius\": 10,"
		+"                    \"circle-color\": \"red\""
		+"                  }"
		+"		        }"
		+"		    ]"
		+"		}";
    
    const glStyle2 = "{"
		+"	    \"version\": 8,"
		+"		    \"name\": \"myMBStyle2\","
		+"		    \"layers\": ["
		+"		        {"
		+"                  \"id\": \"myStyle2\","	
		+"                  \"source\": \"mySource2\","
		+"                  \"paint\": {"
		+"                    \"line-color\": \"green\","
		+"                    \"line-width\": 5"
		+"                  }"
		+"		        }"
		+"		    ]"
		+"		}";
    
    const glStyle3 = "{"
		+"	    \"version\": 8,"
		+"		    \"name\": \"myMBStyle3\","
		+"		    \"layers\": ["
		+"		        {"
		+"                  \"id\": \"myStyle3\","	
		+"                  \"source\": \"mySource3\","
		+"                  \"paint\": {"
		+"                    \"fill-color\": \"blue\""
		+"                  }"
		+"		        }"
		+"		    ]"
		+"		}";
    	
    olms.stylefunction(editionLayer1.OL_layers[0], glStyle1, 'mySource1');
    olms.stylefunction(editionLayer2.OL_layers[0], glStyle2, 'mySource2');
    olms.stylefunction(editionLayer3.OL_layers[0], glStyle3, 'mySource3');   
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
    
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			minScale:2150000,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
