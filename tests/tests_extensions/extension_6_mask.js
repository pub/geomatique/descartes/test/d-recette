proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

var osm;
var carte;

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
    
    var projection = 'EPSG:4326';
	var bounds = [-6.22, 39.46, 9.14, 53.4];
	
    
    // Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:20000000,
					size: [600, 400]
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});


	 osm = new ol.layer.Tile({
        source: new ol.source.OSM()
      });
	 
	 var layers = carte.OL_map.getLayers();
	 layers.insertAt(0,osm);
	 
	 carte.mapContent.events.register('changed', this, addMyLayer);
	 
     var circleGeometry = new ol.geom.Circle([2.72, 45.87], 8);
     var fillStyle = new ol.style.Fill({color: [0, 0, 0, 0]});
     
     osm.on('precompose', function(event) {
         var ctx = event.context;
         var vecCtx = event.vectorContext;
         ctx.save();

         vecCtx.setFillStrokeStyle(fillStyle, null);
         vecCtx.drawCircle(circleGeometry);

         ctx.clip();
         
       });

     osm.on('postcompose', function(event) {
         var ctx = event.context;
         ctx.restore();
       });
	 
	
}

function addMyLayer(){
	var layers = carte.OL_map.getLayers();
	 layers.insertAt(0,osm);
}
