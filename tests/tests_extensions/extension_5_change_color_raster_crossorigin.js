
function chargementCarte() {
	
	var coucheFond = new Descartes.Layer.WMS(
			"Couche test (changement couleur) - Utilisation crossOrigin", 
			[
				{
					serverUrl: "https://ahocevar.com/geoserver/wms",
					serverType: 'geoserver',
			        crossOrigin: 'anonymous',
					layerName: "ne:ne"		}
				],
			{
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		);
	
	var coucheFond2 = new Descartes.Layer.WMS(
			"Couche test - Couleur par défaut - Utilisation crossOrigin", 
			[
				{
					serverUrl: "https://ahocevar.com/geoserver/wms",
					serverType: 'geoserver',
			        crossOrigin: 'anonymous',
					layerName: "ne:ne"		}
				],
			{
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		);
	coucheFond.OL_layers[0].on('postcompose', function(event) {
		greyscale(event.context);
	});
	coucheFond2.OL_layers[0].on('postcompose', function(event) {});
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	// Ajout des autres couches

	contenuCarte.addItem(coucheFond);
	contenuCarte.addItem(coucheFond2);
	
	var bounds = [-20037508,-20037508,20037508,20037508];
    var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					size: [600, 400]
				}
			);

	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector',
						options: {
							view: Descartes.UI.ImageSizeSelectorInPlace
						}
					});
}

function greyscale(context) {

	var canvas = context.canvas;
	 var width = canvas.width;
	 var height = canvas.height;

	var imageData = context.getImageData(0, 0, width, height);
	 var data = imageData.data;

	for(i=0; i<data.length; i += 4){
	  var r = data[i];
	  var g = data[i + 1];
	  var b = data[i + 2];
	  // CIE luminance for the RGB
	  var v = 0.2126 * r + 0.7152 * g + 0.0722 * b;
	  // Show white color instead of black color while loading new tiles:
	  if(v === 0.0)
	   v=255.0;  
	  data[i+0] = v; // Red
	  data[i+1] = v; // Green
	  data[i+2] = v+200; // Blue
	  data[i+3] = 255; // Alpha
	 }

	context.putImageData(imageData,0,0);
	 
	}
