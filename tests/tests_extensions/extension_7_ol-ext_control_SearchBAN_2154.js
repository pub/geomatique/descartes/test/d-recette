proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

var carte;
var vectorLayer;

var monStylePoint = {
    'Point': {
        pointRadius: 4,
        graphicName: 'circle',
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#ee9900'
    }
};

var onSelect;

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = 'EPSG:2154';
    // Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:9800000,
					size: [600, 400]
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	 
	vectorSource = new ol.source.Vector({});
	vectorLayer = new ol.layer.Vector({
        source: vectorSource
    });
	carte.OL_map.addLayer(vectorLayer);

	//OL-EXT searchBan par defaut
	var search = new ol.control.SearchBAN();
	carte.OL_map.addControl (search);
	search.on('select', function(e)
		{	
		carte.OL_map.getView().animate(
			{	center:e.coordinate,
				resolution: Descartes.Utils.getResolutionForScale(2000,"m")
			});
		});

	//OL-EXT searchBan autre possibilité
	var search1 = new ol.control.SearchBAN(
			{	
				target: "SearchBan1",
				placeholder: "ex : 3 rue de Savoie 75006 PARIS",
				url: "https://api-adresse.data.gouv.fr/search/",
				position: true	// Search, with priority to geo position
			});
	carte.OL_map.addControl (search1);
	search1.on('select', onSelect.bind(this));

}

function onSelect(e) {
	
	
    clearOverlays();

    var element = document.createElement('div');
    element.setAttribute('id', 'popup');
    element.className = 'Descartes-popup';

    var text = e.search.properties.label;

    var content = document.createElement('div');
    content.setAttribute('id', 'popup-content');
    content.innerHTML = text;

    element.appendChild(content);

    var popup = new ol.Overlay(({
        //id: this.feature.get('id') + '_overlay',
        id: 'localisationoverlay',
        element: element,
        autoPan: true,
        position: e.coordinate
    }));

    carte.OL_map.addOverlay(popup);
	
	
	
	vectorLayer.getSource().clear();
	var olStyles = Descartes.Symbolizers.getOlStyle(monStylePoint);
	var feature = new ol.Feature(new ol.geom.Point(e.coordinate));
	feature.setStyle(olStyles['point']);
	vectorLayer.getSource().addFeature(feature);
	carte.OL_map.getView().animate(
		{	center: e.coordinate,
			//zoom:carte.OL_map.getView().getMaxZoom()-6
			resolution: Descartes.Utils.getResolutionForScale(2500,"m")
		});
}

function clearOverlays() {
    carte.OL_map.getOverlays().forEach(function (overlay) {
        if (overlay.getId().indexOf('localisationoverlay') !== -1) {
        	carte.OL_map.removeOverlay(overlay);
        }
    });
}