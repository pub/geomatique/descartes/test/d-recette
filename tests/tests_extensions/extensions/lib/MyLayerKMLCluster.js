Descartes.Layer.MyLayerKMLCluster = Descartes.Utils.Class(Descartes.Layer.KML, {

	 distance:100,
	 clusterScaleLimit: 1500000,
	 
	 symbolizersCluster: {
		    'Point': {
		        pointRadius: 20, 
		        graphicName: 'circle',
		        fillColor: '#666666',
		        fillOpacity: 1,
		        strokeWidth: 1,
		        strokeOpacity: 1,
		        strokeColor: '#fff'
		    }
	 
	 },
		
	 initialize: function (title, layersDefinition, options) {

		 Descartes.Layer.KML.prototype.initialize.apply(this, [title, layersDefinition, options]);
		 
		 this.featureStyles = Descartes.Symbolizers.getOlStyle(this.symbolizers); 
		 for(var geometryTypeStyle in this.featureStyles) { 
			 this.featureStyles[geometryTypeStyle].setGeometry(this.featureStyleGeometryFunction);
		 }
		 
		 this.clusterStylePoint = Descartes.Symbolizers.getOlStyle(this.symbolizersCluster)["Point"];
	 },
	 
	 createOL_layer: function (layerDefinition) {
		 
		    var kmlVector = Descartes.Layer.KML.prototype.createOL_layer.apply(this, [layerDefinition]);

	        var clusterSource = new ol.source.Cluster({
	            distance: this.distance,
	            source: kmlVector.getSource(),
	            geometryFunction: function (feature) {
	 	    	   return new ol.geom.Point(ol.extent.getCenter(feature.getGeometry().getExtent()));  
	            }
	        });

	        var that=this;
	        var featureStyleFunction= function (feature, resolution){
		   		var size = feature.get('features').length;
		   		
		   	    var scale = Descartes.Utils.getScaleFromResolution(resolution, 'degrees');

		   		var style;
		   		//if (size > 1) {
		   	    //if (resolution > 0.006) {
		   	    //if (scale > that.clusterScaleLimit && size > 1) {	
		   		if (scale > that.clusterScaleLimit) {
		   	     	return that.custerStyle(size);
		   	   } else{		   		
			   		return that.featureStyles[that.geometryType];
			   }
	   	 	};
	        
	        var vector = new ol.layer.Vector({
	            source: clusterSource,
	            style: featureStyleFunction
	        });
		           
	        return vector;

	    },
	    
	    custerStyle: function (text) {
	    	this.clusterStylePoint.setText(new ol.style.Text({
	             text: text.toString(),
	             fill: new ol.style.Fill({
	               color: '#fff'
	             })
	           }));
	    	
	    	return this.clusterStylePoint;
	    },
	    
	    featureStyleGeometryFunction: function(feature){
	    	 var geometries= [];
	    	 for (var i = 0, len = feature.get('features').length; i < len; i++) {
	    		 var originalFeature = feature.get('features')[i];
	    		 geometries.push(originalFeature.getGeometry());
	    	 }
	    	 return new ol.geom.GeometryCollection(geometries);
	     },
      
	    registerLayerLoading: function (layer, scope, loadStart, loadEnd) {
	        var source = layer.getSource().getSource();
	        if (source instanceof ol.source.Image) {
	            source.on('imageloadstart', loadStart, scope);
	            source.on('imageloadend', loadEnd, scope);
	            source.on('imageloaderror', this._onLoadError, this);
	        } else if (source instanceof ol.source.Tile) {
	            source.on('tileloadstart', loadStart, scope);
	            source.on('tileloadend', loadEnd, scope);
	            source.on('tileloaderror', this._onLoadError, this);
	        } else if (source instanceof ol.source.Vector) {
	            source.on('featureloadstart', loadStart, scope);
	            source.on('featureloadend', loadEnd, scope);
	            source.on('featureloaderror', this._onLoadError, this);
	        }
	    },
	    
	    unregisterLayerLoading: function (layer, scope, loadStart, loadEnd) {
	        var source = layer.getSource().getSource();
	        if (source instanceof ol.source.Image) {
	            source.un('imageloadstart', loadStart, scope);
	            source.un('imageloadend', loadEnd, scope);
	            source.un('imageloaderror', this._onLoadError, this);
	        } else if (source instanceof ol.source.Tile) {
	            source.un('tileloadstart', loadStart, scope);
	            source.un('tileloadend', loadEnd, scope);
	            source.un('tileloaderror', this._onLoadError, this);
	        } else if (source instanceof ol.source.Vector) {
	            source.un('featureloadstart', loadStart, scope);
	            source.un('featureloadend', loadEnd, scope);
	            source.un('featureloaderror', this._onLoadError, this);
	        }
	    },
	    
	CLASS_NAME: "Descartes.Layer.MyLayerKMLCluster"
});
