Descartes.Layer.MyLayerWMS = Descartes.Utils.Class(Descartes.Layer.WMS, {

     
    scaleLimit: 1500000,
    
     initialize: function (title, layersDefinition, options) {
    	 Descartes.Layer.WMS.prototype.initialize.apply(this, [title, layersDefinition, options]);     
     },
    
    createOL_layer: function (layerDefinition) {
        var olLayer = Descartes.Layer.WMS.prototype.createOL_layer.apply(this, [layerDefinition]);
        olLayer.getSource().on('imageloadend', this.onLoadEnd.bind(this), this);
        return olLayer;
    },
    
    setVisibility: function (isVisible) {
        if (this.visible !== isVisible) {
            this.visible = isVisible;
            
            if (isVisible === false) {
                _.each(this.OL_layers, function (olLayer) {
                    olLayer.setVisible(isVisible);
                });
            } else {
                var view = this.OL_layers[0].map.getView();
                var resolution = view.getResolution();
                var units = view.getProjection().getUnits();
                var scale = Descartes.Utils.getScaleFromResolution(resolution, units);
                
                if (scale < this.scaleLimit) {
                    this.OL_layers[0].setVisible(false);
                    this.OL_layers[1].setVisible(true);
                } else {
                    this.OL_layers[0].setVisible(true);
                    this.OL_layers[1].setVisible(false);
                }
            }
  
        }
    },
        
    onLoadEnd: function () {
        var view = this.OL_layers[0].map.getView();
        var resolution = view.getResolution();
        var units = view.getProjection().getUnits();
        var scale = Descartes.Utils.getScaleFromResolution(resolution, units);
        
        if (scale < this.scaleLimit) {
            this.OL_layers[0].setVisible(false);
            this.OL_layers[1].setVisible(true);
        } else {
            this.OL_layers[0].setVisible(true);
            this.OL_layers[1].setVisible(false);
        }
        
    },
    

    CLASS_NAME: "Descartes.Layer.MyLayerWMS"
});