Descartes.UI.ImageSizeSelectorInPlace = Descartes.Utils.Class(Descartes.UI, {
	
	defaultDisplayClasses: {
		titleClassName: "DescartesUI"
	},
	
	imagesDOM: [],
    EVENT_TYPES: ["changeSize"],
	
	initialize: function(div, sizeSelectorModel) {
		Descartes.UI.prototype.initialize.apply(this, [div, sizeSelectorModel, this.EVENT_TYPES]);
	},
	
	draw: function(sizeList, defaultSize) {
		var self = this;
		this.imagesDOM = [];
			
		var titre = document.createElement('div');
		titre.align = "center";
		titre.className = this.displayClasses.titleClassName;
		titre.innerHTML = this.getMessage("TITLE_MESSAGE");
		this.div.appendChild(titre);
		this.div.appendChild(document.createElement('br'));
		
		var table = document.createElement('table');
		var tr = document.createElement('tr');

		for (var indexSize = 0 ; indexSize < sizeList.length ; indexSize++) {
			var td = document.createElement('td');
			td.align = "center";
			
			var mapWidth = sizeList[indexSize][0];
			var mapHeight = sizeList[indexSize][1];
			var coteImage = mapHeight/10;

			var image = document.createElement('img');
			image.id = mapWidth + "x" +mapHeight;
			image.title = this.getMessage("INFO_BUL1") + mapWidth + this.getMessage("INFO_BUL2") + mapHeight;;
			image.src = "extensions/theme/img/mapsize.gif";
			image.width = coteImage;
			image.height = coteImage;
			image.border = (indexSize==defaultSize) ? "1" : "0";
			image.onclick = function() {self.done(this);};

			td.appendChild(image);
			this.imagesDOM.push(image);
			
			tr.appendChild(td);
		}
		table.appendChild(tr);
		this.div.appendChild(table);
	},

	done: function(control) {
		var dims = control.id.split("x");
		this.model.size = new Descartes.Size(dims[0], dims[1]);
		for (var i=0 ; i<this.imagesDOM.length ; i++) {
			this.imagesDOM[i].border = (this.imagesDOM[i] === control) ? "1" : "0";
		}
 		this.events.triggerEvent("changeSize");	
	},
	
	selectSize: function(width, height) {
		for (var i=0 ; i<this.imagesDOM.length ; i++) {
			this.imagesDOM[i].border = (this.imagesDOM[i].id === (width + "x" + height)) ? "1" : "0";
		}
		this.model.size = new Descartes.Size(width, height);
		this.events.triggerEvent("changeSize");	
	},
	
	CLASS_NAME: "Descartes.UI.ImageSizeSelectorInPlace"
});

Descartes.Messages.Descartes_Messages_UI_ImageSizeSelectorInPlace = {
	TITLE_MESSAGE : "Choisir la taille de la carte",
	INFO_BUL1 : "Afficher la carte avec une taille en pixels de " ,
	INFO_BUL2 : " par "
};

