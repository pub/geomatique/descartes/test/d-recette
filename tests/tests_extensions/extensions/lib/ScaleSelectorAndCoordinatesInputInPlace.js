Descartes.UI.ScaleSelectorAndCoordinatesInputInPlace = Descartes.Utils.Class(Descartes.UI, {

	defaultDisplayClasses: {
		textClassName: "DescartesUI",
		labelClassName: "DescartesUI",
		inputClassName: "DescartesUI",
		buttonClassName: "DescartesUI"
	},
    EVENT_TYPES: ["choosed"],

	initialize: function(div, model, options) {
		Descartes.UI.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
	},
	
	draw: function(scales) {
		var self = this;
		
		this.div.appendChild(this.createTextSpan(this.getMessage("TITLE_MESSAGE"),this.displayClasses));
		
		this.form = document.createElement('form');
		
		var optionsSelect = [];
		scales.forEach(
			function(scale) {
				var option = {value:scale.toString(),text:Descartes.Utils.readableScale(scale)};
				optionsSelect.push(option);
			}
   		);

		this.form.appendChild(this.createSelectInput("selectScaleSelector",optionsSelect,this.displayClasses));
		this.form.appendChild(this.createTextInputWithLabel("x",this.getMessage("INVITE1_MESSAGE"), Descartes.Utils.extend({size:10}, this.displayClasses)));
		this.form.appendChild(this.createTextInputWithLabel("y",this.getMessage("INVITE2_MESSAGE"), Descartes.Utils.extend({size:10}, this.displayClasses)));

		var bouton = document.createElement('input');
		bouton.type = "button";
		bouton.value = this.getMessage("BUTTON_MESSAGE");
		bouton.className = this.displayClasses.buttonClassName;
		bouton.onclick = function() {self.done();};
	
		this.div.appendChild(this.form);
		this.div.appendChild(bouton);
	},
	
	done: function() {
		var scaleSelect = this.form.selectScaleSelector;
		this.model.scale = scaleSelect.options[scaleSelect.selectedIndex].value;
		this.model.x = this.form.x.value;
		this.model.y = this.form.y.value;
		this.events.triggerEvent("choosed");
	},
	
	createTextSpan : function(text, options) {
		var defaultOptions = Descartes.Utils.extend({}, options);
		var spanElement = document.createElement('span');
		spanElement.innerHTML = text;
		if (defaultOptions.textClassName !== undefined) {
			spanElement.className = defaultOptions.textClassName;
		}
		return spanElement;
	},
	
	createSelectInput : function(selectName, values, options) {
		var defaultOptions = {size:1};
		defaultOptions = Descartes.Utils.extend(defaultOptions, options);
		var selectElement = document.createElement('select');
		selectElement.name = selectName;
		selectElement.id = selectName;
		selectElement.size = defaultOptions.size;
		if (defaultOptions.selectClassName !== undefined) {
			selectElement.className = defaultOptions.selectClassName;
		}
		for (var i=0, len=values.length ; i<len ; i++) {
			var optionElement = document.createElement('option');
			optionElement.value = values[i].value.toString();
			optionElement.selected = (values[i].value == defaultOptions.value);
			optionElement.innerHTML = values[i].text;
			selectElement.appendChild(optionElement);
		}
		return selectElement;
	},
	
	createTextInputWithLabel : function(inputName, labelText, options) {
		var defaultOptions = Descartes.Utils.extend({}, options);
	
		var globalElement = document.createElement('div');
		if (defaultOptions.globalClassName !== undefined) {
			globalElement.className = defaultOptions.globalClassName;
		}
		globalElement.appendChild(this.createLabelSpan(labelText, defaultOptions));
		globalElement.appendChild(this.createTextInput(inputName, defaultOptions));
		return globalElement;
	},
	
	createLabelSpan : function(labelText, options) {
		var defaultOptions = Descartes.Utils.extend({}, options);
		var labelElement = document.createElement('label');
		labelElement.innerHTML = labelText;
		if (options.separator === undefined || options.separator !== false) {
			labelElement.innerHTML += "&nbsp;:";
		}
		if (defaultOptions.labelClassName !== undefined) {
			labelElement.className = defaultOptions.labelClassName;
		}
		return labelElement;
	},
	
	createTextInput : function(inputName, options) {
		var defaultOptions = {size:50, value:""};
		defaultOptions = Descartes.Utils.extend(defaultOptions, options);
		var inputElement = document.createElement('input');
		inputElement.name = inputName;
		inputElement.id = inputName;
		inputElement.type = "text";
		inputElement.size = defaultOptions.size;
		inputElement.value = defaultOptions.value;
		if (defaultOptions.inputClassName !== undefined) {
			inputElement.className = defaultOptions.inputClassName;
		}
		return inputElement;
	},
	
	CLASS_NAME: "Descartes.UI.ScaleSelectorAndCoordinatesInputInPlace"
});

Descartes.Messages.Descartes_Messages_UI_ScaleSelectorAndCoordinatesInputInPlace = {
	TITLE_MESSAGE : "Echelle et coordonnées",
	BUTTON_MESSAGE : "Recentrer"
};
Descartes.Utils.extend(Descartes.Messages.Descartes_Messages_UI_ScaleSelectorAndCoordinatesInputInPlace, Descartes.Messages.Descartes_Forms_CoordinatesInput);
