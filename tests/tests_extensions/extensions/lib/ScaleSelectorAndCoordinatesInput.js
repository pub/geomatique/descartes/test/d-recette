Descartes.Action.ScaleSelectorAndCoordinatesInput = Descartes.Utils.Class(Descartes.Action, {

	scales: [250000, 500000, 1000000],
	
	initialize: function(div, olMap, options) {
   		this.model = {};
   		
		Descartes.Action.prototype.initialize.call(this, div, olMap);
	
		 if (!_.isNil(options) && !_.isNil(options.view)) {
	            var view = options.view;
	            this.renderer = new view(div, this.model, options);
	        } else {
	            this.renderer = new Descartes.UI.ScaleSelectorAndCoordinatesInputInPlace(div, this.model, options);
	        }

   		this.renderer.events.register('choosed', this, this.gotoScaleAndCoordinates);
   		this.renderer.draw(this.scales);
	},
	
	gotoScaleAndCoordinates:function() {	
		var unit = this.olMap.getView().getProjection().getUnits();
        var resolution = Descartes.Utils.getResolutionForScale(this.model.scale, unit);
        this.olMap.getView().setResolution(resolution);
        this.olMap.getView().setCenter([this.model.x, this.model.y]);
	},
	
	CLASS_NAME: "Descartes.Action.ScaleSelectorAndCoordinatesInput"
});
