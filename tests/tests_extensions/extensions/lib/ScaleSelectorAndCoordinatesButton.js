Descartes.Button.ScaleSelectorAndCoordinatesButton = Descartes.Utils.Class(Descartes.Button, {

	initialize: function(options) {
   		this.enabled = true;
		Descartes.Button.prototype.initialize.apply(this,[options]);
	},

	execute: function() {
		var action = new Descartes.Action.ScaleSelectorAndCoordinatesInput(null, this.olMap,{view: Descartes.UI.ScaleSelectorAndCoordinatesInputDialog});
	},
	
	CLASS_NAME: "Descartes.Button.ScaleSelectorAndCoordinatesButton"
});

Descartes_Messages_Button_ScaleSelectorAndCoordinatesButton = {
	TITLE : "Centrer selon des coordonnées et à une échelle"	
};
