Descartes.UI.ScaleSelectorAndCoordinatesInputDialog = Descartes.Utils.Class(Descartes.UI, {

	defaultDisplayClasses: {
		globalClassName: "DescartesModalDialogLabelAndValue",
		labelClassName: "DescartesModalDialogLabel",
		inputClassName: "DescartesModalDialogInput",
		textClassName: "DescartesModalDialogText",
		selectClassName: "DescartesModalDialogSelect",
		fieldSetClassName: "DescartesModalDialogFieldSet",
		legendClassName: "DescartesModalDialogLegend"
	},

    EVENT_TYPES: ["choosed"],
    
	initialize: function(div, model, options) {
		Descartes.UI.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
	},
	
	draw: function(scales) {
		this.form = document.createElement('form');

		this.form.appendChild(this.createLabelSpan(this.getMessage("TITLE_MESSAGE"),this.displayClasses));
		
		var optionsSelect = [];
		scales.forEach(
			function(scale) {   					
				var option = {value:scale.toString(),text:Descartes.Utils.readableScale(scale)};
				optionsSelect.push(option);
			}
   		);

		this.form.appendChild(this.createSelectInput("selectScaleSelector",optionsSelect,this.displayClasses));
		this.form.appendChild(this.createTextInputWithLabel("x",this.getMessage("INVITE1_MESSAGE"), Descartes.Utils.extend({size:10}, this.displayClasses)));
		this.form.appendChild(this.createTextInputWithLabel("y",this.getMessage("INVITE2_MESSAGE"), Descartes.Utils.extend({size:10}, this.displayClasses)));


        var dialog = new Descartes.UI.ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('DIALOG_TITLE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            size: 'modal-sm',
            content: this.form.innerHTML
        });
        dialog.open($.proxy(this.sendDatas, this));
		
	},
	
    sendDatas: function(result) {
		this.model.scale = result.selectScaleSelector;
		this.model.x = result.x;
		this.model.y = result.y;
		this.events.triggerEvent("choosed");
    },
	
	createSelectInput : function(selectName, values, options) {
		var defaultOptions = {size:1};
		defaultOptions = Descartes.Utils.extend(defaultOptions, options);
		var selectElement = document.createElement('select');
		selectElement.name = selectName;
		selectElement.id = selectName;
		selectElement.size = defaultOptions.size;
		if (defaultOptions.selectClassName !== undefined) {
			selectElement.className = defaultOptions.selectClassName;
		}
		for (var i=0, len=values.length ; i<len ; i++) {
			var optionElement = document.createElement('option');
			optionElement.value = values[i].value.toString();
			optionElement.selected = (values[i].value == defaultOptions.value);
			optionElement.innerHTML = values[i].text;
			selectElement.appendChild(optionElement);
		}
		return selectElement;
	},
	
	createTextInputWithLabel : function(inputName, labelText, options) {
		var defaultOptions = Descartes.Utils.extend({}, options);
	
		var globalElement = document.createElement('div');
		if (defaultOptions.globalClassName !== undefined) {
			globalElement.className = defaultOptions.globalClassName;
		}
		globalElement.appendChild(this.createLabelSpan(labelText, defaultOptions));
		globalElement.appendChild(this.createTextInput(inputName, defaultOptions));
		return globalElement;
	},
	
	createLabelSpan : function(labelText, options) {
		var defaultOptions = Descartes.Utils.extend({}, options);
		var labelElement = document.createElement('label');
		labelElement.innerHTML = labelText;
		if (options.separator === undefined || options.separator !== false) {
			labelElement.innerHTML += "&nbsp;:";
		}
		if (defaultOptions.labelClassName !== undefined) {
			labelElement.className = defaultOptions.labelClassName;
		}
		return labelElement;
	},
	
	createTextInput : function(inputName, options) {
		var defaultOptions = {size:50, value:""};
		defaultOptions = Descartes.Utils.extend(defaultOptions, options);
		var inputElement = document.createElement('input');
		inputElement.name = inputName;
		inputElement.id = inputName;
		inputElement.type = "text";
		inputElement.size = defaultOptions.size;
		inputElement.value = defaultOptions.value;
		if (defaultOptions.inputClassName !== undefined) {
			inputElement.className = defaultOptions.inputClassName;
		}
		return inputElement;
	},
    
	CLASS_NAME: "Descartes.UI.ScaleSelectorAndCoordinatesInputDialog"
});

Descartes.Messages.Descartes_Messages_UI_ScaleSelectorAndCoordinatesInputDialog = {
	DIALOG_TITLE : "Choisir l\'échelle de la carte et les coordonnées de recentrage",
	TITLE_MESSAGE : "Echelle",
	OK_BUTTON : "Recentrer",
	CANCEL_BUTTON : "Annuler"
};
Descartes.Utils.extend(Descartes.Messages.Descartes_Messages_UI_ScaleSelectorAndCoordinatesInputDialog, Descartes.Messages.Descartes_Forms_CoordinatesInput);
