proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargementCarte() {
	
	var coucheFond = new Descartes.Layer.WMS(
			"Fond Géoref - Affichage échelle de gris (grayscale)", 
			[
				{
					serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
					layerName: "fond_vecteur",
					crossOrigin: 'anonymous'		}
				],
			{
				useProxy: true,
				maxScale: 100,
				minScale: 10000001,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		);
	var coucheFond2 = new Descartes.Layer.WMS(
			"Fond Géoref - Couleur par défaut", 
			[
				{
					serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
					layerName: "fond_vecteur",
					crossOrigin: 'anonymous'		}
				],
			{
				useProxy: true,
				maxScale: 100,
				minScale: 10000001,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		);
	coucheFond.OL_layers[0].on('postcompose', function(event) {
		greyscale(event.context);
	});
	coucheFond2.OL_layers[0].on('postcompose', function(event) {});
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	// Ajout des autres couches

	contenuCarte.addItem(coucheFond);
	contenuCarte.addItem(coucheFond2);
	
	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);

	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector',
						options: {
							view: Descartes.UI.ImageSizeSelectorInPlace
						}
					});
}

function greyscale(context) {

	var canvas = context.canvas;
	 var width = canvas.width;
	 var height = canvas.height;

	var imageData = context.getImageData(0, 0, width, height);
	 var data = imageData.data;

	for(i=0; i<data.length; i += 4){
	  var r = data[i];
	  var g = data[i + 1];
	  var b = data[i + 2];
	  // CIE luminance for the RGB
	  var v = 0.2126 * r + 0.7152 * g + 0.0722 * b;
	  // Show white color instead of black color while loading new tiles:
	  if(v === 0.0)
	   v=255.0;  
	  data[i+0] = v; // Red
	  data[i+1] = v; // Green
	  data[i+2] = v; // Blue
	  data[i+3] = 255; // Alpha
	 }

	context.putImageData(imageData,0,0);
	 
	}
