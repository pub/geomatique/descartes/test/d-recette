proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

function chargementCarte() {
	
	var couche4326 = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://vmap0.tiles.osgeo.org/wms/vmap0",
					layerName: "basic",
					featureServerUrl: null,
					featureName: null
				}
			],
			options: {
				//maxScale: 100, // ex  100
				//minScale: 50000,	// ex 500000
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png"
				//displayOrder:2,
			}
		};
	
    var maCouchePointsKMLCluster = new Descartes.Layer.MyLayerKMLCluster(
    	    'couche points (MyLayerKMLCluster) - Style par défaut',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:false,
    	        geometryType: Descartes.Layer.POINT_GEOMETRY
    	    }
    	);
    
    var maCouchePolygonesKMLCluster = new Descartes.Layer.MyLayerKMLCluster(
    	    'Reserves Naturelles (MyLayerKMLCluster) - Style perso',
    	    [{
    	            serverUrl: './data/kml/RNR_2013.KML'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:false,
    	        geometryType: Descartes.Layer.POLYGON_GEOMETRY,
    	        symbolizersCluster: {
    			    'Point': {
    			    	 pointRadius: 20,
    	                 graphicName: 'square',
    	                 points: 4,
    	                 angle: Math.PI / 4,
    	                 fillColor: '#ee0000',
    	                 fillOpacity: 1,
    	                 strokeWidth: 1,
    	                 strokeOpacity: 1,
    	                 strokeColor: '#fff'
    			    }
    		    },
    	        symbolizers:{
				  "Polygon": {
					strokeWidth: 2,
					strokeOpacity: 1, 
					strokeColor: "#ee0000",
					fillColor: "#ee0000", 
					fillOpacity: 0.3 
				  },
				  "MultiPolygon": {
					strokeWidth: 2,
					strokeOpacity: 1,
					strokeColor: "#ee0000", 
					fillColor: "#ee0000", 
					fillOpacity: 0.3
				  }
    	        }
    	    }
    	);
	
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	// Ajout couche KML

	contenuCarte.addItem(maCouchePointsKMLCluster);
	contenuCarte.addItem(maCouchePolygonesKMLCluster);
	//contenuCarte.addItem(couchePointsKML);
	//contenuCarte.addItem(coucheReservesNaturellesRegionalesKML);
	contenuCarte.addItem(new Descartes.Layer.WMS(couche4326.title, couche4326.definition, couche4326.options));
    
    var projection = 'EPSG:4326';
    var bounds = [-5.8, 42.3, 8.13, 52.1];
    
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:20000000,
					size: [600, 400]
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	
}
