var coucheNature, coucheBati, coucheRoutes, coucheFer, coucheEau, coucheToponymes, coucheParkings, coucheStations, groupeInfras, coucheWmsKO, coucheNatureReproj, coucheEauReproj, coucheAggregatWMS, coucheParkingsWmsWIthWfs, coucheStationsWmsWIthWfs;
var coucheEauWfs, coucheNatureWfs, coucheWfsKO, coucheEauWfsReproj, coucheNatureWfsReproj, coucheEauWfsWithWms, coucheNatureWfsWithWms;
var coucheBdxBDorthoWmsc, coucheWmscKO;
var coucheBdxBDorthoTms, coucheTmsKO;
var couchePaysGeoJson, couchePaysGeoJson2, coucheGeoJsonKO, couchePaysGeoJsonReproj;
var coucheReservesNaturellesRegionalesKML, couchePointsKML, coucheLinesKML, coucheKMLKO, couchePointsKMLWithStyleEmbarque, coucheReservesNaturellesRegionalesKMLWithStyleEmbarque, couchePointsKMLReproj;
var groupeFonds;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeCouchesGroupes() {
	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?";

	coucheNature = new Descartes.Layer.WMS(
		"Espaces naturels", 
		[
			{
				serverUrl: serveur,
				layerName: "c_natural_Valeurs_type"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheBati = new Descartes.Layer.WMS(
		"Constructions", 
		[
			{
				serverUrl: serveur,
				layerName: "c_buildings",
				featureServerUrl: serveur,
				featureName: "c_buildings2",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		{
			maxScale: null,
			minScale: 39000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheRoutes = new Descartes.Layer.WMS(
		"Routes", 
		[
			{
				serverUrl: serveur,
				layerName: "c_roads"			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_roads"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheFer = new Descartes.Layer.WMS(
		"Chemins de fer", 
		[
			{
				serverUrl: serveur,
				layerName: "c_railways"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_railways"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheEau = new Descartes.Layer.WMS(
		"Cours d'eau", 
		[
			{
				serverUrl: serveur,
				layerName: "c_waterways_Valeurs_type"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheToponymes = new Descartes.Layer.WMS(
		"Lieux", 
		[
			{
				serverUrl: serveur,
				layerName: "c_places_Etiquettes"			}
			],
		{
			maxScale: null,
			minScale: 190000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: "http://metadataURL.fr",
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);
	
	coucheStations = new Descartes.Layer.WMS(
		"Stations essence", 
		[
			{
				serverUrl: serveur,
				layerName: "c_stations",
			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheParkings = new Descartes.Layer.WMS(
		"Parkings", 
		[
			{
				serverUrl: serveur,
				layerName: "c_parkings",
			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);
	
	coucheWmsKO = new Descartes.Layer.WMS(
			"couche (WMS) KO", 
			[
				{
					serverUrl: serveur,
					layerName: "c_parkingsss",
					featureServerUrl: serveur,
					featureName: "c_parkingsss"
				}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:true,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheNatureReproj = new Descartes.Layer.WMS(
			"Espaces naturels", 
			[
				{
					serverUrl: serveur,
					layerName: "c_natural_Valeurs_type",
					internalProjection: "EPSG:2154"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheEauReproj = new Descartes.Layer.WMS(
			"Cours d'eau", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					internalProjection: "EPSG:2154"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheAggregatWMS = new Descartes.Layer.WMS(
			"Couche aggregat : couches \"Cours d'eau\" et \"Nature\" sur le même serveur", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type,c_natural_Valeurs_type"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheAggregatWMS2 = new Descartes.Layer.WMS(
			"Couche aggregat : couches \"Stations\" et \"Parkings\" sur deux serveurs différents", 
			[
				{
					serverUrl: serveur,
					layerName: "c_stations"
				},
				{
					serverUrl: serveur,
					layerName: "c_parkings"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheAggregatWMS2 = new Descartes.Layer.WMS(
			"Couche aggregat : couches \"Cours d'eau\" et \"Nature\" sur deux serveurs différents", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type"
				},
				{
					serverUrl: serveur,
					layerName: "c_natural_Valeurs_type"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheStationsWmsWIthWfs = new Descartes.Layer.WMS(
			"Stations essence", 
			[
				{
					serverUrl: serveur,
					layerName: "c_stations",
					featureServerUrl: serveur,
					featureName: "c_stations"
				}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:true,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);

		coucheParkingsWmsWIthWfs = new Descartes.Layer.WMS(
			"Parkings", 
			[
				{
					serverUrl: serveur,
					layerName: "c_parkings",
					featureServerUrl: serveur,
					featureName: "c_parkings"
				}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:true,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);

	groupeInfras = new Descartes.Group(
		"Infrastructures", 
		{opened : false}
	);
	
	/************************************************************************
	 * 
	 * COUCHES WFS
	 * 
	 ***********************************************************************/
	
	coucheEauWfs = new Descartes.Layer.WFS(
			"Cours d'eau (WFS)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type"			}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheNatureWfs = new Descartes.Layer.WFS(
			"Espaces naturels (WFS)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_natural_Valeurs_type"			}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheWfsKO = new Descartes.Layer.WFS(
			"Couche (WFS) KO", 
			[
				{
					serverUrl: "http://test",
					layerName: "c_natural_Valeurs_type"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheWfsStyle= new Descartes.Layer.WFS(
			"Couche (WFS) - style particulier", 
			[
				{
					serverUrl: serveur,
					layerName: "c_natural_Valeurs_type"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright",
				symbolizers:{
				  "Point": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#A5F38D" //"#ee9900"
				  },
				  "Line": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "Polygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  },
				  "MultiPoint": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#A5F38D" //"#ee9900"
				  },
				  "MultiLine": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "MultiPolygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  }
				}
			}
		);
	
	coucheEauWfsReproj = new Descartes.Layer.WFS(
			"Cours d'eau (WFS) - Reprojecté (EPSG:2154 en EPSG:3857)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_waterways_Valeurs_type",
					internalProjection: "EPSG:2154"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	coucheNatureWfsReproj = new Descartes.Layer.WFS(
			"Espaces naturels (WFS) - Reprojecté (EPSG:2154 en EPSG:3857)", 
			[
				{
					serverUrl: serveur,
					layerName: "c_natural_Valeurs_type",
					internalProjection: "EPSG:2154"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);

		coucheParkingsWfsWIthWms = new Descartes.Layer.WFS(
			"Parkings", 
			[
				{
					serverUrl: serveur,
					layerName: "c_parkings",
					imageServerUrl: serveur,
					imageLayerName: "c_parkings"
				}
				],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:true,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	/************************************************************************
	 * 
	 * COUCHES WMSC
	 * 
	 ***********************************************************************/

	
	coucheBdxBDorthoWmsc = new Descartes.Layer.WMSC(
            "BD Ortho&reg; (WMS-C)",
            [
                {
                    serverUrl: [
                        "http://georef.e2.rie.gouv.fr/cache/service/wms/",
                        "http://georef2.application.i2/cache/service/wms/",
                        "http://georef3.application.i2/cache/service/wms/",
                        "http://georef4.application.i2/cache/service/wms/"
                    ],
                    layerName: "georef:bdortho2154"
                }
            ],
            {
                origin: [70000, 6030000],
                visible: true,
                tileSize: [512, 512],
                format: "image/jpeg",
                resolutions: [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5],
                attribution: '&copy; IGN - BD Ortho&reg;'
            }
    );
	
	coucheWmscKO = new Descartes.Layer.WMSC(
            "Couche (WMS-C) KO",
            [
                {
                    serverUrl: [
                        "http://georef.e2.rie.gouv.fr/cache/service/wmsss/",
                        "http://georef2.application.i2/cache/service/wmsss/",
                        "http://georef3.application.i2/cache/service/wmsss/",
                        "http://georef4.application.i2/cache/service/wmsss/"
                    ],
                    layerName: "georef:bdortho2154"
                }
            ],
            {
                origin: [70000, 6030000],
                visible: true,
                tileSize: [512, 512],
                format: "image/jpeg",
                resolutions: [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5],
                attribution: '&copy; IGN - BD Ortho&reg;'
            }
    );
	
	/************************************************************************
	 * 
	 * COUCHES TMS
	 * 
	 ***********************************************************************/
	
    coucheBdxBDorthoTms = new Descartes.Layer.TMS(
            "BD Ortho&reg; (TMS)",
            [
                {
                    serverUrl: ["http://georef.e2.rie.gouv.fr/cache/service/tms/",
                                "http://georef2.application.i2/cache/service/tms/",
                                "http://georef3.application.i2/cache/service/tms/",
                                "http://georef4.application.i2/cache/service/tms/"
                            ],
                    layerName: "georef:bdortho2154"
                }
            ],
            {
                origin: [70000, 6030000],
                extent: [70000, 6030000, 1270000, 7130000],
                opacity: 60,
                visible: true,
                format: 'jpeg',
                resolutions: [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5],
                tileSize: [512, 512],
                attribution: '&copy; IGN - BD Ortho&reg;'
            }
    );
    
    coucheTmsKO = new Descartes.Layer.TMS(
            "Couche (TMS) KO",
            [
                {
                    serverUrl: ["http://georef.e2.rie.gouv.fr/cache/service/tms/",
                                "http://georef2.application.i2/cache/service/tms/",
                                "http://georef3.application.i2/cache/service/tms/",
                                "http://georef4.application.i2/cache/service/tms/"
                            ],
                    layerName: "georef:bdortho215444"
                }
            ],
            {
                origin: [70000, 6030000],
                extent: [70000, 6030000, 1270000, 7130000],
                opacity: 60,
                visible: true,
                format: 'jpeg',
                resolutions: [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5],
                tileSize: [512, 512],
                attribution: '&copy; IGN - BD Ortho&reg;'
            }
    );

	/************************************************************************
	 * 
	 * COUCHES WMTS
	 * 
	 ***********************************************************************/
    
    var matrixIdsWMTS = [
         'GoogleMapsCompatible:0',
         'GoogleMapsCompatible:1',
         'GoogleMapsCompatible:2',
         'GoogleMapsCompatible:3',
         'GoogleMapsCompatible:4',
         'GoogleMapsCompatible:5',
         'GoogleMapsCompatible:6',
         'GoogleMapsCompatible:7',
         'GoogleMapsCompatible:8',
         'GoogleMapsCompatible:9',
         'GoogleMapsCompatible:10',
         'GoogleMapsCompatible:11'
     ];
    
    var originsWMTS = [
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508],
       [-20037508.342789248, 20037508]
   ];
             
    var projectionWMTS = new Descartes.Projection('EPSG:3857');
    var extentWMTS = projectionWMTS.getExtent();

    var tileSize = 256;
    var sizeWMTS = ol.extent.getWidth(extentWMTS) / tileSize;

    var resolutionsWMTS = [];

    for (var i = 0; i < 19; i++) {
         resolutionsWMTS[i] = sizeWMTS / Math.pow(2, i);
         //matrixIds[i] = i;
    }

    coucheBdxBDorthoWmts = new Descartes.Layer.WMTS(
    		 'couche georef WMTS', 
    		 [
    		  	{
    		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmts?',
    		  		layerName: 'georefmonde:GoogleMapsCompatible'
    		  	}
    		 ],
    		 {
    			 extent: extentWMTS,
    			 matrixSet: "GoogleMapsCompatible",
    			 projection: projectionWMTS,
    			 matrixIds: matrixIdsWMTS,
    			 origins: originsWMTS,
    			 resolutions: resolutionsWMTS,
    			 tileSize: [256, 256],
    			 format: 'image/jpeg',
    			 visible: true,
    			 opacity: 100
    		 }
    );
    
    coucheWmtsKO = new Descartes.Layer.WMTS(
   		 'couche WMTS KO', 
   		 [
   		  	{
   		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmtsss?',
   		  		layerName: 'georefmonde:GoogleMapsCompatible'
   		  	}
   		 ],
   		 {
   			 extent: extentWMTS,
   			 matrixSet: "GoogleMapsCompatible",
   			 projection: projectionWMTS,
   			 matrixIds: matrixIdsWMTS,
   			 origins: originsWMTS,
   			 resolutions: resolutionsWMTS,
   			 tileSize: [256, 256],
   			 format: 'image/jpeg',
   			 visible: true,
   			 opacity: 100
   		 }
   );
    
	/************************************************************************
	 * 
	 * COUCHES GEOJSON
	 * 
	 ***********************************************************************/
    
    couchePaysGeoJson = new Descartes.Layer.GeoJSON(
    	    'Pays (GeoJson distant) ',
    	    [{
    	            serverUrl: 'https://openlayers.org/en/v4.3.3/examples/data/geojson/countries.geojson'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100
    	    }
    	);
    
    coucheGeoJsonKO = new Descartes.Layer.GeoJSON(
    	    'couche (GeoJson) KO',
    	    [{
    	            serverUrl: 'https://test.json'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100
    	    }
    	);
    
    couchePaysGeoJson2 = new Descartes.Layer.GeoJSON(
	    'Pays (GeoJson local) ',
	    [{
	            serverUrl: './data/geojson/countries.geojson'
	        }],
	    {
	        visible: true,
	        opacity: 100
	    }
	);
    
    couchePaysGeoJsonStyle = new Descartes.Layer.GeoJSON(
    	    'Pays (GeoJson local) - style particulier',
    	    [{
    	            serverUrl: './data/geojson/countries.geojson'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
				symbolizers:{
				  "Point": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#A5F38D" //"#ee9900"
				  },
				  "Line": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "Polygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  },
				  "MultiPoint": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "#A5F38D" //"#ee9900"
				  },
				  "MultiLine": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "MultiPolygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "#A5F38D", //"#ee9900"
					fillColor: "#A5F38D", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  }
				}
    	    }
    	);
    
    couchePaysGeoJsonReproj = new Descartes.Layer.GeoJSON(
    	    'Pays (GeoJson local) - Reprojeté (EPSG:4326 en EPSG:2154)',
    	    [{
    	            serverUrl: './data/geojson/countries.geojson',
    	            internalPorjection: 'EPSG:4326'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100
    	    }
    	);
    
	/************************************************************************
	 * 
	 * COUCHES KML
	 * 
	 ***********************************************************************/

    coucheReservesNaturellesRegionalesKMLWithStyleEmbarque = new Descartes.Layer.KML(
	    'Reserves Naturelles (KML) avec style embarque',
	    [{
	            serverUrl: './data/kml/RNR_2013.KML'
	        }],
	    {
	        visible: true,
	        opacity: 100
	    }
	);
    
    coucheReservesNaturellesRegionalesKML = new Descartes.Layer.KML(
    	    'Reserves Naturelles (KML)',
    	    [{
    	            serverUrl: './data/kml/RNR_2013.KML'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:false
    	    }
    	);
    
    coucheKMLKO = new Descartes.Layer.KML(
    	    'couche (KML) KO',
    	    [{
    	            serverUrl: './pasdefichier.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100
    	    }
    	);
    
    couchePointsKMLWithStyleEmbarque = new Descartes.Layer.KML(
    	    'couche points (KML) avec le style embarqué',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100
    	    }
    	);
    
    couchePointsKML = new Descartes.Layer.KML(
    	    'couche points (KML)',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:false
    	    }
    	);
    
    coucheLinesKML = new Descartes.Layer.KML(
    	    'couche lignes (KML)',
    	    [{
    	            serverUrl: './data/kml/lines.kml'/*,
    	            displayProjection: 'EPSG:2154',
    	            internalProjection: 'EPSG:4326'*/
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100
    	    }
    	);
    
    couchePointsKMLStyle = new Descartes.Layer.KML(
    	    'couche points (KML) - style particulier',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:false,
    	        symbolizers:{
    	          "Point": {
					pointRadius: 4, //6
					graphicName: "circle",
					fillColor: "blue", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "blue" //"#ee9900"
				  },
				  "Line": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "Polygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					fillColor: "blue", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  },
				  "MultiPoint": {
					pointRadius: 1, //6
					graphicName: "circle",
					fillColor: "blue", //"#ee9900"
					fillOpacity: 1, //0.4
					strokeWidth: 1,
					strokeOpacity: 1,
					strokeColor: "blue" //"#ee9900"
				  },
				  "MultiLine": {
					strokeWidth: 3, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					strokeDashstyle: "solide"
				  },
				  "MultiPolygon": {
					strokeWidth: 2, //1
					strokeOpacity: 1, //1
					strokeColor: "blue", //"#ee9900"
					fillColor: "blue", //"#ee9900"
					fillOpacity: 0.3 //0.4
				  }
    	        }
    	    }
    	);
    
    couchePointsKMLReproj = new Descartes.Layer.KML(
    	    'couche points (KML) - Reprojeté (EPSG:4326 en EPSG:2154)',
    	    [{
    	            serverUrl: './data/kml/points.kml'
    	        }],
    	    {
    	        visible: true,
    	        opacity: 100,
    	        keepInternalStyles:false
    	    }
    	);
    
	groupeFonds = {
			title: "Fonds cartographiques",
			options : {
				opened: true
			}
		};
}
