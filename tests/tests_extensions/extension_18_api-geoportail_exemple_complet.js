proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

var carte;

function chargementCarte() {
	
	
	chargeCouchesGroupes();
	
    var fondIGN = new Descartes.Layer.WMS(
            "couche de fond - IGN",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/r/wms?",
                    layerName: "GEOGRAPHICALGRIDSYSTEMS.PLANIGN",

                }
            ],
            {
    			opacity: 50,
    			opacityMax: 100,
            }
        );

	
	var contenuCarte = new Descartes.MapContent({editable: true});
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	
	coucheBase.options.opacity=50;
	coucheBase.options.opacityMax=50;
	coucheBase.title="couche de fond - GéoRef";
	var coucheFond = new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options);
	contenuCarte.addItem(coucheFond,gpFonds);
    contenuCarte.addItem(fondIGN);
	
	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = 'EPSG:2154';

	
    // Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					//minScale:2150000,
					autoSize: true,
					displayExtendedOLExtent: true
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});

	//Barre de recherche API GEOPORTAIL
	var search = new ol.control.SearchEngine({apiKey: "iqqdkoa235g0mf1gr3si0162"}) ;
	carte.OL_map.addControl(search);
	
	//Profil altimétrique le long d’un traçé API GEOPORTAIL
	var elevationPath = new ol.control.ElevationPath({apiKey: "iqqdkoa235g0mf1gr3si0162"});
	carte.OL_map.addControl(elevationPath);
	
	//Widget de gestion d'empilement des couches API GEOPORTAIL
	var layerSwitcher = new ol.control.LayerSwitcher({
		 layers: [
		     {
		         layer : coucheFond.OL_layers[0],
		         config : {
		             title : coucheFond.title,
		             description : "description couche de fond"
		         }
		     },
		     {
		         layer : fondIGN.OL_layers[0],
		         config : {
		             title : fondIGN.title,
		             description : "description couche de fond"
		         }
		     }
		 ]}
	);
	carte.OL_map.addControl(layerSwitcher);
	
	//Widget d’import de couches API GEOPORTAIL
	var layerImport = new ol.control.LayerImport({apiKey: "iqqdkoa235g0mf1gr3si0162"});
	carte.OL_map.addControl(layerImport);
	
	//Adresse ou lieu en un point de la carte API GEOPORTAIL	
	var reverse = new ol.control.ReverseGeocode({apiKey: "iqqdkoa235g0mf1gr3si0162"});
	carte.OL_map.addControl(reverse);
	
	//Calculs d'isochrones API GEOPORTAIL	
	var iso = new ol.control.Isocurve({apiKey: "iqqdkoa235g0mf1gr3si0162"});
	carte.OL_map.addControl(iso);
	
	//Calculs itinéraires API GEOPORTAIL	
	var route = new ol.control.Route({apiKey: "iqqdkoa235g0mf1gr3si0162"}) ;
	carte.OL_map.addControl(route);
	
	//Outils de croquis API GEOPORTAIL
	var drawing = new ol.control.Drawing({});
	carte.OL_map.addControl(drawing);
	
	//Mesurer un azimut API GEOPORTAIL
    var azi = new ol.control.MeasureAzimuth({});       
    carte.OL_map.addControl(azi);


}