proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

var carte;
var vectorLayer;

var monStylePoint = {
    'Point': {
        pointRadius: 4,
        graphicName: 'circle',
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#ee9900'
    }
};

var onSelect;

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

	var bounds = [-6.22, 39.46, 9.14, 53.4];
	var projection = 'EPSG:4326';
	var initbounds = [4.02, 42.60, 7.91, 45.37];
    // Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
					//minScale:2150000,
					size: [600, 400]
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	 
	vectorSource = new ol.source.Vector({});
	vectorLayer = new ol.layer.Vector({
        source: vectorSource
    });
	carte.OL_map.addLayer(vectorLayer);

	//OL-EXT searchBan par defaut
	var search = new ol.control.SearchBAN();
	carte.OL_map.addControl (search);
	search.on('select', function(e) {
		carte.OL_map.getView().animate(
			{	center:e.coordinate,
				resolution: Descartes.Utils.getResolutionForScale(2000,"degrees")
			});
	});
	
	//OL-EXT searchBan autre possibilité
	var search1 = new ol.control.SearchBAN(
			{	
				target: "SearchBan1",
				placeholder: "ex : 3 rue de Savoie 75006 PARIS",
				url: "https://api-adresse.data.gouv.fr/search/",
				position: true	// Search, with priority to geo position
			});
	carte.OL_map.addControl (search1);
	search1.on('select', onSelect);

}

function onSelect(e) {
	
    var element = document.createElement('div');
    element.setAttribute('id', 'popup');
    element.className = 'Descartes-popup';

    var text = e.search.properties.label;

    var content = document.createElement('div');
    content.setAttribute('id', 'popup-content');
    content.innerHTML = text;

    element.appendChild(content);

    var popup = new ol.Overlay(({
        //id: this.feature.get('id') + '_overlay',
        id: 'localisationoverlay',
        element: element,
        autoPan: true,
        position: e.coordinate
    }));

    carte.OL_map.addOverlay(popup);
	
	
	vectorLayer.getSource().clear();
	var olStyles = Descartes.Symbolizers.getOlStyle(monStylePoint);
	var feature = new ol.Feature(new ol.geom.Point(e.coordinate));
	feature.setStyle(olStyles['point']);
	vectorLayer.getSource().addFeature(feature);
	carte.OL_map.getView().animate(
		{	center:e.coordinate,
			//zoom:carte.OL_map.getView().getMaxZoom()-6
			resolution: Descartes.Utils.getResolutionForScale(2500,"degrees")
		});
}