proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {

	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?";

	var coucheAggregat = new Descartes.Layer.MyLayerWMS(
			"Couche aggregat : couches \"Stations\" puis \"Parkings\"  (scale limit: 150000)",
			[
				{
					serverUrl: serveur,
					layerName: "c_stations"
				},
				{
					serverUrl: serveur,
					layerName: "c_parkings"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	var contenuCarte = new Descartes.MapContent();
	contenuCarte.addItem(coucheAggregat);

	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	
    //Interactions openlayers
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	]);
}
