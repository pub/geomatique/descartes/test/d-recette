function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global pilot� par l'arbre des couches
	      save: function (json) {
		  	 //Ici, code MOE qui est spécifique à chaque application métier.
		 	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		    //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		 	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		 	 sendRequestBouchonForSaveElements(json);
		
		 }
	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsStyle3.title, couchePointsStyle3.definition, couchePointsStyle3.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesStyle3.title, coucheLignesStyle3.definition, coucheLignesStyle3.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle3.title, couchePolygonesStyle3.definition, couchePolygonesStyle3.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsStyle3.title, coucheMultiPointsStyle3.definition, coucheMultiPointsStyle3.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesStyle3.title, coucheMultiLignesStyle3.definition, coucheMultiLignesStyle3.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle3.title, coucheMultiPolygonesStyle3.definition, coucheMultiPolygonesStyle3.options);

    contenuCarte.addItem(editionLayer4, gpWFS);
    contenuCarte.addItem(editionLayer5, gpWFS);
    contenuCarte.addItem(editionLayer6, gpWFS);
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsStyle3.title, kmlCouchePointsStyle3.definition, kmlCouchePointsStyle3.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesStyle3.title, kmlCoucheLignesStyle3.definition, kmlCoucheLignesStyle3.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesStyle3.title, kmlCouchePolygonesStyle3.definition, kmlCouchePolygonesStyle3.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsStyle3.title, kmlCoucheMultiPointsStyle3.definition, kmlCoucheMultiPointsStyle3.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesStyle3.title, kmlCoucheMultiLignesStyle3.definition, kmlCoucheMultiLignesStyle3.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesStyle3.title, kmlCoucheMultiPolygonesStyle3.definition, kmlCoucheMultiPolygonesStyle3.options);

    contenuCarte.addItem(editionLayerkml4, gpKML);
    contenuCarte.addItem(editionLayerkml5, gpKML);
    contenuCarte.addItem(editionLayerkml6, gpKML);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsStyle3.title, geojsonCouchePointsStyle3.definition, geojsonCouchePointsStyle3.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesStyle3.title, geojsonCoucheLignesStyle3.definition, geojsonCoucheLignesStyle3.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesStyle3.title, geojsonCouchePolygonesStyle3.definition, geojsonCouchePolygonesStyle3.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsStyle3.title, geojsonCoucheMultiPointsStyle3.definition, geojsonCoucheMultiPointsStyle3.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesStyle3.title, geojsonCoucheMultiLignesStyle3.definition, geojsonCoucheMultiLignesStyle3.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesStyle3.title, geojsonCoucheMultiPolygonesStyle3.definition, geojsonCoucheMultiPolygonesStyle3.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSON);
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
