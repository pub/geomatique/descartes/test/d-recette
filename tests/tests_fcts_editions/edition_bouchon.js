/***********************************
 * 
 * 			CODE BOUCHON
 * 
 ***********************************/

var urlServletBouchon = Descartes.getWebServiceRoot()+"edition";

var urlServletBouchonKml = Descartes.getWebServiceRoot()+"editionKML";

var urlServletBouchonGeoJSON = Descartes.getWebServiceRoot()+"editionGeoJSON";

function sendRequestBouchonForSaveElements(json) {
	
	if (json.fluxWfst) {
		var elementsForSave = JSON.stringify(json.fluxWfst);
		
		var xhr = new XMLHttpRequest();
	    xhr.open('POST', urlServletBouchon);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function () {
	        if (xhr.status === 200) {
	        	var data = xhr.responseXML;
	       	  	if(!data || !data.documentElement) {
	       	  		data = xhr.responseText;
	       	  	}
	              
	       	  	try{
	       	  		data = JSON.parse(data);
		      	  	json.priv={status:data.status,message:data.message};  
	       	  	}catch (e){
	       	  		json.priv={status:500};
	       	  	}
	       	  
	            json.callback.call(json);
	        } else {
	        	json.priv={status:500,message:xhr.responseText};
	        	json.callback.call(json);
	        }
	    };
	    xhr.send(elementsForSave);
		
	} else if (json.fluxKml){
		
		var elementsForSave = json.fluxKml; 
		delete elementsForSave.format;
		var urlwithproxy = decodeURIComponent(elementsForSave.url).split(Descartes.PROXY_SERVER);
	 	var url= urlwithproxy[urlwithproxy.length - 1];
		var splitted = url.split("/");
	 	var fileName= splitted[splitted.length - 1];
		elementsForSave.url = fileName;
		elementsForSave=JSON.stringify(elementsForSave);
		
		
	
		var xhr = new XMLHttpRequest();
	    xhr.open('POST', urlServletBouchonKml);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function () {
	        if (xhr.status === 200) {
	        	var data = xhr.responseXML;
	       	  	if(!data || !data.documentElement) {
	       	  		data = xhr.responseText;
	       	  	}
	              
	       	  	try{
	       	  		data = JSON.parse(data);
		      	  	json.priv={status:data.status,message:data.message};  
	       	  	}catch (e){
	       	  		json.priv={status:500};
	       	  	}
	       	  
	            json.callback.call(json);
	        } else {
	        	json.priv={status:500,message:xhr.responseText};
	        	json.callback.call(json);
	        }
	    };
	    xhr.send(elementsForSave);
	     
	}  else if (json.fluxGeoJSON){
		
		var elementsForSave = json.fluxGeoJSON; 
		delete elementsForSave.format;
		var urlwithproxy = decodeURIComponent(elementsForSave.url).split(Descartes.PROXY_SERVER);
	 	var url= urlwithproxy[urlwithproxy.length - 1];
		var splitted = url.split("/");
	 	var fileName= splitted[splitted.length - 1];
		elementsForSave.url = fileName;
		elementsForSave=JSON.stringify(elementsForSave);
		
		var xhr = new XMLHttpRequest();
	    xhr.open('POST', urlServletBouchonGeoJSON);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function () {
	        if (xhr.status === 200) {
	        	var data = xhr.responseXML;
	       	  	if(!data || !data.documentElement) {
	       	  		data = xhr.responseText;
	       	  	}
	              
	       	  	try{
	       	  		data = JSON.parse(data);
		      	  	json.priv={status:data.status,message:data.message};  
	       	  	}catch (e){
	       	  		json.priv={status:500};
	       	  	}
	       	  
	            json.callback.call(json);
	        } else {
	        	json.priv={status:500,message:xhr.responseText};
	        	json.callback.call(json);
	        }
	    };
	    xhr.send(elementsForSave);
		
	}
	 
}

