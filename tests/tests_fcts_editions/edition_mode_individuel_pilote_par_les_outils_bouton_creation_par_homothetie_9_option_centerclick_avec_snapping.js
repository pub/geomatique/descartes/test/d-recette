function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: false, //mode individuel piloté par les outils
        autoSave:true,  //sauvegarde automatique
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));

    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsFctAvanced3.title, couchePointsFctAvanced3.definition, couchePointsFctAvanced3.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesFctAvanced3.title, coucheLignesFctAvanced3.definition, coucheLignesFctAvanced3.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesFctAvanced3.title, couchePolygonesFctAvanced3.definition, couchePolygonesFctAvanced3.options);
    var editionLayer3bis = new Descartes.Layer.EditionLayer.WFS(coucheClonePolygones.title, coucheClonePolygones.definition, coucheClonePolygones.options);
    editionLayer3bis.title="Ma couche WFS de polygones pour homothétie";
    editionLayer3bis.snapping = {
    	snappingLayersIdentifier:["couchePoints"],
        tolerance: 10
    };
    
    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
    contenuCarte.addItem(editionLayer3bis, gpWFS);

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsFctAvanced3.title, coucheMultiPointsFctAvanced3.definition, coucheMultiPointsFctAvanced3.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesFctAvanced3.title, coucheMultiLignesFctAvanced3.definition, coucheMultiLignesFctAvanced3.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesFctAvanced3.title, coucheMultiPolygonesFctAvanced3.definition, coucheMultiPolygonesFctAvanced3.options);
    var editionLayer6bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones.title, coucheCloneMultiPolygones.definition, coucheCloneMultiPolygones.options);
    editionLayer6bis.title="Ma couche WFS de multi-polygones pour homothétie";
    editionLayer6bis.snapping = {
    	snappingLayersIdentifier:["coucheMultiPoints"],
        tolerance: 10
    };
    
    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    contenuCarte.addItem(editionLayer6bis, gpWFSMulti);
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));

    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsFctAvanced3.title, kmlCouchePointsFctAvanced3.definition, kmlCouchePointsFctAvanced3.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesFctAvanced3.title, kmlCoucheLignesFctAvanced3.definition, kmlCoucheLignesFctAvanced3.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesFctAvanced3.title, kmlCouchePolygonesFctAvanced3.definition, kmlCouchePolygonesFctAvanced3.options);
    var editionLayerkml3bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePolygones.title, kmlCoucheClonePolygones.definition, kmlCoucheClonePolygones.options);
    editionLayerkml3bis.title="Ma couche KML de polygones pour homothétie";
    editionLayerkml3bis.snapping = {
    	snappingLayersIdentifier:["kmlCouchePoints"],
        tolerance: 10
    };
    
    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    contenuCarte.addItem(editionLayerkml3bis, gpKML);

    var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	
    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsFctAvanced3.title, kmlCoucheMultiPointsFctAvanced3.definition, kmlCoucheMultiPointsFctAvanced3.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesFctAvanced3.title, kmlCoucheMultiLignesFctAvanced3.definition, kmlCoucheMultiLignesFctAvanced3.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesFctAvanced3.title, kmlCoucheMultiPolygonesFctAvanced3.definition, kmlCoucheMultiPolygonesFctAvanced3.options);
    var editionLayerkml6bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPolygones.title, kmlCoucheCloneMultiPolygones.definition, kmlCoucheCloneMultiPolygones.options);
    editionLayerkml6bis.title="Ma couche KML de multi-polygones pour homothétie";
    editionLayerkml6bis.snapping = {
    	snappingLayersIdentifier:["kmlCoucheMultiPoints"],
        tolerance: 10
    };
    
    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    contenuCarte.addItem(editionLayerkml6bis, gpKMLMulti);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));

	var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsFctAvanced3.title, geojsonCouchePointsFctAvanced3.definition, geojsonCouchePointsFctAvanced3.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesFctAvanced3.title, geojsonCoucheLignesFctAvanced3.definition, geojsonCoucheLignesFctAvanced3.options);	
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesFctAvanced3.title, geojsonCouchePolygonesFctAvanced3.definition, geojsonCouchePolygonesFctAvanced3.options);
    var editionLayergeojson3bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePolygones.title, geojsonCoucheClonePolygones.definition, geojsonCoucheClonePolygones.options);
    editionLayergeojson3bis.title="Ma couche GEOJSON de polygones pour homothétie";
    editionLayergeojson3bis.snapping = {
    	snappingLayersIdentifier:["geojsonCouchePoints"],
        tolerance: 10
    };
    
    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3bis, gpGEOJSON);

    var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsFctAvanced3.title, geojsonCoucheMultiPointsFctAvanced3.definition, geojsonCoucheMultiPointsFctAvanced3.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesFctAvanced3.title, geojsonCoucheMultiLignesFctAvanced3.definition, geojsonCoucheMultiLignesFctAvanced3.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced3.title, geojsonCoucheMultiPolygonesFctAvanced3.definition, geojsonCoucheMultiPolygonesFctAvanced3.options);
    var editionLayergeojson6bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPolygones.title, geojsonCoucheCloneMultiPolygones.definition, geojsonCoucheCloneMultiPolygones.options);
    editionLayergeojson6bis.title="Ma couche GEOJSON de multi-polygones pour homothétie";
    editionLayergeojson6bis.snapping = {
    	snappingLayersIdentifier:["geojsonCoucheMultiPoints"],
        tolerance: 10
    };
    
    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    contenuCarte.addItem(editionLayergeojson6bis, gpGEOJSONMulti);
      
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION,
	      		args:{
	      			centerClick:true,
	      			editionLayer: editionLayer3bis,
	      			snapping:true
	      		}
	      },
	      {type: Descartes.Map.EDITION_RUBBER_DELETION,
	      		args:{
	      			editionLayer: editionLayer3bis
	      		}
	      },
	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION,
	      		args:{
	      			centerClick:true,
	      			editionLayer: editionLayer6bis,
	      			snapping:true
	      		}
	      },
	      {type: Descartes.Map.EDITION_RUBBER_DELETION,
	      		args:{
	      			editionLayer: editionLayer6bis
	      		}
	      },
	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout de plusieurs zones informatives
	var infos = [
	             {type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'}
	];

	// Ajout de la zone informative
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
