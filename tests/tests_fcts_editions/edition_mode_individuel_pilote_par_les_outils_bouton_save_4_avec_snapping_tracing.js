function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: false, //mode individuel piloté par les outils
		 save: function (json) {
		    	 //Ici, code MOE qui est spécifique à chaque application métier.
		   	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		        //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		   	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		   	 sendRequestBouchonForSaveElements(json);
		
		 }
	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsSnapping.title, couchePointsSnapping.definition, couchePointsSnapping.options);
    coucheLignesSnapping.options.snapping.autotracing = true;
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesSnapping.title, coucheLignesSnapping.definition, coucheLignesSnapping.options);
    couchePolygonesSnapping.options.snapping.autotracing = true;
    var layer3 = new Descartes.Layer.WFS(couchePolygonesSnapping.title, couchePolygonesSnapping.definition, couchePolygonesSnapping.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(layer3, gpWFS);

	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsSnapping.title, kmlCouchePointsSnapping.definition, kmlCouchePointsSnapping.options);
    kmlCoucheLignesSnapping.options.snapping.autotracing = true;
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesSnapping.title, kmlCoucheLignesSnapping.definition, kmlCoucheLignesSnapping.options);
    kmlCouchePolygonesSnapping.options.snapping.autotracing = false;
    var layerkml3 = new Descartes.Layer.KML(kmlCouchePolygonesSnapping.title, kmlCouchePolygonesSnapping.definition, kmlCouchePolygonesSnapping.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(layerkml3, gpKML);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsSnapping.title, geojsonCouchePointsSnapping.definition, geojsonCouchePointsSnapping.options);
    geojsonCoucheLignesSnapping.options.snapping.enable = false;
    geojsonCoucheLignesSnapping.options.snapping.autotracing = true;
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesSnapping.title, geojsonCoucheLignesSnapping.definition, geojsonCoucheLignesSnapping.options);
    geojsonCouchePolygonesSnapping.options.snapping.autotracing = true;
    var layergeojson3 = new Descartes.Layer.GeoJSON(geojsonCouchePolygonesSnapping.title, geojsonCouchePolygonesSnapping.definition, geojsonCouchePolygonesSnapping.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(layergeojson3, gpGEOJSON);
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_DRAW_CREATION,
	       args: {
	    	      editionLayer: editionLayer1,
                  snapping: true,
                  autotracing: true
                 }
	      },
	      {type: Descartes.Map.EDITION_DRAW_CREATION,
		      args: {
		   	      editionLayer: editionLayer2,
	                  snapping: true,
	                  autotracing: true
	                 }
		 },
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION,
		      args: {
		   	   editionLayer: editionLayer2,
		   	   snapping: true
	                 }
		 },
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION,
			 args: {
				 //snapping: true
				 editionLayer: editionLayer2
              }
	      },
	      {type: Descartes.Map.EDITION_ATTRIBUTE,
		      args: {
		   	   editionLayer: editionLayer2
	           }
	      },
	      {type: Descartes.Map.EDITION_RUBBER_DELETION,
		      args: {
		   	   editionLayer: editionLayer2
	           }
	      },
	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
