function chargementCarte() {
	
	chargeEditionCouchesGroupesKML();
		
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
/*	var kmlCouchePoints = {
		    title: "Ma couche KML de points",
		    type: 11,
		    definition: [
		                 {	            	
		                	 //serverUrl: "http://localhost:xxxx/xxxx/datas/kml/points.kml"
		                	 serverUrl: "datas/kml/points.kml"
		        }
		    ],
		    options: {
		    	keepInternalStyles:false,
		        alwaysVisible: false,
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        format: "image/png",
		        displayOrder: 1,
		        geometryType: Descartes.Layer.POINT_GEOMETRY
		    }
		};
*/	
	var coucheBase = {
		title : "Fond de carte",
		type: 0,
		definition: [
			{
				serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur"
			}
		],
		options: {
			maxScale: 100,
			minScale: 10000001,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 50,
			opacityMax: 100,
			legend: [],
			metadataURL: null,
			format: "image/png"
		}
	};

	var groupeFonds = {
	    title: "Fonds cartographiques",
	    options: {
	        opened: true
	    }
	};
	
    var layerkml1 = new Descartes.Layer.KML(kmlCouchePoints.title, kmlCouchePoints.definition, kmlCouchePoints.options);
    var layerkml2 = new Descartes.Layer.KML(kmlCoucheLignes.title, kmlCoucheLignes.definition, kmlCoucheLignes.options);
    var layerkml3 = new Descartes.Layer.KML(kmlCouchePolygones.title, kmlCouchePolygones.definition, kmlCouchePolygones.options);
    var layerkml4 = new Descartes.Layer.KML(kmlCoucheMultiPoints.title, kmlCoucheMultiPoints.definition, kmlCoucheMultiPoints.options);
    var layerkml5 = new Descartes.Layer.KML(kmlCoucheMultiLignes.title, kmlCoucheMultiLignes.definition, kmlCoucheMultiLignes.options);
    var layerkml6 = new Descartes.Layer.KML(kmlCoucheMultiPolygones.title, kmlCoucheMultiPolygones.definition, kmlCoucheMultiPolygones.options);
    
    contenuCarte.addItem(layerkml1);
    contenuCarte.addItem(layerkml2);
    contenuCarte.addItem(layerkml3);
    contenuCarte.addItem(layerkml4);
    contenuCarte.addItem(layerkml5);
    contenuCarte.addItem(layerkml6);
   
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
