function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'�dition
	 Descartes.EditionManager.configure({
        globalEditionMode: false, //mode individuel pilot� par les outils
	    save: function (json) {
		    	 //Ici, code MOE qui est spécifique à chaque application métier.
		   	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		        //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		   	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		   	 sendRequestBouchonForSaveElements(json);
		
		}
    });     

	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
    
    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
    
    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPoints.title, coucheMultiPoints.definition, coucheMultiPoints.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignes.title, coucheMultiLignes.definition, coucheMultiLignes.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygones.title, coucheMultiPolygones.definition, coucheMultiPolygones.options);

    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    
	var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
    
    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPoints.title, kmlCoucheMultiPoints.definition, kmlCoucheMultiPoints.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignes.title, kmlCoucheMultiLignes.definition, kmlCoucheMultiLignes.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygones.title, kmlCoucheMultiPolygones.definition, kmlCoucheMultiPolygones.options);

    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    
	var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
    
    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPoints.title, geojsonCoucheMultiPoints.definition, geojsonCoucheMultiPoints.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignes.title, geojsonCoucheMultiLignes.definition, geojsonCoucheMultiLignes.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygones.title, geojsonCoucheMultiPolygones.definition, geojsonCoucheMultiPolygones.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);


    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
    

	
	                 	 
	                 	 

	
	
	var defaultSize = 3;
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					//
					units: "degree",
					projection: projection,
					displayExtendedOLExtent: true,
					initExtent: bounds,
					maxExtent: bounds,
					maxScale: 100,
					size: [750, 500]
				}
			);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);	 
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {
			type: Descartes.Map.EDITION_SELECTION,
			args: {
			    editionLayer: editionLayer4
			}
          },{
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  editionLayer: editionLayer4
              }
	      },{
	    	  type: Descartes.Map.EDITION_ATTRIBUTE,
	    	  args: {
	    		 editionLayer: editionLayer4
	    	  }
	      },{
	    	  type: Descartes.Map.EDITION_SELECTION_ATTRIBUT,
	    	  args: {
	    		 editionLayer: editionLayer4
	    	  }
	      },{
			type: Descartes.Map.EDITION_SELECTION,
			args: {
			    editionLayer: editionLayer5
			}
          },{
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  editionLayer: editionLayer5
              }
	      },{
	    	  type: Descartes.Map.EDITION_ATTRIBUTE,
	    	  args: {
	    		 editionLayer: editionLayer5
	    	  }
	      },{
	    	  type: Descartes.Map.EDITION_SELECTION_ATTRIBUT,
	    	  args: {
	    		 editionLayer: editionLayer5
	    	  }
	      },{
			type: Descartes.Map.EDITION_SELECTION,
			args: {
			    editionLayer: editionLayer6
			}
          },{
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  editionLayer: editionLayer6
              }
	      },{
	    	  type: Descartes.Map.EDITION_ATTRIBUTE,
	    	  args: {
	    		 editionLayer: editionLayer6
	    	  }
	      },{
	    	  type: Descartes.Map.EDITION_SELECTION_ATTRIBUT,
	    	  args: {
	    		 editionLayer: editionLayer6
	    	  }
	      },
	  ]);
	  
	 
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([

		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
