var carte;
function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.KML(kmlCouchePointsFctAvanced.title, kmlCouchePointsFctAvanced.definition, kmlCouchePointsFctAvanced.options);
    var editionLayerkml2 = new Descartes.Layer.KML(kmlCoucheLignesFctAvanced.title, kmlCoucheLignesFctAvanced.definition, kmlCoucheLignesFctAvanced.options);
    var editionLayerkml2bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneLignes.title, kmlCoucheCloneLignes.definition, kmlCoucheCloneLignes.options);
    editionLayerkml2bis.title="Ma couche KML de lignes pour l'agrégation";
    var editionLayerkml3 = new Descartes.Layer.KML(kmlCouchePolygonesFctAvanced.title, kmlCouchePolygonesFctAvanced.definition, kmlCouchePolygonesFctAvanced.options);
    var editionLayerkml3bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePolygones.title, kmlCoucheClonePolygones.definition, kmlCoucheClonePolygones.options);
    editionLayerkml3bis.title="Ma couche KML de polygones pour l'agrégation";
    
    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml2bis, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    contenuCarte.addItem(editionLayerkml3bis, gpKML);

    var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	
    var editionLayerkml4 = new Descartes.Layer.KML(kmlCoucheMultiPointsFctAvanced.title, kmlCoucheMultiPointsFctAvanced.definition, kmlCoucheMultiPointsFctAvanced.options);
    var editionLayerkml4bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPoints.title, kmlCoucheCloneMultiPoints.definition, kmlCoucheCloneMultiPoints.options);
    editionLayerkml4bis.title="Ma couche KML de multi-points pour l'agrégation";
    var editionLayerkml5 = new Descartes.Layer.KML(kmlCoucheMultiLignesFctAvanced.title, kmlCoucheMultiLignesFctAvanced.definition, kmlCoucheMultiLignesFctAvanced.options);
    var editionLayerkml5bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiLignes.title, kmlCoucheCloneMultiLignes.definition, kmlCoucheCloneMultiLignes.options);
    editionLayerkml5bis.title="Ma couche KML de multi-lignes pour l'agrégation";
    var editionLayerkml6 = new Descartes.Layer.KML(kmlCoucheMultiPolygonesFctAvanced.title, kmlCoucheMultiPolygonesFctAvanced.definition, kmlCoucheMultiPolygonesFctAvanced.options);
    var editionLayerkml6bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPolygones.title, kmlCoucheCloneMultiPolygones.definition, kmlCoucheCloneMultiPolygones.options);
    editionLayerkml6bis.title="Ma couche KML de multi-polygones pour l'agrégation";
   
    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml4bis, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5bis, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    contenuCarte.addItem(editionLayerkml6bis, gpKMLMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_AGGREGATION}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
