proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

var carte;
function chargementCarte() {
	
	
	
	chargeCouchesGroupes();
	chargeEditionCouchesGroupesWFS();
		
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global piloté par l'arbre des couches
	      save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	      }
	});   
	
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePoints.title, couchePoints.definition, couchePoints.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignes.title, coucheLignes.definition, coucheLignes.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygones.title, couchePolygones.definition, couchePolygones.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPoints.title, coucheMultiPoints.definition, coucheMultiPoints.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignes.title, coucheMultiLignes.definition, coucheMultiLignes.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygones.title, coucheMultiPolygones.definition, coucheMultiPolygones.options);

    contenuCarte.addItem(editionLayer4, gpWFS);
    contenuCarte.addItem(editionLayer5, gpWFS);
    contenuCarte.addItem(editionLayer6, gpWFS);

	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
	
	// Construction de la carte
	//var carte = new Descartes.Map.ContinuousScalesMap(
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					
					projection: "EPSG:2154",
					initExtent: bounds,
					maxExtent: bounds,
					maxScale: 100,
					size: [600,400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolsInNamedToolBar(toolsBar,[
	                        {type : Descartes.Map.MAXIMAL_EXTENT},
	                        {type : Descartes.Map.INITIAL_EXTENT, args:bounds},
	                        {type : Descartes.Map.DRAG_PAN},
							{type : Descartes.Map.ZOOM_IN},
							{type : Descartes.Map.ZOOM_OUT},
							{type : Descartes.Map.NAV_HISTORY}
	]);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar(null, 
	//  carte.addEditionToolBar("editionToolBar", 
		[ {type: Descartes.Map.EDITION_SELECTION},
	      {type: Descartes.Map.EDITION_DRAW_CREATION,
		      args: {
	                  snapping: true
	                 }
	      },
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION,
		      args: {
	                  snapping: true
	                 }
	      },
	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_SAVE}
	    ],
	    {toolBar: toolsBar,// imbrication dans la barre principale
	     openerTool: true} 
	  );
	  
	  carte.addToolsInNamedToolBar(toolsBar,[
			{type : Descartes.Map.POINT_SELECTION, args:{resultUiParams:{withCsvExport: true}}},
			{type : Descartes.Map.POINT_RADIUS_SELECTION, args:{resultUiParams:{withCsvExport: true, withReturn: true}}},
			{type : Descartes.Map.RECTANGLE_SELECTION, args:{resultUiParams:{withCsvExport: true, withReturn: true}}},
			{type : Descartes.Map.COORDS_CENTER},
			{type : Descartes.Map.CENTER_MAP},
			{type : Descartes.Map.DISTANCE_MEASURE},
			{type : Descartes.Map.AREA_MEASURE},
			{type : Descartes.Map.PDF_EXPORT},
			{type : Descartes.Map.PNG_EXPORT}
 	]);  

	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	var contentTools = [/*{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL, options:null}*/
						];
	
	carte.addContentManager('layersTree', contentTools, managerOptions);
	
	// Affichage de la carte
	carte.show();

	// Objets de référence administratifs embarqués
/*	var gazetteerLevels = [];
	var niveauChoixRegion = new Descartes.GazetteerLevel("Choisissez une région","Aucune région","reg");
	var niveauChoixDpt = new Descartes.GazetteerLevel("Choisissez un département","Aucun département","dept");
	var niveauChoixCom = new Descartes.GazetteerLevel("Choisissez une commune","Aucune commune","com");
	
	gazetteerLevels.push(niveauChoixRegion);
	gazetteerLevels.push(niveauChoixDpt);
	gazetteerLevels.push(niveauChoixCom);

//	carte.addGazetteer('Gazetteer', "", gazetteerLevels, {location : "../../gazetteer/"});
	carte.addGazetteer('Gazetteer', "", gazetteerLevels);
*/	
	// Ajout des actions
	// Affectation des styles personnalisées pour l'action de type Descartes.Map.SCALE_CHOOSER_ACTION
/*	var actions = [{type : Descartes.Map.SCALE_CHOOSER_ACTION,  div : 'ScaleChooser'},
	               {type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'},
	               {type : Descartes.Map.COORDS_INPUT_ACTION,   div : 'CoordinatesInput'},
	               {type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'},
	               {type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'}
	              ];
	
	carte.addActions(actions);
*/	
	// Ajout de plusieurs zones informatives
/*	var infos = [{type : Descartes.Map.GRAPHIC_SCALE_INFO, div : 'GraphicScale'},
	             {type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'},
	             {type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'},
	             {type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'},
	             {type : Descartes.Map.LEGEND_INFO, div : 'Legend'},
	             {type : Descartes.Map.ATTRIBUTION_INFO, div:'Attribution'}
	];

	// Ajout de la zone informative
	carte.addInfos(infos);
	
	// Ajout du gestionnaire de contexte
	carte.addBookmarksManager('Bookmarks', 'descartes');
	

	//DIRECTIONPANPANEL
	carte.addDirectionalPanPanel();
	
	//MINIMAP
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");
*/
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette
	]);
	
}
