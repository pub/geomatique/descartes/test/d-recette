var carte;
function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	     globalEditionMode: true, //mode global pilot� par l'arbre des couches
	     save: function (json) {
		  	 //Ici, code MOE qui est spécifique à chaque application métier.
		 	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		    //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		 	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		  	 sendRequestBouchonForSaveElements(json);
		
		 }
	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePoints.title, couchePoints.definition, couchePoints.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignes.title, coucheLignes.definition, coucheLignes.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygones.title, couchePolygones.definition, couchePolygones.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
  
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePoints.title, kmlCouchePoints.definition, kmlCouchePoints.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignes.title, kmlCoucheLignes.definition, kmlCoucheLignes.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygones.title, kmlCouchePolygones.definition, kmlCouchePolygones.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePoints.title, geojsonCouchePoints.definition, geojsonCouchePoints.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignes.title, geojsonCoucheLignes.definition, geojsonCoucheLignes.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygones.title, geojsonCouchePolygones.definition, geojsonCouchePolygones.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
    
    //var projection = "EPSG:2154";
    //var bounds = [799205.2,6215857.5,1078390.1,6452614.0];
    

	
	                 	 
	                 	 

	


	
	// Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			//
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_DRAW_CREATION},
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_ATTRIBUTE},
	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
