var geojsonCouchePoints, geojsonCoucheLignes, geojsonCouchePolygones, geojsonCoucheBase, groupeFonds, groupeEditionGEOJSON;
var geojsonCoucheClonePoints, geojsonCoucheCloneLignes, geojsonCoucheClonePolygones;
var geojsonCoucheMultiPoints, geojsonCoucheMultiLignes, geojsonCoucheMultiPolygones, groupeEditionGEOJSONMulti;
var geojsonCoucheCloneMultiPoints, geojsonCoucheCloneMultiLignes, geojsonCoucheCloneMultiPolygones;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeEditionCouchesGroupesGEOJSON() {
	
	var serveurRoot = "https://preprod.descartes.din.developpement-durable.gouv.fr";
	var attribution = "&#169;Serveur Descartes de tests";
	
	// couche de type "point"
	var fichierGeojsonPoints = serveurRoot + "/datas/geojson/points.json";
	var geometryTypeLayerPoints =  Descartes.Layer.POINT_GEOMETRY;
	
	// couche de type "ligne"
	var fichierGeojsonLines = serveurRoot + "/datas/geojson/lines.json";
	var geometryTypeLayerLignes =  Descartes.Layer.LINE_GEOMETRY;
	
	// couche de type "polygone"
	var fichierGeojsonPolygons = serveurRoot + "/datas/geojson/polygons.json";
	var geometryTypeLayerPolygones =  Descartes.Layer.POLYGON_GEOMETRY;
	
	// couche de type "multipoint"
	var fichierGeojsonMultiPoints = serveurRoot + "/datas/geojson/multipoints.json";
	var geometryTypeLayerMultiPoints =  Descartes.Layer.MULTI_POINT_GEOMETRY;
	
	// couche de type "multiligne"
	var fichierGeojsonMultiLines = serveurRoot + "/datas/geojson/multilines.json";
	var geometryTypeLayerMultiLignes =  Descartes.Layer.MULTI_LINE_GEOMETRY;
	
	// couche de type "multipolygone"
	var fichierGeojsonMultiPolygons = serveurRoot + "/datas/geojson/multipolygons.json";
	var geometryTypeLayerMultiPolygones =  Descartes.Layer.MULTI_POLYGON_GEOMETRY;
	
	// Pour clonage
	// couche de type "point"
	var fichierGeojsonClonePoints = serveurRoot + "/datas/geojson/clonepoints.json";
	var geometryTypeLayerClonePoints =  Descartes.Layer.POINT_GEOMETRY;
	
	// couche de type "ligne"
	var fichierGeojsonCloneLines = serveurRoot + "/datas/geojson/clonelines.json";
	var geometryTypeLayerCloneLignes =  Descartes.Layer.LINE_GEOMETRY;
	
	// couche de type "polygone"
	var fichierGeojsonClonePolygons = serveurRoot + "/datas/geojson/clonepolygons.json";
	var geometryTypeLayerClonePolygones =  Descartes.Layer.POLYGON_GEOMETRY;
	
	// couche de type "multipoint"
	var fichierGeojsonCloneMultiPoints = serveurRoot + "/datas/geojson/clonemultipoints.json";
	var geometryTypeLayerCloneMultiPoints =  Descartes.Layer.MULTI_POINT_GEOMETRY;
	
	// couche de type "multiligne"
	var fichierGeojsonCloneMultiLines = serveurRoot + "/datas/geojson/clonemultilines.json";
	var geometryTypeLayerCloneMultiLignes =  Descartes.Layer.MULTI_LINE_GEOMETRY;
	
	// couche de type "multipolygone"
	var fichierGeojsonCloneMultiPolygons = serveurRoot + "/datas/geojson/clonemultipolygons.json";
	var geometryTypeLayerCloneMultiPolygones =  Descartes.Layer.MULTI_POLYGON_GEOMETRY;
	
	/*****************************************************
	  couches pour affichage simple
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
		
	geojsonCouchePoints = {
	    title: "Ma couche GEOJSON de points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	geojsonCouchePoints2 = {
	    title: "Ma couche GEOJSON de points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 10000,
	        minEditionScale: 4000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	geojsonCouchePoints3 = {
	    title: "Ma couche GEOJSON de points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonPoints
	                 }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 50000, 
	        minScale: 2000000, 
	        maxEditionScale: 50000,
	        minEditionScale: 2000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};

	geojsonCoucheLignes = {
	    title: "Ma couche GEOJSON de lignes",
	    type: 12,
	    definition: [
	        {
           	 serverUrl: fichierGeojsonLines
	        }
	    ],
	    options: {
	    	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	geojsonCouchePolygones = {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		       }
		   ],
		   options: {
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
		
	geojsonCoucheMultiPoints = {
	    title: "Ma couche GEOJSON de  multi points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonMultiPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};
	
	geojsonCoucheMultiPoints2 = {
	    title: "Ma couche GEOJSON de multi points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonMultiPoints
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 10000,
	        minEditionScale: 4000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};
	
	geojsonCoucheMultiPoints3 = {
	    title: "Ma couche GEOJSON de multi points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonMultiPoints
	                 }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 50000, 
	        minScale: 2000000, 
	        maxEditionScale: 50000,
	        minEditionScale: 2000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};

	geojsonCoucheMultiLignes = {
	    title: "Ma couche GEOJSON de multi lignes",
	    type: 12,
	    definition: [
	        {
           	 		serverUrl: fichierGeojsonMultiLines
	        }
	    ],
	    options: {
	    	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	geojsonCoucheMultiPolygones = {
	    title: "Ma couche GEOJSON de multi polygones",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonMultiPolygons
	        }
	    ],
	    options: {
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};
	
	/*****************************************************
	  couches pour la modification des styles d'affichage
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	geojsonCouchePointsStyle = {
	    title: "Ma couche GEOJSON de points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonPoints
	                 }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
					           "Point": {
					               fillColor: "blue",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "blue",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
				           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	geojsonCoucheLignesStyle = {
	    title: "Ma couche GEOJSON de lignes",
	    type: 12,
	    definition: [
	        {
           	 serverUrl: fichierGeojsonLines
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
	        	"default":   {//pour affichage
					           "Line": {
					               fillColor: "red",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "red",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	geojsonCouchePolygonesStyle = {
	    title: "Ma couche GEOJSON de polygones",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonPolygons
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
	            		      "Polygon": {
					               fillColor: "yellow",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "yellow",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPolygones
	    }
	};	
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	geojsonCoucheMultiPointsStyle = {
	    title: "Ma couche GEOJSON de multi points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonMultiPoints
	                 }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
					           "MultiPoint": {
					               fillColor: "orange",
					               graphicName:"star",
					               points: 5,
					               radius: 8,
					               radius2: 4,
					               angle: 0,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "orange",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 8
				           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};
	
	geojsonCoucheMultiLignesStyle = {
	    title: "Ma couche GEOJSON de multi lignes",
	    type: 12,
	    definition: [
	        {
           	 serverUrl: fichierGeojsonMultiLines
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
	        	"default":   {//pour affichage
					           "MultiLine": {
					               fillColor: "purple",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "purple",
					               strokeOpacity: 1,
					               strokeWidth: 6,
					               strokeLinecap: "round",
					               strokeDashstyle: "sold"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	geojsonCoucheMultiPolygonesStyle = {
	    title: "Ma couche GEOJSON de multi polygones",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonMultiPolygons
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
	            		      "MultiPolygon": {
					               fillColor: "pink",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "pink",
					               strokeOpacity: 1,
					               strokeWidth: 8,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};	
	
	/***************************************************
	  couches pour la modification des styles d'édition
	 ***************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	geojsonCouchePointsStyle2 = {
	    title: "Ma couche GEOJSON de points",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonPoints
	                 }
	    ],
	    options: {
	        symbolizers: {
				"temporary":   {//pour DrawCreation
					           "Point": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           }
				}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	geojsonCoucheLignesStyle2 = {
	    title: "Ma couche GEOJSON de lignes",
	    type: 12,
	    definition: [
	        {
           	 serverUrl: fichierGeojsonLines
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
		       "temporary":   {//pour DrawCreation
					           "Line": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	geojsonCouchePolygonesStyle2 = {
	    title: "Ma couche GEOJSON de polygones",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonPolygons
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"temporary":   {//pour DrawCreation
					           "Polygon": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
				}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPolygones
	    }
	};	
	
	geojsonCouchePointsStyle3 = {
		   title: "Ma couche GEOJSON de points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		                }
		   ],
		   options: {
		       symbolizers: {
					"temporary":   {//pour DrawCreation
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
					},
					"select":   { //pour GlobalModication
						           "Point": {
						               fillColor: "green",
						               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
					}
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
		
		geojsonCoucheLignesStyle3 = {
		   title: "Ma couche GEOJSON de lignes",
		   type: 12,
		   definition: [
		       {
               	 serverUrl: fichierGeojsonLines
				}
		   ],
		   options: {
		   	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		           ]
		       },
		       symbolizers: {
			       "temporary":   {//pour DrawCreation
						           "Line": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
			       },
				   "select":   {//pour GlobalModication
						           "Line": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
				   },
				   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:4,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                points:4,
							            radius:4,
							            radius2:0,
							            angle:0,
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
				   }
		       },    
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
		
		geojsonCouchePolygonesStyle3 = {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		               	 
		       }
		   ],
		   options: {
		       symbolizers: {
		       	"temporary":   {//pour DrawCreation
						           "Polygon": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					},
					"select":   {//pour GlobalModication
				    		      "Polygon": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					},
				   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 6
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
							            points:4,
							            radius:6,
							            radius2:0,
							            angle:0,
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
				   }
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};	
		geojsonCouchePointsStyle4 = {
			   title: "Ma couche GEOJSON de points",
			   type: 12,
			   definition: [
			                {	            	
			               	 serverUrl: fichierGeojsonPoints
			               	 
			                }
			   ],
			   options: {
			       symbolizers: {
						"temporary":   {//pour DrawCreation
							           "Point": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"create":   {//pour DrawCreation, GlobalModication
							           "Point": {
							               fillColor: "black",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"select":   { //pour GlobalModication
							           "Point": {
							               fillColor: "green",
							               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"delete":   { //pour RubberDeletion
				           "Point": {
				               fillColor: "grey",
				               graphicName:"square",
				               points:4,
				               radius:4,
				               angle:Math.PI / 4,
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "grey",
				               strokeOpacity: 1,
				               strokeWidth: 1,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid",			            
				               pointRadius: 4
				           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPoints
			   }
			};
			
			geojsonCoucheLignesStyle4 = {
			   title: "Ma couche GEOJSON de lignes",
			   type: 12,
			   definition: [
			       {
	                	 serverUrl: fichierGeojsonLines
	                	 
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "temporary":   {//pour DrawCreation
							           "Line": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
				       },
				       "create":   {//pour DrawCreation, GlobalModication
							           "Line": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
					   "select":   {//pour GlobalModication
							           "Line": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
					   "modify":   {//pour GlobalModication, VerticeModification
								   	"Point": {
							       		fillColor: "green",
							               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           },
							           "VirtualPoint": { //pour VerticeModification
							           	 cursor: "pointer",
							                graphicName: "cross",
							                fillColor:"yellow",
							                fillOpacity:1,
							                pointRadius:4,
							                strokeColor:"yellow",
							                strokeDashstyle:"solid",
							                strokeOpacity:1,
							                strokeWidth:1
							           }
					   },
					   "delete":   {//pour RubberDeletion
							           "Line": {
							               fillColor: "grey",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerLignes
			   }
			};
			
			geojsonCouchePolygonesStyle4 = {
			   title: "Ma couche GEOJSON de polygones",
			   type: 12,
			   definition: [
			                {
			               	 serverUrl: fichierGeojsonPolygons
			               	 
			       }
			   ],
			   options: {
			       symbolizers: {
			       	"temporary":   {//pour DrawCreation
							           "Polygon": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"select":   {//pour GlobalModication
					    		      "Polygon": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
					   "modify":   {//pour GlobalModication, VerticeModification
								   	"Point": {
							       		fillColor: "green",
							               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 6
							           },
							           "VirtualPoint": { //pour VerticeModification
							           	 cursor: "pointer",
							                graphicName: "cross",
							                fillColor:"yellow",
							                fillOpacity:1,
							                pointRadius:4,
							                strokeColor:"yellow",
							                strokeDashstyle:"solid",
							                strokeOpacity:1,
							                strokeWidth:1
							           }
					   },
						"delete":   {//pour RubberDeletion
					    		      "Polygon": {
							               fillColor: "grey",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPolygones
			   }
			};	
			
		geojsonCouchePointsStyle5 = {
			   title: "Ma couche GEOJSON de points",
			   type: 12,
			   definition: [
			                {	            	
			               	 serverUrl: fichierGeojsonPoints
			               	 
			                }
			   ],
			   options: {
			       symbolizers: {
						"create":   {//pour DrawCreation, GlobalModication
							           "Point": {
							               fillColor: "black",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"modify":   {//pour DrawCreation, GlobalModication
					           "Point": {
					               fillColor: "black",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "black",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPoints
			   }
			};
			
			geojsonCoucheLignesStyle5 = {
			   title: "Ma couche GEOJSON de lignes",
			   type: 12,
			   definition: [
			       {
	                	 serverUrl: fichierGeojsonLines
	                	 
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "create":   {//pour DrawCreation, GlobalModication
							           "Line": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
				       "modify":   {//pour DrawCreation, GlobalModication
				           "Line": {
				               fillColor: "black",
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "black",
				               strokeOpacity: 1,
				               strokeWidth: 4,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid"
				           }
				       }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerLignes
			   }
			};
			
			geojsonCouchePolygonesStyle5 = {
			   title: "Ma couche GEOJSON de polygones",
			   type: 12,
			   definition: [
			                {
			               	 serverUrl: fichierGeojsonPolygons
			               	 
			       }
			   ],
			   options: {
			       symbolizers: {
						"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"modify":   {//pour DrawCreation, GlobalModication
			    		      "Polygon": {
					               fillColor: "black",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "black",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						}
			       },
			       clone: {
			           supportLayers: [{
		                   id: "geojsonCoucheMultiPolygones",
		                   attributes: [{
		                           from: "d_attrib_2",
		                           to: "d_attrib_2"
		                       },
		                       {
		                           from: "d_attrib_3",
		                           to: "d_attrib_3"
		                       }
		                   ]
		               }]
			       },
			       copy: {
			   		supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
			       },
			       buffer: {
			       	supportLayers: [{id:"geojsonCoucheMultiPolygones"}],
			       	distance:20000/*,
			           enable: true*/
			       }, 
			       halo: {
			       	supportLayers: [{id:"geojsonCoucheMultiPolygones"}],
			       	distance:20000/*,
			           enable: true*/
			       }, 
			       homothetic: {
			           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
			           enable: true*/
			       }, 
			       split: {
			           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
			           enable: true*/
			       }, 
			       divide: {
			           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
			           enable: true*/
			       },
			       aggregate: {
			           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
			           enable: true
			       },
			       substract: {
			           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
			           enable: true
			       },
			       intersect: {
			       	supportLayersIdentifier:["geojsonCoucheMultiPolygones"],
			           enable: true
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPolygones
			   }
			};	
			
			geojsonCouchePointsStyle6 = {
				   title: "Ma couche GEOJSON de points",
				   type: 12,
				   definition: [
				                {	            	
				               	 serverUrl: fichierGeojsonPoints
				               	 
				                }
				   ],
				   options: {
				   	id: "geojsonCouchePoints",
				       symbolizers: {
				       	"default":   {//pour affichage
					           "Point": {
					               fillColor: "blue",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "blue",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
						           }
					       	},
					       	"create":   {//pour DrawCreation, GlobalModication
						           "Point": {
						               fillColor: "black",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
				       		},
				       		"temporary":   {//pour DrawCreation
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
				       		},
								"select":   { //pour GlobalModication
									           "Point": {
									               fillColor: "green",
									               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"delete":   { //pour RubberDeletion
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
				       },
				       copy: {
				       	supportLayersIdentifier: ["geojsonCoucheMultiPoints"],
				       	enable:true
				       },
				       clone: {
				       	supportLayers: [{
			                   id: "geojsonCoucheMultiPoints",
			                   attributes: [{
				                       from: "d_attrib_2",
				                       to: "d_attrib_2"
				                   },
				                   {
				                       from: "d_attrib_3",
				                       to: "d_attrib_3"
				                   }
			                   ]
			               }],
				       	enable:true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["geojsonCoucheMultiPoints"]
				       },
				       intersect: {
				       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerPoints
				   }
				};
				
				geojsonCoucheLignesStyle6 = {
				   title: "Ma couche GEOJSON de lignes",
				   type: 12,
				   definition: [
				       {
		               	 serverUrl: fichierGeojsonLines
		               	 
						}
				   ],
				   options: {
				   	id: "geojsonCoucheLignes",
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: {
				       	"default":   {//pour affichage
					           "Line": {
					               fillColor: "red",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "red",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
								           }
					       },
					       "create":   {//pour DrawCreation, GlobalModication
						           "Line": {
						               fillColor: "black",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
						   },
						   "temporary":   {//pour DrawCreation
					           "Line": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   },
						   "select":   {//pour GlobalModication
					           "Line": {
					               fillColor: "green",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   },
						   "modify":   {//pour GlobalModication, VerticeModification
						   	"Point": {
					       		fillColor: "green",
					               graphicName:"cross",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           },
					           "VirtualPoint": { //pour VerticeModification
					           	 cursor: "pointer",
					                graphicName: "cross",
					                fillColor:"yellow",
					                fillOpacity:1,
					                pointRadius:4,
					                strokeColor:"yellow",
					                strokeDashstyle:"solid",
					                strokeOpacity:1,
					                strokeWidth:1
					           }
						   },
						   "delete":   {//pour RubberDeletion
					           "Line": {
					               fillColor: "grey",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   }
				       },  
				       copy: {
				       	supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
				       	enable:true
				       },
				       clone: {
				       	supportLayers: [{
			                   id: "geojsonCoucheMultiLignes",
			                   attributes: [{
				                       from: "d_attrib_2",
				                       to: "d_attrib_2"
				                   },
				                   {
				                       from: "d_attrib_3",
				                       to: "d_attrib_3"
				                   }
			                   ]
			               }],
				       	enable:true
				       },
				       aggregate: {
				       	supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
				       	enable:true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
				       },
				       split: {
				       	supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
				       },
				       divide: {
				       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"]
				       },
				       substract: {
				       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"]
				       },
				       intersect: {
				       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerLignes
				   }
				};
				
				geojsonCouchePolygonesStyle6 = {
				   title: "Ma couche GEOJSON de polygones",
				   type: 12,
				   definition: [
				                {
				               	 serverUrl: fichierGeojsonPolygons
				               	 
				       }
				   ],
				   options: {
				   	id: "geojsonCouchePolygones",
				       symbolizers: {
				       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
				       	},"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
							},
							"temporary":   {//pour DrawCreation
					           "Polygon": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
							},
							"select":   {//pour GlobalModication
				    		      "Polygon": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							},
						   "modify":   {//pour GlobalModication, VerticeModification
						   	"Point": {
					       		fillColor: "green",
					               graphicName:"cross",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 6
					           },
					           "VirtualPoint": { //pour VerticeModification
					           	 cursor: "pointer",
					                graphicName: "cross",
					                fillColor:"yellow",
					                fillOpacity:1,
					                pointRadius:4,
					                strokeColor:"yellow",
					                strokeDashstyle:"solid",
					                strokeOpacity:1,
					                strokeWidth:1
					           }
						   },
							"delete":   {//pour RubberDeletion
				    		      "Polygon": {
						               fillColor: "grey",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							}
				       },
				       clone: {
				           supportLayers: [{
			                   id: "geojsonCoucheMultiPolygones",
			                   attributes: [{
			                           from: "d_attrib_2",
			                           to: "d_attrib_2"
			                       },
			                       {
			                           from: "d_attrib_3",
			                           to: "d_attrib_3"
			                       }
			                   ]
			               }]
				       },
				       copy: {
				   		supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
				       },
				       buffer: {
				       	supportLayers: [{id:"geojsonCoucheMultiPolygones"}],
				       	distance:20000/*,
				           enable: true*/
				       }, 
				       halo: {
				       	supportLayers: [{id:"geojsonCoucheMultiPoints"},{id:"geojsonCoucheMultiLignes"}],
				       	distance:20000/*,
				           enable: true*/
				       }, 
				       homothetic: {
				           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
				           enable: true*/
				       }, 
				       split: {
				           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
				           enable: true*/
				       }, 
				       divide: {
				           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
				           enable: true*/
				       },
				       aggregate: {
				           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
				           enable: true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
				       },
				       substract: {
				           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
				           enable: true
				       },
				       intersect: {
				       	supportLayersIdentifier:["geojsonCoucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerPolygones
				   }
				};	
			
				geojsonCouchePointsStyle8 = {
					    title: "Ma couche GEOJSON de points",
					    type: 12,
					    definition: [
					                 {	            	
					                	 serverUrl: fichierGeojsonPoints
					                 }
					    ],
					    options: {
					        symbolizers: {
								"temporary":   {//pour DrawCreation
									           "Point": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"autotracing":{
								    'Line': {
								        fillColor: 'grey',
								        fillOpacity: 0.4,
								        hoverFillColor: 'white',
								        hoverFillOpacity: 0.8,
								        strokeColor: 'grey',
								        strokeOpacity: 1,
								        strokeWidth: 5,
								        strokeLinecap: 'round',
								        strokeDashstyle: [5, 10]
								    }
								}
					        },
					        snapping : {
						           tolerance: 10,
						           enable: true,
						           autotracing: true
						        }, 
					        alwaysVisible: false,
					        visible: true,
					        queryable: false,
					        activeToQuery: false,
					        sheetable: false,
					        opacity: 100,
					        opacityMax: 100,
					        legend: null,
					        metadataURL: null,
					        format: "image/png",
					        displayOrder: 1,
					        geometryType: geometryTypeLayerPoints
					    }
					};
					
					geojsonCoucheLignesStyle8 = {
					    title: "Ma couche GEOJSON de lignes",
					    type: 12,
					    definition: [
					        {
				           	 serverUrl: fichierGeojsonLines
							}
					    ],
					    options: {
					    	 attributes: {
					            /*attributeId: {
					                fieldName: "d_attrib_1"
					            },*/
					            attributesEditable: [
					                {fieldName: 'd_attrib_2', label: 'Un attribut'},
					            ]
					        },
					        symbolizers: {
						       "temporary":   {//pour DrawCreation
									           "Line": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
						       },
								"autotracing":{
								    'Line': {
								        fillColor: 'grey',
								        fillOpacity: 0.4,
								        hoverFillColor: 'white',
								        hoverFillOpacity: 0.8,
								        strokeColor: 'grey',
								        strokeOpacity: 1,
								        strokeWidth: 5,
								        strokeLinecap: 'round',
								        strokeDashstyle: [5, 10]
								    }
								}
					        },    
					        snapping : {
						           tolerance: 10,
						           enable: true,
						           autotracing: true
						        }, 
					        alwaysVisible: false,
					        visible: true,
					        queryable: false,
					        activeToQuery: false,
					        sheetable: false,
					        opacity: 100,
					        opacityMax: 100,
					        legend: null,
					        metadataURL: null,
					        format: "image/png",
					        displayOrder: 1,
					        geometryType: geometryTypeLayerLignes
					    }
					};
					
					geojsonCouchePolygonesStyle8 = {
					    title: "Ma couche GEOJSON de polygones",
					    type: 12,
					    definition: [
					                 {
					                	 serverUrl: fichierGeojsonPolygons
					        }
					    ],
					    options: {
					        symbolizers: {
					        	"temporary":   {//pour DrawCreation
									           "Polygon": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
								"autotracing":{
								    'Line': {
								        fillColor: 'grey',
								        fillOpacity: 0.4,
								        hoverFillColor: 'white',
								        hoverFillOpacity: 0.8,
								        strokeColor: 'grey',
								        strokeOpacity: 1,
								        strokeWidth: 5,
								        strokeLinecap: 'round',
								        strokeDashstyle: [5, 10]
								    }
								}
					        },
					        snapping : {
						           tolerance: 10,
						           enable: true,
						           autotracing: true
						        }, 
					        alwaysVisible: false,
					        visible: true,
					        queryable: false,
					        activeToQuery: false,
					        sheetable: false,
					        opacity: 100,
					        opacityMax: 100,
					        legend: null,
					        metadataURL: null,
					        format: "image/png",
					        displayOrder: 1,
					        geometryType: geometryTypeLayerPolygones
					    }
					};	
			
			//---------------------------------------------------------//
			//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
			//---------------------------------------------------------//
			
			geojsonCoucheMultiPointsStyle2 = {
			   title: "Ma couche GEOJSON de multi points",
			   type: 12,
			   definition: [
			                {	            	
			               	 serverUrl: fichierGeojsonMultiPoints
			               	 
			                }
			   ],
			   options: {
			       symbolizers: {
						"temporary":   {//pour DrawCreation
							           "MultiPoint": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiPoints
			   }
			};
			
			geojsonCoucheMultiLignesStyle2 = {
			   title: "Ma couche GEOJSON de multi lignes",
			   type: 12,
			   definition: [
			       {
			       	serverUrl: fichierGeojsonMultiLines
	                	 
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "temporary":   {//pour DrawCreation
							           "MultiLine": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
				       }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiLignes
			   }
			};
			
			geojsonCoucheMultiPolygonesStyle2 = {
			   title: "Ma couche GEOJSON de multipolygones",
			   type: 12,
			   definition: [
			                {
			               	 serverUrl: fichierGeojsonMultiPolygons
			               	 
			       }
			   ],
			   options: {
			       symbolizers: {
			       	"temporary":   {//pour DrawCreation
							           "MultiPolygon": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: false,
			       activeToQuery: false,
			       sheetable: false,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiPolygones
			   }
			};	
			
			geojsonCoucheMultiPointsStyle3 = {
				   title: "Ma couche GEOJSON de multi points",
				   type: 12,
				   definition: [
				                {	            	
				               	 serverUrl: fichierGeojsonMultiPoints
				               	 
				                }
				   ],
				   options: {
				       symbolizers: {
							"temporary":   {//pour DrawCreation
								           "MultiPoint": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							},
							"select":   { //pour GlobalModication
								           "Point": {
								               fillColor: "green",
								               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           },
								           "MultiPoint": {
								               fillColor: "green",
								               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPoints
				   }
				};
				
				geojsonCoucheMultiLignesStyle3 = {
				   title: "Ma couche GEOJSON de multi lignes",
				   type: 12,
				   definition: [
				       {
				       	serverUrl: fichierGeojsonMultiLines
		               	 
						}
				   ],
				   options: {
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: {
					       "temporary":   {//pour DrawCreation
								           "Line": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
					       },
						   "select":   {//pour GlobalModication
								           "MultiLine": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
						   },
						   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
						   }
				       },    
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiLignes
				   }
				};
				
				geojsonCoucheMultiPolygonesStyle3 = {
				   title: "Ma couche GEOJSON de multi polygones",
				   type: 12,
				   definition: [
				                {
				               	 serverUrl: fichierGeojsonMultiPolygons
				               	 
				       }
				   ],
				   options: {
				       symbolizers: {
				       	"temporary":   {//pour DrawCreation
								           "MultiPolygon": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
							"select":   {//pour GlobalModication
								           "MultiPolygon": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
						   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
								               points:4,
								               radius:6,
								               radius2:0,
								               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 6
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
						   }
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPolygones
				   }
				};	
				
				geojsonCoucheMultiPointsStyle4 = {
					   title: "Ma couche GEOJSON de multi points",
					   type: 12,
					   definition: [
					                {	            	
					               	 serverUrl: fichierGeojsonMultiPoints
					               	 
					                }
					   ],
					   options: {
					       symbolizers: {
								"temporary":   {//pour DrawCreation
									           "MultiPoint": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"create":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"select":   { //pour GlobalModication
									           "Point": {
									               fillColor: "green",
									               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPoints
					   }
					};
					
					geojsonCoucheMultiLignesStyle4 = {
					   title: "Ma couche GEOJSON de multi lignes",
					   type: 12,
					   definition: [
					       {
					       	serverUrl: fichierGeojsonMultiLines
			               	 
							}
					   ],
					   options: {
					   	 attributes: {
					           /*attributeId: {
					               fieldName: "d_attrib_1"
					           },*/
					           attributesEditable: [
					               {fieldName: 'd_attrib_2', label: 'Un attribut'},
					           ]
					       },
					       symbolizers: {
						       "temporary":   {//pour DrawCreation
									           "MultiLine": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
						       },
						       "create":   {//pour DrawCreation, GlobalModication
									           "MultiLine": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
							   "select":   {//pour GlobalModication
									           "MultiLine": {
									               fillColor: "green",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
							   "modify":   {//pour GlobalModication, VerticeModification
										   	"Point": {
									       		fillColor: "green",
									               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           },
									           "VirtualPoint": { //pour VerticeModification
									           	 cursor: "pointer",
									                graphicName: "cross",
									                fillColor:"yellow",
									                fillOpacity:1,
									                pointRadius:4,
									                strokeColor:"yellow",
									                strokeDashstyle:"solid",
									                strokeOpacity:1,
									                strokeWidth:1
									           }
							   },
							   "delete":   {//pour RubberDeletion
									           "MultiLine": {
									               fillColor: "grey",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   }
					       },    
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiLignes
					   }
					};
					
					geojsonCoucheMultiPolygonesStyle4 = {
					   title: "Ma couche GEOJSON de multi polygones",
					   type: 12,
					   definition: [
					                {
					               	 serverUrl: fichierGeojsonMultiPolygons
					               	 
					       }
					   ],
					   options: {
					       symbolizers: {
					       	"temporary":   {//pour DrawCreation
									           "MultiPolygon": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
								"create":   {//pour DrawCreation, GlobalModication
							    		      "MultiPolygon": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
								"select":   {//pour GlobalModication
							    		      "MultiPolygon": {
									               fillColor: "green",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
							   "modify":   {//pour GlobalModication, VerticeModification
										   	"Point": {
									       		fillColor: "green",
									               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 6
									           },
									           "VirtualPoint": { //pour VerticeModification
									           	 cursor: "pointer",
									                graphicName: "cross",
									                fillColor:"yellow",
									                fillOpacity:1,
									                pointRadius:4,
									                strokeColor:"yellow",
									                strokeDashstyle:"solid",
									                strokeOpacity:1,
									                strokeWidth:1
									           }
							   },
								"delete":   {//pour RubberDeletion
							    		      "MultiPolygon": {
									               fillColor: "grey",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								}
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPolygones
					   }
					};	
			geojsonCoucheMultiPointsStyle5 = {
				   title: "Ma couche GEOJSON de multi points",
				   type: 12,
				   definition: [
				                {	            	
				               	 serverUrl: fichierGeojsonMultiPoints
				                }
				   ],
				   options: {
				       symbolizers: {
							"create":   {//pour DrawCreation, GlobalModication
								           "MultiPoint": {
								               fillColor: "black",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							},
							"mopdify":   {//pour DrawCreation, GlobalModication
						           "MultiPoint": {
						               fillColor: "black",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPoints
				   }
				};
				
				geojsonCoucheMultiLignesStyle5 = {
				   title: "Ma couche GEOJSON de multi lignes",
				   type: 12,
				   definition: [
				       {
				       		serverUrl: fichierGeojsonMultiLines
		               	 
						}
				   ],
				   options: {
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: { 
					       "create":   {//pour DrawCreation, GlobalModication
								           "MultiLine": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
						   },
					       "modify":   {//pour DrawCreation, GlobalModication
					           "MultiLine": {
					               fillColor: "black",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "black",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
					       }
				       },    
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiLignes
				   }
				};
				
				geojsonCoucheMultiPolygonesStyle5 = {
				   title: "Ma couche GEOJSON de multi polygones",
				   type: 12,
				   definition: [
				                {
				               	 serverUrl: fichierGeojsonMultiPolygons
				               	 
				       }
				   ],
				   options: {
				   	id: "geojsonCoucheMultiPolygones",
				       symbolizers: {
							"create":   {//pour DrawCreation, GlobalModication
								           "MultiPolygon": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
							"modify":   {//pour DrawCreation, GlobalModication
						           "MultiPolygon": {
						               fillColor: "black",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: false,
				       activeToQuery: false,
				       sheetable: false,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPolygones
				   }
				};	
				
				geojsonCoucheMultiPointsStyle6 = {
					   title: "Ma couche GEOJSON de multi points",
					   type: 12,
					   definition: [
					                {	            	
					               	 serverUrl: fichierGeojsonMultiPoints
					               	 
					                }
					   ],
					   options: {
					   	id: "geojsonCoucheMultiPoints",
					       symbolizers: {
					       	"default":   {//pour affichage
						           "MultiPoint": {
						               fillColor: "orange",
						               graphicName:"star",
						               points: 5,
						               radius: 8,
						               radius2: 4,
						               angle: 0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "orange",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 8
						           }
					       	},
					       	"create":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"temporary":   {//pour DrawCreation
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								},
								"select":   { //pour GlobalModication
						           "MultiPoint": {
						               fillColor: "green",
						               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       copy: {
					       	supportLayersIdentifier: ["geojsonCouchePoints"],
					       	enable:true
					       },
					       clone: {
					       	supportLayers: [{
				                   id: "geojsonCouchePoints",
				                   attributes: [{
					                       from: "d_attrib_2",
					                       to: "d_attrib_2"
					                   },
					                   {
					                       from: "d_attrib_3",
					                       to: "d_attrib_3"
					                   }
				                   ]
				               }],
					       	enable:true
					       },
					       aggregate: {
					       	supportLayersIdentifier: ["geojsonCouchePoints"],
					       	enable:true
					       },
					       intersect: {
					       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
					           enable: true
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPoints
					   }
					};
					
					geojsonCoucheMultiLignesStyle6 = {
					   title: "Ma couche GEOJSON de multi lignes",
					   type: 12,
					   definition: [
					       {
					       	serverUrl: fichierGeojsonMultiLines
			               	 
							}
					   ],
					   options: {
					   	id: "geojsonCoucheMultiLignes",
					   	 attributes: {
					           /*attributeId: {
					               fieldName: "d_attrib_1"
					           },*/
					           attributesEditable: [
					               {fieldName: 'd_attrib_2', label: 'Un attribut'},
					           ]
					       },
					       symbolizers: { 
					       	"default":   {//pour affichage
						           "MultiLine": {
						               fillColor: "purple",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "purple",
						               strokeOpacity: 1,
						               strokeWidth: 6,
						               strokeLinecap: "round",
						               strokeDashstyle: "sold"
						           }
					       	},
						       "create":   {//pour DrawCreation, GlobalModication
									           "MultiLine": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
						       "temporary":   {//pour DrawCreation
						           "Line": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
						       },
							   "select":   {//pour GlobalModication
						           "MultiLine": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							   },
							   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
							   },
							   "delete":   {//pour RubberDeletion
						           "MultiLine": {
						               fillColor: "grey",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							   }
					       },
					       copy: {
					       	supportLayersIdentifier: ["geojsonCoucheLignes"],
					       	enable:true
					       },
					       clone: {
					       	supportLayers: [{
				                   id: "geojsonCoucheLignes",
				                   attributes: [{
					                       from: "d_attrib_2",
					                       to: "d_attrib_2"
					                   },
					                   {
					                       from: "d_attrib_3",
					                       to: "d_attrib_3"
					                   }
				                   ]
				               }],
					       	enable:true
					       },
					       aggregate: {
					       	supportLayersIdentifier: ["geojsonCoucheLignes"],
					       	enable:true
					       },
					       split: {
					       	supportLayersIdentifier: ["geojsonCoucheLignes"]
					       },
					       divide: {
					       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"]
					       },
					       substract: {
					       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"]
					       },
					       intersect: {
					       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"]
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiLignes
					   }
					};
					
					geojsonCoucheMultiPolygonesStyle6 = {
					   title: "Ma couche GEOJSON de multi polygones",
					   type: 12,
					   definition: [
					                {
					               	 serverUrl: fichierGeojsonMultiPolygons
					               	 
					       }
					   ],
					   options: {
					   	id: "geojsonCoucheMultiPolygones",
					       symbolizers: {
					       	"default":   {//pour affichage
							           "MultiPolygon": {
							               fillColor: "pink",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               pointerEvents: "visiblePainted",
							               cursor: "pointer",
							               strokeColor: "pink",
							               strokeOpacity: 1,
							               strokeWidth: 8,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					       	},
					       	"temporary":   {//pour DrawCreation
						           "MultiPolygon": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					       	},
					       	"select":   {//pour GlobalModication
							           "MultiPolygon": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
								},
							   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 6
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
							   },
							   "create":   {//pour DrawCreation, GlobalModication
								           "MultiPolygon": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPolygon": {
						               fillColor: "grey",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       clone: {
					           supportLayers: [{
				                   id: "geojsonCouchePolygones",
				                   attributes: [{
				                           from: "d_attrib_2",
				                           to: "d_attrib_2"
				                       },
				                       {
				                           from: "d_attrib_3",
				                           to: "d_attrib_3"
				                       }
				                   ]
				               }]
					       },
					       copy: {
					   		supportLayersIdentifier: ["geojsonCouchePolygones"]
					       },
					       buffer: {
					       	supportLayers: [{id:"geojsonCouchePolygones"}],
					       	distance:20000/*,
					           enable: true*/
					       }, 
					       halo: {
					       	supportLayers: [{id:"geojsonCoucheMultiPoints"},{id:"geojsonCoucheMultiLignes"}],
					       	distance:20000/*,
					           enable: true*/
					       }, 
					       homothetic: {
					           supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
					           enable: true*/
					       }, 
					       split: {
					           supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
					           enable: true*/
					       }, 
					       divide: {
					           supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheLignes"]/*,
					           enable: true*/
					       },
					       aggregate: {
					           supportLayersIdentifier: ["geojsonCouchePolygones"],
					           enable: true
					       },
					       substract: {
					           supportLayersIdentifier: ["geojsonCouchePolygones"],
					           enable: true
					       },
					       intersect: {
					       	supportLayersIdentifier:["geojsonCouchePolygones","geojsonCoucheLignes","geojsonCouchePoints"],
					           enable: true
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: false,
					       activeToQuery: false,
					       sheetable: false,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPolygones
					   }
					};

					geojsonCouchePointsStyle7 = {
							   title: "Ma couche GEOJSON de points",
							   type: 12,
							   definition: [
							                {	            	
							               	 serverUrl: fichierGeojsonPoints
							               	 
							                }
							   ],
							   options: {
							       symbolizers: {
										"modify":   {//pour DrawCreation, GlobalModication
									           "Point": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
										}
							       },
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerPoints
							   }
							};
							
							geojsonCoucheLignesStyle7 = {
							   title: "Ma couche GEOJSON de lignes",
							   type: 12,
							   definition: [
							       {
					                	 serverUrl: fichierGeojsonLines
					                	 
									}
							   ],
							   options: {
							   	 attributes: {
							           /*attributeId: {
							               fieldName: "d_attrib_1"
							           },*/
							           attributesEditable: [
							               {fieldName: 'd_attrib_2', label: 'Un attribut'},
							           ]
							       },
							       symbolizers: {
								       "modify":   {//pour DrawCreation, GlobalModication
								           "Line": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 2,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
								       }
							       },    
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerLignes
							   }
							};
							
							geojsonCouchePolygonesStyle7 = {
							   title: "Ma couche GEOJSON de polygones",
							   type: 12,
							   definition: [
							                {
							               	 serverUrl: fichierGeojsonPolygons
							               	 
							       }
							   ],
							   options: {
							       symbolizers: {
										"modify":   {//pour DrawCreation, GlobalModication
							    		      "Polygon": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 2,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
										}
							       },
							       clone: {
							           supportLayers: [{
						                   id: "geojsonCoucheMultiPolygones",
						                   attributes: [{
						                           from: "d_attrib_2",
						                           to: "d_attrib_2"
						                       },
						                       {
						                           from: "d_attrib_3",
						                           to: "d_attrib_3"
						                       }
						                   ]
						               }]
							       },
							       copy: {
							   		supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
							       },
							       buffer: {
							       	supportLayers: [{id:"geojsonCoucheMultiPolygones"}],
							       	distance:20000/*,
							           enable: true*/
							       }, 
							       halo: {
							       	supportLayers: [{id:"geojsonCoucheMultiPolygones"}],
							       	distance:20000/*,
							           enable: true*/
							       }, 
							       homothetic: {
							           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
							           enable: true*/
							       }, 
							       split: {
							           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
							           enable: true*/
							       }, 
							       divide: {
							           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
							           enable: true*/
							       },
							       aggregate: {
							           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
							           enable: true
							       },
							       substract: {
							           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
							           enable: true
							       },
							       intersect: {
							       	supportLayersIdentifier:["geojsonCoucheMultiPolygones"],
							           enable: true
							       },
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerPolygones
							   }
							};
					geojsonCoucheMultiPointsStyle7 = {
							   title: "Ma couche GEOJSON de multi points",
							   type: 12,
							   definition: [
							                {	            	
							               	 serverUrl: fichierGeojsonMultiPoints
							                }
							   ],
							   options: {
							       symbolizers: {
										"mopdify":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
										}
							       },
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerMultiPoints
							   }
							};
							
							geojsonCoucheMultiLignesStyle7 = {
							   title: "Ma couche GEOJSON de multi lignes",
							   type: 12,
							   definition: [
							       {
							       		serverUrl: fichierGeojsonMultiLines
					               	 
									}
							   ],
							   options: {
							   	 attributes: {
							           /*attributeId: {
							               fieldName: "d_attrib_1"
							           },*/
							           attributesEditable: [
							               {fieldName: 'd_attrib_2', label: 'Un attribut'},
							           ]
							       },
							       symbolizers: { 
								       "modify":   {//pour DrawCreation, GlobalModication
								           "MultiLine": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 2,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
								       }
							       },    
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerMultiLignes
							   }
							};
							
							geojsonCoucheMultiPolygonesStyle7 = {
							   title: "Ma couche GEOJSON de multi polygones",
							   type: 12,
							   definition: [
							                {
							               	 serverUrl: fichierGeojsonMultiPolygons
							               	 
							       }
							   ],
							   options: {
							   	id: "geojsonCoucheMultiPolygones",
							       symbolizers: {
										"modify":   {//pour DrawCreation, GlobalModication
									           "MultiPolygon": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 2,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
										}
							       },
							       alwaysVisible: false,
							       visible: true,
							       queryable: false,
							       activeToQuery: false,
							       sheetable: false,
							       opacity: 100,
							       opacityMax: 100,
							       legend: null,
							       metadataURL: null,
							       format: "image/png",
							       displayOrder: 1,
							       geometryType: geometryTypeLayerMultiPolygones
							   }
							};						
							
							geojsonCoucheMultiPointsStyle8 = {
									   title: "Ma couche GEOJSON de multi points",
									   type: 12,
									   definition: [
									                {	            	
									               	 serverUrl: fichierGeojsonMultiPoints
									               	 
									                }
									   ],
									   options: {
									       symbolizers: {
												"temporary":   {//pour DrawCreation
													           "MultiPoint": {
													               fillColor: "grey",
													               graphicName:"square",
													               points:4,
													               radius:4,
													               angle:Math.PI / 4,
													               fillOpacity: 0.4,
													               hoverFillColor: "white",
													               hoverFillOpacity: 0.8,
													               strokeColor: "grey",
													               strokeOpacity: 1,
													               strokeWidth: 1,
													               strokeLinecap: "round",
													               strokeDashstyle: "solid",			            
													               pointRadius: 4
													           }
												},
												"autotracing":{
												    'Line': {
												        fillColor: 'grey',
												        fillOpacity: 0.4,
												        hoverFillColor: 'white',
												        hoverFillOpacity: 0.8,
												        strokeColor: 'grey',
												        strokeOpacity: 1,
												        strokeWidth: 5,
												        strokeLinecap: 'round',
												        strokeDashstyle: [5, 10]
												    }
												}
									       },
									        snapping : {
										           tolerance: 10,
										           enable: true
										        }, 
									       alwaysVisible: false,
									       visible: true,
									       queryable: false,
									       activeToQuery: false,
									       sheetable: false,
									       opacity: 100,
									       opacityMax: 100,
									       legend: null,
									       metadataURL: null,
									       format: "image/png",
									       displayOrder: 1,
									       geometryType: geometryTypeLayerMultiPoints
									   }
									};
									
									geojsonCoucheMultiLignesStyle8 = {
									   title: "Ma couche GEOJSON de multi lignes",
									   type: 12,
									   definition: [
									       {
									       	serverUrl: fichierGeojsonMultiLines
							                	 
											}
									   ],
									   options: {
									   	 attributes: {
									           /*attributeId: {
									               fieldName: "d_attrib_1"
									           },*/
									           attributesEditable: [
									               {fieldName: 'd_attrib_2', label: 'Un attribut'},
									           ]
									       },
									       symbolizers: {
										       "temporary":   {//pour DrawCreation
													           "MultiLine": {
													               fillColor: "grey",
													               graphicName:"square",
													               points:4,
													               radius:4,
													               angle:Math.PI / 4,
													               fillOpacity: 0.4,
													               hoverFillColor: "white",
													               hoverFillOpacity: 0.8,
													               strokeColor: "grey",
													               strokeOpacity: 1,
													               strokeWidth: 1,
													               strokeLinecap: "round",
													               strokeDashstyle: "solid"
													           }
										       },
												"autotracing":{
												    'Line': {
												        fillColor: 'grey',
												        fillOpacity: 0.4,
												        hoverFillColor: 'white',
												        hoverFillOpacity: 0.8,
												        strokeColor: 'grey',
												        strokeOpacity: 1,
												        strokeWidth: 5,
												        strokeLinecap: 'round',
												        strokeDashstyle: [5, 10]
												    }
												}
									       },    
									        snapping : {
										           tolerance: 10,
										           enable: true,
										           autotracing: true
										        }, 
									       alwaysVisible: false,
									       visible: true,
									       queryable: false,
									       activeToQuery: false,
									       sheetable: false,
									       opacity: 100,
									       opacityMax: 100,
									       legend: null,
									       metadataURL: null,
									       format: "image/png",
									       displayOrder: 1,
									       geometryType: geometryTypeLayerMultiLignes
									   }
									};
									
									geojsonCoucheMultiPolygonesStyle8 = {
									   title: "Ma couche GEOJSON de multipolygones",
									   type: 12,
									   definition: [
									                {
									               	 serverUrl: fichierGeojsonMultiPolygons
									               	 
									       }
									   ],
									   options: {
									       symbolizers: {
									       	"temporary":   {//pour DrawCreation
													           "MultiPolygon": {
													               fillColor: "grey",
													               graphicName:"square",
													               points:4,
													               radius:4,
													               angle:Math.PI / 4,
													               fillOpacity: 0.4,
													               hoverFillColor: "white",
													               hoverFillOpacity: 0.8,
													               strokeColor: "grey",
													               strokeOpacity: 1,
													               strokeWidth: 1,
													               strokeLinecap: "round",
													               strokeDashstyle: "solid"
													           }
												},
												"autotracing":{
												    'Line': {
												        fillColor: 'grey',
												        fillOpacity: 0.4,
												        hoverFillColor: 'white',
												        hoverFillOpacity: 0.8,
												        strokeColor: 'grey',
												        strokeOpacity: 1,
												        strokeWidth: 5,
												        strokeLinecap: 'round',
												        strokeDashstyle: [5, 10]
												    }
												}
									       },
									        snapping : {
										           tolerance: 10,
										           enable: true,
										           autotracing: true
										        }, 
									       alwaysVisible: false,
									       visible: true,
									       queryable: false,
									       activeToQuery: false,
									       sheetable: false,
									       opacity: 100,
									       opacityMax: 100,
									       legend: null,
									       metadataURL: null,
									       format: "image/png",
									       displayOrder: 1,
									       geometryType: geometryTypeLayerMultiPolygones
									   }
									};	
	/***************************
	  couches pour le snapping
	 ****************************/
					
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	geojsonCouchePointsSnapping = {
		   title: "Ma couche GEOJSON de points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCouchePoints",
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },     
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	geojsonCouchePointsSnapping2 = {
		   title: "Ma couche GEOJSON de points",
		   type: 1,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCouchePoints",
		       /*snapping : {
		           tolerance: 10,
		           enable: true
		       },  */   
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	geojsonCoucheLignesSnapping = {
	    title: "Ma couche GEOJSON de lignes",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonLines
           	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"geojsonCoucheLignes",
		   snapping : {
		   	snappingLayersIdentifier:["geojsonCouchePoints","geojsonCouchePolygones"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	geojsonCouchePolygonesSnapping = {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCouchePolygones",
		   	snapping : {
		           tolerance: 10,
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};

	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	geojsonCoucheMultiPointsSnapping = {
		   title: "Ma couche GEOJSON de multi points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiPoints",
		       snapping : {
		           tolerance: 10,
		           enable: false
		       },     
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	geojsonCoucheMultiLignesSnapping = {
	    title: "Ma couche GEOJSON de multi lignes",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonMultiLines
           	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"geojsonCoucheMultiLignes",
		   snapping : {
		   	snappingLayersIdentifier:["geojsonCoucheMultiPoints","geojsonCoucheMultiPolygones"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	geojsonCoucheMultiPolygonesSnapping = {
	    title: "Ma couche GEOJSON de multi polygones",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheMultiPolygones",
	    	snapping : {
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};
	
	
	/*********************************************
	  couches pour les outils d'édition "avancés"
	 *********************************************/
					
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	geojsonCouchePointsFctAvanced = {
		   title: "Ma couche GEOJSON de points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCouchePoints",
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	geojsonCouchePointsFctAvancedBis = {
		   title: "Ma couche GEOJSON de points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCouchePoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true,
		       	attributsNotClonable:[{fieldName: 'd_attrib_3'}]
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	geojsonCouchePointsFctAvanced2 = {
		   title: "Ma couche GEOJSON de points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCouchePoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiPoints"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPoints"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	geojsonCouchePointsFctAvanced3 = {
		   title: "Ma couche GEOJSON de points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCouchePoints",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiPoints"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	geojsonCoucheLignesFctAvanced = {
	    title: "Ma couche GEOJSON de lignes",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonLines
           	 
	        }
	    ],
	    options: {
	        id:"geojsonCoucheLignes",
	        copy: {
	        	enable: true
	        },
	        clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	geojsonCoucheLignesFctAvancedBis = {
		   title: "Ma couche GEOJSON de lignes",
		   type: 12,
		   definition: [
		       {
		       	serverUrl: fichierGeojsonLines
               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
	
	geojsonCoucheLignesFctAvanced2 = {
		   title: "Ma couche GEOJSON de lignes",
		   type: 12,
		   definition: [
		       {
		       	serverUrl: fichierGeojsonLines
               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
		       },
		       split: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["geojsonCouchePoints","geojsonCoucheMultiLignes","geojsonCouchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
	
	geojsonCoucheLignesFctAvanced3 = {
		   title: "Ma couche GEOJSON de lignes",
		   type: 12,
		   definition: [
		       {
		       	serverUrl: fichierGeojsonLines
               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheLignes",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
		       	enable:true
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
		       	enable:true
		       },
		       substract: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
	
	geojsonCouchePolygonesFctAvanced = {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCouchePolygones",
		   	copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       aggregate: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	geojsonCouchePolygonesFctAvancedBis = {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCouchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	geojsonCouchePolygonesFctAvanced2 = {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCouchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
		       },
		       split: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["geojsonCoucheLignes","geojsonCoucheMultiPolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	geojsonCouchePolygonesFctAvanced3 = {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCouchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
		       	enable:true
		       },
		       halo: {
		       	supportLayers: [{id:"geojsonCouchePoints"}, {id:"geojsonCoucheLignes"}, {id:"geojsonCoucheMultiPolygones"}, {id:"geojsonCoucheMultiPoints"}, {id:"geojsonCoucheMultiLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"geojsonCoucheMultiPolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["geojsonCouchePoints","geojsonCoucheLignes"]
		       },
		       substract: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
        		   "default":{// EDITION WFS
        		   	"Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
        		   }	
		       },*/
		       /*symbolizers: { //WFS
     		      "Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};


	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	geojsonCoucheMultiPointsFctAvanced = {
		   title: "Ma couche GEOJSON de multi points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiPoints", 
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       aggregate: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	geojsonCoucheMultiPointsFctAvancedBis = {
		   title: "Ma couche GEOJSON de multi points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiPoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	geojsonCoucheMultiPointsFctAvanced2 = {
		   title: "Ma couche GEOJSON de multi points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiPoints", 
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCouchePoints"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCouchePoints"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	geojsonCoucheMultiPointsFctAvanced3 = {
		   title: "Ma couche GEOJSON de multi points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonMultiPoints
		               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiPoints", 
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCouchePoints"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCouchePoints"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	geojsonCoucheMultiLignesFctAvanced = {
	    title: "Ma couche GEOJSON de multi lignes",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonMultiLines
           	 
	        }
	    ],
	    options: {
	        id:"geojsonCoucheMultiLignes",
	        copy: {
	        	enable: true
	        },
	        clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	geojsonCoucheMultiLignesFctAvancedBis = {
		   title: "Ma couche GEOJSON de multi lignes",
		   type: 12,
		   definition: [
		       {
		       	serverUrl: fichierGeojsonMultiLines
               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes
		   }
		};
	
	geojsonCoucheMultiLignesFctAvanced2 = {
		   title: "Ma couche GEOJSON de multi lignes",
		   type: 12,
		   definition: [
		       {
		       	serverUrl: fichierGeojsonMultiLines
               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCoucheLignes"]
		       },
		       split: {
		       	supportLayersIdentifier: ["geojsonCoucheLignes"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiPoints","geojsonCoucheLignes","geojsonCouchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCoucheLignes"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes
		   }
		};
	
	geojsonCoucheMultiLignesFctAvanced3 = {
		   title: "Ma couche GEOJSON de multi lignes",
		   type: 12,
		   definition: [
		       {
		       	serverUrl: fichierGeojsonMultiLines
               	 
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiLignes",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["geojsonCoucheLignes"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCoucheLignes"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["geojsonCoucheLignes","geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
		           enable: true*/
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes
		   }
		};
	
	geojsonCoucheMultiPolygonesFctAvanced = {
	    title: "Ma couche GEOJSON de multi polygones",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheMultiPolygones",
	    	copy: {
	        	enable: true
	        },
	    	clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};
	
	geojsonCoucheMultiPolygonesFctAvancedBis = {
		   title: "Ma couche GEOJSON de multi polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonMultiPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCoucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		       	enable: true
		       },
		   	clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones
		   }
		};
	
	geojsonCoucheMultiPolygonesFctAvanced2 = {
		   title: "Ma couche GEOJSON de multi polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonMultiPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCoucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["geojsonCouchePolygones"]
		       },
		       split: {
		       	supportLayersIdentifier: ["geojsonCouchePolygones"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["geojsonCoucheMultiLignes","geojsonCouchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones
		   }
		};
	
	geojsonCoucheMultiPolygonesFctAvanced3 = {
		   title: "Ma couche GEOJSON de multi polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonMultiPolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCoucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["geojsonCouchePolygones"],
		       	enable:true
		       },
		       halo: {
		       	supportLayers: [{id:"geojsonCoucheMultiPoints"}, {id:"geojsonCoucheMultiLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"geojsonCouchePolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiLignes","geojsonCoucheMultiPoints"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":{
	     		      "Polygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           },
			          "MultiPolygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones
		   }
		};
	
	/*****************************************************
	  couches pour le clonage
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
		
	geojsonCoucheClonePoints = {
	    title: "Ma couche GEOJSON de points pour clonage",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonClonePoints
	                	 
	        }
	    ],
	    options: { 
	    	id:"geojsonCoucheClonePoints",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        copy: {
	    		supportLayersIdentifier: ["geojsonCouchePoints"]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCouchePoints",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPoints"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCouchePoints"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerClonePoints
	    }
	};
	
	geojsonCoucheCloneLignes = {
	    title: "Ma couche GEOJSON de lignes pour clonage",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonCloneLines
           	 	
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheCloneLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        copy: {
	    		supportLayersIdentifier: ["geojsonCoucheLignes"]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["geojsonCoucheLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["geojsonCoucheLignes"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCoucheLignes"],
	            enable: true
	        },
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCoucheLignes"]/*,
	            enable: true*/
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        substract: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneLignes
	    }
	};
	
	geojsonCoucheCloneLignes2 = {
	    title: "Ma couche GEOJSON de lignes pour clonage",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonCloneLines
           	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheCloneLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["geojsonCoucheLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["geojsonCoucheLignes"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCoucheLignes"],
	            enable: true
	        },
	        snapping: {
	        	snappingLayersIdentifier:["geojsonCouchePoints"],
	            tolerance: 10/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCoucheLignes"]/*,
	            enable: true*/
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneLignes
	    }
	};
	
	geojsonCoucheClonePolygones = {
		   title: "Ma couche GEOJSON de polygones pour clonage",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonClonePolygons
		               	 
		       }
		   ],
		   options: {
		   	id:"geojsonCoucheClonePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
		               {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		   		supportLayersIdentifier: ["geojsonCouchePolygones"]
		       },
		       halo: {
		       	supportLayers: [{id:"geojsonCouchePoints"}, {id:"geojsonCoucheLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"geojsonCouchePolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       clone: {
		           supportLayers: [{
	                    id: "geojsonCouchePolygones",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_cloneattrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_cloneattrib_3"
	                        }
	                    ]
	                }]
		       },
		       homothetic: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"],
		           enable: true
		       }, 
		       split: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
		           enable: true*/
		       },
		       divide: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
		           enable: true*/
		       },
		       intersect: {
		       	supportLayersIdentifier:["geojsonCouchePolygones"],
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["geojsonCouchePolygones"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
	            		      "Polygon": {
					               fillColor: "yellow",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "yellow",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerClonePolygones
		   }
		};
	
	geojsonCoucheClonePolygones2 = {
	    title: "Ma couche GEOJSON de polygones pour clonage",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonClonePolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheClonePolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCouchePolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        homothetic: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"],
	            enable: true
	        }, 
	        split: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCouchePolygones"],
	            enable: true
	        },
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"],
	            enable: true
	        },
	        snapping: {
	        	snappingLayersIdentifier:["geojsonCouchePoints"],
	            tolerance: 10/*,
	            enable: true*/
	        },
	        /*symbolizers: {
	        	"default":   {//pour affichage
            		      "Polygon": {
				               fillColor: "yellow",
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "yellow",
				               strokeOpacity: 1,
				               strokeWidth: 4,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid"
				           }
	        	}
	        },*/
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerClonePolygones
	    }
	};
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
		
	geojsonCoucheCloneMultiPoints = {
	    title: "Ma couche GEOJSON de  multi points pour clonage",
	    type: 12,
	    definition: [
	                 {	            	
	                	 serverUrl: fichierGeojsonCloneMultiPoints
	                	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheCloneMultiPoints",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheMultiPoints",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["geojsonCoucheMultiPoints"]
	        },
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPoints"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCoucheMultiPoints"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPoints
	    }
	};
	
	geojsonCoucheCloneMultiLignes = {
	    title: "Ma couche GEOJSON de multi lignes pour clonage",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonCloneMultiLines
           	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheCloneMultiLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheMultiLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
	                ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
	        },
	        split: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCoucheMultiLignes"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiLignes
	    }
	};
	
	geojsonCoucheCloneMultiLignes2 = {
	    title: "Ma couche GEOJSON de multi lignes pour clonage",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonCloneMultiLines
           	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheCloneMultiLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheMultiLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
	                ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        snapping: {
	        	//snappingLayersIdentifier:["geojsonCoucheMultiLignes"],
	            tolerance: 10,
	            enable: true
	        }, 
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCoucheMultiLignes"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiLignes
	    }
	};
	
	geojsonCoucheCloneMultiPolygones = {
	    title: "Ma couche GEOJSON de multi polygones pour clonage",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonCloneMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheCloneMultiPolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheMultiPolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
	        },
	        buffer: {
	        	supportLayers: [{id:"geojsonCoucheMultiPolygones"}]/*,
	            enable: true*/
	        }, 
	        halo: {
	        	supportLayers: [{id:"geojsonCoucheMultiPoints"}, {id:"geojsonCoucheMultiLignes"}]/*,
	            enable: true*/
	        }, 
	        homothetic: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCoucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPolygones
	    }
	};
	
	geojsonCoucheCloneMultiPolygones2 = {
	    title: "Ma couche GEOJSON de multi polygones pour clonage",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonCloneMultiPolygons
	                	 
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheCloneMultiPolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheMultiPolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        homothetic: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
	            enable: true*/
	        },
	        snapping: {
	        	snappingLayersIdentifier:["geojsonCoucheMultiLignes"],
	            tolerance: 10,
	            enable: true
	        }, 
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCoucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPolygones
	    }
	};
	
	
	/***************************
	  couches pour exemples complets
	 ****************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	geojsonCouchePointsFull = {
		   title: "Ma couche GEOJSON de points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonPoints
		       }
		   ],
		   options: {
		       id:"geojsonCouchePoints",
		       attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
		           ]
		       },
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },
		       clone: {
		           supportLayers: [{
	                    id: "geojsonCoucheMultiPoints",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_attrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_attrib_3"
	                        }
	                    ]
	                }]
		       },
		       copy: {
		   		supportLayersIdentifier: ["geojsonCoucheMultiPoints"]
		       }, 
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPoints"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier:["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       maxScale: 1000, 
		       minScale: 40000, 
		       maxEditionScale: 100,
		       minEditionScale: 40000,
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: true,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
		       attribution: attribution
		   }
		};
	
	geojsonCoucheLignesFull = {
	    title: "Ma couche GEOJSON de lignes",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonLines 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"geojsonCoucheLignes",
		   snapping : {
		   	snappingLayersIdentifier:["geojsonCouchePoints"],
	            tolerance: 10,
	            enable: true
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCoucheMultiLignes",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_attrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_attrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["geojsonCoucheMultiLignes"]
	        }, 
	        split: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"]/*,
	            enable: true*/
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["geojsonCoucheMultiLignes"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCouchePolygones","geojsonCoucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: true,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	geojsonCouchePolygonesFull= {
		   title: "Ma couche GEOJSON de polygones",
		   type: 12,
		   definition: [
		                {
		               	 serverUrl: fichierGeojsonPolygons
		       }
		   ],
		   options: {
		   	id:"geojsonCouchePolygones",
		   	snapping : {
		           tolerance: 10,
		           enable: false
		       },
		       clone: {
		           supportLayers: [{
	                    id: "geojsonCoucheMultiPolygones",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_attrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_attrib_3"
	                        }
	                    ]
	                }]
		       },
		       copy: {
		   		supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]
		       },
		       buffer: {
		       	supportLayers: [{id:"geojsonCoucheMultiPolygones"}],
		       	distance:20000/*,
		           enable: true*/
		       }, 
		       halo: {
		       	supportLayers: [{id:"geojsonCouchePoints"}, {id:"geojsonCoucheLignes"}],
		       	distance:20000/*,
		           enable: true*/
		       }, 
		       homothetic: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
		           enable: true*/
		       }, 
		       split: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
		           enable: true*/
		       }, 
		       divide: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"]/*,
		           enable: true*/
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier:["geojsonCoucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: true,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
		       attribution: attribution
		   }
		};

	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	geojsonCoucheMultiPointsFull = {
		   title: "Ma couche GEOJSON de multi points",
		   type: 12,
		   definition: [
		                {	            	
		               	 serverUrl: fichierGeojsonMultiPoints
		       }
		   ],
		   options: {
		       id:"geojsonCoucheMultiPoints",
		       attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
		           ]
		       },
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },     
		       maxScale: 1000, 
		       minScale: 40000, 
		       maxEditionScale: 100,
		       minEditionScale: 40000,
		       alwaysVisible: false,
		       visible: true,
		       queryable: false,
		       activeToQuery: false,
		       sheetable: false,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
		       attribution: attribution
		   }
		};
	
	geojsonCoucheMultiLignesFull = {
	    title: "Ma couche GEOJSON de multi lignes",
	    type: 12,
	    definition: [
	        {
	        	serverUrl: fichierGeojsonMultiLines
           	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"geojsonCoucheMultiLignes",
		   snapping : {
		   	snappingLayersIdentifier:["geojsonCoucheMultiPoints"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes,
	        attribution: attribution
	    }
	};
	
	geojsonCoucheMultiPolygonesFull= {
	    title: "Ma couche GEOJSON de multi polygones",
	    type: 12,
	    definition: [
	                 {
	                	 serverUrl: fichierGeojsonMultiPolygons
	        }
	    ],
	    options: {
	    	id:"geojsonCoucheMultiPolygones",
	    	snapping : {
	            tolerance: 10,
	            enable: false
	        },
	        clone: {
	            supportLayers: [{
                    id: "geojsonCouchePolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_attrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_attrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["geojsonCouchePolygones"]
	        },
	        buffer: {
	        	supportLayers: [{id:"geojsonCouchePolygones"}],
	        	distance:20000/*,
	            enable: true*/
	        }, 
	        halo: {
	        	supportLayers: [{id:"geojsonCoucheMultiPoints"}, {id:"geojsonCoucheMultiLignes"}],
	        	distance:20000/*,
	            enable: true*/
	        }, 
	        homothetic: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["geojsonCouchePolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["geojsonCouchePolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones,
	        attribution: attribution
	    }
	};

	/***************************
	  couche de fond
	 ****************************/
	
	coucheBase = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
					layerName: "fond_vecteur"
				}
			],
			options: {
				maxScale: 100,
				minScale: 10000001,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		};

		groupeFonds = {
		   title: "Fonds cartographiques",
		   options: {
		       opened: true
		   }
		};
		 groupeEditionGEOJSON = {
		     title: "Mes couches d'édition GEOJSON - objet simple",
		     options: {
		         opened: true
		     }
		 };
		 
		 groupeEditionGEOJSONMulti = {
		     title: "Mes couches d'édition GEOJSON - objet composite",
		     options: {
		         opened: true
		     }
		 };
}