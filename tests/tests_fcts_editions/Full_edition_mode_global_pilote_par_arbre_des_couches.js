proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
proj4.defs('urn:ogc:def:crs:EPSG::4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

var carte;
function chargementCarte() {
	
	chargeCouchesGroupes();
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
		
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global piloté par l'arbre des couches
	      save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	      }
	});   
	
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsFull.title, couchePointsFull.definition, couchePointsFull.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesFull.title, coucheLignesFull.definition, coucheLignesFull.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesFull.title, couchePolygonesFull.definition, couchePolygonesFull.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsFull.title, coucheMultiPointsFull.definition, coucheMultiPointsFull.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesFull.title, coucheMultiLignesFull.definition, coucheMultiLignesFull.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesFull.title, coucheMultiPolygonesFull.definition, coucheMultiPolygonesFull.options);

    contenuCarte.addItem(editionLayer4, gpWFS);
    contenuCarte.addItem(editionLayer5, gpWFS);
    contenuCarte.addItem(editionLayer6, gpWFS);
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsFull.title, kmlCouchePointsFull.definition, kmlCouchePointsFull.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesFull.title, kmlCoucheLignesFull.definition, kmlCoucheLignesFull.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesFull.title, kmlCouchePolygonesFull.definition, kmlCouchePolygonesFull.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsFull.title, kmlCoucheMultiPointsFull.definition, kmlCoucheMultiPointsFull.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesFull.title, kmlCoucheMultiLignesFull.definition, kmlCoucheMultiLignesFull.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesFull.title, kmlCoucheMultiPolygonesFull.definition, kmlCoucheMultiPolygonesFull.options);

    contenuCarte.addItem(editionLayerkml4, gpKML);
    contenuCarte.addItem(editionLayerkml5, gpKML);
    contenuCarte.addItem(editionLayerkml6, gpKML); 
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsFull.title, geojsonCouchePointsFull.definition, geojsonCouchePointsFull.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesFull.title, geojsonCoucheLignesFull.definition, geojsonCoucheLignesFull.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesFull.title, geojsonCouchePolygonesFull.definition, geojsonCouchePolygonesFull.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsFull.title, geojsonCoucheMultiPointsFull.definition, geojsonCoucheMultiPointsFull.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesFull.title, geojsonCoucheMultiLignesFull.definition, geojsonCoucheMultiLignesFull.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesFull.title, geojsonCoucheMultiPolygonesFull.definition, geojsonCoucheMultiPolygonesFull.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSON); 
	
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	var d_coucheParkings = contenuCarte.addItem(coucheParkings, groupeInfras);
	var d_coucheStations = contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	
	var bounds = [799205.2,6215857.5,1078390.1,6452614.0];


	
	                 	 
	                 	 

	
	
	
	
	// Construction de la carte
	//var carte = new Descartes.Map.ContinuousScalesMap(
	carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					
					projection: "EPSG:2154",
					initExtent: bounds,
					maxExtent: bounds,
					maxScale: 100,
					size: [600,400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolsInNamedToolBar(toolsBar,[
	                        {type : Descartes.Map.MAXIMAL_EXTENT},
	                        {type : Descartes.Map.INITIAL_EXTENT, args:bounds},
	                        {type : Descartes.Map.DRAG_PAN},
							{type : Descartes.Map.ZOOM_IN},
							{type : Descartes.Map.ZOOM_OUT},
							{type : Descartes.Map.NAV_HISTORY}
	]);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar(null, 
	//  carte.addEditionToolBar("editionToolBar", 
		[ {type: Descartes.Map.EDITION_SELECTION},
	      {type: Descartes.Map.EDITION_DRAW_CREATION,
		      args: {
	                  snapping: true
	                 }
	      },
	      {type: Descartes.Map.EDITION_COPY_CREATION},
	      {type: Descartes.Map.EDITION_CLONE_CREATION},
	      {type: Descartes.Map.EDITION_BUFFER_NORMAL_CREATION},
	      {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION},
	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION},
	      {type: Descartes.Map.EDITION_SPLIT,
	       args: {
	          drawingType: Descartes.Layer.POINT_GEOMETRY
	       }
	      },
	      {type: Descartes.Map.EDITION_SPLIT},
	      {type: Descartes.Map.EDITION_SPLIT,
		      args: {
	           drawingType: Descartes.Layer.POLYGON_GEOMETRY
	       }
	      },
	      {type: Descartes.Map.EDITION_DIVIDE},
	      {type: Descartes.Map.EDITION_AGGREGATION},
	      {type: Descartes.Map.EDITION_UNAGGREGATION_CREATION},
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION,
		      args: {
	                  snapping: true
	                 }
	      },
	      {type: Descartes.Map.EDITION_MERGE_MODIFICATION},
	      {type: Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION},
	      {type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_ATTRIBUTE},
	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION},
	      {type: Descartes.Map.EDITION_SAVE}
	    ],
	    {toolBar: toolsBar,// imbrication dans la barre principale
	     openerTool: true,
			panel:true,
			title:"Outils d'édition",
			draggable: {
	            enable: true,
	            containment: 'parent'
	        }} 
	  );
	  
	  carte.addToolsInNamedToolBar(toolsBar,[
			{type : Descartes.Map.POINT_SELECTION, args:{resultUiParams:{withCsvExport: true}}},
			{type : Descartes.Map.POINT_RADIUS_SELECTION, args:{resultUiParams:{withCsvExport: true, withReturn: true}}},
			{type : Descartes.Map.RECTANGLE_SELECTION, args:{resultUiParams:{withCsvExport: true, withReturn: true}}},
			{type : Descartes.Map.COORDS_CENTER},
			{type : Descartes.Map.CENTER_MAP},
			{type : Descartes.Map.DISTANCE_MEASURE},
			{type : Descartes.Map.AREA_MEASURE},
			{type : Descartes.Map.PDF_EXPORT},
			{type : Descartes.Map.PNG_EXPORT}
 	]);  

	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	var contentTools = [{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL, options:null},
						{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL, options:null}
						];
	
	carte.addContentManager('layersTree', contentTools, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout d'un gestionnaire de requetes
	var gestionnaireRequetes = carte.addRequestManager('Requetes');
	var requeteStations = new Descartes.Request(d_coucheStations, "Stations essence", Descartes.Layer.POINT_GEOMETRY);
	requeteStations.addMember(new Descartes.RequestMember("Une station", 'name', "==", ['Station Total', 'BP'], true));
	gestionnaireRequetes.addRequest(requeteStations);
	
	var requeteParkings = new Descartes.Request(d_coucheParkings, "Parkings", Descartes.Layer.POINT_GEOMETRY);
	requeteParkings.addMember(new Descartes.RequestMember("Nom du parking", 'name', "==", [], true));
	gestionnaireRequetes.addRequest(requeteParkings);

	// Objets de référence administratifs embarqués
	var gazetteerLevels = [];
	var niveauChoixRegion = new Descartes.GazetteerLevel("Choisissez une région","Aucune région","reg");
	var niveauChoixDpt = new Descartes.GazetteerLevel("Choisissez un département","Aucun département","dept");
	var niveauChoixCom = new Descartes.GazetteerLevel("Choisissez une commune","Aucune commune","com");
	
	gazetteerLevels.push(niveauChoixRegion);
	gazetteerLevels.push(niveauChoixDpt);
	gazetteerLevels.push(niveauChoixCom);

//	carte.addGazetteer('Gazetteer', "", gazetteerLevels, {location : "../../gazetteer/"});
	carte.addGazetteer('Gazetteer', "", gazetteerLevels);
	
	// Ajout des actions
	// Affectation des styles personnalisées pour l'action de type Descartes.Map.SCALE_CHOOSER_ACTION
	var actions = [{type : Descartes.Map.SCALE_CHOOSER_ACTION,  div : 'ScaleChooser'},
	               {type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'},
	               {type : Descartes.Map.COORDS_INPUT_ACTION,   div : 'CoordinatesInput'},
	               {type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'},
	               {type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'}
	              ];
	
	carte.addActions(actions);
	
	// Ajout de plusieurs zones informatives
	var infos = [{type : Descartes.Map.GRAPHIC_SCALE_INFO, div : 'GraphicScale'},
	             {type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'},
	             {type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'},
	             {type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'},
	             {type : Descartes.Map.LEGEND_INFO, div : 'Legend'},
	             {type : Descartes.Map.ATTRIBUTION_INFO, div:'Attribution'}
	];

	// Ajout de la zone informative
	carte.addInfos(infos);
	
	// Ajout du gestionnaire de contexte
//	carte.addBookmarksManager('Bookmarks', 'descartes');
	
	// Ajout des infos-bulle
	var toolTipLayers = [{layer: coucheParkings , fields: ['name']}];
	carte.addToolTip('ToolTip',toolTipLayers);	

	//DIRECTIONPANPANEL
	carte.addDirectionalPanPanel();
	
	//MINIMAP
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");

	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette
	]);
	
}
