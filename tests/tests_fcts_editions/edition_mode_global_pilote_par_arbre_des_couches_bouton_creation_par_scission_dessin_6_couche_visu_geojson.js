function chargementCarte() {
	
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson2 = new Descartes.Layer.GeoJSON(geojsonCoucheLignesFctAvanced3.title, geojsonCoucheLignesFctAvanced3.definition, geojsonCoucheLignesFctAvanced3.options);
    var editionLayergeojson2bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneLignes.title, geojsonCoucheCloneLignes.definition, geojsonCoucheCloneLignes.options);
    editionLayergeojson2bis.title="Ma couche GEOJSON de lignes pour la scission";
    var editionLayergeojson3 = new Descartes.Layer.GeoJSON(geojsonCouchePolygonesFctAvanced3.title, geojsonCouchePolygonesFctAvanced3.definition, geojsonCouchePolygonesFctAvanced3.options);
    var editionLayergeojson3bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePolygones.title, geojsonCoucheClonePolygones.definition, geojsonCoucheClonePolygones.options);
    editionLayergeojson3bis.title="Ma couche GEOJSON de polygones pour la scission";
    
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2bis, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3bis, gpGEOJSON);
    
    var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    var editionLayergeojson5 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiLignesFctAvanced3.title, geojsonCoucheMultiLignesFctAvanced3.definition, geojsonCoucheMultiLignesFctAvanced3.options);
    var editionLayergeojson5bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiLignes.title, geojsonCoucheCloneMultiLignes.definition, geojsonCoucheCloneMultiLignes.options);
    editionLayergeojson5bis.title="Ma couche GEOJSON de multi-lignes pour la scission";
    var editionLayergeojson6 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced3.title, geojsonCoucheMultiPolygonesFctAvanced3.definition, geojsonCoucheMultiPolygonesFctAvanced3.options);
    var editionLayergeojson6bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPolygones.title, geojsonCoucheCloneMultiPolygones.definition, geojsonCoucheCloneMultiPolygones.options);
    editionLayergeojson6bis.title="Ma couche GEOJSON de multi-polygones pour la scission";
    
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5bis, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    contenuCarte.addItem(editionLayergeojson6bis, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_SPLIT,
	       args: {
              drawingType: Descartes.Layer.POINT_GEOMETRY
	       }
          },
          {type: Descartes.Map.EDITION_SPLIT},
          {type: Descartes.Map.EDITION_SPLIT,
  	       args: {
               drawingType: Descartes.Layer.POLYGON_GEOMETRY
           }
          }
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
