function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	
	
	
		
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});


		couchePolygonesStyle.title = "couche - pattern: cross";
		couchePolygonesStyle.options.symbolizers= {
			"default":{
		       "Polygon": {            
		    	   fillPatternName: "cross"
		       }
			}
	    };
		var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle.title, couchePolygonesStyle.definition, couchePolygonesStyle.options);

		couchePolygonesStyle.title = "couche - pattern: dot";
		couchePolygonesStyle.options.symbolizers= {
			"default":{
		       "Polygon": {            
		    	   fillPatternName: "dot"
		       }
			}
	    };
		var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle.title, couchePolygonesStyle.definition, couchePolygonesStyle.options);

		couchePolygonesStyle.title = "couche - pattern: hatch";
		couchePolygonesStyle.options.symbolizers= {
			"default":{
		       "Polygon": {            
		    	   fillPatternName: "hatch"
		       }
			}
	    };
		var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle.title, couchePolygonesStyle.definition, couchePolygonesStyle.options);
	

	coucheMultiPolygonesStyle.title = "couche - pattern: brick, color: red";
	coucheMultiPolygonesStyle.options.symbolizers= {
			"default":{
		       "MultiPolygon": {            
		    	   fillPatternName: "brick",
		    	   fillPatternColor: "red"
		       }
			}
	 };
	var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle.title, coucheMultiPolygonesStyle.definition, coucheMultiPolygonesStyle.options);

	coucheMultiPolygonesStyle.title = "couche - pattern: pines, color: brown, background color: green";
	coucheMultiPolygonesStyle.options.symbolizers= {
		"default":{
	       "MultiPolygon": {            
	    	   fillPatternName: "pines",
	    	   fillPatternColor: "brown",
	    	   fillPatternBackground: "rgba(0,255,0,0.5)",
	    	   fillColor: "green",
	    	   fillOpacity: 0.5
	       }
		}
 };
	var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle.title, coucheMultiPolygonesStyle.definition, coucheMultiPolygonesStyle.options);

	coucheMultiPolygonesStyle.title = "couche - pattern grass, color: green";
	coucheMultiPolygonesStyle.options.symbolizers= {
		"default":{
	       "MultiPolygon": {            
	    	   fillPatternName: "grass",
	    	   fillPatternColor: "green"
	       }
		}
 };
	var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle.title, coucheMultiPolygonesStyle.definition, coucheMultiPolygonesStyle.options);

	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));	
	contenuCarte.addItem(editionLayer1, gpWFS);
	contenuCarte.addItem(editionLayer2, gpWFS);
	contenuCarte.addItem(editionLayer3, gpWFS);

	var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 

   
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
