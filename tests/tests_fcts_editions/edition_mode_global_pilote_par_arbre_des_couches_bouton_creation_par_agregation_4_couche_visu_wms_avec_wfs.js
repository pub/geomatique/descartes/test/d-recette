function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
	//GB Modifs EPSG
	couchePointsFctAvanced.title = "Ma couche WMS de points avec WFS associé";
	coucheLignesFctAvanced.title = "Ma couche WMS de lignes avec WFS associé";
	couchePolygonesFctAvanced.title = "Ma couche WMS de polygones avec WFS associé";
	coucheMultiPointsFctAvanced.title = "Ma couche WMS de multipoints avec WFS associé";
	coucheMultiLignesFctAvanced.title = "Ma couche WMS de multilignes avec WFS associé";
	coucheMultiPolygonesFctAvanced.title = "Ma couche WMS de multipolygones avec WFS associé";

	couchePointsFctAvanced.definition[0].layerName = couchePointsFctAvanced.definition[0].featurePrefix+":"+couchePointsFctAvanced.definition[0].layerName;
	coucheLignesFctAvanced.definition[0].layerName = coucheLignesFctAvanced.definition[0].featurePrefix+":"+coucheLignesFctAvanced.definition[0].layerName;
	couchePolygonesFctAvanced.definition[0].layerName = couchePolygonesFctAvanced.definition[0].featurePrefix+":"+couchePolygonesFctAvanced.definition[0].layerName;
	coucheMultiPointsFctAvanced.definition[0].layerName = coucheMultiPointsFctAvanced.definition[0].featurePrefix+":"+coucheMultiPointsFctAvanced.definition[0].layerName;
	coucheMultiLignesFctAvanced.definition[0].layerName = coucheMultiLignesFctAvanced.definition[0].featurePrefix+":"+coucheMultiLignesFctAvanced.definition[0].layerName;
	coucheMultiPolygonesFctAvanced.definition[0].layerName = coucheMultiPolygonesFctAvanced.definition[0].featurePrefix+":"+coucheMultiPolygonesFctAvanced.definition[0].layerName;
	
	couchePointsFctAvanced.definition[0].internalProjection = "EPSG:4326";
	coucheLignesFctAvanced.definition[0].internalProjection = "EPSG:4326";
	couchePolygonesFctAvanced.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPointsFctAvanced.definition[0].internalProjection = "EPSG:4326";
	coucheMultiLignesFctAvanced.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPolygonesFctAvanced.definition[0].internalProjection = "EPSG:4326";

	couchePointsFctAvanced.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheLignesFctAvanced.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	couchePolygonesFctAvanced.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPointsFctAvanced.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiLignesFctAvanced.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPolygonesFctAvanced.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	
    var editionLayer1 = new Descartes.Layer.WMS(couchePointsFctAvanced.title, couchePointsFctAvanced.definition, couchePointsFctAvanced.options);
    var editionLayer2 = new Descartes.Layer.WMS(coucheLignesFctAvanced.title, coucheLignesFctAvanced.definition, coucheLignesFctAvanced.options);
    var editionLayer2bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneLignes.title, coucheCloneLignes.definition, coucheCloneLignes.options);
    editionLayer2bis.title="Ma couche de lignes pour l'agrégation";
    var editionLayer3 = new Descartes.Layer.WMS(couchePolygonesFctAvanced.title, couchePolygonesFctAvanced.definition, couchePolygonesFctAvanced.options);
    var editionLayer3bis = new Descartes.Layer.EditionLayer.WFS(coucheClonePolygones.title, coucheClonePolygones.definition, coucheClonePolygones.options);
    editionLayer3bis.title="Ma couche de polygones pour l'agrégation";
    
    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer2bis, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
    contenuCarte.addItem(editionLayer3bis, gpWFS);

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer4 = new Descartes.Layer.WMS(coucheMultiPointsFctAvanced.title, coucheMultiPointsFctAvanced.definition, coucheMultiPointsFctAvanced.options);
    var editionLayer4bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPoints.title, coucheCloneMultiPoints.definition, coucheCloneMultiPoints.options);
    editionLayer4bis.title="Ma couche de multi-points pour l'agrégation";
    var editionLayer5 = new Descartes.Layer.WMS(coucheMultiLignesFctAvanced.title, coucheMultiLignesFctAvanced.definition, coucheMultiLignesFctAvanced.options);
    var editionLayer5bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiLignes.title, coucheCloneMultiLignes.definition, coucheCloneMultiLignes.options);
    editionLayer5bis.title="Ma couche de multi-lignes pour l'agrégation";
    var editionLayer6 = new Descartes.Layer.WMS(coucheMultiPolygonesFctAvanced.title, coucheMultiPolygonesFctAvanced.definition, coucheMultiPolygonesFctAvanced.options);
    var editionLayer6bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones.title, coucheCloneMultiPolygones.definition, coucheCloneMultiPolygones.options);
    editionLayer6bis.title="Ma couche de multi-polygones pour l'agrégation";
    
    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer4bis, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer5bis, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    contenuCarte.addItem(editionLayer6bis, gpWFSMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_AGGREGATION}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
