function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsFctAvanced2.title, couchePointsFctAvanced2.definition, couchePointsFctAvanced2.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesFctAvanced2.title, coucheLignesFctAvanced2.definition, coucheLignesFctAvanced2.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesFctAvanced2.title, couchePolygonesFctAvanced2.definition, couchePolygonesFctAvanced2.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsFctAvanced2.title, coucheMultiPointsFctAvanced2.definition, coucheMultiPointsFctAvanced2.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesFctAvanced2.title, coucheMultiLignesFctAvanced2.definition, coucheMultiLignesFctAvanced2.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesFctAvanced2.title, coucheMultiPolygonesFctAvanced2.definition, coucheMultiPolygonesFctAvanced2.options);

    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsFctAvanced2.title, kmlCouchePointsFctAvanced2.definition, kmlCouchePointsFctAvanced2.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesFctAvanced2.title, kmlCoucheLignesFctAvanced2.definition, kmlCoucheLignesFctAvanced2.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesFctAvanced2.title, kmlCouchePolygonesFctAvanced2.definition, kmlCouchePolygonesFctAvanced2.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

	var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
    
    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsFctAvanced2.title, kmlCoucheMultiPointsFctAvanced2.definition, kmlCoucheMultiPointsFctAvanced2.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesFctAvanced2.title, kmlCoucheMultiLignesFctAvanced2.definition, kmlCoucheMultiLignesFctAvanced2.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesFctAvanced2.title, kmlCoucheMultiPolygonesFctAvanced2.definition, kmlCoucheMultiPolygonesFctAvanced2.options);

    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsFctAvanced2.title, geojsonCouchePointsFctAvanced2.definition, geojsonCouchePointsFctAvanced2.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesFctAvanced2.title, geojsonCoucheLignesFctAvanced2.definition, geojsonCoucheLignesFctAvanced2.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesFctAvanced2.title, geojsonCouchePolygonesFctAvanced2.definition, geojsonCouchePolygonesFctAvanced2.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

	var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
    
    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsFctAvanced2.title, geojsonCoucheMultiPointsFctAvanced2.definition, geojsonCoucheMultiPointsFctAvanced2.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesFctAvanced2.title, geojsonCoucheMultiLignesFctAvanced2.definition, geojsonCoucheMultiLignesFctAvanced2.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced2.title, geojsonCoucheMultiPolygonesFctAvanced2.definition, geojsonCoucheMultiPolygonesFctAvanced2.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_UNAGGREGATION_CREATION}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
