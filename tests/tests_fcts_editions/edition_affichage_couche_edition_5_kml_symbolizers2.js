function chargementCarte() {
	
	chargeEditionCouchesGroupesKML();
	
	
	
		
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
    var title =  kmlCouchePolygonesStyle.title;
    kmlCouchePolygonesStyle.title = title + " - pattern: cross";
	kmlCouchePolygonesStyle.options.symbolizers= {
		"default":{
	       "Polygon": {            
	    	   fillPatternName: "cross"
	       }
		}
    };
	var editionLayer1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesStyle.title, kmlCouchePolygonesStyle.definition, kmlCouchePolygonesStyle.options);

	kmlCouchePolygonesStyle.title = title + " - pattern: dot";
	kmlCouchePolygonesStyle.options.symbolizers= {
		"default":{
	       "Polygon": {            
	    	   fillPatternName: "dot"
	       }
		}
    };
	var editionLayer2 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesStyle.title, kmlCouchePolygonesStyle.definition, kmlCouchePolygonesStyle.options);

	kmlCouchePolygonesStyle.title = title + " - pattern: hatch";
	kmlCouchePolygonesStyle.options.symbolizers= {
		"default":{
	       "Polygon": {            
	    	   fillPatternName: "hatch"
	       }
		}
    };
	var editionLayer3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesStyle.title, kmlCouchePolygonesStyle.definition, kmlCouchePolygonesStyle.options);

var title2 =  kmlCoucheMultiPolygonesStyle.title;
kmlCoucheMultiPolygonesStyle.title = title2 + " - pattern: brick, color: red";
kmlCoucheMultiPolygonesStyle.options.symbolizers= {
		"default":{
	       "MultiPolygon": {            
	    	   fillPatternName: "brick",
	    	   fillPatternColor: "red"
	       }
		}
 };
var editionLayer4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesStyle.title, kmlCoucheMultiPolygonesStyle.definition, kmlCoucheMultiPolygonesStyle.options);

kmlCoucheMultiPolygonesStyle.title = title2 + " - pattern: pines, color: brown, background color: green";
kmlCoucheMultiPolygonesStyle.options.symbolizers= {
	"default":{
       "MultiPolygon": {            
    	   fillPatternName: "pines",
    	   fillPatternColor: "brown",
    	   fillPatternBackground: "rgba(0,255,0,0.5)",
    	   fillColor: "green",
    	   fillOpacity: 0.5
       }
	}
};
var editionLayer5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesStyle.title, kmlCoucheMultiPolygonesStyle.definition, kmlCoucheMultiPolygonesStyle.options);

kmlCoucheMultiPolygonesStyle.title = title2 + " - pattern grass, color: green";
kmlCoucheMultiPolygonesStyle.options.symbolizers= {
	"default":{
       "MultiPolygon": {            
    	   fillPatternName: "grass",
    	   fillPatternColor: "green"
       }
	}
};
var editionLayer6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesStyle.title, kmlCoucheMultiPolygonesStyle.definition, kmlCoucheMultiPolygonesStyle.options);

	
	
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	

	contenuCarte.addItem(editionLayer1, gpKML);
	contenuCarte.addItem(editionLayer2, gpKML);
	contenuCarte.addItem(editionLayer3, gpKML);

	var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	

    contenuCarte.addItem(editionLayer4, gpKMLMulti);
    contenuCarte.addItem(editionLayer5, gpKMLMulti);
    contenuCarte.addItem(editionLayer6, gpKMLMulti); 

   
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
