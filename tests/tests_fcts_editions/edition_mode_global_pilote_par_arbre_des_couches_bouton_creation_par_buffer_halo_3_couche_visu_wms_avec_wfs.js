proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
	//GB Modifs EPSG
	couchePointsFctAvanced3.title = "Ma couche WMS de points avec WFS associé";
	coucheLignesFctAvanced3.title = "Ma couche WMS de lignes avec WFS associé";
	coucheMultiPointsFctAvanced3.title = "Ma couche WMS de multipoints avec WFS associé";
	coucheMultiLignesFctAvanced3.title = "Ma couche WMS de multilignes avec WFS associé";
	couchePointsFctAvanced3.definition[0].layerName = couchePointsFctAvanced3.definition[0].featurePrefix+":"+couchePointsFctAvanced3.definition[0].layerName;
	coucheLignesFctAvanced3.definition[0].layerName = coucheLignesFctAvanced3.definition[0].featurePrefix+":"+coucheLignesFctAvanced3.definition[0].layerName;
	coucheMultiPointsFctAvanced3.definition[0].layerName = coucheMultiPointsFctAvanced3.definition[0].featurePrefix+":"+coucheMultiPointsFctAvanced3.definition[0].layerName;
	coucheMultiLignesFctAvanced3.definition[0].layerName = coucheMultiLignesFctAvanced3.definition[0].featurePrefix+":"+coucheMultiLignesFctAvanced3.definition[0].layerName;
	couchePointsFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	coucheLignesFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPointsFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	coucheMultiLignesFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	couchePointsFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheLignesFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPointsFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiLignesFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	
    var editionLayer1 = new Descartes.Layer.WMS(couchePointsFctAvanced3.title, couchePointsFctAvanced3.definition, couchePointsFctAvanced3.options);
    var editionLayer2 = new Descartes.Layer.WMS(coucheLignesFctAvanced3.title, coucheLignesFctAvanced3.definition, coucheLignesFctAvanced3.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesFctAvanced3.title, couchePolygonesFctAvanced3.definition, couchePolygonesFctAvanced3.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer4 = new Descartes.Layer.WMS(coucheMultiPointsFctAvanced3.title, coucheMultiPointsFctAvanced3.definition, coucheMultiPointsFctAvanced3.options);
    var editionLayer5 = new Descartes.Layer.WMS(coucheMultiLignesFctAvanced3.title, coucheMultiLignesFctAvanced3.definition, coucheMultiLignesFctAvanced3.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesFctAvanced3.title, coucheMultiPolygonesFctAvanced3.definition, coucheMultiPolygonesFctAvanced3.options);

    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
   	      {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION},
   	      {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION,
   	    	  args:{
   	    		 explodeMultiGeometry:false
   	    	  }
   	      },
	      {type: Descartes.Map.EDITION_DRAW_CREATION},
	      {type: Descartes.Map.EDITION_RUBBER_DELETION}
   	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
