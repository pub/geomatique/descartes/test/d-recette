function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global pilot� par l'arbre des couches
	      save: function (json) {
		  	 //Ici, code MOE qui est spécifique à chaque application métier.
		 	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		    //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		 	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		  	 sendRequestBouchonForSaveElements(json);
		
		 }
	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsStyle4.title, couchePointsStyle4.definition, couchePointsStyle4.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesStyle4.title, coucheLignesStyle4.definition, coucheLignesStyle4.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle4.title, couchePolygonesStyle4.definition, couchePolygonesStyle4.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsStyle4.title, coucheMultiPointsStyle4.definition, coucheMultiPointsStyle4.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesStyle4.title, coucheMultiLignesStyle4.definition, coucheMultiLignesStyle4.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle4.title, coucheMultiPolygonesStyle4.definition, coucheMultiPolygonesStyle4.options);

    contenuCarte.addItem(editionLayer4, gpWFS);
    contenuCarte.addItem(editionLayer5, gpWFS);
    contenuCarte.addItem(editionLayer6, gpWFS);
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsStyle4.title, kmlCouchePointsStyle4.definition, kmlCouchePointsStyle4.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesStyle4.title, kmlCoucheLignesStyle4.definition, kmlCoucheLignesStyle4.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesStyle4.title, kmlCouchePolygonesStyle4.definition, kmlCouchePolygonesStyle4.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsStyle4.title, kmlCoucheMultiPointsStyle4.definition, kmlCoucheMultiPointsStyle4.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesStyle4.title, kmlCoucheMultiLignesStyle4.definition, kmlCoucheMultiLignesStyle4.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesStyle4.title, kmlCoucheMultiPolygonesStyle4.definition, kmlCoucheMultiPolygonesStyle4.options);

    contenuCarte.addItem(editionLayerkml4, gpKML);
    contenuCarte.addItem(editionLayerkml5, gpKML);
    contenuCarte.addItem(editionLayerkml6, gpKML);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsStyle4.title, geojsonCouchePointsStyle4.definition, geojsonCouchePointsStyle4.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesStyle4.title, geojsonCoucheLignesStyle4.definition, geojsonCoucheLignesStyle4.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesStyle4.title, geojsonCouchePolygonesStyle4.definition, geojsonCouchePolygonesStyle4.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsStyle4.title, geojsonCoucheMultiPointsStyle4.definition, geojsonCoucheMultiPointsStyle4.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesStyle4.title, geojsonCoucheMultiLignesStyle4.definition, geojsonCoucheMultiLignesStyle4.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesStyle4.title, geojsonCoucheMultiPolygonesStyle4.definition, geojsonCoucheMultiPolygonesStyle4.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSON);
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
