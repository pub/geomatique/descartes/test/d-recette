var couchePoints, coucheLignes, couchePolygones, coucheBase, groupeFonds, groupeEditionWFS;
var coucheClonePoints, coucheCloneLignes, coucheClonePolygones;
var coucheMultiPoints, coucheMultiLignes, coucheMultiPolygones, groupeEditionWFSMulti;
var coucheCloneMultiPoints, coucheCloneMultiLignes, coucheCloneMultiPolygones;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeEditionCouchesGroupesWFS() {
	
	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/geoserver/descartes/ows?";

	var featureNameSpace = "descartes";
	var featurePrefix = "descartes";
	var serverVersion = "1.1.0";
	var attribution = "&#169;Serveur Descartes de tests";
	var internalProjection = "EPSG:2154";
	
	// couche de type "point"
	var layerNamePoints = "points2154";
	var featureGeometryNamePoints = "points_geom";
	var geometryTypeLayerPoints =  Descartes.Layer.POINT_GEOMETRY;
	
	// couche de type "ligne"
	var layerNameLignes = "lignes2154";
	var featureGeometryNameLignes = "lignes_geom";
	var geometryTypeLayerLignes =  Descartes.Layer.LINE_GEOMETRY;
	
	// couche de type "polygone"
	var layerNamePolygones = "polygones2154";
	var featureGeometryNamePolygones = "polygones_geom";
	var geometryTypeLayerPolygones =  Descartes.Layer.POLYGON_GEOMETRY;
	
	// couche de type "multipoint"
	var layerNameMultiPoints = "multipoints2154";
	var featureGeometryNameMultiPoints = "multipoints_geom";
	var geometryTypeLayerMultiPoints =  Descartes.Layer.MULTI_POINT_GEOMETRY;
	
	// couche de type "multiligne"
	var layerNameMultiLignes = "multilignes2154";
	var featureGeometryNameMultiLignes = "multilignes_geom";
	var geometryTypeLayerMultiLignes =  Descartes.Layer.MULTI_LINE_GEOMETRY;
	
	// couche de type "multipolygone"
	var layerNameMultiPolygones = "multipolygones2154";
	var featureGeometryNameMultiPolygones = "multipolygones_geom";
	var geometryTypeLayerMultiPolygones =  Descartes.Layer.MULTI_POLYGON_GEOMETRY;
	
	// Pour clonage
	// couche de type "point"
	var layerNameClonePoints = "clonepoints2154";
	var featureGeometryNameClonePoints = "points_geom";
	var geometryTypeLayerClonePoints =  Descartes.Layer.POINT_GEOMETRY;
	
	// couche de type "ligne"
	var layerNameCloneLignes = "clone2154lignes";
	var featureGeometryNameCloneLignes = "lignes_geom";
	var geometryTypeLayerCloneLignes =  Descartes.Layer.LINE_GEOMETRY;
	
	// couche de type "polygone"
	var layerNameClonePolygones = "clone2154polygones";
	var featureGeometryNameClonePolygones = "polygones_geom";
	var geometryTypeLayerClonePolygones =  Descartes.Layer.POLYGON_GEOMETRY;
	
	// couche de type "multipoint"
	var layerNameCloneMultiPoints = "clonemultipoints2154";
	var featureGeometryNameCloneMultiPoints = "multipoints_geom";
	var geometryTypeLayerCloneMultiPoints =  Descartes.Layer.MULTI_POINT_GEOMETRY;
	
	// couche de type "multiligne"
	var layerNameCloneMultiLignes = "clonemultilignes2154";
	var featureGeometryNameCloneMultiLignes = "multilignes_geom";
	var geometryTypeLayerCloneMultiLignes =  Descartes.Layer.MULTI_LINE_GEOMETRY;
	
	// couche de type "multipolygone"
	var layerNameCloneMultiPolygones = "clonemultipolygones2154";
	var featureGeometryNameCloneMultiPolygones = "multipolygones_geom";
	var geometryTypeLayerCloneMultiPolygones =  Descartes.Layer.MULTI_POLYGON_GEOMETRY;
	
	/*****************************************************
	  couches pour affichage simple
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
		
	couchePoints = {
	    title: "Ma couche WFS de points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNamePoints,
	                	 featureServerUrl: serveur,
	                	 featureName: layerNamePoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNamePoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	couchePoints2 = {
	    title: "Ma couche WFS de points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNamePoints,
	                 	 featureServerUrl: serveur,
	                	 featureName: layerNamePoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNamePoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 10000,
	        minEditionScale: 4000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	couchePoints3 = {
	    title: "Ma couche WFS de points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNamePoints,
	                	 imageServerUrl:serveur,
	                	 imageLayerName: featurePrefix+":"+layerNamePoints,
	                	 featureServerUrl: serveur,
	                	 featureName: layerNamePoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNamePoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	                 }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 50000,
	        minEditionScale: 2000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};

	coucheLignes = {
	    title: "Ma couche WFS de lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameLignes,
					featureServerUrl: serveur,
					featureName: layerNameLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	couchePolygones = {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	  featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
		
	coucheMultiPoints = {
	    title: "Ma couche WFS de  multi points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNameMultiPoints,
	                	 featureServerUrl: serveur,
	                	 featureName: layerNameMultiPoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNameMultiPoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};
	
	coucheMultiPoints2 = {
	    title: "Ma couche WFS de multi points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNameMultiPoints,
	                 	 featureServerUrl: serveur,
	                	 featureName: layerNameMultiPoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNameMultiPoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 10000,
	        minEditionScale: 4000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};
	
	coucheMultiPoints3 = {
	    title: "Ma couche WFS de multi points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNameMultiPoints,
	                	 imageServerUrl:serveur,
	                	 imageLayerName: featurePrefix+":"+layerNameMultiPoints,
	                	 featureServerUrl: serveur,
	                	 featureName: layerNameMultiPoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNameMultiPoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	                 }
	    ],
	    options: {
	        attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	                {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
	            ]
	        },
	        maxScale: 10000, 
	        minScale: 4000000, 
	        maxEditionScale: 50000,
	        minEditionScale: 2000000,
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};

	coucheMultiLignes = {
	    title: "Ma couche WFS de multi lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameMultiLignes,
					featureServerUrl: serveur,
					featureName: layerNameMultiLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameMultiLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	coucheMultiPolygones = {
	    title: "Ma couche WFS de multi polygones - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameMultiPolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameMultiPolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameMultiPolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};
	
	/*****************************************************
	  couches pour la modification des styles d'affichage
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	couchePointsStyle = {
	    title: "Ma couche WFS de points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNamePoints,              
	                	 featureServerUrl: serveur,
	                	 featureName: layerNamePoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNamePoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	                 }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
					           "Point": {
					               fillColor: "blue",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "blue",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
				           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	coucheLignesStyle = {
	    title: "Ma couche WFS de lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameLignes,
					featureServerUrl: serveur,
					featureName: layerNameLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
	        	"default":   {//pour affichage
					           "Line": {
					               fillColor: "red",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "red",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	couchePolygonesStyle = {
	    title: "Ma couche WFS de polygones - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNamePolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNamePolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNamePolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
	            		      "Polygon": {
					               fillColor: "yellow",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "yellow",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPolygones
	    }
	};	
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	coucheMultiPointsStyle = {
	    title: "Ma couche WFS de multi points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNameMultiPoints,              
	                	 featureServerUrl: serveur,
	                	 featureName: layerNameMultiPoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNameMultiPoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	                 }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
					           "MultiPoint": {
					               fillColor: "orange",
					               graphicName:"star",
					               points: 5,
					               radius: 8,
					               radius2: 4,
					               angle: 0,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "orange",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 8
				           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPoints
	    }
	};
	
	coucheMultiLignesStyle = {
	    title: "Ma couche WFS de multi lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameMultiLignes,
					featureServerUrl: serveur,
					featureName: layerNameMultiLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameMultiLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
	        	"default":   {//pour affichage
					           "MultiLine": {
					               fillColor: "purple",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "purple",
					               strokeOpacity: 1,
					               strokeWidth: 6,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	coucheMultiPolygonesStyle = {
	    title: "Ma couche WFS de multi polygones - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameMultiPolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameMultiPolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameMultiPolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"default":   {//pour affichage
	            		      "MultiPolygon": {
					               fillColor: "pink",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "pink",
					               strokeOpacity: 1,
					               strokeWidth: 8,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
	        	}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};	
	
	/***************************************************
	  couches pour la modification des styles d'édition
	 ***************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	couchePointsStyle2 = {
	    title: "Ma couche WFS de points - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNamePoints,              
	                	 featureServerUrl: serveur,
	                	 featureName: layerNamePoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNamePoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	                 }
	    ],
	    options: {
	        symbolizers: {
				"temporary":   {//pour DrawCreation
					           "Point": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           }
				}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPoints
	    }
	};
	
	coucheLignesStyle2 = {
	    title: "Ma couche WFS de lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameLignes,
					featureServerUrl: serveur,
					featureName: layerNameLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
			}
	    ],
	    options: {
	    	 attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        symbolizers: {
		       "temporary":   {//pour DrawCreation
					           "Line": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       }
	        },    
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	couchePolygonesStyle2 = {
	    title: "Ma couche WFS de polygones - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNamePolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNamePolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNamePolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	        symbolizers: {
	        	"temporary":   {//pour DrawCreation
					           "Polygon": {
					               fillColor: "grey",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
				}
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerPolygones
	    }
	};	
	
	couchePointsStyle3 = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,              
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	 featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		                }
		   ],
		   options: {
		       symbolizers: {
					"temporary":   {//pour DrawCreation
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
					},
					"select":   { //pour GlobalModication
						           "Point": {
						               fillColor: "green",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
					}
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
		
		coucheLignesStyle3 = {
		   title: "Ma couche WFS de lignes - EPSG:2154",
		   type: 10,
		   definition: [
		       {
		           	serverUrl: serveur,
						layerName: layerNameLignes,
						featureServerUrl: serveur,
						featureName: layerNameLignes,
						featureNameSpace: featureNameSpace,
						featurePrefix: featurePrefix,
						featureGeometryName: featureGeometryNameLignes,
						serverVersion: serverVersion,
	                	 internalProjection: internalProjection
				}
		   ],
		   options: {
		   	 attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		           ]
		       },
		       symbolizers: {
			       "temporary":   {//pour DrawCreation
						           "Line": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
			       },
				   "select":   {//pour GlobalModication
						           "Line": {
						               fillColor: "green",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
				   },
				   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:4,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
				   }
		       },    
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
		
		couchePolygonesStyle3 = {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	  featureNameSpace: featureNameSpace,
		           	  featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       symbolizers: {
		       	"temporary":   {//pour DrawCreation
						           "Polygon": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					},
					"select":   {//pour GlobalModication
				    		      "Polygon": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					},
				   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 6
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
				   }
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};	
		couchePointsStyle4 = {
			   title: "Ma couche WFS de points - EPSG:2154",
			   type: 10,
			   definition: [
			                {	            	
			               	 serverUrl: serveur,
			               	 layerName: layerNamePoints,              
			               	 featureServerUrl: serveur,
			               	 featureName: layerNamePoints,
			               	 featureNameSpace: featureNameSpace,
			                 featurePrefix: featurePrefix,
			               	 featureGeometryName: featureGeometryNamePoints,
			               	 serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
			                }
			   ],
			   options: {
			       symbolizers: {
						"temporary":   {//pour DrawCreation
							           "Point": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"create":   {//pour DrawCreation, GlobalModication
							           "Point": {
							               fillColor: "black",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"select":   { //pour GlobalModication
							           "Point": {
							               fillColor: "green",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						},
						"delete":   { //pour RubberDeletion
				           "Point": {
				               fillColor: "grey",
				               graphicName:"square",
				               points:4,
				               radius:4,
				               angle:Math.PI / 4,
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "grey",
				               strokeOpacity: 1,
				               strokeWidth: 1,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid",			            
				               pointRadius: 4
				           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPoints
			   }
			};
			
			coucheLignesStyle4 = {
			   title: "Ma couche WFS de lignes - EPSG:2154",
			   type: 10,
			   definition: [
			       {
			           	serverUrl: serveur,
							layerName: layerNameLignes,
							featureServerUrl: serveur,
							featureName: layerNameLignes,
							featureNameSpace: featureNameSpace,
							featurePrefix: featurePrefix,
							featureGeometryName: featureGeometryNameLignes,
							serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "temporary":   {//pour DrawCreation
							           "Line": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
				       },
				       "create":   {//pour DrawCreation, GlobalModication
							           "Line": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
					   "select":   {//pour GlobalModication
							           "Line": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   },
					   "modify":   {//pour GlobalModication, VerticeModification
								   	"Point": {
							       			fillColor: "green",
							               graphicName:"cross",
							               points:4,
							               radius:6,
							               radius2:0,
							               angle:0,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           },
							           "VirtualPoint": { //pour VerticeModification
							           	 cursor: "pointer",
							                graphicName: "cross",
							                fillColor:"yellow",
							                fillOpacity:1,
							                pointRadius:4,
							                strokeColor:"yellow",
							                strokeDashstyle:"solid",
							                strokeOpacity:1,
							                strokeWidth:1
							           }
					   },
					   "delete":   {//pour RubberDeletion
							           "Line": {
							               fillColor: "grey",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerLignes
			   }
			};
			
			couchePolygonesStyle4 = {
			   title: "Ma couche WFS de polygones - EPSG:2154",
			   type: 10,
			   definition: [
			                {
			           	  serverUrl: serveur,
			           	  layerName: layerNamePolygones,
			           	  featureServerUrl: serveur,
			           	  featureName: layerNamePolygones,
			           	  featureNameSpace: featureNameSpace,
			           	  featurePrefix: featurePrefix,
			           	  featureGeometryName: featureGeometryNamePolygones,
			           	  serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
			       }
			   ],
			   options: {
			       symbolizers: {
			       	"temporary":   {//pour DrawCreation
							           "Polygon": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
						"select":   {//pour GlobalModication
					    		      "Polygon": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						},
					   "modify":   {//pour GlobalModication, VerticeModification
								   	"Point": {
							       		fillColor: "green",
							               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 6
							           },
							           "VirtualPoint": { //pour VerticeModification
							           	 cursor: "pointer",
							                graphicName: "cross",
							                fillColor:"yellow",
							                fillOpacity:1,
							                pointRadius:4,
							                strokeColor:"yellow",
							                strokeDashstyle:"solid",
							                strokeOpacity:1,
							                strokeWidth:1
							           }
					   },
						"delete":   {//pour RubberDeletion
					    		      "Polygon": {
							               fillColor: "grey",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPolygones
			   }
			};	
			
		couchePointsStyle5 = {
			   title: "Ma couche WFS de points - EPSG:2154",
			   type: 10,
			   definition: [
			                {	            	
			               	 serverUrl: serveur,
			               	 layerName: layerNamePoints,              
			               	 featureServerUrl: serveur,
			               	 featureName: layerNamePoints,
			               	 featureNameSpace: featureNameSpace,
			               	 featurePrefix: featurePrefix,
			               	 featureGeometryName: featureGeometryNamePoints,
			               	 serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
			                }
			   ],
			   options: {
			       symbolizers: {
						"create":   {//pour DrawCreation, GlobalModication
							           "Point": {
							               fillColor: "black",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPoints
			   }
			};
			
			coucheLignesStyle5 = {
			   title: "Ma couche WFS de lignes - EPSG:2154",
			   type: 10,
			   definition: [
			       {
			           	serverUrl: serveur,
							layerName: layerNameLignes,
							featureServerUrl: serveur,
							featureName: layerNameLignes,
							featureNameSpace: featureNameSpace,
							featurePrefix: featurePrefix,
							featureGeometryName: featureGeometryNameLignes,
							serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "create":   {//pour DrawCreation, GlobalModication
							           "Line": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					   }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerLignes
			   }
			};
			
			couchePolygonesStyle5 = {
			   title: "Ma couche WFS de polygones - EPSG:2154",
			   type: 10,
			   definition: [
			                {
			           	  serverUrl: serveur,
			           	  layerName: layerNamePolygones,
			           	  featureServerUrl: serveur,
			           	  featureName: layerNamePolygones,
			           	  featureNameSpace: featureNameSpace,
			           	  featurePrefix: featurePrefix,
			           	  featureGeometryName: featureGeometryNamePolygones,
			           	  serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
			       }
			   ],
			   options: {
			       symbolizers: {
						"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						}
			       },
			       clone: {
			           supportLayers: [{
		                   id: "coucheMultiPolygones",
		                   attributes: [{
		                           from: "d_attrib_2",
		                           to: "d_attrib_2"
		                       },
		                       {
		                           from: "d_attrib_3",
		                           to: "d_attrib_3"
		                       }
		                   ]
		               }]
			       },
			       copy: {
			   		supportLayersIdentifier: ["coucheMultiPolygones"]
			       },
			       buffer: {
			       	supportLayers: [{id:"coucheMultiPolygones"}],
			       	distance:20000/*,
			           enable: true*/
			       }, 
			       halo: {
			       	supportLayers: [{id:"coucheMultiPolygones"}],
			       	distance:20000/*,
			           enable: true*/
			       }, 
			       homothetic: {
			           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
			           enable: true*/
			       }, 
			       split: {
			           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
			           enable: true*/
			       }, 
			       divide: {
			           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
			           enable: true*/
			       },
			       aggregate: {
			           supportLayersIdentifier: ["coucheMultiPolygones"],
			           enable: true
			       },
			       substract: {
			           supportLayersIdentifier: ["coucheMultiPolygones"],
			           enable: true
			       },
			       intersect: {
			       	supportLayersIdentifier:["coucheMultiPolygones"],
			           enable: true
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerPolygones
			   }
			};	
			
			couchePointsStyle6 = {
				   title: "Ma couche WFS de points - EPSG:2154",
				   type: 10,
				   definition: [
				                {	            	
				               	 serverUrl: serveur,
				               	 layerName: layerNamePoints,              
				               	 featureServerUrl: serveur,
				               	 featureName: layerNamePoints,
				               	 featureNameSpace: featureNameSpace,
				                 featurePrefix: featurePrefix,
				               	 featureGeometryName: featureGeometryNamePoints,
				               	 serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
				                }
				   ],
				   options: {
				   	id: "couchePoints",
				       symbolizers: {
				       	"default":   {//pour affichage
					           "Point": {
					               fillColor: "blue",
					               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "blue",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
						           }
					       	},
					       	"create":   {//pour DrawCreation, GlobalModication
						           "Point": {
						               fillColor: "black",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
				       		},
				       		"temporary":   {//pour DrawCreation
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
				       		},
								"select":   { //pour GlobalModication
									           "Point": {
									               fillColor: "green",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"delete":   { //pour RubberDeletion
						           "Point": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
				       },
				       copy: {
				       	supportLayersIdentifier: ["coucheMultiPoints"],
				       	enable:true
				       },
				       clone: {
				       	supportLayers: [{
			                   id: "coucheMultiPoints",
			                   attributes: [{
				                       from: "d_attrib_2",
				                       to: "d_attrib_2"
				                   },
				                   {
				                       from: "d_attrib_3",
				                       to: "d_attrib_3"
				                   }
			                   ]
			               }],
				       	enable:true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["coucheMultiPoints"]
				       },
				       intersect: {
				       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerPoints
				   }
				};
				
				coucheLignesStyle6 = {
				   title: "Ma couche WFS de lignes - EPSG:2154",
				   type: 10,
				   definition: [
				       {
				           	serverUrl: serveur,
								layerName: layerNameLignes,
								featureServerUrl: serveur,
								featureName: layerNameLignes,
								featureNameSpace: featureNameSpace,
								featurePrefix: featurePrefix,
								featureGeometryName: featureGeometryNameLignes,
								serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
						}
				   ],
				   options: {
				   	id: "coucheLignes",
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: {
				       	"default":   {//pour affichage
					           "Line": {
					               fillColor: "red",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               pointerEvents: "visiblePainted",
					               cursor: "pointer",
					               strokeColor: "red",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
								           }
					       },
					       "create":   {//pour DrawCreation, GlobalModication
						           "Line": {
						               fillColor: "black",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "black",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
						   },
						   "temporary":   {//pour DrawCreation
						         "Line": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						         }
						   },
						   "select":   {//pour GlobalModication
					           "Line": {
					               fillColor: "green",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   },
						   "modify":   {//pour GlobalModication, VerticeModification
						   	"Point": {
					       		fillColor: "green",
					               graphicName:"cross",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 4
					           },
					           "VirtualPoint": { //pour VerticeModification
					           	 cursor: "pointer",
					                graphicName: "cross",
					                fillColor:"yellow",
					                fillOpacity:1,
					                pointRadius:4,
					                strokeColor:"yellow",
					                strokeDashstyle:"solid",
					                strokeOpacity:1,
					                strokeWidth:1
					           }
						   },
						   "delete":   {//pour RubberDeletion
					           "Line": {
					               fillColor: "grey",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
						   }
				       },  
				       copy: {
				       	supportLayersIdentifier: ["coucheMultiLignes"],
				       	enable:true
				       },
				       clone: {
				       	supportLayers: [{
			                   id: "coucheMultiLignes",
			                   attributes: [{
				                       from: "d_attrib_2",
				                       to: "d_attrib_2"
				                   },
				                   {
				                       from: "d_attrib_3",
				                       to: "d_attrib_3"
				                   }
			                   ]
			               }],
				       	enable:true
				       },
				       aggregate: {
				       	supportLayersIdentifier: ["coucheMultiLignes"],
				       	enable:true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["coucheMultiLignes"]
				       },
				       split: {
				       	supportLayersIdentifier: ["coucheMultiLignes"]
				       },
				       divide: {
				       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"]
				       },
				       substract: {
				       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"]
				       },
				       intersect: {
				       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerLignes
				   }
				};
				
				couchePolygonesStyle6 = {
				   title: "Ma couche WFS de polygones - EPSG:2154",
				   type: 10,
				   definition: [
				                {
				           	  serverUrl: serveur,
				           	  layerName: layerNamePolygones,
				           	  featureServerUrl: serveur,
				           	  featureName: layerNamePolygones,
				           	  featureNameSpace: featureNameSpace,
				           	  featurePrefix: featurePrefix,
				           	  featureGeometryName: featureGeometryNamePolygones,
				           	  serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
				       }
				   ],
				   options: {
				   	id: "couchePolygones",
				       symbolizers: {
				       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
				       	},"create":   {//pour DrawCreation, GlobalModication
					    		      "Polygon": {
							               fillColor: "black",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "black",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
							},
							"temporary":   {//pour DrawCreation
					           "Polygon": {
					               fillColor: "grey",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
					               fillOpacity: 0.4,
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "grey",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
							},
							"select":   {//pour GlobalModication
				    		      "Polygon": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							},
						   "modify":   {//pour GlobalModication, VerticeModification
						   	"Point": {
					       		fillColor: "green",
					               graphicName:"cross",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "green",
					               strokeOpacity: 1,
					               strokeWidth: 1,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid",			            
					               pointRadius: 6
					           },
					           "VirtualPoint": { //pour VerticeModification
					           	 cursor: "pointer",
					                graphicName: "cross",
					                fillColor:"yellow",
					                fillOpacity:1,
					                pointRadius:4,
					                strokeColor:"yellow",
					                strokeDashstyle:"solid",
					                strokeOpacity:1,
					                strokeWidth:1
					           }
						   },
							"delete":   {//pour RubberDeletion
				    		      "Polygon": {
						               fillColor: "grey",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							}
				       },
				       clone: {
				           supportLayers: [{
			                   id: "coucheMultiPolygones",
			                   attributes: [{
			                           from: "d_attrib_2",
			                           to: "d_attrib_2"
			                       },
			                       {
			                           from: "d_attrib_3",
			                           to: "d_attrib_3"
			                       }
			                   ]
			               }]
				       },
				       copy: {
				   		supportLayersIdentifier: ["coucheMultiPolygones"]
				       },
				       buffer: {
				       	supportLayers: [{id:"coucheMultiPolygones"}],
				       	distance:20000/*,
				           enable: true*/
				       }, 
				       halo: {
				       	supportLayers: [{id:"coucheMultiPoints"},{id:"coucheMultiLignes"}],
				       	distance:20000/*,
				           enable: true*/
				       }, 
				       homothetic: {
				           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
				           enable: true*/
				       }, 
				       split: {
				           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
				           enable: true*/
				       }, 
				       divide: {
				           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
				           enable: true*/
				       },
				       aggregate: {
				           supportLayersIdentifier: ["coucheMultiPolygones"],
				           enable: true
				       },
				       unaggregate: {
				       	supportLayersIdentifier: ["coucheMultiPolygones"]
				       },
				       substract: {
				           supportLayersIdentifier: ["coucheMultiPolygones"],
				           enable: true
				       },
				       intersect: {
				       	supportLayersIdentifier:["coucheMultiPolygones"],
				           enable: true
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerPolygones
				   }
				};	
			
			//---------------------------------------------------------//
			//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
			//---------------------------------------------------------//
			
			coucheMultiPointsStyle2 = {
			   title: "Ma couche WFS de multi points - EPSG:2154",
			   type: 10,
			   definition: [
			                {	            	
			               	 serverUrl: serveur,
			               	 layerName: layerNameMultiPoints,              
			               	 featureServerUrl: serveur,
			               	 featureName: layerNameMultiPoints,
			               	 featureNameSpace: featureNameSpace,
			                 featurePrefix: featurePrefix,
			               	 featureGeometryName: featureGeometryNameMultiPoints,
			               	 serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
			                }
			   ],
			   options: {
			       symbolizers: {
						"temporary":   {//pour DrawCreation
							           "MultiPoint": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid",			            
							               pointRadius: 4
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiPoints
			   }
			};
			
			coucheMultiLignesStyle2 = {
			   title: "Ma couche WFS de multi lignes - EPSG:2154",
			   type: 10,
			   definition: [
			       {
			           	serverUrl: serveur,
							layerName: layerNameMultiLignes,
							featureServerUrl: serveur,
							featureName: layerNameMultiLignes,
							featureNameSpace: featureNameSpace,
							featurePrefix: featurePrefix,
							featureGeometryName: featureGeometryNameMultiLignes,
							serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
					}
			   ],
			   options: {
			   	 attributes: {
			           /*attributeId: {
			               fieldName: "d_attrib_1"
			           },*/
			           attributesEditable: [
			               {fieldName: 'd_attrib_2', label: 'Un attribut'},
			           ]
			       },
			       symbolizers: {
				       "temporary":   {//pour DrawCreation
							           "MultiLine": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
				       }
			       },    
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiLignes
			   }
			};
			
			coucheMultiPolygonesStyle2 = {
			   title: "Ma couche WFS de multipolygones - EPSG:2154",
			   type: 10,
			   definition: [
			                {
			           	  serverUrl: serveur,
			           	  layerName: layerNameMultiPolygones,
			           	  featureServerUrl: serveur,
			           	  featureName: layerNameMultiPolygones,
			           	  featureNameSpace: featureNameSpace,
			           	featurePrefix: featurePrefix,
			           	  featureGeometryName: featureGeometryNameMultiPolygones,
			           	  serverVersion: serverVersion,
		                	 internalProjection: internalProjection	
			       }
			   ],
			   options: {
			       symbolizers: {
			       	"temporary":   {//pour DrawCreation
							           "MultiPolygon": {
							               fillColor: "grey",
							               graphicName:"square",
							               points:4,
							               radius:4,
							               angle:Math.PI / 4,
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "grey",
							               strokeOpacity: 1,
							               strokeWidth: 1,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
						}
			       },
			       alwaysVisible: false,
			       visible: true,
			       queryable: true,
			       activeToQuery: false,
			       sheetable: true,
			       opacity: 100,
			       opacityMax: 100,
			       legend: null,
			       metadataURL: null,
			       format: "image/png",
			       displayOrder: 1,
			       geometryType: geometryTypeLayerMultiPolygones
			   }
			};	
			
			coucheMultiPointsStyle3 = {
				   title: "Ma couche WFS de multi points - EPSG:2154",
				   type: 10,
				   definition: [
				                {	            	
				               	 serverUrl: serveur,
				               	 layerName: layerNameMultiPoints,              
				               	 featureServerUrl: serveur,
				               	 featureName: layerNameMultiPoints,
				               	 featureNameSpace: featureNameSpace,
				               	featurePrefix: featurePrefix,
				               	 featureGeometryName: featureGeometryNameMultiPoints,
				               	 serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
				                }
				   ],
				   options: {
				       symbolizers: {
							"temporary":   {//pour DrawCreation
								           "MultiPoint": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							},
							"select":   { //pour GlobalModication
								           "Point": {
								               fillColor: "green",
								               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           },
								           "MultiPoint": {
								               fillColor: "green",
								               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPoints
				   }
				};
				
				coucheMultiLignesStyle3 = {
				   title: "Ma couche WFS de multi lignes - EPSG:2154",
				   type: 10,
				   definition: [
				       {
				           	serverUrl: serveur,
								layerName: layerNameMultiLignes,
								featureServerUrl: serveur,
								featureName: layerNameMultiLignes,
								featureNameSpace: featureNameSpace,
								featurePrefix: featurePrefix,
								featureGeometryName: featureGeometryNameMultiLignes,
								serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
						}
				   ],
				   options: {
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: {
					       "temporary":   {//pour DrawCreation
								           "MultiLine": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
					       },
						   "select":   {//pour GlobalModication
								           "MultiLine": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
						   },
						   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
						   }
				       },    
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiLignes
				   }
				};
				
				coucheMultiPolygonesStyle3 = {
				   title: "Ma couche WFS de multi polygones - EPSG:2154",
				   type: 10,
				   definition: [
				                {
				           	  serverUrl: serveur,
				           	  layerName: layerNameMultiPolygones,
				           	  featureServerUrl: serveur,
				           	  featureName: layerNameMultiPolygones,
				           	  featureNameSpace: featureNameSpace,
				           	featurePrefix: featurePrefix,
				           	  featureGeometryName: featureGeometryNameMultiPolygones,
				           	  serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
				       }
				   ],
				   options: {
				       symbolizers: {
				       	"temporary":   {//pour DrawCreation
								           "MultiPolygon": {
								               fillColor: "grey",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "grey",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
							"select":   {//pour GlobalModication
						    		      "Polygon": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           },
								           "MultiPolygon": {
								               fillColor: "green",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							},
						   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 6
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
						   }
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPolygones
				   }
				};	
				
				coucheMultiPointsStyle4 = {
					   title: "Ma couche WFS de multi points - EPSG:2154",
					   type: 10,
					   definition: [
					                {	            	
					               	 serverUrl: serveur,
					               	 layerName: layerNameMultiPoints,              
					               	 featureServerUrl: serveur,
					               	 featureName: layerNameMultiPoints,
					               	 featureNameSpace: featureNameSpace,
					               	featurePrefix: featurePrefix,
					               	 featureGeometryName: featureGeometryNameMultiPoints,
					               	 serverVersion: serverVersion,
				                	 internalProjection: internalProjection			
					                }
					   ],
					   options: {
					       symbolizers: {
								"temporary":   {//pour DrawCreation
									           "MultiPoint": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"create":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"select":   { //pour GlobalModication
									           "Point": {
									               fillColor: "green",
									               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: true,
					       activeToQuery: false,
					       sheetable: true,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPoints
					   }
					};
					
					coucheMultiLignesStyle4 = {
					   title: "Ma couche WFS de multi lignes - EPSG:2154",
					   type: 10,
					   definition: [
					       {
					           	serverUrl: serveur,
									layerName: layerNameMultiLignes,
									featureServerUrl: serveur,
									featureName: layerNameMultiLignes,
									featureNameSpace: featureNameSpace,
									featurePrefix: featurePrefix,
									featureGeometryName: featureGeometryNameMultiLignes,
									serverVersion: serverVersion,
				                	 internalProjection: internalProjection			
							}
					   ],
					   options: {
					   	 attributes: {
					           /*attributeId: {
					               fieldName: "d_attrib_1"
					           },*/
					           attributesEditable: [
					               {fieldName: 'd_attrib_2', label: 'Un attribut'},
					           ]
					       },
					       symbolizers: {
						       "temporary":   {//pour DrawCreation
									           "MultiLine": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
						       },
						       "create":   {//pour DrawCreation, GlobalModication
									           "MultiLine": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
							   "select":   {//pour GlobalModication
									           "Line": {
									               fillColor: "green",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
							   "modify":   {//pour GlobalModication, VerticeModification
										   	"Point": {
									       		fillColor: "green",
									               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           },
									           "VirtualPoint": { //pour VerticeModification
									           	 cursor: "pointer",
									                graphicName: "cross",
									                fillColor:"yellow",
									                fillOpacity:1,
									                pointRadius:4,
									                strokeColor:"yellow",
									                strokeDashstyle:"solid",
									                strokeOpacity:1,
									                strokeWidth:1
									           }
							   },
							   "delete":   {//pour RubberDeletion
									           "MultiLine": {
									               fillColor: "grey",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   }
					       },    
					       alwaysVisible: false,
					       visible: true,
					       queryable: true,
					       activeToQuery: false,
					       sheetable: true,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiLignes
					   }
					};
					
					coucheMultiPolygonesStyle4 = {
					   title: "Ma couche WFS de multi polygones - EPSG:2154",
					   type: 10,
					   definition: [
					                {
					           	  serverUrl: serveur,
					           	  layerName: layerNameMultiPolygones,
					           	  featureServerUrl: serveur,
					           	  featureName: layerNameMultiPolygones,
					           	  featureNameSpace: featureNameSpace,
					           	featurePrefix: featurePrefix,
					           	  featureGeometryName: featureGeometryNameMultiPolygones,
					           	  serverVersion: serverVersion,
				                	 internalProjection: internalProjection			
					       }
					   ],
					   options: {
					       symbolizers: {
					       	"temporary":   {//pour DrawCreation
									           "MultiPolygon": {
									               fillColor: "grey",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
								"create":   {//pour DrawCreation, GlobalModication
							    		      "MultiPolygon": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
								"select":   {//pour GlobalModication
							    		      "Polygon": {
									               fillColor: "green",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								},
							   "modify":   {//pour GlobalModication, VerticeModification
										   	"Point": {
									       		fillColor: "green",
									               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "green",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 6
									           },
									           "VirtualPoint": { //pour VerticeModification
									           	 cursor: "pointer",
									                graphicName: "cross",
									                fillColor:"yellow",
									                fillOpacity:1,
									                pointRadius:4,
									                strokeColor:"yellow",
									                strokeDashstyle:"solid",
									                strokeOpacity:1,
									                strokeWidth:1
									           }
							   },
								"delete":   {//pour RubberDeletion
							    		      "MultiPolygon": {
									               fillColor: "grey",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "grey",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
								}
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: true,
					       activeToQuery: false,
					       sheetable: true,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPolygones
					   }
					};	
			coucheMultiPointsStyle5 = {
				   title: "Ma couche WFS de multi points - EPSG:2154",
				   type: 10,
				   definition: [
				                {	            	
				               	 serverUrl: serveur,
				               	 layerName: layerNameMultiPoints,              
				               	 featureServerUrl: serveur,
				               	 featureName: layerNameMultiPoints,
				               	 featureNameSpace: featureNameSpace,
				               	featurePrefix: featurePrefix,
				               	 featureGeometryName: featureGeometryNameMultiPoints,
				               	 serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
				                }
				   ],
				   options: {
				       symbolizers: {
							"create":   {//pour DrawCreation, GlobalModication
								           "MultiPoint": {
								               fillColor: "black",
								               graphicName:"square",
								               points:4,
								               radius:4,
								               angle:Math.PI / 4,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 4
								           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPoints
				   }
				};
				
				coucheMultiLignesStyle5 = {
				   title: "Ma couche WFS de multi lignes - EPSG:2154",
				   type: 10,
				   definition: [
				       {
				           	serverUrl: serveur,
								layerName: layerNameMultiLignes,
								featureServerUrl: serveur,
								featureName: layerNameMultiLignes,
								featureNameSpace: featureNameSpace,
								featurePrefix: featurePrefix,
								featureGeometryName: featureGeometryNameMultiLignes,
								serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
						}
				   ],
				   options: {
				   	 attributes: {
				           /*attributeId: {
				               fieldName: "d_attrib_1"
				           },*/
				           attributesEditable: [
				               {fieldName: 'd_attrib_2', label: 'Un attribut'},
				           ]
				       },
				       symbolizers: { 
					       "create":   {//pour DrawCreation, GlobalModication
								           "MultiLine": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
						   }
				       },    
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiLignes
				   }
				};
				
				coucheMultiPolygonesStyle5 = {
				   title: "Ma couche WFS de multi polygones - EPSG:2154",
				   type: 10,
				   definition: [
				                {
				           	  serverUrl: serveur,
				           	  layerName: layerNameMultiPolygones,
				           	  featureServerUrl: serveur,
				           	  featureName: layerNameMultiPolygones,
				           	  featureNameSpace: featureNameSpace,
				           	featurePrefix: featurePrefix,
				           	  featureGeometryName: featureGeometryNameMultiPolygones,
				           	  serverVersion: serverVersion,
			                	 internalProjection: internalProjection		
				       }
				   ],
				   options: {
				   	id: "coucheMultiPolygones",
				       symbolizers: {
							"create":   {//pour DrawCreation, GlobalModication
								           "MultiPolygon": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
							}
				       },
				       alwaysVisible: false,
				       visible: true,
				       queryable: true,
				       activeToQuery: false,
				       sheetable: true,
				       opacity: 100,
				       opacityMax: 100,
				       legend: null,
				       metadataURL: null,
				       format: "image/png",
				       displayOrder: 1,
				       geometryType: geometryTypeLayerMultiPolygones
				   }
				};	
				
				coucheMultiPointsStyle6 = {
					   title: "Ma couche WFS de multi points - EPSG:2154",
					   type: 10,
					   definition: [
					                {	            	
					               	 serverUrl: serveur,
					               	 layerName: layerNameMultiPoints,              
					               	 featureServerUrl: serveur,
					               	 featureName: layerNameMultiPoints,
					               	 featureNameSpace: featureNameSpace,
					               	featurePrefix: featurePrefix,
					               	 featureGeometryName: featureGeometryNameMultiPoints,
					               	 serverVersion: serverVersion,
				                	 internalProjection: internalProjection			
					                }
					   ],
					   options: {
					   	id: "coucheMultiPoints",
					       symbolizers: {
					       	"default":   {//pour affichage
						           "MultiPoint": {
						               fillColor: "orange",
						               graphicName:"star",
						               points: 5,
						               radius: 8,
						               radius2: 4,
						               angle: 0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "orange",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 8
						           }
					       	},
					       	"create":   {//pour DrawCreation, GlobalModication
									           "MultiPoint": {
									               fillColor: "black",
									               graphicName:"square",
									               points:4,
									               radius:4,
									               angle:Math.PI / 4,
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 1,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid",			            
									               pointRadius: 4
									           }
								},
								"temporary":   {//pour DrawCreation
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								},
								"select":   { //pour GlobalModication
						           "Point": {
						               fillColor: "green",
						               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "MultiPoint": {
						               fillColor: "green",
						               graphicName:"square",
					               points:4,
					               radius:4,
					               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       copy: {
					       	supportLayersIdentifier: ["couchePoints"],
					       	enable:true
					       },
					       clone: {
					       	supportLayers: [{
				                   id: "couchePoints",
				                   attributes: [{
					                       from: "d_attrib_2",
					                       to: "d_attrib_2"
					                   },
					                   {
					                       from: "d_attrib_3",
					                       to: "d_attrib_3"
					                   }
				                   ]
				               }],
					       	enable:true
					       },
					       aggregate: {
					       	supportLayersIdentifier: ["couchePoints"],
					       	enable:true
					       },
					       intersect: {
					       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
					           enable: true
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: true,
					       activeToQuery: false,
					       sheetable: true,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPoints
					   }
					};
					
					coucheMultiLignesStyle6 = {
					   title: "Ma couche WFS de multi lignes - EPSG:2154",
					   type: 10,
					   definition: [
					       {
					           	serverUrl: serveur,
									layerName: layerNameMultiLignes,
									featureServerUrl: serveur,
									featureName: layerNameMultiLignes,
									featureNameSpace: featureNameSpace,
									featurePrefix: featurePrefix,
									featureGeometryName: featureGeometryNameMultiLignes,
									serverVersion: serverVersion,
				                	 internalProjection: internalProjection			
							}
					   ],
					   options: {
					   	id: "coucheMultiLignes",
					   	 attributes: {
					           /*attributeId: {
					               fieldName: "d_attrib_1"
					           },*/
					           attributesEditable: [
					               {fieldName: 'd_attrib_2', label: 'Un attribut'},
					           ]
					       },
					       symbolizers: { 
					       	"default":   {//pour affichage
						           "MultiLine": {
						               fillColor: "purple",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               pointerEvents: "visiblePainted",
						               cursor: "pointer",
						               strokeColor: "purple",
						               strokeOpacity: 1,
						               strokeWidth: 6,
						               strokeLinecap: "round",
						               strokeDashstyle: "sold"
						           }
					       	},
						       "create":   {//pour DrawCreation, GlobalModication
									           "MultiLine": {
									               fillColor: "black",
									               fillOpacity: 0.4,
									               hoverFillColor: "white",
									               hoverFillOpacity: 0.8,
									               strokeColor: "black",
									               strokeOpacity: 1,
									               strokeWidth: 4,
									               strokeLinecap: "round",
									               strokeDashstyle: "solid"
									           }
							   },
						       "temporary":   {//pour DrawCreation
						           "MultiLine": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
						       },
							   "select":   {//pour GlobalModication
						           "Line": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           },
						           "MultiLine": {
						               fillColor: "green",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							   },
							   "modify":   {//pour GlobalModication, VerticeModification
							   	"Point": {
						       		fillColor: "green",
						               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "green",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           },
						           "VirtualPoint": { //pour VerticeModification
						           	 cursor: "pointer",
						                graphicName: "cross",
						                fillColor:"yellow",
						                fillOpacity:1,
						                pointRadius:4,
						                strokeColor:"yellow",
						                strokeDashstyle:"solid",
						                strokeOpacity:1,
						                strokeWidth:1
						           }
							   },
							   "delete":   {//pour RubberDeletion
						           "MultiLine": {
						               fillColor: "grey",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
							   }
					       },
					       copy: {
					       	supportLayersIdentifier: ["coucheLignes"],
					       	enable:true
					       },
					       clone: {
					       	supportLayers: [{
				                   id: "coucheLignes",
				                   attributes: [{
					                       from: "d_attrib_2",
					                       to: "d_attrib_2"
					                   },
					                   {
					                       from: "d_attrib_3",
					                       to: "d_attrib_3"
					                   }
				                   ]
				               }],
					       	enable:true
					       },
					       aggregate: {
					       	supportLayersIdentifier: ["coucheLignes"],
					       	enable:true
					       },
					       split: {
					       	supportLayersIdentifier: ["coucheLignes"]
					       },
					       divide: {
					       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"]
					       },
					       substract: {
					       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"]
					       },
					       intersect: {
					       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"]
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: true,
					       activeToQuery: false,
					       sheetable: true,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiLignes
					   }
					};
					
					coucheMultiPolygonesStyle6 = {
					   title: "Ma couche WFS de multi polygones - EPSG:2154",
					   type: 10,
					   definition: [
					                {
					           	  serverUrl: serveur,
					           	  layerName: layerNameMultiPolygones,
					           	  featureServerUrl: serveur,
					           	  featureName: layerNameMultiPolygones,
					           	  featureNameSpace: featureNameSpace,
					           	featurePrefix: featurePrefix,
					           	  featureGeometryName: featureGeometryNameMultiPolygones,
					           	  serverVersion: serverVersion,
				                	 internalProjection: internalProjection			
					       }
					   ],
					   options: {
					   	id: "coucheMultiPolygones",
					       symbolizers: {
					       	"default":   {//pour affichage
							           "MultiPolygon": {
							               fillColor: "pink",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               pointerEvents: "visiblePainted",
							               cursor: "pointer",
							               strokeColor: "pink",
							               strokeOpacity: 1,
							               strokeWidth: 8,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
					       	},
					       	"temporary":   {//pour DrawCreation
						           "MultiPolygon": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
					       	},
					       	"select":   {//pour GlobalModication
					    		      "Polygon": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           },
							           "MultiPolygon": {
							               fillColor: "green",
							               fillOpacity: 0.4,
							               hoverFillColor: "white",
							               hoverFillOpacity: 0.8,
							               strokeColor: "green",
							               strokeOpacity: 1,
							               strokeWidth: 4,
							               strokeLinecap: "round",
							               strokeDashstyle: "solid"
							           }
								},
							   "modify":   {//pour GlobalModication, VerticeModification
									   	"Point": {
								       		fillColor: "green",
								               graphicName:"cross",
						               points:4,
						               radius:6,
						               radius2:0,
						               angle:0,
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "green",
								               strokeOpacity: 1,
								               strokeWidth: 1,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid",			            
								               pointRadius: 6
								           },
								           "VirtualPoint": { //pour VerticeModification
								           	 cursor: "pointer",
								                graphicName: "cross",
								                fillColor:"yellow",
								                fillOpacity:1,
								                pointRadius:4,
								                strokeColor:"yellow",
								                strokeDashstyle:"solid",
								                strokeOpacity:1,
								                strokeWidth:1
								           }
							   },
							   "create":   {//pour DrawCreation, GlobalModication
								           "MultiPolygon": {
								               fillColor: "black",
								               fillOpacity: 0.4,
								               hoverFillColor: "white",
								               hoverFillOpacity: 0.8,
								               strokeColor: "black",
								               strokeOpacity: 1,
								               strokeWidth: 4,
								               strokeLinecap: "round",
								               strokeDashstyle: "solid"
								           }
								},
								"delete":   { //pour RubberDeletion
						           "MultiPoint": {
						               fillColor: "grey",
						               graphicName:"square",
						               points:4,
						               radius:4,
						               angle:Math.PI / 4,
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "grey",
						               strokeOpacity: 1,
						               strokeWidth: 1,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid",			            
						               pointRadius: 4
						           }
								}
					       },
					       clone: {
					           supportLayers: [{
				                   id: "couchePolygones",
				                   attributes: [{
				                           from: "d_attrib_2",
				                           to: "d_attrib_2"
				                       },
				                       {
				                           from: "d_attrib_3",
				                           to: "d_attrib_3"
				                       }
				                   ]
				               }]
					       },
					       copy: {
					   		supportLayersIdentifier: ["couchePolygones"]
					       },
					       buffer: {
					       	supportLayers: [{id:"couchePolygones"}],
					       	distance:20000/*,
					           enable: true*/
					       }, 
					       halo: {
					       	supportLayers: [{id:"coucheMultiPoints"},{id:"coucheMultiLignes"}],
					       	distance:20000/*,
					           enable: true*/
					       }, 
					       homothetic: {
					           supportLayersIdentifier: ["couchePolygones"]/*,
					           enable: true*/
					       }, 
					       split: {
					           supportLayersIdentifier: ["couchePolygones"]/*,
					           enable: true*/
					       }, 
					       divide: {
					           supportLayersIdentifier: ["couchePolygones","coucheLignes"]/*,
					           enable: true*/
					       },
					       aggregate: {
					           supportLayersIdentifier: ["couchePolygones"],
					           enable: true
					       },
					       substract: {
					           supportLayersIdentifier: ["couchePolygones"],
					           enable: true
					       },
					       intersect: {
					       	supportLayersIdentifier:["couchePolygones","coucheLignes","couchePoints"],
					           enable: true
					       },
					       alwaysVisible: false,
					       visible: true,
					       queryable: true,
					       activeToQuery: false,
					       sheetable: true,
					       opacity: 100,
					       opacityMax: 100,
					       legend: null,
					       metadataURL: null,
					       format: "image/png",
					       displayOrder: 1,
					       geometryType: geometryTypeLayerMultiPolygones
					   }
					};
					
	/***************************
	  couches pour le snapping
	 ****************************/
					
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	couchePointsSnapping = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNamePoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"couchePoints",
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },     
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	couchePointsSnapping2 = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 1,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNamePoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"couchePoints",
		       /*snapping : {
		           tolerance: 10,
		           enable: true
		       },  */   
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	coucheLignesSnapping = {
	    title: "Ma couche WFS de lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameLignes,
					featureServerUrl: serveur,
					featureName: layerNameLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"coucheLignes",
		   snapping : {
		   	snappingLayersIdentifier:["couchePoints","couchePolygones"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	couchePolygonesSnapping = {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	   featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"couchePolygones",
		   	snapping : {
		           tolerance: 10,
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};

	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	coucheMultiPointsSnapping = {
		   title: "Ma couche WFS de multi points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNameMultiPoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNameMultiPoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNameMultiPoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNameMultiPoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiPoints",
		       snapping : {
		           tolerance: 10,
		           enable: false
		       },     
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	coucheMultiLignesSnapping = {
	    title: "Ma couche WFS de multi lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameMultiLignes,
					featureServerUrl: serveur,
					featureName: layerNameMultiLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameMultiLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"coucheMultiLignes",
		   snapping : {
		   	snappingLayersIdentifier:["coucheMultiPoints","coucheMultiPolygones"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	coucheMultiPolygonesSnapping = {
	    title: "Ma couche WFS de multi polygones - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameMultiPolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameMultiPolygones,
	            	   featureNameSpace: featureNameSpace,
	            	   featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameMultiPolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	id:"coucheMultiPolygones",
	    	snapping : {
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};
	
	
	/*********************************************
	  couches pour les outils d'édition "avancés"
	 *********************************************/
					
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	couchePointsFctAvanced = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNamePoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"couchePoints",
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	couchePointsFctAvancedBis = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNamePoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"couchePoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true,
		       	attributsNotClonable:[{fieldName: 'd_attrib_3'}]
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	couchePointsFctAvanced2 = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNamePoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"couchePoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["coucheMultiPoints"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiPoints"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	couchePointsFctAvanced3 = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNamePoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"couchePoints",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["coucheMultiPoints"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints
		   }
		};
	
	coucheLignesFctAvanced = {
	    title: "Ma couche WFS de lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameLignes,
					featureServerUrl: serveur,
					featureName: layerNameLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	        id:"coucheLignes",
	        copy: {
	        	enable: true
	        },
	        clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes
	    }
	};
	
	coucheLignesFctAvancedBis = {
		   title: "Ma couche WFS de lignes - EPSG:2154",
		   type: 10,
		   definition: [
		       {
		           	serverUrl: serveur,
						layerName: layerNameLignes,
						featureServerUrl: serveur,
						featureName: layerNameLignes,
						featureNameSpace: featureNameSpace,
						featurePrefix: featurePrefix,
						featureGeometryName: featureGeometryNameLignes,
						serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
	
	coucheLignesFctAvanced2 = {
		   title: "Ma couche WFS de lignes - EPSG:2154",
		   type: 10,
		   definition: [
		       {
		           	serverUrl: serveur,
						layerName: layerNameLignes,
						featureServerUrl: serveur,
						featureName: layerNameLignes,
						featureNameSpace: featureNameSpace,
						featurePrefix: featurePrefix,
						featureGeometryName: featureGeometryNameLignes,
						serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["coucheMultiLignes"]
		       },
		       split: {
		       	supportLayersIdentifier: ["coucheMultiLignes"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["couchePoints","coucheMultiLignes","couchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["coucheMultiLignes"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiLignes"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
	
	coucheLignesFctAvanced3 = {
		   title: "Ma couche WFS de lignes - EPSG:2154",
		   type: 10,
		   definition: [
		       {
		           	serverUrl: serveur,
						layerName: layerNameLignes,
	                	imageServerUrl:serveur,
	                	imageLayerName: featurePrefix+":"+layerNameLignes,
						featureServerUrl: serveur,
						featureName: layerNameLignes,
						featureNameSpace: featureNameSpace,
						featurePrefix: featurePrefix,
						featureGeometryName: featureGeometryNameLignes,
						serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheLignes",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["coucheMultiLignes"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["coucheMultiLignes"],
		       	enable:true
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiLignes"],
		       	enable:true
		       },
		       substract: {
		           supportLayersIdentifier: ["couchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerLignes
		   }
		};
	
	couchePolygonesFctAvanced = {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	   featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"couchePolygones",
		   	copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       aggregate: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	couchePolygonesFctAvancedBis = {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	   featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"couchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	couchePolygonesFctAvanced2 = {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	   featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"couchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["coucheMultiPolygones"]
		       },
		       split: {
		       	supportLayersIdentifier: ["coucheMultiPolygones"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["coucheLignes","coucheMultiPolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["coucheMultiPolygones"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiPolygones"]
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
		           		      "Polygon": {
						               fillColor: "yellow",
						               fillOpacity: 0.4,
						               hoverFillColor: "white",
						               hoverFillOpacity: 0.8,
						               strokeColor: "yellow",
						               strokeOpacity: 1,
						               strokeWidth: 4,
						               strokeLinecap: "round",
						               strokeDashstyle: "solid"
						           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};
	
	couchePolygonesFctAvanced3 = {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		                 imageServerUrl:serveur,
		                 imageLayerName: featurePrefix+":"+layerNamePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	  featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"couchePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["coucheMultiPolygones"],
		       	enable:true
		       },
		       halo: {
		       	supportLayers: [{id:"couchePoints"}, {id:"coucheLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"coucheMultiPolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["coucheMultiPolygones"],
		           enable: true
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiPolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["couchePoints","coucheLignes"]
		       },
		       substract: {
		           supportLayersIdentifier: ["coucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
        		   "default":{// EDITION WFS
        		   	"Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
        		   }	
		       },*/
		       /*symbolizers: { //WFS
     		      "Polygon": {//pour affichage
			               fillColor: "red",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "red",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones
		   }
		};


	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	coucheMultiPointsFctAvanced = {
		   title: "Ma couche WFS de multi points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNameMultiPoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNameMultiPoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNameMultiPoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNameMultiPoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiPoints", 
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       aggregate: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	coucheMultiPointsFctAvancedBis = {
		   title: "Ma couche WFS de multi points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNameMultiPoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNameMultiPoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNameMultiPoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNameMultiPoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiPoints",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	coucheMultiPointsFctAvanced2 = {
		   title: "Ma couche WFS de multi points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNameMultiPoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNameMultiPoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNameMultiPoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNameMultiPoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiPoints", 
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["couchePoints"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["couchePoints"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	coucheMultiPointsFctAvanced3 = {
		   title: "Ma couche WFS de multi points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNameMultiPoints,
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNameMultiPoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNameMultiPoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNameMultiPoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiPoints", 
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["couchePoints"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["couchePoints"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints
		   }
		};
	
	coucheMultiLignesFctAvanced = {
	    title: "Ma couche WFS de multi lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameMultiLignes,
					featureServerUrl: serveur,
					featureName: layerNameMultiLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameMultiLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	        id:"coucheMultiLignes",
	        copy: {
	        	enable: true
	        },
	        clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes
	    }
	};
	
	coucheMultiLignesFctAvancedBis = {
		   title: "Ma couche WFS de multi lignes - EPSG:2154",
		   type: 10,
		   definition: [
		       {
		           	serverUrl: serveur,
						layerName: layerNameMultiLignes,
						featureServerUrl: serveur,
						featureName: layerNameMultiLignes,
						featureNameSpace: featureNameSpace,
						featurePrefix: featurePrefix,
						featureGeometryName: featureGeometryNameMultiLignes,
						serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	enable: true
		       },
		       clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes
		   }
		};
	
	coucheMultiLignesFctAvanced2 = {
		   title: "Ma couche WFS de multi lignes - EPSG:2154",
		   type: 10,
		   definition: [
		       {
		           	serverUrl: serveur,
						layerName: layerNameMultiLignes,
						featureServerUrl: serveur,
						featureName: layerNameMultiLignes,
						featureNameSpace: featureNameSpace,
						featurePrefix: featurePrefix,
						featureGeometryName: featureGeometryNameMultiLignes,
						serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiLignes",
		       attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["coucheLignes"]
		       },
		       split: {
		       	supportLayersIdentifier: ["coucheLignes"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["coucheMultiPoints","coucheLignes","couchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["coucheLignes"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes
		   }
		};
	
	coucheMultiLignesFctAvanced3 = {
		   title: "Ma couche WFS de multi lignes - EPSG:2154",
		   type: 10,
		   definition: [
		       {
		           	serverUrl: serveur,
						layerName: layerNameMultiLignes,
	                	imageServerUrl:serveur,
	                	imageLayerName: featurePrefix+":"+layerNameMultiLignes,
						featureServerUrl: serveur,
						featureName: layerNameMultiLignes,
						featureNameSpace: featureNameSpace,
						featurePrefix: featurePrefix,
						featureGeometryName: featureGeometryNameMultiLignes,
						serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiLignes",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		       	supportLayersIdentifier: ["coucheLignes"],
		       	enable:true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["coucheLignes"],
		       	enable:true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["coucheLignes","coucheMultiPolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
		           enable: true*/
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiLignes
		   }
		};
	
	coucheMultiPolygonesFctAvanced = {
	    title: "Ma couche WFS de multi polygones - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameMultiPolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameMultiPolygones,
	            	   featureNameSpace: featureNameSpace,
	            	   featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameMultiPolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	id:"coucheMultiPolygones",
	    	copy: {
	        	enable: true
	        },
	    	clone: {
	        	enable: true
	        },
	        split: {
	            enable: true
	        },
	        divide: {
	            enable: true
	        },
	        aggregate: {
	        	enable: true
	        },
	        intersect: {
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones
	    }
	};
	
	coucheMultiPolygonesFctAvancedBis = {
		   title: "Ma couche WFS de multi polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNameMultiPolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNameMultiPolygones,
		           	   featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNameMultiPolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"coucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		       	enable: true
		       },
		   	clone: {
		       	enable: true
		       },
		       split: {
		           enable: true
		       },
		       divide: {
		           enable: true
		       },
		       intersect: {
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones
		   }
		};
	
	coucheMultiPolygonesFctAvanced2 = {
		   title: "Ma couche WFS de multi polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNameMultiPolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNameMultiPolygones,
		           	   featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNameMultiPolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"coucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["couchePolygones"]
		       },
		       split: {
		       	supportLayersIdentifier: ["couchePolygones"]
		       },
		       divide: {
		       	supportLayersIdentifier: ["coucheMultiLignes","couchePolygones"]
		       },
		       aggregate: {
		           supportLayersIdentifier: ["couchePolygones"]
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones
		   }
		};
	
	coucheMultiPolygonesFctAvanced3 = {
		   title: "Ma couche WFS de multi polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNameMultiPolygones,
		                 imageServerUrl:serveur,
		                 imageLayerName: featurePrefix+":"+layerNameMultiPolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNameMultiPolygones,
		           	  featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNameMultiPolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"coucheMultiPolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_attrib_1', label: 'Un attribut'},
		               {fieldName: 'd_attrib_2', label: 'Un attribut'}
		           ]
		       },
		   	copy: {
		   		supportLayersIdentifier: ["couchePolygones"],
		       	enable:true
		       },
		       halo: {
		       	supportLayers: [{id:"coucheMultiPoints"}, {id:"coucheMultiLignes"}],
		           distance: 20000
		           //,	enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"couchePolygones"}],
		           distance: 20000/*,
		           enable: true*/
		       },
		       aggregate: {
		           supportLayersIdentifier: ["couchePolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["couchePolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier: ["couchePolygones","coucheMultiLignes","coucheMultiPoints"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":{
	     		      "Polygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           },
			          "MultiPolygon": {//pour affichage
			               fillColor: "yellow",
			               fillOpacity: 0.4,
			               hoverFillColor: "white",
			               hoverFillOpacity: 0.8,
			               strokeColor: "yellow",
			               strokeOpacity: 1,
			               strokeWidth: 4,
			               strokeLinecap: "round",
			               strokeDashstyle: "solid",
			               pointerEvents: "visiblePainted",
			               cursor: "pointer",
			           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPolygones
		   }
		};
	
	/*****************************************************
	  couches pour le clonage
	 ******************************************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
		
	coucheClonePoints = {
	    title: "Ma couche WFS de points pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNameClonePoints,
	                	 featureServerUrl: serveur,
	                	 featureName: layerNameClonePoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNameClonePoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: { 
	    	id:"coucheClonePoints",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        copy: {
	    		supportLayersIdentifier: ["couchePoints"]
	        },
	        clone: {
	            supportLayers: [{
                    id: "couchePoints",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["coucheMultiPoints"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["couchePoints"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerClonePoints
	    }
	};
	
	coucheCloneLignes = {
	    title: "Ma couche WFS de lignes pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameCloneLignes,
					featureServerUrl: serveur,
					featureName: layerNameCloneLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameCloneLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	id:"coucheCloneLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        copy: {
	    		supportLayersIdentifier: ["coucheLignes"]
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["coucheLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["coucheLignes"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["coucheLignes"],
	            enable: true
	        },
	        aggregate: {
	            supportLayersIdentifier: ["coucheLignes"]/*,
	            enable: true*/
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["coucheMultiLignes"]/*,
	            enable: true*/
	        },
	        substract: {
	            supportLayersIdentifier: ["couchePolygones"]
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneLignes
	    }
	};
	
	coucheCloneLignes2 = {
	    title: "Ma couche WFS de lignes pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameCloneLignes,
					featureServerUrl: serveur,
					featureName: layerNameCloneLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameCloneLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	id:"coucheCloneLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
                    ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["coucheLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["coucheLignes"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["coucheLignes"],
	            enable: true
	        },
	        snapping: {
	        	snappingLayersIdentifier:["couchePoints"],
	            tolerance: 10/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["coucheLignes"]/*,
	            enable: true*/
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneLignes
	    }
	};
	
	coucheClonePolygones = {
		   title: "Ma couche WFS de polygones pour clonage - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNameClonePolygones,
		           	  featureServerUrl: serveur,
		           	  featureName: layerNameClonePolygones,
		           	  featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNameClonePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"coucheClonePolygones",
		   	attributes: {
		           attributesEditable: [
		               {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
		               {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
		           ]
		       },
		       copy: {
		   		supportLayersIdentifier: ["couchePolygones"]
		       },
		       halo: {
		       	supportLayers: [{id:"couchePoints"}, {id:"coucheLignes"}],
		           distance: 20000,
		           enable: true
		       },
		       buffer: {
		       	supportLayers: [{id:"couchePolygones"}],
		           distance: 20000,
		           enable: true
		       },
		       clone: {
		           supportLayers: [{
	                    id: "couchePolygones",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_cloneattrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_cloneattrib_3"
	                        }
	                    ]
	                }]
		       },
		       homothetic: {
		           supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
		           enable: true
		       }, 
		       split: {
		           supportLayersIdentifier: ["couchePolygones"]/*,
		           enable: true*/
		       },
		       divide: {
		           supportLayersIdentifier: ["couchePolygones"]/*,
		           enable: true*/
		       },
		       intersect: {
		       	supportLayersIdentifier:["couchePolygones"],
		           enable: true
		       },
		       aggregate: {
		           supportLayersIdentifier: ["couchePolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["couchePolygones"]
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiPolygones"],
		           enable: true
		       },
		       /*symbolizers: {
		       	"default":   {//pour affichage
	            		      "Polygon": {
					               fillColor: "yellow",
					               fillOpacity: 0.4,
					               hoverFillColor: "white",
					               hoverFillOpacity: 0.8,
					               strokeColor: "yellow",
					               strokeOpacity: 1,
					               strokeWidth: 4,
					               strokeLinecap: "round",
					               strokeDashstyle: "solid"
					           }
		       	}
		       },*/
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerClonePolygones
		   }
		};
	
	coucheClonePolygones2 = {
	    title: "Ma couche WFS de polygones pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameClonePolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameClonePolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameClonePolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	id:"coucheClonePolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "couchePolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        homothetic: {
	            supportLayersIdentifier: ["couchePolygones"],
	            enable: true
	        }, 
	        split: {
	            supportLayersIdentifier: ["couchePolygones"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["couchePolygones"]/*,
	            enable: true*/
	        },
	        intersect: {
	        	supportLayersIdentifier:["couchePolygones"],
	            enable: true
	        },
	        aggregate: {
	            supportLayersIdentifier: ["couchePolygones"],
	            enable: true
	        },
	        snapping: {
	        	snappingLayersIdentifier:["couchePoints"],
	            tolerance: 10/*,
	            enable: true*/
	        },
	        /*symbolizers: {
	        	"default":   {//pour affichage
            		      "Polygon": {
				               fillColor: "yellow",
				               fillOpacity: 0.4,
				               hoverFillColor: "white",
				               hoverFillOpacity: 0.8,
				               strokeColor: "yellow",
				               strokeOpacity: 1,
				               strokeWidth: 4,
				               strokeLinecap: "round",
				               strokeDashstyle: "solid"
				           }
	        	}
	        },*/
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerClonePolygones
	    }
	};
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
		
	coucheCloneMultiPoints = {
	    title: "Ma couche WFS de  multi points pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	                 {	            	
	                	 serverUrl: serveur,
	                	 layerName: layerNameCloneMultiPoints,
	                	 featureServerUrl: serveur,
	                	 featureName: layerNameCloneMultiPoints,
	                	 featureNameSpace: featureNameSpace,
	                	 featurePrefix: featurePrefix,
	                	 featureGeometryName: featureGeometryNameCloneMultiPoints,
	                	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	id:"coucheCloneMultiPoints",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheMultiPoints",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["coucheMultiPoints"]
	        },
	        aggregate: {
	            supportLayersIdentifier: ["coucheMultiPoints"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["coucheMultiPoints"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPoints
	    }
	};
	
	coucheCloneMultiLignes = {
	    title: "Ma couche WFS de multi lignes pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameCloneMultiLignes,
					featureServerUrl: serveur,
					featureName: layerNameCloneMultiLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameCloneMultiLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	id:"coucheCloneMultiLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheMultiLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
	                ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["coucheMultiLignes"]
	        },
	        split: {
	            supportLayersIdentifier: ["coucheMultiLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["coucheMultiLignes"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["coucheMultiLignes"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["coucheMultiPolygones"]
	        },
	        intersect: {
	        	supportLayersIdentifier:["coucheMultiLignes"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiLignes
	    }
	};
	
	coucheCloneMultiLignes2 = {
	    title: "Ma couche WFS de multi lignes pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameCloneMultiLignes,
					featureServerUrl: serveur,
					featureName: layerNameCloneMultiLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameCloneMultiLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	id:"coucheCloneMultiLignes",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheMultiLignes",
                    attributes: [{
	                        from: "d_attrib_2",
	                        to: "d_cloneattrib_2"
	                    },
	                    {
	                        from: "d_attrib_3",
	                        to: "d_cloneattrib_3"
	                    }
	                ]
                }]
	        },
	        split: {
	            supportLayersIdentifier: ["coucheMultiLignes"]/*,
	            enable: true*/
	        },
	        divide: {
	            supportLayersIdentifier: ["coucheMultiLignes"]/*,
	            enable: true*/
	        },
	        snapping: {
	        	//snappingLayersIdentifier:["coucheMultiLignes"],
	            tolerance: 10,
	            enable: true
	        }, 
	        aggregate: {
	            supportLayersIdentifier: ["coucheMultiLignes"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["coucheMultiLignes"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiLignes
	    }
	};
	
	coucheCloneMultiPolygones = {
	    title: "Ma couche WFS de multi polygones pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameCloneMultiPolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameCloneMultiPolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameCloneMultiPolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	id:"coucheCloneMultiPolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheMultiPolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["coucheMultiPolygones"]
	        },
	        buffer: {
	        	supportLayers: [{id:"coucheMultiPolygones"}]/*,
	            enable: true*/
	        }, 
	        halo: {
	        	supportLayers: [{id:"coucheMultiPoints"}, {id:"coucheMultiLignes"}]/*,
	            enable: true*/
	        }, 
	        homothetic: {
	            supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["coucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["coucheMultiPolygones"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["coucheMultiPolygones"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["coucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["coucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPolygones
	    }
	};
	
	coucheCloneMultiPolygones2 = {
	    title: "Ma couche WFS de multi polygones pour clonage - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameCloneMultiPolygones,
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameCloneMultiPolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameCloneMultiPolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	id:"coucheCloneMultiPolygones",
	    	attributes: {
	            attributesEditable: [
	                {fieldName: 'd_cloneattrib_2', label: 'Un attribut'},
	                {fieldName: 'd_cloneattrib_3', label: 'Un attribut'}
	            ]
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheMultiPolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_cloneattrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_cloneattrib_3"
                        }
                    ]
                }]
	        },
	        homothetic: {
	            supportLayersIdentifier: ["coucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["coucheMultiPolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["coucheMultiPolygones"]/*,
	            enable: true*/
	        },
	        snapping: {
	        	snappingLayersIdentifier:["coucheMultiLignes"],
	            tolerance: 10,
	            enable: true
	        }, 
	        aggregate: {
	            supportLayersIdentifier: ["coucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["coucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerCloneMultiPolygones
	    }
	};
	
	
	/***************************
	  couches pour exemples complets
	 ****************************/
	
	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE SIMPLE GEOMETRIE -----------// 
	//---------------------------------------------------------//
	
	couchePointsFull = {
		   title: "Ma couche WFS de points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNamePoints,
		               	 //internalProjection:"EPSG:4326",
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNamePoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNamePoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNamePoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"couchePoints",
		       attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
		           ]
		       },
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },
		       clone: {
		           supportLayers: [{
	                    id: "coucheMultiPoints",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_attrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_attrib_3"
	                        }
	                    ]
	                }]
		       },
		       copy: {
		   		supportLayersIdentifier: ["coucheMultiPoints"]
		       }, 
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiPoints"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier:["couchePolygones","coucheMultiPolygones"],
		           enable: true
		       },
		       maxScale: 1000, 
		       minScale: 80000, 
		       maxEditionScale: 100,
		       minEditionScale: 40000,
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPoints,
		       attribution: attribution
		   }
		};
	
	coucheLignesFull = {
	    title: "Ma couche WFS de lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameLignes,
					//internalProjection:"EPSG:4326",
					featureServerUrl: serveur,
					featureName: layerNameLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"coucheLignes",
		   snapping : {
		   	snappingLayersIdentifier:["couchePoints"],
	            tolerance: 10,
	            enable: true
	        },
	        clone: {
	            supportLayers: [{
                    id: "coucheMultiLignes",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_attrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_attrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["coucheMultiLignes"]
	        }, 
	        split: {
	            supportLayersIdentifier: ["coucheMultiLignes"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["coucheMultiLignes"]/*,
	            enable: true*/
	        },
	        unaggregate: {
	            supportLayersIdentifier: ["coucheMultiLignes"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["couchePolygones","coucheMultiPolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["couchePolygones","coucheMultiPolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerLignes,
	        attribution: attribution
	    }
	};
	
	couchePolygonesFull= {
		   title: "Ma couche WFS de polygones - EPSG:2154",
		   type: 10,
		   definition: [
		                {
		           	  serverUrl: serveur,
		           	  layerName: layerNamePolygones,
		           	  //internalProjection:"EPSG:4326",
		           	  featureServerUrl: serveur,
		           	  featureName: layerNamePolygones,
		           	  featureNameSpace: featureNameSpace,
		           	featurePrefix: featurePrefix,
		           	  featureGeometryName: featureGeometryNamePolygones,
		           	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		   	id:"couchePolygones",
		   	snapping : {
		           tolerance: 10,
		           enable: false
		       },
		       clone: {
		           supportLayers: [{
	                    id: "coucheMultiPolygones",
	                    attributes: [{
	                            from: "d_attrib_2",
	                            to: "d_attrib_2"
	                        },
	                        {
	                            from: "d_attrib_3",
	                            to: "d_attrib_3"
	                        }
	                    ]
	                }]
		       },
		       copy: {
		   		supportLayersIdentifier: ["coucheMultiPolygones"]
		       },
		       buffer: {
		       	supportLayers: [{id:"coucheMultiPolygones"}],
		       	distance:20000/*,
		           enable: true*/
		       }, 
		       halo: {
		       	supportLayers: [{id:"couchePoints"}, {id:"coucheLignes"}],
		       	distance:20000/*,
		           enable: true*/
		       }, 
		       homothetic: {
		           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
		           enable: true*/
		       }, 
		       split: {
		           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
		           enable: true*/
		       }, 
		       divide: {
		           supportLayersIdentifier: ["coucheMultiPolygones"]/*,
		           enable: true*/
		       },
		       unaggregate: {
		           supportLayersIdentifier: ["coucheMultiPolygones"],
		           enable: true
		       },
		       substract: {
		           supportLayersIdentifier: ["coucheMultiPolygones"],
		           enable: true
		       },
		       intersect: {
		       	supportLayersIdentifier:["coucheMultiPolygones"],
		           enable: true
		       },
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerPolygones,
		       attribution: attribution
		   }
		};

	//---------------------------------------------------------//
	//-----------  COUCHES DE TYPE MULTI GEOMETRIES -----------// 
	//---------------------------------------------------------//
	
	coucheMultiPointsFull = {
		   title: "Ma couche WFS de multi points - EPSG:2154",
		   type: 10,
		   definition: [
		                {	            	
		               	 serverUrl: serveur,
		               	 layerName: layerNameMultiPoints,
		               	 //internalProjection:"EPSG:4326",
		               	 imageServerUrl:serveur,
		               	 imageLayerName: featurePrefix+":"+layerNameMultiPoints,
		               	 featureServerUrl: serveur,
		               	 featureName: layerNameMultiPoints,
		               	 featureNameSpace: featureNameSpace,
		               	featurePrefix: featurePrefix,
		               	 featureGeometryName: featureGeometryNameMultiPoints,
		               	 serverVersion: serverVersion,
	                	 internalProjection: internalProjection
		       }
		   ],
		   options: {
		       id:"coucheMultiPoints",
		       attributes: {
		           /*attributeId: {
		               fieldName: "d_attrib_1"
		           },*/
		           attributesEditable: [
		               {fieldName: 'd_attrib_2', label: 'Un attribut'},
		               {fieldName: 'd_attrib_3', label: 'Un autre attribut'}
		           ]
		       },
		       snapping : {
		           tolerance: 10,
		           enable: true
		       },     
		       maxScale: 1000, 
		       minScale: 80000, 
		       maxEditionScale: 100,
		       minEditionScale: 40000,
		       alwaysVisible: false,
		       visible: true,
		       queryable: true,
		       activeToQuery: false,
		       sheetable: true,
		       opacity: 100,
		       opacityMax: 100,
		       legend: null,
		       metadataURL: null,
		       format: "image/png",
		       displayOrder: 1,
		       geometryType: geometryTypeLayerMultiPoints,
		       attribution: attribution
		   }
		};
	
	coucheMultiLignesFull = {
	    title: "Ma couche WFS de multi lignes - EPSG:2154",
	    type: 10,
	    definition: [
	        {
	            	serverUrl: serveur,
					layerName: layerNameMultiLignes,
					//internalProjection:"EPSG:4326",
					featureServerUrl: serveur,
					featureName: layerNameMultiLignes,
					featureNameSpace: featureNameSpace,
					featurePrefix: featurePrefix,
					featureGeometryName: featureGeometryNameMultiLignes,
					serverVersion: serverVersion,
               	 internalProjection: internalProjection            	 
	        }
	    ],
	    options: {
	    	attributes: {
	            /*attributeId: {
	                fieldName: "d_attrib_1"
	            },*/
	            attributesEditable: [
	                {fieldName: 'd_attrib_2', label: 'Un attribut'},
	            ]
	        },
	        id:"coucheMultiLignes",
		   snapping : {
		   	snappingLayersIdentifier:["coucheMultiPoints"],
	            tolerance: 10,
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiLignes,
	        attribution: attribution
	    }
	};
	
	coucheMultiPolygonesFull= {
	    title: "Ma couche WFS de multi polygones - EPSG:2154",
	    type: 10,
	    definition: [
	                 {
	            	  serverUrl: serveur,
	            	  layerName: layerNameMultiPolygones,
	            	  //internalProjection:"EPSG:4326",
	            	  featureServerUrl: serveur,
	            	  featureName: layerNameMultiPolygones,
	            	  featureNameSpace: featureNameSpace,
	            	  featurePrefix: featurePrefix,
	            	  featureGeometryName: featureGeometryNameMultiPolygones,
	            	  serverVersion: serverVersion,
	                	 internalProjection: internalProjection
	        }
	    ],
	    options: {
	    	id:"coucheMultiPolygones",
	    	snapping : {
	            tolerance: 10,
	            enable: false
	        },
	        clone: {
	            supportLayers: [{
                    id: "couchePolygones",
                    attributes: [{
                            from: "d_attrib_2",
                            to: "d_attrib_2"
                        },
                        {
                            from: "d_attrib_3",
                            to: "d_attrib_3"
                        }
                    ]
                }]
	        },
	        copy: {
	    		supportLayersIdentifier: ["couchePolygones"]
	        },
	        buffer: {
	        	supportLayers: [{id:"couchePolygones"}],
	        	distance:20000/*,
	            enable: true*/
	        }, 
	        halo: {
	        	supportLayers: [{id:"coucheMultiPoints"}, {id:"coucheMultiLignes"}],
	        	distance:20000/*,
	            enable: true*/
	        }, 
	        homothetic: {
	            supportLayersIdentifier: ["couchePolygones"]/*,
	            enable: true*/
	        }, 
	        split: {
	            supportLayersIdentifier: ["couchePolygones"]/*,
	            enable: true*/
	        }, 
	        divide: {
	            supportLayersIdentifier: ["couchePolygones"]/*,
	            enable: true*/
	        },
	        aggregate: {
	            supportLayersIdentifier: ["couchePolygones"],
	            enable: true
	        },
	        substract: {
	            supportLayersIdentifier: ["couchePolygones"],
	            enable: true
	        },
	        intersect: {
	        	supportLayersIdentifier:["couchePolygones"],
	            enable: true
	        },
	        alwaysVisible: false,
	        visible: true,
	        queryable: true,
	        activeToQuery: false,
	        sheetable: true,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        format: "image/png",
	        displayOrder: 1,
	        geometryType: geometryTypeLayerMultiPolygones,
	        attribution: attribution
	    }
	};

	/***************************
	  couche de fond
	 ****************************/
	
	coucheBase = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
					layerName: "fond_vecteur"
				}
			],
			options: {
				maxScale: 100,
				minScale: 10000001,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		};

  groupeFonds = {
      title: "Fonds cartographiques",
      options: {
          opened: true
      }
  };

  groupeEditionWFS = {
      title: "Mes couches d'édition WFS - objet simple",
      options: {
          opened: true
      }
  };
  
  groupeEditionWFSMulti = {
      title: "Mes couches d'édition WFS - objet composite",
      options: {
          opened: true
      }
  };
}