function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global pilot� par l'arbre des couches
	      save: function (json) {
		  	 //Ici, code MOE qui est spécifique à chaque application métier.
		 	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		    //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		 	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		 	 sendRequestBouchonForSaveElements(json);
		
		 }
	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsStyle6.title, couchePointsStyle6.definition, couchePointsStyle6.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesStyle6.title, coucheLignesStyle6.definition, coucheLignesStyle6.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle6.title, couchePolygonesStyle6.definition, couchePolygonesStyle6.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsStyle6.title, coucheMultiPointsStyle6.definition, coucheMultiPointsStyle6.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesStyle6.title, coucheMultiLignesStyle6.definition, coucheMultiLignesStyle6.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle6.title, coucheMultiPolygonesStyle6.definition, coucheMultiPolygonesStyle6.options);

    contenuCarte.addItem(editionLayer4, gpWFS);
    contenuCarte.addItem(editionLayer5, gpWFS);
    contenuCarte.addItem(editionLayer6, gpWFS);

	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsStyle6.title, kmlCouchePointsStyle6.definition, kmlCouchePointsStyle6.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesStyle6.title, kmlCoucheLignesStyle6.definition, kmlCoucheLignesStyle6.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesStyle6.title, kmlCouchePolygonesStyle6.definition, kmlCouchePolygonesStyle6.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsStyle6.title, kmlCoucheMultiPointsStyle6.definition, kmlCoucheMultiPointsStyle6.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesStyle6.title, kmlCoucheMultiLignesStyle6.definition, kmlCoucheMultiLignesStyle6.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesStyle6.title, kmlCoucheMultiPolygonesStyle6.definition, kmlCoucheMultiPolygonesStyle6.options);

    contenuCarte.addItem(editionLayerkml4, gpKML);
    contenuCarte.addItem(editionLayerkml5, gpKML);
    contenuCarte.addItem(editionLayerkml6, gpKML);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsStyle6.title, geojsonCouchePointsStyle6.definition, geojsonCouchePointsStyle6.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesStyle6.title, geojsonCoucheLignesStyle6.definition, geojsonCoucheLignesStyle6.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesStyle6.title, geojsonCouchePolygonesStyle6.definition, geojsonCouchePolygonesStyle6.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsStyle6.title, geojsonCoucheMultiPointsStyle6.definition, geojsonCoucheMultiPointsStyle6.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesStyle6.title, geojsonCoucheMultiLignesStyle6.definition, geojsonCoucheMultiLignesStyle6.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesStyle6.title, geojsonCoucheMultiPolygonesStyle6.definition, geojsonCoucheMultiPolygonesStyle6.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSON);
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
       	      {type: Descartes.Map.EDITION_SELECTION/*,
    	    	  args:{
    	    		  showInfos:{attributes:true}
    	    	  }*/
    	      },
    	      {type: Descartes.Map.EDITION_DRAW_CREATION},
    	      {type: Descartes.Map.EDITION_COPY_CREATION},
    	      {type: Descartes.Map.EDITION_CLONE_CREATION},
    	      {type: Descartes.Map.EDITION_BUFFER_NORMAL_CREATION},
    	      {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION},
    	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION},
    	      {type: Descartes.Map.EDITION_SPLIT,
    	       args: {
                  drawingType: Descartes.Layer.POINT_GEOMETRY
    	       }
              },
              {type: Descartes.Map.EDITION_SPLIT},
              {type: Descartes.Map.EDITION_SPLIT,
      	       args: {
                   drawingType: Descartes.Layer.POLYGON_GEOMETRY
               }
              },
    	      {type: Descartes.Map.EDITION_DIVIDE},
    	      {type: Descartes.Map.EDITION_AGGREGATION},
    	      {type: Descartes.Map.EDITION_UNAGGREGATION_CREATION},
    	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
    	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
    	      {type: Descartes.Map.EDITION_MERGE_MODIFICATION},
    	      {type: Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION},
    	      {type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION},
    	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
    	      {type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION},
    	      {type: Descartes.Map.EDITION_ATTRIBUTE},
    	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION},
    	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
