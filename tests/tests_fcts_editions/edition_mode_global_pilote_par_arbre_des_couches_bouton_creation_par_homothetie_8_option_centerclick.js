function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        autoSave:true,  //sauvegarde automatique
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesFctAvanced3.title, couchePolygonesFctAvanced3.definition, couchePolygonesFctAvanced3.options);
    var editionLayer3bis = new Descartes.Layer.EditionLayer.WFS(coucheClonePolygones.title, coucheClonePolygones.definition, coucheClonePolygones.options);
    editionLayer3bis.title="Ma couche WFS de polygones pour homothétie";

    contenuCarte.addItem(editionLayer3, gpWFS);
    contenuCarte.addItem(editionLayer3bis, gpWFS);

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesFctAvanced3.title, coucheMultiPolygonesFctAvanced3.definition, coucheMultiPolygonesFctAvanced3.options);
    var editionLayer6bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones.title, coucheCloneMultiPolygones.definition, coucheCloneMultiPolygones.options);
    editionLayer6bis.title="Ma couche WFS de multi-polygones pour homothétie";

    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    contenuCarte.addItem(editionLayer6bis, gpWFSMulti);
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesFctAvanced3.title, kmlCouchePolygonesFctAvanced3.definition, kmlCouchePolygonesFctAvanced3.options);
    var editionLayerkml3bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePolygones.title, kmlCoucheClonePolygones.definition, kmlCoucheClonePolygones.options);
    editionLayerkml3bis.title="Ma couche KML de polygones pour homothétie";

    contenuCarte.addItem(editionLayerkml3, gpKML);
    contenuCarte.addItem(editionLayerkml3bis, gpKML);

    var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesFctAvanced3.title, kmlCoucheMultiPolygonesFctAvanced3.definition, kmlCoucheMultiPolygonesFctAvanced3.options);
    var editionLayerkml6bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPolygones.title, kmlCoucheCloneMultiPolygones.definition, kmlCoucheCloneMultiPolygones.options);
    editionLayerkml6bis.title="Ma couche KML de multi-polygones pour homothétie";

    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    contenuCarte.addItem(editionLayerkml6bis, gpKMLMulti);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesFctAvanced3.title, geojsonCouchePolygonesFctAvanced3.definition, geojsonCouchePolygonesFctAvanced3.options);
    var editionLayergeojson3bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePolygones.title, geojsonCoucheClonePolygones.definition, geojsonCoucheClonePolygones.options);
    editionLayergeojson3bis.title="Ma couche GEOJSON de polygones pour homothétie";

    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3bis, gpGEOJSON);

    var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced3.title, geojsonCoucheMultiPolygonesFctAvanced3.definition, geojsonCoucheMultiPolygonesFctAvanced3.options);
    var editionLayergeojson6bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPolygones.title, geojsonCoucheCloneMultiPolygones.definition, geojsonCoucheCloneMultiPolygones.options);
    editionLayergeojson6bis.title="Ma couche GEOJSON de multi-polygones pour homothétie";

    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    contenuCarte.addItem(editionLayergeojson6bis, gpGEOJSONMulti);
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION,
	      		args:{
	      			centerClick:true
	      		}
	      }, 
	      {type: Descartes.Map.EDITION_RUBBER_DELETION}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout de plusieurs zones informatives
	var infos = [
	             {type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'}
	];

	// Ajout de la zone informative
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
