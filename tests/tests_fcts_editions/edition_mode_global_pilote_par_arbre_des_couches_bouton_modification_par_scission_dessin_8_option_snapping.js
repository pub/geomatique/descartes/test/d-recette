function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
	coucheCloneLignes2.options.split.enable = true;
	coucheClonePolygones2.options.split.enable = true;
	coucheCloneMultiLignes2.options.split.enable = true;
	coucheCloneMultiPolygones2.options.split.enable = true;
	
	var editionLayer1 = new Descartes.Layer.WFS(couchePointsFctAvanced3.title, couchePointsFctAvanced3.definition, couchePointsFctAvanced3.options);
    var editionLayer2 = new Descartes.Layer.WFS(coucheLignesFctAvanced3.title, coucheLignesFctAvanced3.definition, coucheLignesFctAvanced3.options);
    var editionLayer2bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneLignes2.title, coucheCloneLignes2.definition, coucheCloneLignes2.options);
    editionLayer2bis.title="Ma couche WFS de lignes pour la scission";
    var editionLayer3 = new Descartes.Layer.WFS(couchePolygonesFctAvanced3.title, couchePolygonesFctAvanced3.definition, couchePolygonesFctAvanced3.options);
    var editionLayer3bis = new Descartes.Layer.EditionLayer.WFS(coucheClonePolygones2.title, coucheClonePolygones2.definition, coucheClonePolygones2.options);
    editionLayer3bis.title="Ma couche WFS de polygones pour la scission";
    
    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer2bis, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
    contenuCarte.addItem(editionLayer3bis, gpWFS);
    
    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer5 = new Descartes.Layer.WFS(coucheMultiLignesFctAvanced3.title, coucheMultiLignesFctAvanced3.definition, coucheMultiLignesFctAvanced3.options);
    var editionLayer5bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiLignes2.title, coucheCloneMultiLignes2.definition, coucheCloneMultiLignes2.options);
    editionLayer5bis.title="Ma couche WFS de multi-lignes pour la scission";
    var editionLayer6 = new Descartes.Layer.WFS(coucheMultiPolygonesFctAvanced3.title, coucheMultiPolygonesFctAvanced3.definition, coucheMultiPolygonesFctAvanced3.options);
    var editionLayer6bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones2.title, coucheCloneMultiPolygones2.definition, coucheCloneMultiPolygones2.options);
    editionLayer6bis.title="Ma couche WFS de multi-polygones pour la scission";
    
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer5bis, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    contenuCarte.addItem(editionLayer6bis, gpWFSMulti); 
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
	kmlCoucheCloneLignes2.options.split.enable = true;
	kmlCoucheClonePolygones2.options.split.enable = true;
	kmlCoucheCloneMultiLignes2.options.split.enable = true;
	kmlCoucheCloneMultiPolygones2.options.split.enable = true;
	
	var editionLayerkml1 = new Descartes.Layer.KML(kmlCouchePointsFctAvanced3.title, kmlCouchePointsFctAvanced3.definition, kmlCouchePointsFctAvanced3.options);
    var editionLayerkml2 = new Descartes.Layer.KML(kmlCoucheLignesFctAvanced3.title, kmlCoucheLignesFctAvanced3.definition, kmlCoucheLignesFctAvanced3.options);
    var editionLayerkml2bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneLignes2.title, kmlCoucheCloneLignes2.definition, kmlCoucheCloneLignes2.options);
    editionLayerkml2bis.title="Ma couche KML de lignes pour la scission";
    var editionLayerkml3 = new Descartes.Layer.KML(kmlCouchePolygonesFctAvanced3.title, kmlCouchePolygonesFctAvanced3.definition, kmlCouchePolygonesFctAvanced3.options);
    var editionLayerkml3bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePolygones2.title, kmlCoucheClonePolygones2.definition, kmlCoucheClonePolygones2.options);
    editionLayerkml3bis.title="Ma couche KML de polygones pour la scission";
    
    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml2bis, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    contenuCarte.addItem(editionLayerkml3bis, gpKML);
    
    var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	
    var editionLayerkml5 = new Descartes.Layer.KML(kmlCoucheMultiLignesFctAvanced3.title, kmlCoucheMultiLignesFctAvanced3.definition, kmlCoucheMultiLignesFctAvanced3.options);
    var editionLayerkml5bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiLignes2.title, kmlCoucheCloneMultiLignes2.definition, kmlCoucheCloneMultiLignes2.options);
    editionLayerkml5bis.title="Ma couche KML de multi-lignes pour la scission";
    var editionLayerkml6 = new Descartes.Layer.KML(kmlCoucheMultiPolygonesFctAvanced3.title, kmlCoucheMultiPolygonesFctAvanced3.definition, kmlCoucheMultiPolygonesFctAvanced3.options);
    var editionLayerkml6bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPolygones2.title, kmlCoucheCloneMultiPolygones2.definition, kmlCoucheCloneMultiPolygones2.options);
    editionLayerkml6bis.title="Ma couche KML de multi-polygones pour la scission";
    
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5bis, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    contenuCarte.addItem(editionLayerkml6bis, gpKMLMulti); 
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
	geojsonCoucheCloneLignes2.options.split.enable = true;
	geojsonCoucheClonePolygones2.options.split.enable = true;
	geojsonCoucheCloneMultiLignes2.options.split.enable = true;
	geojsonCoucheCloneMultiPolygones2.options.split.enable = true;
	
	var editionLayergeojson1 = new Descartes.Layer.GeoJSON(geojsonCouchePointsFctAvanced3.title, geojsonCouchePointsFctAvanced3.definition, geojsonCouchePointsFctAvanced3.options);
    var editionLayergeojson2 = new Descartes.Layer.GeoJSON(geojsonCoucheLignesFctAvanced3.title, geojsonCoucheLignesFctAvanced3.definition, geojsonCoucheLignesFctAvanced3.options);
    var editionLayergeojson2bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneLignes2.title, geojsonCoucheCloneLignes2.definition, geojsonCoucheCloneLignes2.options);
    editionLayergeojson2bis.title="Ma couche GEOJSON de lignes pour la scission";
    var editionLayergeojson3 = new Descartes.Layer.GeoJSON(geojsonCouchePolygonesFctAvanced3.title, geojsonCouchePolygonesFctAvanced3.definition, geojsonCouchePolygonesFctAvanced3.options);
    var editionLayergeojson3bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePolygones2.title, geojsonCoucheClonePolygones2.definition, geojsonCoucheClonePolygones2.options);
    editionLayergeojson3bis.title="Ma couche GEOJSON de polygones pour la scission";
    
    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2bis, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3bis, gpGEOJSON);
    
    var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    var editionLayergeojson5 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiLignesFctAvanced3.title, geojsonCoucheMultiLignesFctAvanced3.definition, geojsonCoucheMultiLignesFctAvanced3.options);
    var editionLayergeojson5bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiLignes2.title, geojsonCoucheCloneMultiLignes2.definition, geojsonCoucheCloneMultiLignes2.options);
    editionLayergeojson5bis.title="Ma couche GEOJSON de multi-lignes pour la scission";
    var editionLayergeojson6 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced3.title, geojsonCoucheMultiPolygonesFctAvanced3.definition, geojsonCoucheMultiPolygonesFctAvanced3.options);
    var editionLayergeojson6bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPolygones2.title, geojsonCoucheCloneMultiPolygones2.definition, geojsonCoucheCloneMultiPolygones2.options);
    editionLayergeojson6bis.title="Ma couche GEOJSON de multi-polygones pour la scission";
    
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5bis, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    contenuCarte.addItem(editionLayergeojson6bis, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	       {type: Descartes.Map.EDITION_SELECTION},
	       {type: Descartes.Map.EDITION_SPLIT,
		      args: {
	              drawingType: Descartes.Layer.POINT_GEOMETRY,
	              snapping:true
		      }
          },
          {type: Descartes.Map.EDITION_SPLIT,
   	       args: {
   	    	 snapping:true
 	       }
          },
          {type: Descartes.Map.EDITION_SPLIT,
  	       args: {
               drawingType: Descartes.Layer.POLYGON_GEOMETRY,
               snapping:true
           }
          }
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
