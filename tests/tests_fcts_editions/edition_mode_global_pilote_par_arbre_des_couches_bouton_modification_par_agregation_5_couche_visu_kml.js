function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
	var editionLayerkml1 = new Descartes.Layer.KML(kmlCouchePointsFctAvanced3.title, kmlCouchePointsFctAvanced3.definition, kmlCouchePointsFctAvanced3.options);
    var editionLayerkml2 = new Descartes.Layer.KML(kmlCoucheLignesFctAvanced3.title, kmlCoucheLignesFctAvanced3.definition, kmlCoucheLignesFctAvanced3.options);
    var editionLayerkml2bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneLignes.title, kmlCoucheCloneLignes2.definition, kmlCoucheCloneLignes2.options);
    editionLayerkml2bis.title="Ma couche KML de lignes pour la scission";
    var editionLayerkml3 = new Descartes.Layer.KML(kmlCouchePolygonesFctAvanced3.title, kmlCouchePolygonesFctAvanced3.definition, kmlCouchePolygonesFctAvanced3.options);
    var editionLayerkml3bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePolygones.title, kmlCoucheClonePolygones2.definition, kmlCoucheClonePolygones2.options);
    editionLayerkml3bis.title="Ma couche KML de polygones pour la scission";
    
    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml2bis, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    contenuCarte.addItem(editionLayerkml3bis, gpKML);
    
    var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	
    var editionLayerkml5 = new Descartes.Layer.KML(kmlCoucheMultiLignesFctAvanced3.title, kmlCoucheMultiLignesFctAvanced3.definition, kmlCoucheMultiLignesFctAvanced3.options);
    var editionLayerkml5bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiLignes2.title, kmlCoucheCloneMultiLignes2.definition, kmlCoucheCloneMultiLignes2.options);
    editionLayerkml5bis.title="Ma couche KML de multi-lignes pour la scission";
    var editionLayerkml6 = new Descartes.Layer.KML(kmlCoucheMultiPolygonesFctAvanced3.title, kmlCoucheMultiPolygonesFctAvanced3.definition, kmlCoucheMultiPolygonesFctAvanced3.options);
    var editionLayerkml6bis = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPolygones2.title, kmlCoucheCloneMultiPolygones2.definition, kmlCoucheCloneMultiPolygones2.options);
    editionLayerkml6bis.title="Ma couche KML de multi-polygones pour la scission";
    
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5bis, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    contenuCarte.addItem(editionLayerkml6bis, gpKMLMulti); 
      
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	                                            
	      {type: Descartes.Map.EDITION_SELECTION},
	      {type: Descartes.Map.EDITION_AGGREGATION}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
