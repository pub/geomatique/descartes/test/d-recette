function chargementCarte() {
		
	chargeEditionCouchesGroupesWFS();
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var coucheBase = {
		title : "Fond de carte",
		type: 0,
		definition: [
			{
				serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur"
			}
		],
		options: {
			maxScale: 100,
			minScale: 10000001,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 50,
			opacityMax: 100,
			legend: [],
			metadataURL: null,
			format: "image/png"
		}
	};

	var groupeFonds = {
	    title: "Fonds cartographiques",
	    options: {
	        opened: true
	    }
	};
	    
    var layerwfs1 = new Descartes.Layer.WFS(couchePoints.title, couchePoints.definition, couchePoints.options);
    var layerwfs2 = new Descartes.Layer.WFS(coucheLignes.title, coucheLignes.definition, coucheLignes.options);
    var layerwfs3 = new Descartes.Layer.WFS(couchePolygones.title, couchePolygones.definition, couchePolygones.options);
    var layerwfs4 = new Descartes.Layer.WFS(coucheMultiPoints.title, coucheMultiPoints.definition, coucheMultiPoints.options);
    var layerwfs5 = new Descartes.Layer.WFS(coucheMultiLignes.title, coucheMultiLignes.definition, coucheMultiLignes.options);
    var layerwfs6 = new Descartes.Layer.WFS(coucheMultiPolygones.title, coucheMultiPolygones.definition, coucheMultiPolygones.options);
    
    contenuCarte.addItem(layerwfs1);
    contenuCarte.addItem(layerwfs2);
    contenuCarte.addItem(layerwfs3);
    contenuCarte.addItem(layerwfs4);
    contenuCarte.addItem(layerwfs5);
    contenuCarte.addItem(layerwfs6);
   
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
    
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
