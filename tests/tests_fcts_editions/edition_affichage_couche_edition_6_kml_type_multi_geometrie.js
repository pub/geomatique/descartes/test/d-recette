function chargementCarte() {
	
	chargeEditionCouchesGroupesKML();
	
	
	
		
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
	var editionLayer1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePoints.title, kmlCouchePoints.definition, kmlCouchePoints.options);
	var editionLayer2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignes.title, kmlCoucheLignes.definition, kmlCoucheLignes.options);
	var editionLayer3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygones.title, kmlCouchePolygones.definition, kmlCouchePolygones.options);

	contenuCarte.addItem(editionLayer1, gpKML);
	contenuCarte.addItem(editionLayer2, gpKML);
	contenuCarte.addItem(editionLayer3, gpKML);
	
	var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));

	var editionLayer4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPoints.title, kmlCoucheMultiPoints.definition, kmlCoucheMultiPoints.options);
	var editionLayer5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignes.title, kmlCoucheMultiLignes.definition, kmlCoucheMultiLignes.options);
	var editionLayer6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygones.title, kmlCoucheMultiPolygones.definition, kmlCoucheMultiPolygones.options);

	contenuCarte.addItem(editionLayer4, gpKMLMulti);
	contenuCarte.addItem(editionLayer5, gpKMLMulti);
	contenuCarte.addItem(editionLayer6, gpKMLMulti);

    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
