var editionLayer1;
var editionLayer2;
var editionLayer3;

var carte;

function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	
		
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: false,
        save: function (json) {
   	     	 //Ici, code MOE qui est spécifique à chaque application métier.
   	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
   	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
   	     	   	
        	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	    	 
         	//json.priv={status:200,message:"DESCARTES n'est pas chargé de faire la sauvegarde des objets géographiques.</br></br>Ici, pour les tests, la sauvegarde a été faite par du code particulier contenu dans les fichiers de tests."};  
           //json.callback.call(json);

        }
    });     

	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    layer1 = new Descartes.Layer.WFS(couchePoints.title, couchePoints.definition, couchePoints.options);
    layer2 = new Descartes.Layer.WFS(coucheLignes.title, coucheLignes.definition, coucheLignes.options);
    editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygones.title, couchePolygones.definition, couchePolygones.options);

    contenuCarte.addItem(layer1, gpWFS);
    contenuCarte.addItem(layer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

	var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));

	var layer4 = new Descartes.Layer.WFS(coucheMultiPoints.title, coucheMultiPoints.definition, coucheMultiPoints.options);
	var layer5 = new Descartes.Layer.WFS(coucheMultiLignes.title, coucheMultiLignes.definition, coucheMultiLignes.options);
	var layer6 = new Descartes.Layer.WFS(coucheMultiPolygones.title, coucheMultiPolygones.definition, coucheMultiPolygones.options);

	contenuCarte.addItem(layer4, gpWFSMulti);
	contenuCarte.addItem(layer5, gpWFSMulti);
	contenuCarte.addItem(layer6, gpWFSMulti);
	
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePoints.title, kmlCouchePoints.definition, kmlCouchePoints.options);
    layerkml2 = new Descartes.Layer.KML(kmlCoucheLignes.title, kmlCoucheLignes.definition, kmlCoucheLignes.options);
    layerkml3 = new Descartes.Layer.KML(kmlCouchePolygones.title, kmlCouchePolygones.definition, kmlCouchePolygones.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(layerkml2, gpKML);
    contenuCarte.addItem(layerkml3, gpKML);

	var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));

	var layerkml4 = new Descartes.Layer.KML(kmlCoucheMultiPoints.title, kmlCoucheMultiPoints.definition, kmlCoucheMultiPoints.options);
	var layerkml5 = new Descartes.Layer.KML(kmlCoucheMultiLignes.title, kmlCoucheMultiLignes.definition, kmlCoucheMultiLignes.options);
	var layerkml6 = new Descartes.Layer.KML(kmlCoucheMultiPolygones.title, kmlCoucheMultiPolygones.definition, kmlCoucheMultiPolygones.options);

	contenuCarte.addItem(layerkml4, gpKMLMulti);
	contenuCarte.addItem(layerkml5, gpKMLMulti);
	contenuCarte.addItem(layerkml6, gpKMLMulti);
	
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePoints.title, geojsonCouchePoints.definition, geojsonCouchePoints.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignes.title, geojsonCoucheLignes.definition, geojsonCoucheLignes.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygones.title, geojsonCouchePolygones.definition, geojsonCouchePolygones.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

	var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
    
    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPoints.title, geojsonCoucheMultiPoints.definition, geojsonCoucheMultiPoints.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignes.title, geojsonCoucheMultiLignes.definition, geojsonCoucheMultiLignes.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygones.title, geojsonCoucheMultiPolygones.definition, geojsonCoucheMultiPolygones.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
	
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [600, 400]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	//Ajout d'un barre d'outils d'édition
	 carte.addEditionToolBar('editionToolBar', [
			{
			   type: Descartes.Map.EDITION_SELECTION,
			   args: {
			       editionLayer: editionLayer3
			   }
			},
	       {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  editionLayer: editionLayer3
              }
	      },
	      {
	          type: Descartes.Map.EDITION_GLOBAL_MODIFICATION,
	          args: {
	        	  editionLayer: editionLayer3
              }
	      },
	      {
	          type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION,
	          args: {
	        	  editionLayer: editionLayer3
              }
	      }, {
	          type: Descartes.Map.EDITION_VERTICE_MODIFICATION,
	          args: {
	        	  editionLayer: editionLayer3
              }
	      }, {
	          type: Descartes.Map.EDITION_RUBBER_DELETION,
	          args: {
	        	  editionLayer: editionLayer3
              }
	      }, {
	          type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION,
	          args: {
	        	  editionLayer: editionLayer3
              }
	      }
	 ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
	initSelects();
	
}

function initSelects () {
	editionLayer3.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer3);
}
