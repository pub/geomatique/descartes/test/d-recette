function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
	//GB Modifs EPSG
	couchePointsFctAvanced2.title = "Ma couche WMS de points avec WFS associé";
	coucheLignesFctAvanced2.title = "Ma couche WMS de lignes avec WFS associé";
	couchePolygonesFctAvanced2.title = "Ma couche WMS de polygones avec WFS associé";
	coucheMultiPointsFctAvanced2.title = "Ma couche WMS de multipoints avec WFS associé";
	coucheMultiLignesFctAvanced2.title = "Ma couche WMS de multilignes avec WFS associé";
	coucheMultiPolygonesFctAvanced2.title = "Ma couche WMS de multipolygones avec WFS associé";

	couchePointsFctAvanced2.definition[0].layerName = couchePointsFctAvanced2.definition[0].featurePrefix+":"+couchePointsFctAvanced2.definition[0].layerName;
	coucheLignesFctAvanced2.definition[0].layerName = coucheLignesFctAvanced2.definition[0].featurePrefix+":"+coucheLignesFctAvanced2.definition[0].layerName;
	couchePolygonesFctAvanced2.definition[0].layerName = couchePolygonesFctAvanced2.definition[0].featurePrefix+":"+couchePolygonesFctAvanced2.definition[0].layerName;
	coucheMultiPointsFctAvanced2.definition[0].layerName = coucheMultiPointsFctAvanced2.definition[0].featurePrefix+":"+coucheMultiPointsFctAvanced2.definition[0].layerName;
	coucheMultiLignesFctAvanced2.definition[0].layerName = coucheMultiLignesFctAvanced2.definition[0].featurePrefix+":"+coucheMultiLignesFctAvanced2.definition[0].layerName;
	coucheMultiPolygonesFctAvanced2.definition[0].layerName = coucheMultiPolygonesFctAvanced2.definition[0].featurePrefix+":"+coucheMultiPolygonesFctAvanced2.definition[0].layerName;
	
	couchePointsFctAvanced2.definition[0].internalProjection = "EPSG:4326";
	coucheLignesFctAvanced2.definition[0].internalProjection = "EPSG:4326";
	couchePolygonesFctAvanced2.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPointsFctAvanced2.definition[0].internalProjection = "EPSG:4326";
	coucheMultiLignesFctAvanced2.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPolygonesFctAvanced2.definition[0].internalProjection = "EPSG:4326";

	couchePointsFctAvanced2.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheLignesFctAvanced2.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	couchePolygonesFctAvanced2.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPointsFctAvanced2.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiLignesFctAvanced2.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPolygonesFctAvanced2.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";	

	var editionLayer1 = new Descartes.Layer.WMS(couchePointsFctAvanced2.title, couchePointsFctAvanced2.definition, couchePointsFctAvanced2.options);
    var editionLayer2 = new Descartes.Layer.WMS(coucheLignesFctAvanced2.title, coucheLignesFctAvanced2.definition, coucheLignesFctAvanced2.options);
    var editionLayer3 = new Descartes.Layer.WMS(couchePolygonesFctAvanced2.title, couchePolygonesFctAvanced2.definition, couchePolygonesFctAvanced2.options);
    var editionLayer1b = new Descartes.Layer.EditionLayer.WFS(coucheClonePoints.title, coucheClonePoints.definition, coucheClonePoints.options);
    var editionLayer2b = new Descartes.Layer.EditionLayer.WFS(coucheCloneLignes.title, coucheCloneLignes.definition, coucheCloneLignes.options);
    var editionLayer3b = new Descartes.Layer.EditionLayer.WFS(coucheClonePolygones.title, coucheClonePolygones.definition, coucheClonePolygones.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
    contenuCarte.addItem(editionLayer1b, gpWFS);
    contenuCarte.addItem(editionLayer2b, gpWFS);
    contenuCarte.addItem(editionLayer3b, gpWFS);
    

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer4 = new Descartes.Layer.WMS(coucheMultiPointsFctAvanced2.title, coucheMultiPointsFctAvanced2.definition, coucheMultiPointsFctAvanced2.options);
    var editionLayer5 = new Descartes.Layer.WMS(coucheMultiLignesFctAvanced2.title, coucheMultiLignesFctAvanced2.definition, coucheMultiLignesFctAvanced2.options);
    var editionLayer6 = new Descartes.Layer.WMS(coucheMultiPolygonesFctAvanced2.title, coucheMultiPolygonesFctAvanced2.definition, coucheMultiPolygonesFctAvanced2.options);
    var editionLayer4b = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPoints.title, coucheCloneMultiPoints.definition, coucheCloneMultiPoints.options);
    var editionLayer5b = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiLignes.title, coucheCloneMultiLignes.definition, coucheCloneMultiLignes.options);
    var editionLayer6b = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones.title, coucheCloneMultiPolygones.definition, coucheCloneMultiPolygones.options);

    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    contenuCarte.addItem(editionLayer4b, gpWFSMulti);
    contenuCarte.addItem(editionLayer5b, gpWFSMulti);
    contenuCarte.addItem(editionLayer6b, gpWFSMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_CLONE_CREATION},
	      {type: Descartes.Map.EDITION_SELECTION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_ATTRIBUTE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
