proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

var editionLayer1;
var editionLayer2;
var editionLayer3;
var editionLayer4;
var editionLayer5;
var editionLayer6;
var editionLayer1b;
var editionLayer2b;
var editionLayer3b;
var editionLayer4b;
var editionLayer5b;
var editionLayer6b;

var carte;

function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	
		
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        onlyAppMode:true,
        save: function (json) {
   	     	 //Ici, code MOE qui est spécifique à chaque application métier.
   	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
   	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
   	     	   	
        	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	    	 
         	//json.priv={status:200,message:"DESCARTES n'est pas chargé de faire la sauvegarde des objets géographiques.</br></br>Ici, pour les tests, la sauvegarde a été faite par du code particulier contenu dans les fichiers de tests."};  
           //json.callback.call(json);

        }
    });     

	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsFctAvanced3.title, couchePointsFctAvanced3.definition, couchePointsFctAvanced3.options);
    delete editionLayer1.copy;
    editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesFctAvanced3.title, coucheLignesFctAvanced3.definition, coucheLignesFctAvanced3.options);
    delete editionLayer2.copy;
    delete editionLayer2.aggregate;
    delete editionLayer2.unaggregate;
    editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesFctAvanced3.title, couchePolygonesFctAvanced3.definition, couchePolygonesFctAvanced3.options);
    delete editionLayer3.copy;
    delete editionLayer3.buffer;
    delete editionLayer3.halo;
    delete editionLayer3.aggregate;
    delete editionLayer3.unaggregate;
    editionLayer1b = new Descartes.Layer.EditionLayer.WFS(coucheClonePoints.title, coucheClonePoints.definition, coucheClonePoints.options);
    editionLayer2b = new Descartes.Layer.EditionLayer.WFS(coucheCloneLignes.title, coucheCloneLignes.definition, coucheCloneLignes.options);
    editionLayer3b = new Descartes.Layer.EditionLayer.WFS(coucheClonePolygones.title, coucheClonePolygones.definition, coucheClonePolygones.options);
    editionLayer3b.copy.enable=true;
    editionLayer3b.clone.enable=true;
    editionLayer3b.substract.enable=true;
    editionLayer3b.homothetic.enable=true;
    editionLayer3b.buffer.enable=false;
    editionLayer3b.halo.enable=false;
    editionLayer3b.split.enable=true;
    editionLayer3b.divide.enable=true;
    
    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
    contenuCarte.addItem(editionLayer1b, gpWFS);
    contenuCarte.addItem(editionLayer2b, gpWFS);
    contenuCarte.addItem(editionLayer3b, gpWFS);
    

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsFctAvanced3.title, coucheMultiPointsFctAvanced3.definition, coucheMultiPointsFctAvanced3.options);
    delete editionLayer4.copy;
    delete editionLayer4.aggregate;
    editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesFctAvanced3.title, coucheMultiLignesFctAvanced3.definition, coucheMultiLignesFctAvanced3.options);
    delete editionLayer5.copy;
    delete editionLayer5.aggregate;
    editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesFctAvanced3.title, coucheMultiPolygonesFctAvanced3.definition, coucheMultiPolygonesFctAvanced3.options);
    delete editionLayer6.copy;
    delete editionLayer6.buffer;
    delete editionLayer6.halo;
    delete editionLayer6.aggregate;
    delete editionLayer6.unaggregate;
    editionLayer4b = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPoints.title, coucheCloneMultiPoints.definition, coucheCloneMultiPoints.options);
    editionLayer5b = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiLignes.title, coucheCloneMultiLignes.definition, coucheCloneMultiLignes.options);
    editionLayer6b = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones.title, coucheCloneMultiPolygones.definition, coucheCloneMultiPolygones.options);
    editionLayer6b.copy.enable=true;
    editionLayer6b.clone.enable=true;
    editionLayer6b.substract.enable=true;
    editionLayer6b.homothetic.enable=true;
    editionLayer6b.buffer.enable=false;
    editionLayer6b.halo.enable=false;
    editionLayer6b.split.enable=true;
    editionLayer6b.divide.enable=true;
    
    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    contenuCarte.addItem(editionLayer4b, gpWFSMulti);
    contenuCarte.addItem(editionLayer5b, gpWFSMulti);
    contenuCarte.addItem(editionLayer6b, gpWFSMulti); 
 
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsFctAvanced3.title, kmlCouchePointsFctAvanced3.definition, kmlCouchePointsFctAvanced3.options);
    delete editionLayerkml1.copy;
    editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesFctAvanced3.title, kmlCoucheLignesFctAvanced3.definition, kmlCoucheLignesFctAvanced3.options);
    delete editionLayerkml2.copy;
    delete editionLayerkml2.aggregate;
    delete editionLayerkml2.unaggregate;
    editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesFctAvanced3.title, kmlCouchePolygonesFctAvanced3.definition, kmlCouchePolygonesFctAvanced3.options);
    delete editionLayerkml3.copy;
    delete editionLayerkml3.buffer;
    delete editionLayerkml3.halo;
    delete editionLayerkml3.aggregate;
    delete editionLayerkml3.unaggregate;
    editionLayerkml1b = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePoints.title, kmlCoucheClonePoints.definition, kmlCoucheClonePoints.options);
    editionLayerkml2b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneLignes.title, kmlCoucheCloneLignes.definition, kmlCoucheCloneLignes.options);
    editionLayerkml3b = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePolygones.title, kmlCoucheClonePolygones.definition, kmlCoucheClonePolygones.options);
    editionLayerkml3b.copy.enable=true;
    editionLayerkml3b.clone.enable=true;
    editionLayerkml3b.substract.enable=true;
    editionLayerkml3b.homothetic.enable=true;
    editionLayerkml3b.buffer.enable=false;
    editionLayerkml3b.halo.enable=false;
    editionLayerkml3b.split.enable=true;
    editionLayerkml3b.divide.enable=true;
    
    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    contenuCarte.addItem(editionLayerkml1b, gpKML);
    contenuCarte.addItem(editionLayerkml2b, gpKML);
    contenuCarte.addItem(editionLayerkml3b, gpKML);
    

    var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	
    editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsFctAvanced3.title, kmlCoucheMultiPointsFctAvanced3.definition, kmlCoucheMultiPointsFctAvanced3.options);
    delete editionLayerkml4.copy;
    delete editionLayerkml4.aggregate;
    editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesFctAvanced3.title, kmlCoucheMultiLignesFctAvanced3.definition, kmlCoucheMultiLignesFctAvanced3.options);
    delete editionLayerkml5.copy;
    delete editionLayerkml5.aggregate;
    editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesFctAvanced3.title, kmlCoucheMultiPolygonesFctAvanced3.definition, kmlCoucheMultiPolygonesFctAvanced3.options);
    delete editionLayerkml6.copy;
    delete editionLayerkml6.buffer;
    delete editionLayerkml6.halo;
    delete editionLayerkml6.aggregate;
    delete editionLayerkml6.unaggregate;
    editionLayerkml4b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPoints.title, kmlCoucheCloneMultiPoints.definition, kmlCoucheCloneMultiPoints.options);
    editionLayerkml5b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiLignes.title, kmlCoucheCloneMultiLignes.definition, kmlCoucheCloneMultiLignes.options);
    editionLayerkml6b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPolygones.title, kmlCoucheCloneMultiPolygones.definition, kmlCoucheCloneMultiPolygones.options);
    editionLayerkml6b.copy.enable=true;
    editionLayerkml6b.clone.enable=true;
    editionLayerkml6b.substract.enable=true;
    editionLayerkml6b.homothetic.enable=true;
    editionLayerkml6b.buffer.enable=false;
    editionLayerkml6b.halo.enable=false;
    editionLayerkml6b.split.enable=true;
    editionLayerkml6b.divide.enable=true;
    
    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    contenuCarte.addItem(editionLayerkml4b, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5b, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6b, gpKMLMulti);   
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsFctAvanced3.title, geojsonCouchePointsFctAvanced3.definition, geojsonCouchePointsFctAvanced3.options);
    delete editionLayergeojson1.copy;
    editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesFctAvanced3.title, geojsonCoucheLignesFctAvanced3.definition, geojsonCoucheLignesFctAvanced3.options);
    delete editionLayergeojson2.copy;
    delete editionLayergeojson2.aggregate;
    delete editionLayergeojson2.unaggregate;
    editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesFctAvanced3.title, geojsonCouchePolygonesFctAvanced3.definition, geojsonCouchePolygonesFctAvanced3.options);
    delete editionLayergeojson3.copy;
    delete editionLayergeojson3.buffer;
    delete editionLayergeojson3.halo;
    delete editionLayergeojson3.aggregate;
    delete editionLayergeojson3.unaggregate;
    editionLayergeojson1b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePoints.title, geojsonCoucheClonePoints.definition, geojsonCoucheClonePoints.options);
    editionLayergeojson2b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneLignes.title, geojsonCoucheCloneLignes.definition, geojsonCoucheCloneLignes.options);
    editionLayergeojson3b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePolygones.title, geojsonCoucheClonePolygones.definition, geojsonCoucheClonePolygones.options);
    editionLayergeojson3b.copy.enable=true;
    editionLayergeojson3b.clone.enable=true;
    editionLayergeojson3b.substract.enable=true;
    editionLayergeojson3b.homothetic.enable=true;
    editionLayergeojson3b.buffer.enable=false;
    editionLayergeojson3b.halo.enable=false;
    editionLayergeojson3b.split.enable=true;
    editionLayergeojson3b.divide.enable=true;
    
    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson1b, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2b, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3b, gpGEOJSON);
    

    var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsFctAvanced3.title, geojsonCoucheMultiPointsFctAvanced3.definition, geojsonCoucheMultiPointsFctAvanced3.options);
    delete editionLayergeojson4.copy;
    delete editionLayergeojson4.aggregate;
    editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesFctAvanced3.title, geojsonCoucheMultiLignesFctAvanced3.definition, geojsonCoucheMultiLignesFctAvanced3.options);
    delete editionLayergeojson5.copy;
    delete editionLayergeojson5.aggregate;
    editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced3.title, geojsonCoucheMultiPolygonesFctAvanced3.definition, geojsonCoucheMultiPolygonesFctAvanced3.options);
    delete editionLayergeojson6.copy;
    delete editionLayergeojson6.buffer;
    delete editionLayergeojson6.halo;
    delete editionLayergeojson6.aggregate;
    delete editionLayergeojson6.unaggregate;
    editionLayergeojson4b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPoints.title, geojsonCoucheCloneMultiPoints.definition, geojsonCoucheCloneMultiPoints.options);
    editionLayergeojson5b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiLignes.title, geojsonCoucheCloneMultiLignes.definition, geojsonCoucheCloneMultiLignes.options);
    editionLayergeojson6b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPolygones.title, geojsonCoucheCloneMultiPolygones.definition, geojsonCoucheCloneMultiPolygones.options);
    editionLayergeojson6b.copy.enable=true;
    editionLayergeojson6b.clone.enable=true;
    editionLayergeojson6b.substract.enable=true;
    editionLayergeojson6b.homothetic.enable=true;
    editionLayergeojson6b.buffer.enable=false;
    editionLayergeojson6b.halo.enable=false;
    editionLayergeojson6b.split.enable=true;
    editionLayergeojson6b.divide.enable=true;
    
    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    contenuCarte.addItem(editionLayergeojson4b, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5b, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6b, gpGEOJSONMulti);   
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [600, 400]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	//Ajout d'un barre d'outils d'édition
	carte.addEditionToolBar('editionToolBar', [
	       {type: Descartes.Map.EDITION_COPY_CREATION},
		  {type: Descartes.Map.EDITION_CLONE_CREATION},
		  {type: Descartes.Map.EDITION_BUFFER_NORMAL_CREATION},
		  {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION},
		  {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION},
	       {type: Descartes.Map.EDITION_SPLIT,
	  	       args: {
	               drawingType: Descartes.Layer.POLYGON_GEOMETRY
	           }
	       },
		  {type: Descartes.Map.EDITION_DIVIDE},
		  {type: Descartes.Map.EDITION_AGGREGATION},
		  {type: Descartes.Map.EDITION_UNAGGREGATION_CREATION}
    ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
	initSelects();
}

function initSelects () {
	
	editionLayer1b.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer1b);
	editionLayer2b.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer2b);
	editionLayer3b.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer3b);
	editionLayer4b.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer4b);
	editionLayer5b.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer5b);
	editionLayer6b.OL_layers[0].getSource().on("featureloadend", init_selects_editionLayer6b);
	
}

