function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.KML(kmlCouchePointsFctAvanced2.title, kmlCouchePointsFctAvanced2.definition, kmlCouchePointsFctAvanced2.options);
    var editionLayerkml2 = new Descartes.Layer.KML(kmlCoucheLignesFctAvanced2.title, kmlCoucheLignesFctAvanced2.definition, kmlCoucheLignesFctAvanced2.options);
    var editionLayerkml3 = new Descartes.Layer.KML(kmlCouchePolygonesFctAvanced2.title, kmlCouchePolygonesFctAvanced2.definition, kmlCouchePolygonesFctAvanced2.options);
    var editionLayerkml1b = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePoints.title, kmlCoucheClonePoints.definition, kmlCoucheClonePoints.options);
    var editionLayerkml2b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneLignes.title, kmlCoucheCloneLignes.definition, kmlCoucheCloneLignes.options);
    var editionLayerkml3b = new Descartes.Layer.EditionLayer.KML(kmlCoucheClonePolygones.title, kmlCoucheClonePolygones.definition, kmlCoucheClonePolygones.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    contenuCarte.addItem(editionLayerkml1b, gpKML);
    contenuCarte.addItem(editionLayerkml2b, gpKML);
    contenuCarte.addItem(editionLayerkml3b, gpKML);
    

    var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
	
    var editionLayerkml4 = new Descartes.Layer.KML(kmlCoucheMultiPointsFctAvanced2.title, kmlCoucheMultiPointsFctAvanced2.definition, kmlCoucheMultiPointsFctAvanced2.options);
    var editionLayerkml5 = new Descartes.Layer.KML(kmlCoucheMultiLignesFctAvanced2.title, kmlCoucheMultiLignesFctAvanced2.definition, kmlCoucheMultiLignesFctAvanced2.options);
    var editionLayerkml6 = new Descartes.Layer.KML(kmlCoucheMultiPolygonesFctAvanced2.title, kmlCoucheMultiPolygonesFctAvanced2.definition, kmlCoucheMultiPolygonesFctAvanced2.options);
    var editionLayerkml4b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPoints.title, kmlCoucheCloneMultiPoints.definition, kmlCoucheCloneMultiPoints.options);
    var editionLayerkml5b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiLignes.title, kmlCoucheCloneMultiLignes.definition, kmlCoucheCloneMultiLignes.options);
    var editionLayerkml6b = new Descartes.Layer.EditionLayer.KML(kmlCoucheCloneMultiPolygones.title, kmlCoucheCloneMultiPolygones.definition, kmlCoucheCloneMultiPolygones.options);

    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    contenuCarte.addItem(editionLayerkml4b, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5b, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6b, gpKMLMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_CLONE_CREATION},
	      {type: Descartes.Map.EDITION_SELECTION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_ATTRIBUTE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
