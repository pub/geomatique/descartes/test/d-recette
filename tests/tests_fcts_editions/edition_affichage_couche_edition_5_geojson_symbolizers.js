function chargementCarte() {
	
	chargeEditionCouchesGroupesGEOJSON();
	
	
	
		
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
	var editionLayer1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsStyle.title, geojsonCouchePointsStyle.definition, geojsonCouchePointsStyle.options);
	var editionLayer2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesStyle.title, geojsonCoucheLignesStyle.definition, geojsonCoucheLignesStyle.options);
	var editionLayer3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesStyle.title, geojsonCouchePolygonesStyle.definition, geojsonCouchePolygonesStyle.options);

	contenuCarte.addItem(editionLayer1, gpGEOJSON);
	contenuCarte.addItem(editionLayer2, gpGEOJSON);
	contenuCarte.addItem(editionLayer3, gpGEOJSON);

	var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    var editionLayer4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsStyle.title, geojsonCoucheMultiPointsStyle.definition, geojsonCoucheMultiPointsStyle.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesStyle.title, geojsonCoucheMultiLignesStyle.definition, geojsonCoucheMultiLignesStyle.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesStyle.title, geojsonCoucheMultiPolygonesStyle.definition, geojsonCoucheMultiPolygonesStyle.options);

    contenuCarte.addItem(editionLayer4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayer5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayer6, gpGEOJSONMulti); 

   
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
