proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

var carte;
function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global pilot� par l'arbre des couches
	      save: function (json) {
		  	 //Ici, code MOE qui est spécifique à chaque application métier.
		 	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		    //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		 	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		  	 sendRequestBouchonForSaveElements(json);
		
		 }
	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer6b = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones.title, coucheCloneMultiPolygones.definition, coucheCloneMultiPolygones.options);
    editionLayer6b.copy.enable=true;
    editionLayer6b.clone.enable=true;
    editionLayer6b.substract.enable=true;
    editionLayer6b.homothetic.enable=true;
    editionLayer6b.buffer.enable=true;
    editionLayer6b.halo.enable=true;
    editionLayer6b.split.enable=true;
    editionLayer6b.divide.enable=true;
    
    contenuCarte.addItem(editionLayer6b, gpWFSMulti); 
         
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_SELECTION/*,
	    	  args:{
	    		  showInfos:{attributes:true}
	    	  }*/
	      },
	      {type: Descartes.Map.EDITION_DRAW_CREATION},
	      {type: Descartes.Map.EDITION_COPY_CREATION},
	      {type: Descartes.Map.EDITION_CLONE_CREATION},
	      {type: Descartes.Map.EDITION_BUFFER_NORMAL_CREATION},
	      {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION},
	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION},
	      {type: Descartes.Map.EDITION_SPLIT,
	       args: {
              drawingType: Descartes.Layer.POINT_GEOMETRY
	       }
          },
          {type: Descartes.Map.EDITION_SPLIT},
          {type: Descartes.Map.EDITION_SPLIT,
  	       args: {
               drawingType: Descartes.Layer.POLYGON_GEOMETRY
           }
          },
	      {type: Descartes.Map.EDITION_DIVIDE},
	      {type: Descartes.Map.EDITION_AGGREGATION},
	      {type: Descartes.Map.EDITION_UNAGGREGATION_CREATION},
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
	      {type: Descartes.Map.EDITION_MERGE_MODIFICATION},
	      {type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_ATTRIBUTE},
	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION},
	      {type: Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION},
	      {type: Descartes.Map.EDITION_SELECTION_ATTRIBUT},
	      {type: Descartes.Map.EDITION_SELECTION_DELETION},
	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
