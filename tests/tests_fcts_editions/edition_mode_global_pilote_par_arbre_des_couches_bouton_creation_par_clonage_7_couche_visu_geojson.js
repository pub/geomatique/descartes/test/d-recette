function chargementCarte() {
	
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.GeoJSON(geojsonCouchePointsFctAvanced2.title, geojsonCouchePointsFctAvanced2.definition, geojsonCouchePointsFctAvanced2.options);
    var editionLayergeojson2 = new Descartes.Layer.GeoJSON(geojsonCoucheLignesFctAvanced2.title, geojsonCoucheLignesFctAvanced2.definition, geojsonCoucheLignesFctAvanced2.options);
    var editionLayergeojson3 = new Descartes.Layer.GeoJSON(geojsonCouchePolygonesFctAvanced2.title, geojsonCouchePolygonesFctAvanced2.definition, geojsonCouchePolygonesFctAvanced2.options);
    var editionLayergeojson1b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePoints.title, geojsonCoucheClonePoints.definition, geojsonCoucheClonePoints.options);
    var editionLayergeojson2b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneLignes.title, geojsonCoucheCloneLignes.definition, geojsonCoucheCloneLignes.options);
    var editionLayergeojson3b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePolygones.title, geojsonCoucheClonePolygones.definition, geojsonCoucheClonePolygones.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson1b, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2b, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3b, gpGEOJSON);
    

    var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    var editionLayergeojson4 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiPointsFctAvanced2.title, geojsonCoucheMultiPointsFctAvanced2.definition, geojsonCoucheMultiPointsFctAvanced2.options);
    var editionLayergeojson5 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiLignesFctAvanced2.title, geojsonCoucheMultiLignesFctAvanced2.definition, geojsonCoucheMultiLignesFctAvanced2.options);
    var editionLayergeojson6 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced2.title, geojsonCoucheMultiPolygonesFctAvanced2.definition, geojsonCoucheMultiPolygonesFctAvanced2.options);
    var editionLayergeojson4b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPoints.title, geojsonCoucheCloneMultiPoints.definition, geojsonCoucheCloneMultiPoints.options);
    var editionLayergeojson5b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiLignes.title, geojsonCoucheCloneMultiLignes.definition, geojsonCoucheCloneMultiLignes.options);
    var editionLayergeojson6b = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPolygones.title, geojsonCoucheCloneMultiPolygones.definition, geojsonCoucheCloneMultiPolygones.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    contenuCarte.addItem(editionLayergeojson4b, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5b, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6b, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_CLONE_CREATION},
	      {type: Descartes.Map.EDITION_SELECTION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_ATTRIBUTE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
