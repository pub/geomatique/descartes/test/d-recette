function init_selects_editionLayer1 (e) {
	if(e && e.target && e.target.getFeatures()){
		select(e.target.getFeatures(),"select_modif_global1",editionLayer1,selectForEdition,Descartes.Map.EDITION_GLOBAL_MODIFICATION);
		select(e.target.getFeatures(),"select_modif_vertice1",editionLayer1,selectForEdition,Descartes.Map.EDITION_VERTICE_MODIFICATION); 
		select(e.target.getFeatures(),"select_visu1",editionLayer1,selection); 
		select(e.target.getFeatures(),"select_sup1",editionLayer1,selectForDeletion);
	}
}
function init_selects_editionLayer2 (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_global2",editionLayer2,selectForEdition,Descartes.Map.EDITION_GLOBAL_MODIFICATION); 
	  select(e.target.getFeatures(),"select_modif_vertice2",editionLayer2,selectForEdition,Descartes.Map.EDITION_VERTICE_MODIFICATION); 
	  select(e.target.getFeatures(),"select_visu2",editionLayer2,selection); 
	  select(e.target.getFeatures(),"select_sup2",editionLayer2,selectForDeletion); 
	}
}
function init_selects_editionLayer3 (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_global3",editionLayer3,selectForEdition,Descartes.Map.EDITION_GLOBAL_MODIFICATION); 
	  select(e.target.getFeatures(),"select_modif_vertice3",editionLayer3,selectForEdition,Descartes.Map.EDITION_VERTICE_MODIFICATION); 
	  select(e.target.getFeatures(),"select_visu3",editionLayer3,selection); 
	  select(e.target.getFeatures(),"select_sup3",editionLayer3,selectForDeletion); 
	}
}
function init_selects_editionLayer4 (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_global4",editionLayer4,selectForEdition,Descartes.Map.EDITION_GLOBAL_MODIFICATION); 
	  select(e.target.getFeatures(),"select_modif_vertice4",editionLayer4,selectForEdition,Descartes.Map.EDITION_VERTICE_MODIFICATION); 
	  select(e.target.getFeatures(),"select_visu4",editionLayer4,selection); 
	  select(e.target.getFeatures(),"select_sup4",editionLayer4,selectForDeletion); 
	}
}
function init_selects_editionLayer5 (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_global5",editionLayer5,selectForEdition,Descartes.Map.EDITION_GLOBAL_MODIFICATION); 
	  select(e.target.getFeatures(),"select_modif_vertice5",editionLayer5,selectForEdition,Descartes.Map.EDITION_VERTICE_MODIFICATION); 
	  select(e.target.getFeatures(),"select_visu5",editionLayer5,selection); 
	  select(e.target.getFeatures(),"select_sup5",editionLayer5,selectForDeletion); 
	}
}
function init_selects_editionLayer6 (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_global6",editionLayer6,selectForEdition,Descartes.Map.EDITION_GLOBAL_MODIFICATION); 
	  select(e.target.getFeatures(),"select_modif_vertice6",editionLayer6,selectForEdition,Descartes.Map.EDITION_VERTICE_MODIFICATION); 
	  select(e.target.getFeatures(),"select_visu6",editionLayer6,selection); 
	  select(e.target.getFeatures(),"select_sup6",editionLayer6,selectForDeletion); 
	}
}

function init_selects_editionLayer1b (e) {
	if(e && e.target && e.target.getFeatures()){
		//select(e.target.getFeatures(),"select_visu1",editionLayer1b,selection); 
		//select(e.target.getFeatures(),"select_sup1",editionLayer1b,selectForDeletion);
	}
}
function init_selects_editionLayer2b (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_aggregat2",editionLayer2b,selectForEdition,Descartes.Map.EDITION_AGGREGATION); 
	  select(e.target.getFeatures(),"select_modif_split2",editionLayer2b,selectForEdition,Descartes.Map.EDITION_SPLIT); 
	  select(e.target.getFeatures(),"select_modif_divide2",editionLayer2b,selectForEdition,Descartes.Map.EDITION_DIVIDE); 
	}
}
function init_selects_editionLayer3b (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_substract3",editionLayer3b,selectForEdition,Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION); 
	  select(e.target.getFeatures(),"select_modif_aggregat3",editionLayer3b,selectForEdition,Descartes.Map.EDITION_AGGREGATION); 
	  select(e.target.getFeatures(),"select_modif_split3",editionLayer3b,selectForEdition,Descartes.Map.EDITION_SPLIT); 
	  select(e.target.getFeatures(),"select_modif_divide3",editionLayer3b,selectForEdition,Descartes.Map.EDITION_DIVIDE); 


	}
}
function init_selects_editionLayer4b (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_aggregat4",editionLayer4b,selectForEdition,Descartes.Map.EDITION_AGGREGATION); 
	}
}
function init_selects_editionLayer5b (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_aggregat5",editionLayer5b,selectForEdition,Descartes.Map.EDITION_AGGREGATION); 
	  select(e.target.getFeatures(),"select_modif_split5",editionLayer5b,selectForEdition,Descartes.Map.EDITION_SPLIT); 
	  select(e.target.getFeatures(),"select_modif_divide5",editionLayer5b,selectForEdition,Descartes.Map.EDITION_DIVIDE); 

	}
}
function init_selects_editionLayer6b (e) {
	if(e && e.target && e.target.getFeatures()){
	  select(e.target.getFeatures(),"select_modif_substract6",editionLayer6b,selectForEdition,Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION); 
	  select(e.target.getFeatures(),"select_modif_aggregat6",editionLayer6b,selectForEdition,Descartes.Map.EDITION_AGGREGATION); 
	  select(e.target.getFeatures(),"select_modif_split6",editionLayer6b,selectForEdition,Descartes.Map.EDITION_SPLIT); 
	  select(e.target.getFeatures(),"select_modif_divide6",editionLayer6b,selectForEdition,Descartes.Map.EDITION_DIVIDE); 

	}
}


function select(features,selectid,editionLayer,fct,outil) {
	  
  var div = document.getElementById(selectid);
  
  if(div){
	  if(div.childNodes){
		 for (var i=(div.childNodes.length-1) ; i>=0 ; i--) {
			div.removeChild(div.childNodes[i]);
		 } 
	  }

	  var optionsSelect = [];
	  optionsSelect.push({value:"",text:"Sélectionner un identifiant"});
	  if(features){
		 for (var i=0, len=features.length ; i < len ; i++) {
			 	var feature = features[i];
				var option = {value:feature.getId(),text:feature.getId().substring(feature.getId().indexOf(".")+1)};
				optionsSelect.push(option);
		 }
	  }
	  
	  optionsSelect = optionsSelect.sort(function(a, b) {
		 return a.text - b.text;
      });
	  
	  var selectElement = createSelectInputWithLabel(selectid,editionLayer.title,optionsSelect);
	  selectElement.onchange = function() {
		 if(this.lastChild[this.lastChild.selectedIndex].value !== ""){
			 if(outil!==null){
				 fct(editionLayer,this.lastChild[this.lastChild.selectedIndex].value,outil);  
			 } else {
				 fct(editionLayer,this.lastChild[this.lastChild.selectedIndex].value);
			 }
			
		 }
		 
	  };

	  div.appendChild(selectElement); 
  }
  
}


function launchCreation(editionLayer,objectId,activeTool,optionsTool) {
    var json = {
        editionLayer: editionLayer,
        objectId: objectId,
        activeTool: activeTool
    };
    if(optionsTool){
    	json.optionsTool=optionsTool;
    }
    Descartes.EditionManager.createObject(json);
}

function selectForEdition(editionLayer,objectId,activateTool) {

    var json = {
        editionLayer: editionLayer,
        objectId: objectId,
        activeTool: activateTool
    };
    Descartes.EditionManager.selectForEdition(json);
  
}
function selectForDeletion(editionLayer,objectId) {

    var json = {
        editionLayer: editionLayer,
        objectId: objectId
    };
    Descartes.EditionManager.selectForDeletion(json);
    
    carte.OL_map.zoomToMaxExtent();
   
}

 function selection(editionLayer,objectId) {

    var json = {
        editionLayer: editionLayer,
        objectId: objectId
    };
    Descartes.EditionManager.select(json);
 
}
	
function createLabelSpan(labelText, options) {
	var defaultOptions = Descartes.Utils.extend({}, options);
	var labelElement = document.createElement('label');
	labelElement.innerHTML = labelText;
	if (options.separator === undefined || options.separator !== false) {
		labelElement.innerHTML += "&nbsp;:";
	}
	if (defaultOptions.labelClassName !== undefined) {
		labelElement.className = defaultOptions.labelClassName;
	}
	return labelElement;
}
	
function createSelectInput(selectName, values, options) {
	var defaultOptions = {size:1};
	defaultOptions = Descartes.Utils.extend(defaultOptions, options);
	var selectElement = document.createElement('select');
	selectElement.name = selectName;
	selectElement.id = selectName;
	selectElement.size = defaultOptions.size;
	if (defaultOptions.selectClassName !== undefined) {
		selectElement.className = defaultOptions.selectClassName;
	}
	for (var i=0, len=values.length ; i<len ; i++) {
		var optionElement = document.createElement('option');
		optionElement.value = values[i].value.toString();
		optionElement.selected = (values[i].value == defaultOptions.value);
		optionElement.innerHTML = values[i].text;
		selectElement.appendChild(optionElement);
	}
	return selectElement;
}
	
 function createSelectInputWithLabel(selectName, labelText, values, options) {
	var defaultOptions = Descartes.Utils.extend({}, options);

	var globalElement = document.createElement('div');
	if (defaultOptions.globalClassName !== undefined) {
		globalElement.className = defaultOptions.globalClassName;
	}
	globalElement.appendChild(createLabelSpan(labelText, defaultOptions));
	globalElement.appendChild(createSelectInput(selectName, values, defaultOptions));
	return globalElement;
}