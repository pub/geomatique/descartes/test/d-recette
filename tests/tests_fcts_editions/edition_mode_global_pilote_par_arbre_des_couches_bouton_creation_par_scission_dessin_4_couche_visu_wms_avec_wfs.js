function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
	//GB Modifs EPSG
	coucheLignesFctAvanced3.title = "Ma couche WMS de lignes avec WFS associé";
	couchePolygonesFctAvanced3.title = "Ma couche WMS de polygones avec WFS associé";
	coucheMultiLignesFctAvanced3.title = "Ma couche WMS de multilignes avec WFS associé";
	coucheMultiPolygonesFctAvanced3.title = "Ma couche WMS de multipolygones avec WFS associé";
	
	coucheLignesFctAvanced3.definition[0].layerName = coucheLignesFctAvanced3.definition[0].featurePrefix+":"+coucheLignesFctAvanced3.definition[0].layerName;
	couchePolygonesFctAvanced3.definition[0].layerName = couchePolygonesFctAvanced3.definition[0].featurePrefix+":"+couchePolygonesFctAvanced3.definition[0].layerName;
	coucheMultiLignesFctAvanced3.definition[0].layerName = coucheMultiLignesFctAvanced3.definition[0].featurePrefix+":"+coucheMultiLignesFctAvanced3.definition[0].layerName;
	coucheMultiPolygonesFctAvanced3.definition[0].layerName = coucheMultiPolygonesFctAvanced3.definition[0].featurePrefix+":"+coucheMultiPolygonesFctAvanced3.definition[0].layerName;
	
	coucheLignesFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	couchePolygonesFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	coucheMultiLignesFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPolygonesFctAvanced3.definition[0].internalProjection = "EPSG:4326";
	
	coucheLignesFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	couchePolygonesFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiLignesFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPolygonesFctAvanced3.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";

	
    var editionLayer2 = new Descartes.Layer.WMS(coucheLignesFctAvanced3.title, coucheLignesFctAvanced3.definition, coucheLignesFctAvanced3.options);
    var editionLayer2bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneLignes.title, coucheCloneLignes.definition, coucheCloneLignes.options);
    editionLayer2bis.title="Ma couche de lignes pour la scission";
    var editionLayer3 = new Descartes.Layer.WMS(couchePolygonesFctAvanced3.title, couchePolygonesFctAvanced3.definition, couchePolygonesFctAvanced3.options);
    var editionLayer3bis = new Descartes.Layer.EditionLayer.WFS(coucheClonePolygones.title, coucheClonePolygones.definition, coucheClonePolygones.options);
    editionLayer3bis.title="Ma couche de polygones pour la scission";
    
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer2bis, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
    contenuCarte.addItem(editionLayer3bis, gpWFS);
    
    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer5 = new Descartes.Layer.WMS(coucheMultiLignesFctAvanced3.title, coucheMultiLignesFctAvanced3.definition, coucheMultiLignesFctAvanced3.options);
    var editionLayer5bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiLignes.title, coucheCloneMultiLignes.definition, coucheCloneMultiLignes.options);
    editionLayer5bis.title="Ma couche de multi-lignes pour la scission";
    var editionLayer6 = new Descartes.Layer.WMS(coucheMultiPolygonesFctAvanced3.title, coucheMultiPolygonesFctAvanced3.definition, coucheMultiPolygonesFctAvanced3.options);
    var editionLayer6bis = new Descartes.Layer.EditionLayer.WFS(coucheCloneMultiPolygones.title, coucheCloneMultiPolygones.definition, coucheCloneMultiPolygones.options);
    editionLayer6bis.title="Ma couche de multi-polygones pour la scission";
    
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer5bis, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    contenuCarte.addItem(editionLayer6bis, gpWFSMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_SPLIT,
	       args: {
              drawingType: Descartes.Layer.POINT_GEOMETRY
	       }
          },
          {type: Descartes.Map.EDITION_SPLIT},
          {type: Descartes.Map.EDITION_SPLIT,
  	       args: {
               drawingType: Descartes.Layer.POLYGON_GEOMETRY
           }
          }
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
