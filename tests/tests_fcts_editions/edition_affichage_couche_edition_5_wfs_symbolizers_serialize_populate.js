function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	
	
	
		
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
	var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsStyle.title, couchePointsStyle.definition, couchePointsStyle.options);
	var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesStyle.title, coucheLignesStyle.definition, coucheLignesStyle.options);
	var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle.title, couchePolygonesStyle.definition, couchePolygonesStyle.options);

	contenuCarte.addItem(editionLayer1, gpWFS);
	contenuCarte.addItem(editionLayer2, gpWFS);
	contenuCarte.addItem(editionLayer3, gpWFS);

	var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsStyle.title, coucheMultiPointsStyle.definition, coucheMultiPointsStyle.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesStyle.title, coucheMultiLignesStyle.definition, coucheMultiLignesStyle.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle.title, coucheMultiPolygonesStyle.definition, coucheMultiPolygonesStyle.options);

    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 

   
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
	var items = carte.mapContent.serialize();
    //console.log(items);
	//console.log(JSON.stringify(items));
	//console.log(JSON.parse(items));   
	carte.mapContent.removeItemByIndex("2",false);
	carte.mapContent.removeItemByIndex("1",false);
	carte.mapContent.removeItemByIndex("0",false);
	alert(JSON.stringify(items));
	carte.mapContentManager.populate(items);
}
