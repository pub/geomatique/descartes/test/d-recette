var carte;
function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     

	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));

	//GB Modifs EPSG
	couchePointsSnapping2.title = "Ma couche WMS de points avec WFS associé";
	couchePolygonesSnapping.title = "Ma couche WMS de polygones avec WFS associé";
	coucheMultiPointsSnapping.title = "Ma couche WMS de multipoints avec WFS associé";
	coucheMultiPolygonesSnapping.title = "Ma couche WMS de multipolygones avec WFS associé";
	
	couchePointsSnapping2.definition[0].layerName = couchePointsSnapping2.definition[0].featurePrefix+":"+couchePointsSnapping2.definition[0].layerName;
	couchePolygonesSnapping.definition[0].layerName = couchePolygonesSnapping.definition[0].featurePrefix+":"+couchePolygonesSnapping.definition[0].layerName;
	coucheMultiPointsSnapping.definition[0].layerName = coucheMultiPointsSnapping.definition[0].featurePrefix+":"+coucheMultiPointsSnapping.definition[0].layerName;
	coucheMultiPolygonesSnapping.definition[0].layerName = coucheMultiPolygonesSnapping.definition[0].featurePrefix+":"+coucheMultiPolygonesSnapping.definition[0].layerName;
	
	couchePointsSnapping2.definition[0].internalProjection = "EPSG:4326";
	couchePolygonesSnapping.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPointsSnapping.definition[0].internalProjection = "EPSG:4326";
	coucheMultiPolygonesSnapping.definition[0].internalProjection = "EPSG:4326";
	
	couchePointsSnapping2.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	couchePolygonesSnapping.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPointsSnapping.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	coucheMultiPolygonesSnapping.definition[0].featureInternalProjection = "urn:ogc:def:crs:EPSG::4326";
	
    var layer1 = new Descartes.Layer.WMS(couchePointsSnapping2.title, couchePointsSnapping2.definition, couchePointsSnapping2.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesSnapping.title, coucheLignesSnapping.definition, coucheLignesSnapping.options);
    var layer3 = new Descartes.Layer.WMS(couchePolygonesSnapping.title, couchePolygonesSnapping.definition, couchePolygonesSnapping.options);

    
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(layer3, gpWFS);
    contenuCarte.addItem(layer1, gpWFS);
    
    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
		
    var layer4 = new Descartes.Layer.WMS(coucheMultiPointsSnapping.title, coucheMultiPointsSnapping.definition, coucheMultiPointsSnapping.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesSnapping.title, coucheMultiLignesSnapping.definition, coucheMultiLignesSnapping.options);
    var layer6 = new Descartes.Layer.WMS(coucheMultiPolygonesSnapping.title, coucheMultiPolygonesSnapping.definition, coucheMultiPolygonesSnapping.options);

    
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(layer4, gpWFSMulti);
    contenuCarte.addItem(layer6, gpWFSMulti);
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
		
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  snapping: true
              }
	      }
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
