function chargementCarte() {
	
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     

	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var layergeojson1 = new Descartes.Layer.GeoJSON(geojsonCouchePointsSnapping.title, geojsonCouchePointsSnapping.definition, geojsonCouchePointsSnapping.options);
    geojsonCoucheLignesSnapping.options.snapping.enable = false;
    geojsonCoucheLignesSnapping.options.snapping.autotracing = true;
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesSnapping.title, geojsonCoucheLignesSnapping.definition, geojsonCoucheLignesSnapping.options);
    geojsonCouchePolygonesSnapping.options.snapping.autotracing = true;
    var layergeojson3 = new Descartes.Layer.GeoJSON(geojsonCouchePolygonesSnapping.title, geojsonCouchePolygonesSnapping.definition, geojsonCouchePolygonesSnapping.options);

    contenuCarte.addItem(layergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(layergeojson3, gpGEOJSON);

    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
		
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  snapping: true,
                  autotracing: true
              }
	      }
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
