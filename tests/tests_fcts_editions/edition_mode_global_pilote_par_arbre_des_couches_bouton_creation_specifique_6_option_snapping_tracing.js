function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsSnapping.title, couchePointsSnapping.definition, couchePointsSnapping.options);
    coucheLignesSnapping.options.snapping.autotracing = true;
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesSnapping.title, coucheLignesSnapping.definition, coucheLignesSnapping.options);
    couchePolygonesSnapping.options.snapping.autotracing = true;
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesSnapping.title, couchePolygonesSnapping.definition, couchePolygonesSnapping.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

	var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsSnapping.title, coucheMultiPointsSnapping.definition, coucheMultiPointsSnapping.options);
    coucheMultiLignesSnapping.options.snapping.autotracing = true;
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesSnapping.title, coucheMultiLignesSnapping.definition, coucheMultiLignesSnapping.options);
    coucheMultiPolygonesSnapping.options.snapping.autotracing = true;
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesSnapping.title, coucheMultiPolygonesSnapping.definition, coucheMultiPolygonesSnapping.options);

    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsSnapping.title, kmlCouchePointsSnapping.definition, kmlCouchePointsSnapping.options);
    kmlCoucheLignesSnapping.options.snapping.autotracing = true;
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesSnapping.title, kmlCoucheLignesSnapping.definition, kmlCoucheLignesSnapping.options);
    kmlCouchePolygonesSnapping.options.snapping.autotracing = false;
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesSnapping.title, kmlCouchePolygonesSnapping.definition, kmlCouchePolygonesSnapping.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

	var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
    
    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsSnapping.title, kmlCoucheMultiPointsSnapping.definition, kmlCoucheMultiPointsSnapping.options);
    kmlCoucheMultiLignesSnapping.options.snapping.autotracing = true;
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesSnapping.title, kmlCoucheMultiLignesSnapping.definition, kmlCoucheMultiLignesSnapping.options);
    kmlCoucheMultiPolygonesSnapping.options.snapping.autotracing = false;
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesSnapping.title, kmlCoucheMultiPolygonesSnapping.definition, kmlCoucheMultiPolygonesSnapping.options);

    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsSnapping.title, geojsonCouchePointsSnapping.definition, geojsonCouchePointsSnapping.options);
    geojsonCoucheLignesSnapping.options.snapping.enable = false;
    geojsonCoucheLignesSnapping.options.snapping.autotracing = true;
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesSnapping.title, geojsonCoucheLignesSnapping.definition, geojsonCoucheLignesSnapping.options);
    geojsonCouchePolygonesSnapping.options.snapping.autotracing = true;
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesSnapping.title, geojsonCouchePolygonesSnapping.definition, geojsonCouchePolygonesSnapping.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

	var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
    
    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsSnapping.title, geojsonCoucheMultiPointsSnapping.definition, geojsonCoucheMultiPointsSnapping.options);
    geojsonCoucheMultiLignesSnapping.options.snapping.enable = false;
    geojsonCoucheMultiLignesSnapping.options.snapping.autotracing = true;
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesSnapping.title, geojsonCoucheMultiLignesSnapping.definition, geojsonCoucheMultiLignesSnapping.options);
    geojsonCoucheMultiPolygonesSnapping.options.snapping.autotracing = true;
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesSnapping.title, geojsonCoucheMultiPolygonesSnapping.definition, geojsonCoucheMultiPolygonesSnapping.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
		
	 //Ajout d'un barre d'outils d'�dition
	  carte.addEditionToolBar('editionToolBar', [
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  geometryType: Descartes.Layer.POINT_GEOMETRY,
                  snapping: true,
                  autotracing: true
              }
	      },
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  geometryType: Descartes.Layer.LINE_GEOMETRY,
                  snapping: true,
                  autotracing: true
              }
	      },
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  geometryType: Descartes.Layer.POLYGON_GEOMETRY,
                  snapping: true,
                  autotracing: true
              }
	      },
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  geometryType: Descartes.Layer.MULTI_POINT_GEOMETRY,
                  snapping: true,
                  autotracing: true
              }
	      },
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  geometryType: Descartes.Layer.MULTI_LINE_GEOMETRY,
                  snapping: true,
                  autotracing: true
              }
	      },
	      {
	          type: Descartes.Map.EDITION_DRAW_CREATION,
	          args: {
                  geometryType: Descartes.Layer.MULTI_POLYGON_GEOMETRY,
                  snapping: true,
                  autotracing: true
              }
	      }
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
