function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsFctAvancedBis.title, couchePointsFctAvancedBis.definition, couchePointsFctAvancedBis.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesFctAvancedBis.title, coucheLignesFctAvancedBis.definition, coucheLignesFctAvancedBis.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesFctAvancedBis.title, couchePolygonesFctAvancedBis.definition, couchePolygonesFctAvancedBis.options);

    var d_editionLayer1 = contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var gpWFSMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionWFSMulti.title, groupeEditionWFSMulti.options));
	
    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsFctAvancedBis.title, coucheMultiPointsFctAvancedBis.definition, coucheMultiPointsFctAvancedBis.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesFctAvancedBis.title, coucheMultiLignesFctAvancedBis.definition, coucheMultiLignesFctAvancedBis.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesFctAvancedBis.title, coucheMultiPolygonesFctAvancedBis.definition, coucheMultiPolygonesFctAvancedBis.options);

    contenuCarte.addItem(editionLayer4, gpWFSMulti);
    contenuCarte.addItem(editionLayer5, gpWFSMulti);
    contenuCarte.addItem(editionLayer6, gpWFSMulti); 
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsFctAvancedBis.title, kmlCouchePointsFctAvancedBis.definition, kmlCouchePointsFctAvancedBis.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesFctAvancedBis.title, kmlCoucheLignesFctAvancedBis.definition, kmlCoucheLignesFctAvancedBis.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesFctAvancedBis.title, kmlCouchePolygonesFctAvancedBis.definition, kmlCouchePolygonesFctAvancedBis.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

	var gpKMLMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionKMLMulti.title, groupeEditionKMLMulti.options));
    
    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsFctAvancedBis.title, kmlCoucheMultiPointsFctAvancedBis.definition, kmlCoucheMultiPointsFctAvancedBis.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesFctAvancedBis.title, kmlCoucheMultiLignesFctAvancedBis.definition, kmlCoucheMultiLignesFctAvancedBis.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesFctAvancedBis.title, kmlCoucheMultiPolygonesFctAvancedBis.definition, kmlCoucheMultiPolygonesFctAvancedBis.options);

    contenuCarte.addItem(editionLayerkml4, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml5, gpKMLMulti);
    contenuCarte.addItem(editionLayerkml6, gpKMLMulti); 
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsFctAvancedBis.title, geojsonCouchePointsFctAvancedBis.definition, geojsonCouchePointsFctAvancedBis.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesFctAvancedBis.title, geojsonCoucheLignesFctAvancedBis.definition, geojsonCoucheLignesFctAvancedBis.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesFctAvancedBis.title, geojsonCouchePolygonesFctAvancedBis.definition, geojsonCouchePolygonesFctAvancedBis.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

	var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
    
    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsFctAvancedBis.title, geojsonCoucheMultiPointsFctAvancedBis.definition, geojsonCoucheMultiPointsFctAvancedBis.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesFctAvancedBis.title, geojsonCoucheMultiLignesFctAvancedBis.definition, geojsonCoucheMultiLignesFctAvancedBis.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesFctAvancedBis.title, geojsonCoucheMultiPolygonesFctAvancedBis.definition, geojsonCoucheMultiPolygonesFctAvancedBis.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_CLONE_CREATION},
	      {type: Descartes.Map.EDITION_SELECTION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION,
	    	  args:{
	    		 showInfos:{attributes:true}
	    	  }
	      },
	      {type: Descartes.Map.EDITION_ATTRIBUTE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	

}
