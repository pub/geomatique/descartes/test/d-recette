proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON(); 
		
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global pilot� par l'arbre des couches
	      save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	      }

	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePoints.title, couchePoints.definition, couchePoints.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignes.title, coucheLignes.definition, coucheLignes.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygones.title, couchePolygones.definition, couchePolygones.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePoints.title, kmlCouchePoints.definition, kmlCouchePoints.options);
    editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignes.title, kmlCoucheLignes.definition, kmlCoucheLignes.options);
    editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygones.title, kmlCouchePolygones.definition, kmlCouchePolygones.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePoints.title, geojsonCouchePoints.definition, geojsonCouchePoints.options);
    editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignes.title, geojsonCoucheLignes.definition, geojsonCoucheLignes.options);
    editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygones.title, geojsonCouchePolygones.definition, geojsonCouchePolygones.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_DRAW_CREATION},
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_ATTRIBUTE},
	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//DIRECTIONPANPANEL
	carte.addDirectionalPanPanel();
	
	//LOCALISATION A L'ADRESSE
	carte.addAction({type : Descartes.Action.LocalisationAdresse,  div : 'localisationAdresse'/*, options:{label:false}*/});
	
	//LOCALISATION PARCELLAIRE
	//Toutes les régions de la metropole
	var gazetteerLevels = [];
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez une région","Aucune région","reg"));	 
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez un département","Aucun département","dept"));  
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez une commune","Aucune commune","com"));  
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez une section cadastrale","Aucune section cadastrale pour cette commune",""));
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez une parcelle","Aucune parcelle pour cette section cadastrale",""));
    var gazetteerService = new Descartes.LocalisationParcellaire(Descartes.LocalisationParcellaire.NIVEAU_REGION);
    carte.addGazetteer('Gazetteer', "", gazetteerLevels,{service:gazetteerService/*, label:false*/});  
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
