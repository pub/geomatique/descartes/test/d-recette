proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
proj4.defs('urn:ogc:def:crs:EPSG::4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

var context = {};
function chargementCarte() {
	chargeCouchesGroupesContext();
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	// constitution de l'objet JSON de contexte
	var contextFile = (new Descartes.Url(this.location.href)).getParamValue("context");
	
	if (contextFile !== "") {
		// contexte sauvegardé
		var xhr = new XMLHttpRequest();
        xhr.open('GET', Descartes.CONTEXT_MANAGER_SERVER+"?" + contextFile);
        xhr.onload = function () {
            if (xhr.status === 200) {
            	getContext(xhr);
            } else {
            	stopMap();
            }
        };
        xhr.send();
	
	} else {
		context.bbox = {xMin:799205.2, yMin:6215857.5, xMax:1078390.1, yMax:6452614.0};

		context.size = {w:600 , h:400};
		context.items = [];
	
		context.items[0] = Descartes.Utils.extend(coucheToponymes, {itemType:"Layer"});
		context.items[1] = Descartes.Utils.extend(groupeInfras, {itemType:"Group", items:[]});
		context.items[1].items[0] = Descartes.Utils.extend(coucheParkings, {itemType:"Layer"});
		context.items[1].items[1] = Descartes.Utils.extend(coucheStations, {itemType:"Layer"});
		context.items[1].items[2] = Descartes.Utils.extend(coucheRoutes, {itemType:"Layer"});
		context.items[1].items[3] = Descartes.Utils.extend(coucheFer, {itemType:"Layer"});
		context.items[2] = Descartes.Utils.extend(coucheEau, {itemType:"Layer"});
		context.items[3] = Descartes.Utils.extend(coucheBati, {itemType:"Layer"});
		context.items[4] = Descartes.Utils.extend(coucheNature, {itemType:"Layer"});
		context.items[5] = Descartes.Utils.extend(groupeEditionWFS, {itemType:"Group", items:[]});
		context.items[5].items[0] = Descartes.Utils.extend(couchePointsFull, {itemType:"Layer"});
		context.items[5].items[1] = Descartes.Utils.extend(coucheLignesFull, {itemType:"Layer"});
		context.items[5].items[2] = Descartes.Utils.extend(couchePolygonesFull, {itemType:"Layer"});
		context.items[5].items[3] = Descartes.Utils.extend(coucheMultiPointsFull, {itemType:"Layer"});
		context.items[5].items[4] = Descartes.Utils.extend(coucheMultiLignesFull, {itemType:"Layer"});
		context.items[5].items[5] = Descartes.Utils.extend(coucheMultiPolygonesFull, {itemType:"Layer"});
		context.items[6] = Descartes.Utils.extend(groupeEditionKML, {itemType:"Group", items:[]});
		context.items[6].items[0] = Descartes.Utils.extend(kmlCouchePointsFull, {itemType:"Layer"});
		context.items[6].items[1] = Descartes.Utils.extend(kmlCoucheLignesFull, {itemType:"Layer"});
		context.items[6].items[2] = Descartes.Utils.extend(kmlCouchePolygonesFull, {itemType:"Layer"});
		context.items[6].items[3] = Descartes.Utils.extend(kmlCoucheMultiPointsFull, {itemType:"Layer"});
		context.items[6].items[4] = Descartes.Utils.extend(kmlCoucheMultiLignesFull, {itemType:"Layer"});
		context.items[6].items[5] = Descartes.Utils.extend(kmlCoucheMultiPolygonesFull, {itemType:"Layer"});
		context.items[7] = Descartes.Utils.extend(groupeEditionGEOJSON, {itemType:"Group", items:[]});
		context.items[7].items[0] = Descartes.Utils.extend(geojsonCouchePointsFull, {itemType:"Layer"});
		context.items[7].items[1] = Descartes.Utils.extend(geojsonCoucheLignesFull, {itemType:"Layer"});
		context.items[7].items[2] = Descartes.Utils.extend(geojsonCouchePolygonesFull, {itemType:"Layer"});
		context.items[7].items[3] = Descartes.Utils.extend(geojsonCoucheMultiPointsFull, {itemType:"Layer"});
		context.items[7].items[4] = Descartes.Utils.extend(geojsonCoucheMultiLignesFull, {itemType:"Layer"});
		context.items[7].items[5] = Descartes.Utils.extend(geojsonCoucheMultiPolygonesFull, {itemType:"Layer"});
		doMap();
	}
}
	
function doMap(){
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global piloté par l'arbre des couches
	      save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	      }
	});  
	
	var contenuCarte = new Descartes.MapContent({editable: true});

	contenuCarte.populate(context.items);
	
	var maxBounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
	var initBounds = [context.bbox.xMin, context.bbox.yMin, context.bbox.xMax, context.bbox.yMax];
	var projection = 'EPSG:2154';
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initBounds,
					maxExtent: maxBounds,
					minScale:2150000,
					maxScale:100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolsInNamedToolBar(toolsBar,[
	                        {type : Descartes.Map.MAXIMAL_EXTENT},
	                        {type : Descartes.Map.INITIAL_EXTENT, args:initBounds},
	                        {type : Descartes.Map.DRAG_PAN},
							{type : Descartes.Map.ZOOM_IN},
							{type : Descartes.Map.ZOOM_OUT},
							{type : Descartes.Map.NAV_HISTORY}
	]);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar(null, 
	//  carte.addEditionToolBar("editionToolBar", 
		[ {type: Descartes.Map.EDITION_SELECTION},
	      {type: Descartes.Map.EDITION_DRAW_CREATION,
		      args: {
	                  snapping: true
	                 }
	      },
	      {type: Descartes.Map.EDITION_COPY_CREATION},
	      {type: Descartes.Map.EDITION_CLONE_CREATION},
	      {type: Descartes.Map.EDITION_BUFFER_NORMAL_CREATION},
	      {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION},
	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION},
	      {type: Descartes.Map.EDITION_SPLIT,
	       args: {
	          drawingType: Descartes.Layer.POINT_GEOMETRY
	       }
	      },
	      {type: Descartes.Map.EDITION_SPLIT},
	      {type: Descartes.Map.EDITION_SPLIT,
		      args: {
	           drawingType: Descartes.Layer.POLYGON_GEOMETRY
	       }
	      },
	      {type: Descartes.Map.EDITION_DIVIDE},
	      {type: Descartes.Map.EDITION_AGGREGATION},
	      {type: Descartes.Map.EDITION_UNAGGREGATION_CREATION},
	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION,
		      args: {
	                  snapping: true
	                 }
	      },
	      {type: Descartes.Map.EDITION_MERGE_MODIFICATION},
	      {type: Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION},
	      {type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION},
	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION},
	      {type: Descartes.Map.EDITION_ATTRIBUTE},
	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION},
	      {type: Descartes.Map.EDITION_SAVE}
	    ],
	    {toolBar: toolsBar,// imbrication dans la barre principale
	     openerTool: true,
			panel:true,
			title:"Outils d'édition",
			draggable: {
	            enable: true,
	            containment: 'parent'
	        }} 
	  );
	  
	  carte.addToolsInNamedToolBar(toolsBar,[
			{type : Descartes.Map.POINT_SELECTION, args:{resultUiParams:{withCsvExport: true}}},
			{type : Descartes.Map.POINT_RADIUS_SELECTION, args:{resultUiParams:{withCsvExport: true, withReturn: true}}},
			{type : Descartes.Map.RECTANGLE_SELECTION, args:{resultUiParams:{withCsvExport: true, withReturn: true}}},
			{type : Descartes.Map.COORDS_CENTER},
			{type : Descartes.Map.CENTER_MAP},
			{type : Descartes.Map.DISTANCE_MEASURE},
			{type : Descartes.Map.AREA_MEASURE},
			{type : Descartes.Map.PDF_EXPORT},
			{type : Descartes.Map.PNG_EXPORT}
 	]);  
	
	carte.addContentManager(
			'layersTree',
			[
				{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
				{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
				{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
				{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
				{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
				{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
				{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL}
			],
			{
				toolBarDiv: "managerToolBar"
			}
		);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});
	
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");
	
	// Ajout du gestionnaire de requete
	var laCoucheBati = carte.mapContent.getLayerById("coucheBati");
	var gestionnaireRequetes = carte.addRequestManager('Requetes');
	var requeteBati = new Descartes.Request(laCoucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);
	
	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	
	// Ajout du gestionnaire d'info-bulle
	var laCoucheParkings = carte.mapContent.getLayerById("coucheParkings");
	var laCoucheStations = carte.mapContent.getLayerById("coucheStations");
	carte.addToolTip('ToolTip', [
		{layer: laCoucheParkings, fields: ['name']},
		{layer: laCoucheStations, fields: ['name']}
	]);

	
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes');
}

function getContext(transport) {
	
	var response = transport.responseText;
		if (response.charAt(0) !== "{") {
			//alert("La carte est disponible jusqu'au " + response.substring(0, response.indexOf("{")));
			response = response.substring(response.indexOf("{"));
		}
	context = JSON.parse(response);
	doMap();
	
}

function stopMap() {
	alert("Impossible de recharger la carte: problème lors du chargement du fichier de contexte");
}