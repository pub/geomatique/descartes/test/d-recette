var carte;
function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	

	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
        globalEditionMode: true, //mode global pilot� par l'arbre des couches
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.GeoJSON(geojsonCouchePointsFctAvanced.title, geojsonCouchePointsFctAvanced.definition, geojsonCouchePointsFctAvanced.options);
    var editionLayergeojson2 = new Descartes.Layer.GeoJSON(geojsonCoucheLignesFctAvanced.title, geojsonCoucheLignesFctAvanced.definition, geojsonCoucheLignesFctAvanced.options);
    var editionLayergeojson2bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneLignes.title, geojsonCoucheCloneLignes.definition, geojsonCoucheCloneLignes.options);
    editionLayergeojson2bis.title="Ma couche GEOJSON de lignes pour l'agrégation";
    var editionLayergeojson3 = new Descartes.Layer.GeoJSON(geojsonCouchePolygonesFctAvanced.title, geojsonCouchePolygonesFctAvanced.definition, geojsonCouchePolygonesFctAvanced.options);
    var editionLayergeojson3bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheClonePolygones.title, geojsonCoucheClonePolygones.definition, geojsonCoucheClonePolygones.options);
    editionLayergeojson3bis.title="Ma couche GEOJSON de polygones pour l'agrégation";
    
    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2bis, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3bis, gpGEOJSON);

    var gpGEOJSONMulti = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSONMulti.title, groupeEditionGEOJSONMulti.options));
	
    var editionLayergeojson4 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiPointsFctAvanced.title, geojsonCoucheMultiPointsFctAvanced.definition, geojsonCoucheMultiPointsFctAvanced.options);
    var editionLayergeojson4bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPoints.title, geojsonCoucheCloneMultiPoints.definition, geojsonCoucheCloneMultiPoints.options);
    editionLayergeojson4bis.title="Ma couche GEOJSON de multi-points pour l'agrégation";
    var editionLayergeojson5 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiLignesFctAvanced.title, geojsonCoucheMultiLignesFctAvanced.definition, geojsonCoucheMultiLignesFctAvanced.options);
    var editionLayergeojson5bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiLignes.title, geojsonCoucheCloneMultiLignes.definition, geojsonCoucheCloneMultiLignes.options);
    editionLayergeojson5bis.title="Ma couche GEOJSON de multi-lignes pour l'agrégation";
    var editionLayergeojson6 = new Descartes.Layer.GeoJSON(geojsonCoucheMultiPolygonesFctAvanced.title, geojsonCoucheMultiPolygonesFctAvanced.definition, geojsonCoucheMultiPolygonesFctAvanced.options);
    var editionLayergeojson6bis = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheCloneMultiPolygones.title, geojsonCoucheCloneMultiPolygones.definition, geojsonCoucheCloneMultiPolygones.options);
    editionLayergeojson6bis.title="Ma couche GEOJSON de multi-polygones pour l'agrégation";
   
    contenuCarte.addItem(editionLayergeojson4, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson4bis, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson5bis, gpGEOJSONMulti);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSONMulti); 
    contenuCarte.addItem(editionLayergeojson6bis, gpGEOJSONMulti); 
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	      {type: Descartes.Map.EDITION_AGGREGATION}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
