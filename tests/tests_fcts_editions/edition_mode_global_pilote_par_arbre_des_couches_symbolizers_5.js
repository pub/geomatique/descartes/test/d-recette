function chargementCarte() {
	
	chargeEditionCouchesGroupesWFS();
	chargeEditionCouchesGroupesKML();
	chargeEditionCouchesGroupesGEOJSON();
	
	
	 
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		 autoSave:false,  //sauvegarde manuelle
	      globalEditionMode: true, //mode global pilot� par l'arbre des couches
	      save: function (json) {
		  	 //Ici, code MOE qui est spécifique à chaque application métier.
		 	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
		    //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
		    	   	
		 	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
		 	 sendRequestBouchonForSaveElements(json);
		
		 }
	  });    
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	var gpWFS = contenuCarte.addItem(new Descartes.Group(groupeEditionWFS.title, groupeEditionWFS.options));
	
    var editionLayer1 = new Descartes.Layer.EditionLayer.WFS(couchePointsStyle5.title, couchePointsStyle5.definition, couchePointsStyle5.options);
    var editionLayer2 = new Descartes.Layer.EditionLayer.WFS(coucheLignesStyle5.title, coucheLignesStyle5.definition, coucheLignesStyle5.options);
    var editionLayer3 = new Descartes.Layer.EditionLayer.WFS(couchePolygonesStyle5.title, couchePolygonesStyle5.definition, couchePolygonesStyle5.options);

    contenuCarte.addItem(editionLayer1, gpWFS);
    contenuCarte.addItem(editionLayer2, gpWFS);
    contenuCarte.addItem(editionLayer3, gpWFS);

    var editionLayer4 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPointsStyle5.title, coucheMultiPointsStyle5.definition, coucheMultiPointsStyle5.options);
    var editionLayer5 = new Descartes.Layer.EditionLayer.WFS(coucheMultiLignesStyle5.title, coucheMultiLignesStyle5.definition, coucheMultiLignesStyle5.options);
    var editionLayer6 = new Descartes.Layer.EditionLayer.WFS(coucheMultiPolygonesStyle5.title, coucheMultiPolygonesStyle5.definition, coucheMultiPolygonesStyle5.options);

    contenuCarte.addItem(editionLayer4, gpWFS);
    contenuCarte.addItem(editionLayer5, gpWFS);
    contenuCarte.addItem(editionLayer6, gpWFS);
    
	var gpKML = contenuCarte.addItem(new Descartes.Group(groupeEditionKML.title, groupeEditionKML.options));
	
    var editionLayerkml1 = new Descartes.Layer.EditionLayer.KML(kmlCouchePointsStyle5.title, kmlCouchePointsStyle5.definition, kmlCouchePointsStyle5.options);
    var editionLayerkml2 = new Descartes.Layer.EditionLayer.KML(kmlCoucheLignesStyle5.title, kmlCoucheLignesStyle5.definition, kmlCoucheLignesStyle5.options);
    var editionLayerkml3 = new Descartes.Layer.EditionLayer.KML(kmlCouchePolygonesStyle5.title, kmlCouchePolygonesStyle5.definition, kmlCouchePolygonesStyle5.options);

    contenuCarte.addItem(editionLayerkml1, gpKML);
    contenuCarte.addItem(editionLayerkml2, gpKML);
    contenuCarte.addItem(editionLayerkml3, gpKML);

    var editionLayerkml4 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPointsStyle5.title, kmlCoucheMultiPointsStyle5.definition, kmlCoucheMultiPointsStyle5.options);
    var editionLayerkml5 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiLignesStyle5.title, kmlCoucheMultiLignesStyle5.definition, kmlCoucheMultiLignesStyle5.options);
    var editionLayerkml6 = new Descartes.Layer.EditionLayer.KML(kmlCoucheMultiPolygonesStyle5.title, kmlCoucheMultiPolygonesStyle5.definition, kmlCoucheMultiPolygonesStyle5.options);

    contenuCarte.addItem(editionLayerkml4, gpKML);
    contenuCarte.addItem(editionLayerkml5, gpKML);
    contenuCarte.addItem(editionLayerkml6, gpKML);
    
	var gpGEOJSON = contenuCarte.addItem(new Descartes.Group(groupeEditionGEOJSON.title, groupeEditionGEOJSON.options));
	
    var editionLayergeojson1 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePointsStyle5.title, geojsonCouchePointsStyle5.definition, geojsonCouchePointsStyle5.options);
    var editionLayergeojson2 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheLignesStyle5.title, geojsonCoucheLignesStyle5.definition, geojsonCoucheLignesStyle5.options);
    var editionLayergeojson3 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCouchePolygonesStyle5.title, geojsonCouchePolygonesStyle5.definition, geojsonCouchePolygonesStyle5.options);

    contenuCarte.addItem(editionLayergeojson1, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson2, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson3, gpGEOJSON);

    var editionLayergeojson4 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPointsStyle5.title, geojsonCoucheMultiPointsStyle5.definition, geojsonCoucheMultiPointsStyle5.options);
    var editionLayergeojson5 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiLignesStyle5.title, geojsonCoucheMultiLignesStyle5.definition, geojsonCoucheMultiLignesStyle5.options);
    var editionLayergeojson6 = new Descartes.Layer.EditionLayer.GeoJSON(geojsonCoucheMultiPolygonesStyle5.title, geojsonCoucheMultiPolygonesStyle5.definition, geojsonCoucheMultiPolygonesStyle5.options);

    contenuCarte.addItem(editionLayergeojson4, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson5, gpGEOJSON);
    contenuCarte.addItem(editionLayergeojson6, gpGEOJSON);

    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
       	      {type: Descartes.Map.EDITION_SELECTION/*,
    	    	  args:{
    	    		  showInfos:{attributes:true}
    	    	  }*/
    	      },
    	      {type: Descartes.Map.EDITION_DRAW_CREATION},
    	      {type: Descartes.Map.EDITION_COPY_CREATION},
    	      {type: Descartes.Map.EDITION_CLONE_CREATION},
    	      {type: Descartes.Map.EDITION_BUFFER_NORMAL_CREATION},
    	      {type: Descartes.Map.EDITION_BUFFER_HALO_CREATION},
    	      {type: Descartes.Map.EDITION_HOMOTHETIC_CREATION},
    	      {type: Descartes.Map.EDITION_SPLIT,
    	       args: {
                  drawingType: Descartes.Layer.POINT_GEOMETRY
    	       }
              },
              {type: Descartes.Map.EDITION_SPLIT},
              {type: Descartes.Map.EDITION_SPLIT,
      	       args: {
                   drawingType: Descartes.Layer.POLYGON_GEOMETRY
               }
              },
    	      {type: Descartes.Map.EDITION_DIVIDE},
    	      {type: Descartes.Map.EDITION_AGGREGATION},
    	      {type: Descartes.Map.EDITION_UNAGGREGATION_CREATION},
    	      {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
    	      {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
    	      {type: Descartes.Map.EDITION_MERGE_MODIFICATION},
    	      {type: Descartes.Map.EDITION_SELECTION_SUBSTRACT_MODIFICATION},
    	      {type: Descartes.Map.EDITION_COMPOSITE_GLOBAL_MODIFICATION},
    	      {type: Descartes.Map.EDITION_RUBBER_DELETION},
    	      {type: Descartes.Map.EDITION_COMPOSITE_RUBBER_DELETION},
    	      {type: Descartes.Map.EDITION_ATTRIBUTE},
    	      {type: Descartes.Map.EDITION_INTERSECT_INFORMATION},
    	      {type: Descartes.Map.EDITION_SAVE}
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}
