proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	Descartes.Messages.Descartes_Messages_UI_GazetteerInPlace.BUTTON_MESSAGE="<i class=\"fa fa-crosshairs\"></i> Localiser";
	Descartes.Messages.Descartes_Messages_UI_GazetteerInPlace.TITLE_MESSAGE="<i class=\"fa fa-map-marker\"></i> Localisation";
	Descartes.Messages.Descartes_Messages_UI_ScaleChooserInPlace.BUTTON_MESSAGE= "<i class=\"fa fa-check\"></i> Activer";
	Descartes.Messages.Descartes_Messages_UI_ScaleChooserInPlace.TITLE_MESSAGE="<i class=\"fa fa-list\"></i> Choisir l\'échelle de la carte";
	Descartes.Messages.Descartes_Messages_UI_ScaleSelectorInPlace.TITLE_MESSAGE="<i class=\"fa fa-list\"></i> Choisir l\'échelle de la carte";
	Descartes.Messages.Descartes_Messages_UI_SizeSelectorInPlace.TITLE_MESSAGE="<i class=\"fa fa-image\"></i> Choisir la taille de la carte";
	Descartes.Messages.Descartes_Messages_UI_CoordinatesInputInPlace.BUTTON_MESSAGE="<i class=\"fa fa-crosshairs\"></i> Recentrer";
	Descartes.Messages.Descartes_Messages_UI_CoordinatesInputInPlace.TITLE_MESSAGE="<i class=\"fa fa-edit\"></i> Choisir les coordonnées";
	Descartes.Messages.Descartes_Messages_UI_PrinterSetupInPlace.BUTTON_MESSAGE="<i class=\"fa fa-file\"></i> Générer le PDF";
	Descartes.Messages.Descartes_Messages_UI_PrinterSetupInPlace.TITLE_MESSAGE="<i class=\"fa fa-wrench\"></i> Paramètres de la mise en page";
	Descartes.Messages.Descartes_Messages_UI_BookmarksInPlace.TITLE_MESSAGE="<i class=\"fa fa-cog\"></i> Gérer des vues personnalisées";
	Descartes.Messages.Descartes_Messages_UI_BookmarksInPlace.BUTTON_MESSAGE="<i class=\"fa fa-save\"></i> Enregistrer la vue courante";
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.TITLE_MESSAGE= '<i class=\"fa fa-list\"></i> Requêtes attributaires';
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.BUTTON_MESSAGE_RECHERCHE="<i class=\"fa fa-search\"></i> Rechercher";
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.BUTTON_MESSAGE_UNSELECT="<i class=\"fa fa-trash\"></i> Effacer la sélection";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.DIALOG_TITLE= "<i class=\"fa fa-wrench\"></i> Paramètres de la mise en page";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.OK_BUTTON= "<i class=\"fa fa-file\"></i> Générer le PDF";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.CANCEL_BUTTON= "<i class=\"fa fa-times\"></i> Annuler";
    Descartes.Messages.Descartes_Messages_Info_Legend.TITLE_MESSAGE= "<i class=\"fa fa-image\"></i> Légende";
	
	var contenuCarte = new Descartes.MapContent();
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});
	
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");

	// Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes');
	var requeteBati = new Descartes.Request(coucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);
	
	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	
	// Ajout du gestionnaire d'info-bulle
	carte.addToolTip('ToolTip', [
  		{layer: coucheParkings, fields: ['name']},
  		{layer: coucheStations, fields: ['name']}
  	]);
  	
  	/*carte.addToolTip('ToolTip', [ // pour intranet
   		{layer: coucheParkings, fields: ['name']},
   		{layer: coucheStations, fields: ['name']}
   	]);*/

	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes');
	
	// Ajout du gestionnaire de localisation rapide
	carte.addDefaultGazetteer('Gazetteer', "93", Descartes.Action.DefaultGazetteer.DEPARTEMENT);
}
