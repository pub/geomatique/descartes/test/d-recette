proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
/*	  Descartes.Tool.Selection.RectangleSelection.prototype.executeQuery=function(geometry) {
	        
	        var _parser = new jsts.io.OL3Parser();    
	        
	        if (this.mapContent !== null) {
	            var layers = this.mapContent.getQueryableLayers();
	            
	            if(layers.length!==0){
	                var featureLayers = [];
	                for(var iLayer=0,len=layers.length;iLayer<len;iLayer++){
	                    var layer = layers[iLayer];
	                    if (layer.getFeatureOL_layers()) {
	                        featureLayers.push(layer);
	                    }
	                }
	                
	                if(featureLayers.length === 0){
	                    alert("Aucune couche de type vector");
	                    this.effacerSelection();
	                } else{
	                    var tab=[];
	                    var nb=0;
	                    var model = [];
	                    var sourceGeom = _parser.read(geometry.getGeometry());
	                    for(var i=0,len=featureLayers.length;i<len;i++){
	                        
	                        var element={};
	                        element.layerTitle = featureLayers[i].title;
	                        element.layerDatas = [];
	                        
	                        
	                        var features = featureLayers[i].getFeatureOL_layers()[0].getSource().getFeatures();
	                        for (var j = 0; j < features.length; j++) {
	                            var aFeature = features[j];
	                            var aGeometry = _parser.read(aFeature.getGeometry());
	                            
	                            if (sourceGeom.intersects(aGeometry)) {
	                                tab.push(aFeature);
	                                nb++;
	                                
	                                var ssElmt = {
	                                        "Identifiant":aFeature.getId()
	                                };
	                                
	                                var strAttributes = "Aucun attribut disponible";
	                                if(aFeature.attributes){
	                                    var str="";
	                                    for (var attribute in aFeature.attributes) {
	                                        if(aFeature.attributes[attribute] && aFeature.attributes[attribute].value){
	                                            str+=attribute+": "+aFeature.attributes[attribute].value+", ";
	                                        }else{
	                                            str+=attribute+": "+aFeature.attributes[attribute]+", ";
	                                        }
	                                    }
	                                    if(str.lastIndexOf(",") !=-1){
	                                        str=str.substr(0,str.lastIndexOf(","));
	                                    }
	                                    if(str !== ""){
	                                        strAttributes = str;
	                                    }
	                                }

	                                ssElmt["Attributs"] = strAttributes;
	                                
	                                if(this.resultUiParams.withReturn){
	                                    ssElmt["bounds"]=aFeature.getGeometry().getExtent();
	                                }
	                                element.layerDatas.push(ssElmt);
	                            }
	                        }
	                        if(element.layerDatas.length>0){
		                       model.push(element);
		                   }
	                    }
	                
	                }
	                 
	                    var options={};
	                    options.withCsvExport = this.resultUiParams.withCsvExport;
	                    var renderer = new Descartes.UI.TabbedDataGrids(this.resultUiParams.div, model, options);
	                    renderer.events.register('gotoFeatureBounds', this, this.gotoFeatureBounds);
	                    renderer.draw(model);
	                
	            }else{
	                alert(Descartes_SEARCH_ERROR_C_INTERROGEABLE);
	                this.effacerSelection();
	            }
	        }
	    };
	    
	    Descartes.Tool.Selection.RectangleSelection.prototype.gotoFeatureBounds= function(boundsAndScales) {

        if (!(boundsAndScales instanceof Array) && boundsAndScales.data) {
            //cas type_widget
            boundsAndScales = boundsAndScales.data;
        } else if (!(boundsAndScales instanceof Array)) {
            //cas type_popup, type_inplace
            boundsAndScales = eval(boundsAndScales);
        }
        var bounds = boundsAndScales[0];
        if (!(bounds instanceof Array) && bounds.data) {
            bounds = bounds.data;
        }
        var layerMinScale = boundsAndScales[1];
        var layerMaxScale = boundsAndScales[2];

        var xmin = bounds[0], ymin = bounds[1], xmax = bounds[2], ymax = bounds[3];

        var mapType = this.olMap.get('mapType');
        var constrainResolution = mapType === Descartes.Map.MAP_TYPES.DISCRETE;

        var extent = [xmin, ymin, xmax, ymax];
        if ((xmax - xmin) < Descartes.MIN_BBOX && (ymax - ymin) < Descartes.MIN_BBOX) {
            var newXmin = xmin + (xmax - xmin) / 2 - Descartes.MIN_BBOX / 2;
            var newXmax = xmin + (xmax - xmin) / 2 + Descartes.MIN_BBOX / 2;
            var newYmin = ymin + (ymax - ymin) / 2 - Descartes.MIN_BBOX / 2;
            var newYmax = ymin + (ymax - ymin) / 2 + Descartes.MIN_BBOX / 2;
            extent = [newXmin, newYmin, newXmax, newYmax];
        }
        this.olMap.getView().fit(extent, {
            constrainResolution: constrainResolution,
            minResolution: this.olMap.getView().getMinResolution(),
            maxResolution: this.olMap.getView().getMaxResolution()
        });

        var currentRes = this.olMap.getView().getResolution();
        var unit = this.olMap.getView().getProjection().getUnits();
        var newRes = currentRes;
        if (layerMinScale !== null) {
            var layerMinRes = Utils.getResolutionForScale(layerMinScale, unit);
            if (layerMinRes < currentRes) {
                newRes = layerMinRes;
            }
        }
        if (layerMaxScale !== null) {
            var layerMaxRes = Utils.getResolutionForScale(layerMaxScale, unit);
            if (layerMaxRes > currentRes) {
                newRes = layerMaxRes;
            }
        }

        if (newRes !== currentRes) {
            this.olMap.getView().setResolution(newRes);
        }


	    };*/
	
	
	var contenuCarte = new Descartes.MapContent();
	// Ajout couches wfs
	contenuCarte.addItem(coucheEauWfs);	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION, args:{resultUiParams:{withCsvExport: true, withReturn: true}}});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
