proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	
	Descartes.Button.ContentTask.ChooseWmsLayers.prototype.execute= function () {
        
        var urls = [ {
        	code : "Mon Serveur 1",
        	value : "http://ws.carmencarto.fr/WMS/119/fxx_inpn?"
        }, {
        	code : "Mon Serveur 2",
        	value : "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geocat_metr.map"
        } , {
        	code : "Mon Serveur 3",
        	value : "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d"
        }];
        
		this.form = document.createElement('form');

		var label = document.createElement('label');
		label.className = "DescartesUILabel";
		label.innerHTML = "Veuillez sélectionner un serveur WMS : ";
		
		this.form.appendChild(label);
		
		var optionsSelect = [];
		urls.forEach(
			function(url) {   					
				var option = {value:url.value,text:url.code};
				optionsSelect.push(option);
			}
   		);

		var selectElement = document.createElement("select");
		selectElement.className = "DescartesUISelect form-control";
		selectElement.name = "urlSelector";
		selectElement.size = "1";

		for (var i=0, len=urls.length ; i<len ; i++) {
			var optionElement = document.createElement('option');
			optionElement.value =urls[i].value;
			optionElement.innerHTML = urls[i].code.toString();
			selectElement.appendChild(optionElement);
		}

		this.form.appendChild(selectElement);

        var dialog = new Descartes.UI.ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('DIALOG_TITLE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            size: 'modal-sm',
            content: this.form.innerHTML
        });
        dialog.open($.proxy(this.requestServer, this));      
        
    };
    
    
    Descartes.Button.ContentTask.ChooseWmsLayers.prototype.requestServer= function (result) {
        var url = new Descartes.Url(result.urlSelector);
        this._useUrlCapabilities = "false";
        this.wmsUrl = result.url;
        url.alterOrAddParam('REQUEST', 'GetCapabilities');
        url.alterOrAddParam('SERVICE', 'WMS');
        url.alterOrAddParam('VERSION', '1.3.0');
        var urlProxy = new Descartes.Url(Descartes.PROXY_SERVER);
        var urlProxyString = urlProxy.getUrlString();
        if (urlProxyString.indexOf('?') === -1) {
            urlProxyString += '?';
        }

        var finalUrl = urlProxyString + url.getUrlString();

        if (this.waitingMsg) {
            this.showWaitingMessage();
        }

        $.ajax({
            url: finalUrl
        }).done($.proxy(this.showLayers, this)).fail($.proxy(this.handleError, this));
    };
	
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	
	carte.addContentManager(
		'layersTree',
		[
			{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL}
		],
		{
			toolBarDiv: "managerToolBar"
		}
	);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition',options:{projection:false, separator:" - "}});
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
			
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");

}