proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({
		editable: true,
		fctDisplayLegend:true,
		fctQueryable:false,
		displayMoreInfos: true});
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);
	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
	var initbounds = [862500, 6237000, 925300, 6288000];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar', null, {vertical:true});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});

    var managerOptions = {
            uiOptions: {
                moreInfosUiParams: {
    				fctDisplayLegend:true
    			}
            }
        };
	
    carte.addContentManager("layersTree", null, managerOptions);
	
	// Affichage de la carte
	carte.show();
//	
//	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector',
//		options : {label:false}});
//	
//
//	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : "GraphicScale"});
//	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
//	
	
/*	var route = new ol.control.Route({apiKey: "2pidjvd3oh54bqfml4x5b4km", target:"toolBarRoute"}) ;
	carte.OL_map.addControl(route);
	
	carte.addOpenLayersControls([
      {
        type: Descartes.Map.OL_ZOOM,
        args: {
          target: "toolBarZoom",
          className: 'map-tools-zoom',
        },
      },{
           type: Descartes.Map.OL_FULL_SCREEN,
           args: {
             target: "toolBarFull",
             className: 'map-tools-fullscreen',
           }
         }]);
*/	
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}}   
	]);
	
}