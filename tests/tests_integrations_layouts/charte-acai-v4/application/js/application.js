// Montrer/cacher le div de gauche
function ShowHidePanel(panelId, day) {
	$(panelId).toggle();
	if ($(panelId).is(':visible')) {
		if ($("#droite").is(':visible')) {
			$("#milieu").toggleClass('col-md-6', true);
			$("#milieu").toggleClass('col-md-9', false);
		} else {
			$("#milieu").toggleClass('col-md-12', false);
			$("#milieu").toggleClass('col-md-9', true);
		}
		
	} else {
		if ($("#droite").is(':visible')) {
			$("#milieu").toggleClass('col-md-6', false);
			$("#milieu").toggleClass('col-md-9', true);
		} else {
			$("#milieu").toggleClass('col-md-9', false);
			$("#milieu").toggleClass('col-md-12', true);
		}
	}
	
	carte.autoSizeMap();
	
};

//Montrer/cacher le div de gauche
function ShowHidePanel2(panelId, day) {
	$(panelId).toggle();
	if ($(panelId).is(':visible')) {
		if ($("#gauche").is(':visible')) {
			$("#milieu").toggleClass('col-md-6', true);
			$("#milieu").toggleClass('col-md-9', false);
		} else {
			$("#milieu").toggleClass('col-md-12', false);
			$("#milieu").toggleClass('col-md-9', true);
		}
		
	} else {
		if ($("#gauche").is(':visible')) {
			$("#milieu").toggleClass('col-md-6', false);
			$("#milieu").toggleClass('col-md-9', true);
		} else {
			$("#milieu").toggleClass('col-md-9', false);
			$("#milieu").toggleClass('col-md-12', true);
		}
	}
	
	carte.autoSizeMap();
	
};