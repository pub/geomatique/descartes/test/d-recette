proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	Descartes.Messages.Descartes_Messages_UI_GazetteerInPlace.BUTTON_MESSAGE="<i class=\"fa fa-crosshairs\"></i> Localiser";
	Descartes.Messages.Descartes_Messages_UI_GazetteerInPlace.TITLE_MESSAGE="<i class=\"fa fa-map-marker\"></i> Localisation";
	Descartes.Messages.Descartes_Messages_UI_ScaleChooserInPlace.BUTTON_MESSAGE= "<i class=\"fa fa-check\"></i> Activer";
	Descartes.Messages.Descartes_Messages_UI_ScaleChooserInPlace.TITLE_MESSAGE="<i class=\"fa fa-list\"></i> Choisir l\'échelle de la carte";
	Descartes.Messages.Descartes_Messages_UI_ScaleSelectorInPlace.TITLE_MESSAGE="<i class=\"fa fa-list\"></i> Choisir l\'échelle de la carte";
	Descartes.Messages.Descartes_Messages_UI_SizeSelectorInPlace.TITLE_MESSAGE="<i class=\"fa fa-image\"></i> Choisir la taille de la carte";
	Descartes.Messages.Descartes_Messages_UI_CoordinatesInputInPlace.BUTTON_MESSAGE="<i class=\"fa fa-crosshairs\"></i> Recentrer";
	Descartes.Messages.Descartes_Messages_UI_CoordinatesInputInPlace.TITLE_MESSAGE="<i class=\"fa fa-edit\"></i> Choisir les coordonnées";
	Descartes.Messages.Descartes_Messages_UI_PrinterSetupInPlace.BUTTON_MESSAGE="<i class=\"fa fa-file\"></i> Générer le PDF";
	Descartes.Messages.Descartes_Messages_UI_PrinterSetupInPlace.TITLE_MESSAGE="<i class=\"fa fa-wrench\"></i> Paramètres de la mise en page";
	Descartes.Messages.Descartes_Messages_UI_BookmarksInPlace.TITLE_MESSAGE="<i class=\"fa fa-cog\"></i> Gérer des vues personnalisées";
	Descartes.Messages.Descartes_Messages_UI_BookmarksInPlace.BUTTON_MESSAGE="<i class=\"fa fa-save\"></i> Enregistrer la vue courante";
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.TITLE_MESSAGE= '<i class=\"fa fa-list\"></i> Requêtes attributaires';
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.BUTTON_MESSAGE_RECHERCHE="<i class=\"fa fa-search\"></i> Rechercher";
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.BUTTON_MESSAGE_UNSELECT="<i class=\"fa fa-trash\"></i> Effacer la sélection";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.DIALOG_TITLE= "<i class=\"fa fa-wrench\"></i> Paramètres de la mise en page";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.OK_BUTTON= "<i class=\"fa fa-file\"></i> Générer le PDF";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.CANCEL_BUTTON= "<i class=\"fa fa-times\"></i> Annuler";
    Descartes.Messages.Descartes_Messages_Info_Legend.TITLE_MESSAGE= "<i class=\"fa fa-image\"></i> Légende";

	
    Descartes.Descartes_Papers= [
    	{name: 'A2', width:420, length:594},
    	{name: 'A3', width:297, length:420},
        {name: 'A4', width: 210, length: 297},
        {name: 'A5', width: 148, length: 210}
    ];

    Descartes.Descartes_Papers.push( {name:"A0", width: 1189, length: 841} );
    Descartes.Descartes_Papers.push( {name:"A1", width: 841, length: 594} );
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);
	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
	var initbounds = [862500, 6237000, 925300, 6288000];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
					minScale:2150000,
					autoScreenSize: true,
					displayExtendedOLExtent: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	
	carte.addContentManager(
		'layersTree',
		[
			{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL}
		],
		{
			toolBarDiv: "managerToolBar"
		}
	);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
//	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
//	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
//	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector',
		options : {optionsPanel: {collapsible: true, collapsed: true}}});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser',
		options : {optionsPanel: {collapsible: true, collapsed: true}}});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput',
		options : {optionsPanel: {collapsible: true, collapsed: true}}});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf',
		options : {optionsPanel: {collapsible: true, collapsed: true}}});
	
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
//	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");
		
	// Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes',{optionsPanel: {collapsible: true, collapsed: true}});
	var requeteBati = new Descartes.Request(coucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);
	
	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	
	// Ajout du gestionnaire d'info-bulle
	/*carte.addToolTip('ToolTip', [
  		{layer: coucheParkings, fields: ['name']},
  		{layer: coucheStations, fields: ['name']}
  	]);*/
  	
  	/*carte.addToolTip('ToolTip', [ // pour intranet
   		{layer: coucheParkings, fields: ['name']},
   		{layer: coucheStations, fields: ['name']}
   	]);*/

	
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes',{behavior : Descartes.Action.BookmarksManager.BEHAVIOR_PERSONAL, optionsPanel: {collapsible: true, collapsed: true}});
	
	// Ajout du gestionnaire de localisation rapide
	carte.addDefaultGazetteer('Gazetteer', "93", Descartes.Action.DefaultGazetteer.DEPARTEMENT,{optionsPanel: {collapsible: true, collapsed: true}});

    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}}   
	]);
}

$(function(){

	$('#slide-submenu').on('click',function() {			        
        $(this).closest('.list-group').fadeOut('slide',function(){
        	$('.mini-submenu').fadeIn();	
        });
        
      });

	$('.mini-submenu').on('click',function(){		
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
	})
	
	$('#slide-submenu2').on('click',function() {			        
        $(this).closest('.list-group').fadeOut('slide',function(){
        	$('.mini-submenu2').fadeIn();	
        });
        
      });

	$('.mini-submenu2').on('click',function(){		
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu2').hide();
	})
})
