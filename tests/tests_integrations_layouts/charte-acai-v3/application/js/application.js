// Montrer/cacher le div de gauche
function ShowHidePanel(panelId, day) {
	$(panelId).toggle();
	if ($(panelId).is(':visible')) {
		if ($("#droite").is(':visible')) {
			$("#milieu").toggleClass('col-md-5', true);
			$("#milieu").toggleClass('col-md-8', false);
		} else {
			$("#milieu").toggleClass('col-md-14', false);
			$("#milieu").toggleClass('col-md-8', true);
		}
		
	} else {
		if ($("#droite").is(':visible')) {
			$("#milieu").toggleClass('col-md-5', false);
			$("#milieu").toggleClass('col-md-8', true);
		} else {
			$("#milieu").toggleClass('col-md-8', false);
			$("#milieu").toggleClass('col-md-11', true);
		}
	}
	
	carte.autoSizeMap();
	
};

//Montrer/cacher le div de gauche
function ShowHidePanel2(panelId, day) {
	$(panelId).toggle();
	if ($(panelId).is(':visible')) {
		if ($("#gauche").is(':visible')) {
			$("#milieu").toggleClass('col-md-5', true);
			$("#milieu").toggleClass('col-md-8', false);
		} else {
			$("#milieu").toggleClass('col-md-11', false);
			$("#milieu").toggleClass('col-md-8', true);
		}
		
	} else {
		if ($("#gauche").is(':visible')) {
			$("#milieu").toggleClass('col-md-5', false);
			$("#milieu").toggleClass('col-md-8', true);
		} else {
			$("#milieu").toggleClass('col-md-8', false);
			$("#milieu").toggleClass('col-md-11', true);
		}
	}
	
	carte.autoSizeMap();
	
};

function InitShowHidePanel(panelId) {
		$(panelId).toggle();
};