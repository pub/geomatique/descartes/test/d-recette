/***********************************
 * 
 * 			CODE BOUCHON
 * 
 ***********************************/

function sendRequestBouchonForSaveElements(json) {
   if (json.fluxSimple) {
	   if (json.fluxSimple.addObjects && json.fluxSimple.addObjects.length > 0) {
		   var addFeatures = json.fluxSimple.addObjects;
		   for (var i = 0; i < addFeatures.length; i++) {
			   var feature = new ol.Feature({
				    geometry: addFeatures[i].geometry
				});
				feature.setId(addFeatures[i].objectId);
				if (addFeatures[i].attributs) {
					feature.setProperties(addFeatures[i].attributs);
				}
			   myFeatures.push(feature);
		   }

	   } 
	   
	   if (json.fluxSimple.updatedObjects && json.fluxSimple.updatedObjects.length > 0) {
		   var updatedFeatures = json.fluxSimple.updatedObjects
		   for (var i = 0; i < updatedFeatures.length; i++) {
			   for (var j = 0; j < myFeatures.length; j++) {
				   if (myFeatures[j].getId() === updatedFeatures[i].objectId) {
					   myFeatures[j].setGeometry(updatedFeatures[i].geometry);
					   break;
				   }
			   }
		   }
		   
	   } 
	   
	   if (json.fluxSimple.removedObjects && json.fluxSimple.removedObjects.length > 0) { 
		   var removedFeatures = json.fluxSimple.removedObjects
		   var newMyFeature = [];
		   for (var i = 0; i < removedFeatures.length; i++) {
			   for (var j = 0; j < myFeatures.length; j++) {
				   if (myFeatures[j].getId() !== removedFeatures[i].objectId) {
					   newMyFeature.push(myFeatures[j]);
				   }
			   }
		   }
		   myFeatures = newMyFeature;
	   } 
	   
	   json.priv={status:200,message:"Sauvegarde ok"};  
   } else {
	   json.priv={status:500,message:"erreur lors de la sauvegarde"};
   }
   
   json.callback.call(json); 
}

