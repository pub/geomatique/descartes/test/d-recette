Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeCouchesGroupes() {

	coucheGenericVectorVide = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [{}],
		    options: {
		        alwaysVisible: false,
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null
		    }
		};
	
	coucheGenericVectorVide2 = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [{}],
		    options: {
		        alwaysVisible: false,
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        hide: true
		    }
		};
	
	coucheGenericVector = {
	    title: "Ma couche GenericVector",
	    type: Descartes.Layer.TYPE_GenericVector,
	    definition: [
	                 {	 
	                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
	                		
	                		var markerPoint = new ol.Feature({
                			    geometry: new ol.geom.Point([1.317688888888891, 45.55597046260131])
                			});
                			var markerPoint2 = new ol.Feature({
                			    geometry: new ol.geom.Point([0.3737481481481506, 46.56841490704575])
                			});
                			
                			var markerPolygon= new ol.Feature({
                			    geometry: new ol.geom.Polygon([	[[1.8546370370370404,47.26352601815686],[1.4919703703703728,46.296414907045744],[2.2475259259259293,46.17552601815686],[1.8546370370370404,47.26352601815686]]])
                			});
                			
                			var markerLigne= new ol.Feature({
                			    geometry: new ol.geom.LineString([[0.10779680049086071,45.24686233328316], [0.8356841244345237,44.955707403705695], [1.625961790430499,45.30925267533547], [2.4162394564264753,45.16367521054674]])
                			});
                			
                			 
                			this.addFeatures([markerPoint,markerPoint2,markerPolygon,markerLigne]);

	                	 }

	                 }
	    ],
	    options: {
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null
	    }
	};
	
	coucheGenericVector2 = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [
		                 {	 
		                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {

		                		//var wkt = "GEOMETRYCOLLECTION(POINT(1.317688888888891 45.55597046260131),POINT(0.3737481481481506 46.56841490704575),POLYGON((1.8546370370370404 47.26352601815686,1.4919703703703728 46.296414907045744,2.2475259259259293 46.17552601815686,1.8546370370370404 47.26352601815686)),LINESTRING(0.10779680049086071 45.24686233328316,0.8356841244345237 44.955707403705695,1.625961790430499 45.30925267533547,2.4162394564264753 45.16367521054674))";

		                		var wkt1 = "POINT(1.317688888888891 45.55597046260131)";               			
	                			var wkt2 = "POINT(0.3737481481481506 46.56841490704575)";              			
	                			var wkt3 = "POLYGON((1.8546370370370404 47.26352601815686, 1.4919703703703728 46.296414907045744, 2.2475259259259293 46.17552601815686, 1.8546370370370404 47.26352601815686))";
	                			var wkt4 = "LINESTRING(0.10779680049086071 45.24686233328316,0.8356841244345237 44.955707403705695,1.625961790430499 45.30925267533547,2.4162394564264753 45.16367521054674)";              			

	                			var format = new ol.format.WKT();
	                			this.addFeatures([format.readFeature(wkt1),format.readFeature(wkt2),format.readFeature(wkt3),format.readFeature(wkt4)]);
	                			
		                	 }

		                 }
		    ],
		    options: {
		        alwaysVisible: false,
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null
		    }
		};
	
	coucheGenericVector3 = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [
		                 {	 
		                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {

	                			//var geojson = '{"crs":{"properties":{"name":"EPSG:4326"},"type":"name"},"features":[{"id":"dObjectId_bqsc8nw5nx","properties":{},"type":"Feature","geometry":{"type":"Point","coordinates":[-2.3317933476562,46.774353663086]}},{"id":"dObjectId_z2a0p4pwigq","properties":{},"type":"Feature","geometry":{"type":"Point","coordinates":[-1.1948333476562,47.559889663086]}},{"id":"dObjectId_x3o8hrcnreg","properties":{},"type":"Feature","geometry":{"type":"Point","coordinates":[-3.8821933476562,51.525468224609]}},{"id":"dObjectId_32tavc155qn","properties":{},"type":"Feature","geometry":{"type":"Point","coordinates":[-1.9803693476562,46.815697663086]}},{"id":"dObjectId_osyubags1i","properties":{},"type":"Feature","geometry":{"type":"Point","coordinates":[-1.4842413476562,45.947473663086]}},{"id":"dObjectId_a0gg3eqjlwr","properties":{},"type":"Feature","geometry":{"type":"Point","coordinates":[5.380664463457764,43.27614387582341]}},{"id":"dObjectId_euqg7ys31be","properties":{},"type":"Feature","geometry":{"type":"LineString","coordinates":[[0.10779680049086071,45.24686233328316],[0.8356841244345237,44.955707403705695],[1.625961790430499,45.30925267533547],[2.4162394564264753,45.16367521054674]]}}],"type":"FeatureCollection"}';
		                	    var geojson = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Point","coordinates":[1.317688888888891,45.55597046260131]},"properties":{"Nom": "NomA"}},{"type":"Feature","geometry":{"type":"Point","coordinates":[0.3737481481481506,46.56841490704575]},"properties":{"Nom": "NomB"}},{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[1.8546370370370404,47.26352601815686],[1.4919703703703728,46.296414907045744],[2.2475259259259293,46.17552601815686],[1.8546370370370404,47.26352601815686]]]},"properties":{"Nom": "NomC"}},{"type":"Feature","geometry":{"type":"LineString","coordinates":[[0.10779680049086071,45.24686233328316],[0.8356841244345237,44.955707403705695],[1.625961790430499,45.30925267533547],[2.4162394564264753,45.16367521054674]]},"properties":{"Nom": "NomD"}}]}';
	                			var format = new ol.format.GeoJSON();
	                			var features = format.readFeatures(geojson);
	                			this.addFeatures(features);

		                	 }

		                 }
		    ],
		    options: {
		        alwaysVisible: false,
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null
		    }
		};

	coucheGenericVector4 = {
	    title: "Ma couche GenericVector",
	    type: Descartes.Layer.TYPE_GenericVector,
	    definition: [
	                 {	 
	                	 displayFeatures: getMyFeatures()
	                 }
	    ],
	    options: {
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null
	    }
	};
	
	coucheGenericVector5 = {
	    title: "Ma couche GenericVector",
	    type: Descartes.Layer.TYPE_GenericVector,
	    definition: [
	                 {	 
	                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
		                		
		                		var markerPoint = new ol.Feature({
	                			    geometry: new ol.geom.Point([1.317688888888891, 45.55597046260131]),
	                			});
	                			var markerPoint2 = new ol.Feature({
	                			    geometry: new ol.geom.Point([0.3737481481481506, 46.56841490704575]),
	                			});
	                			 
	                			this.addFeatures([markerPoint,markerPoint2]);

		                	 }
	                 }
	    ],
	    options: {
	        alwaysVisible: false,
	        visible: true,
	        queryable: false,
	        activeToQuery: false,
	        sheetable: false,
	        opacity: 100,
	        opacityMax: 100,
	        legend: null,
	        metadataURL: null,
	        symbolizers:{
  	          "Point": {
					externalGraphic:"marker.png",
				  }
  	        }
	    }
	};
	
	coucheGenericVector6 = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [
		                 {	 
		                	 displayFeatures: getMyFeatures()
		                 }
		    ],
		    options: {
		        alwaysVisible: false,
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        symbolizersFunction: function(feature) {
    	        	var style = new ol.style.Style({
    	                image: new ol.style.Circle({
    	                  radius: 1,
    	                  fill: new ol.style.Fill({
    	                    color: 'black'
    	                  })
    	                }),
    	                text: new ol.style.Text({
  		                    text: '\uf041', // fa-map-marker
  		                    font: 'normal 18px FontAwesome',
  		                    fill: new ol.style.Fill({color: 'black'})
  		                  })
    	              });
    	              return style;
			      }
		    }
		};
	
	coucheBase = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
					layerName: "fond_vecteur"
				}
			],
			options: {
				maxScale: 100,
				minScale: 10000001,
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 50,
				opacityMax: 100,
				legend: [],
				metadataURL: null,
				format: "image/png"
			}
		};

		groupeFonds = {
		    title: "Fonds cartographiques",
		    options: {
		        opened: true
		    }
		};
}
function getMyFeatures(){

	var markerPoint = new ol.Feature({
	    geometry: new ol.geom.Point([1.317688888888891, 45.55597046260131]),
	});
	var markerPoint2 = new ol.Feature({
	    geometry: new ol.geom.Point([0.3737481481481506, 46.56841490704575]),
	});
	 
	return [markerPoint,markerPoint2];

}
