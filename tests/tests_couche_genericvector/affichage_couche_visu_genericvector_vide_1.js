function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	    
    layerCoucheGenericVector = new Descartes.Layer.GenericVector(coucheGenericVectorVide.title, coucheGenericVectorVide.definition, coucheGenericVectorVide.options);
    
    contenuCarte.addItem(layerCoucheGenericVector);
    
  
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
    
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);    
	
    var markerPoint = new ol.Feature({
	    geometry: new ol.geom.Point([1.317688888888891, 45.55597046260131]),
	});
	var markerPoint2 = new ol.Feature({
	    geometry: new ol.geom.Point([0.3737481481481506, 46.56841490704575]),
	});
	
	var markerPolygon= new ol.Feature({
	    geometry: new ol.geom.Polygon([	[[1.8546370370370404,47.26352601815686],[1.4919703703703728,46.296414907045744],[2.2475259259259293,46.17552601815686],[1.8546370370370404,47.26352601815686]]]),
	});
    
	var markerLigne= new ol.Feature({
	    geometry: new ol.geom.LineString([[0.10779680049086071,45.24686233328316], [0.8356841244345237,44.955707403705695], [1.625961790430499,45.30925267533547], [2.4162394564264753,45.16367521054674]])
	});
	
    layerCoucheGenericVector.addFeatures([markerPoint,markerPoint2,markerPolygon,markerLigne]);

	
}
