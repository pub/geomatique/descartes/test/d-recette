var editionLayer1;

function chargementCarte() {
	
	chargeEditionCouchesGroupes();
	
	 //Configuration du gestionnaire d'édition
	 Descartes.EditionManager.configure({
		autoSave: false,
		globalEditionMode: true,
        save: function (json) {
	     	 //Ici, code MOE qui est spécifique à chaque application métier.
	    	 //ce code doit se charger de la sauvegarde des éléments fournis par Descartes
	         //et doit retourner une réponse à Descartes dans le format imposé (cf. documentation).
	     	   	
	    	 //Pour que les exemples Descartes fonctionnent, utilisation d'une méthode "bouchon"
	    	 sendRequestBouchonForSaveElements(json);
	
	    }
    });     
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
    editionLayer1 = new Descartes.Layer.EditionLayer.GenericVector(coucheEditionGenericVector4.title, coucheEditionGenericVector4.definition, coucheEditionGenericVector4.options);
    editionLayer1.setVisibility(false);
    
    contenuCarte.addItem(editionLayer1);
    
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
 
	
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			autoSize: true
		}
	);
	
	 //Ajout d'un barre d'outils d'édition
	  carte.addEditionToolBar('editionToolBar', [
	       {
               type: Descartes.Map.EDITION_DRAW_CREATION,
               args: {
                   geometryType: Descartes.Layer.POINT_GEOMETRY,
                   editionLayer: editionLayer1,
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_CREATION,
               args: {
                   geometryType: Descartes.Layer.LINE_GEOMETRY,
                   editionLayer: editionLayer1,
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_CREATION,
               args: {
                   geometryType: Descartes.Layer.POLYGON_GEOMETRY,
                   editionLayer: editionLayer1,
               }
           },
           {
               type: Descartes.Map.EDITION_GLOBAL_MODIFICATION,
         	   args: {
                  editionLayer: editionLayer1
               }
           }, {
               type: Descartes.Map.EDITION_VERTICE_MODIFICATION,
         	   args: {
                   editionLayer: editionLayer1
                }
           },
           {
               type: Descartes.Map.EDITION_ATTRIBUTE,
         	   args: {
                   editionLayer: editionLayer1
                }
           },
           {
               type: Descartes.Map.EDITION_RUBBER_DELETION,
         	   args: {
                   editionLayer: editionLayer1
                }
           },
           {
               type: Descartes.Map.EDITION_SAVE
           }
	  ]);
	
	// Affichage de la carte
	carte.show();
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
}

function enableEdition(editionLayer) {
	document.getElementById("disableEdition").disabled=false;
    document.getElementById("enableEdition").disabled=true;
    editionLayer1.setVisibility(true);
	Descartes.EditionManager.enableEditionLayer(editionLayer);

}
function disableEdition(editionLayer) {
	document.getElementById("disableEdition").disabled=true;
    document.getElementById("enableEdition").disabled=false;
    editionLayer1.setVisibility(false);
	Descartes.EditionManager.disableEditionLayer(editionLayer);
}