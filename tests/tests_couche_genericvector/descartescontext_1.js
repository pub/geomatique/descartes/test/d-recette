var context = {
	"map" : {
		"type": "Continuous",
		"div": "map",
		"mapParams": {
			"projection": "EPSG:4326",
			"displayExtendedOLExtent": true,
			"initExtent": [-0.615, 41.657, 5.721, 51.993],
			"maxExtent": [-0.615, 41.657, 5.721, 51.993],
			"minScale": null,
			"maxScale": 100,
			"autoSize": true
		}
	},
	"mapContent": {
			"mapContentParams": {
				"editable": true
			},
			"mapContentManager":{
				"div": "layersTree",
				"contentTools": null,
				"options": {}
			},
			"items" : [{
				"itemType" : "Layer",
				"title" : "Ma Couche Vector",
				"type" : Descartes.Layer.TYPE_GenericVector,
				"options" : {
					"id" : "maCoucheVector",
					"queryable" : false
				}
			},
			{
				"itemType" : "Layer",
				"title" : "OSM",
				"type" : Descartes.Layer.TYPE_OSM
			}
		]
	}
};