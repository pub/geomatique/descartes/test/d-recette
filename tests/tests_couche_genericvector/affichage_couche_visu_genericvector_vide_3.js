function chargementCarte() {
	carte = Descartes.applyDescartesContext(context);
	
}

function addFeature1() {
    document.getElementById("addFeature1").disabled=true;
    var markerPoint = new ol.Feature({
	    geometry: new ol.geom.Point([1.317688888888891, 45.55597046260131]),
	});
    
    var maCouche = carte.mapContent.getLayerById("maCoucheVector");
    maCouche.addFeatures([markerPoint]);

}

function addFeature2() {
	document.getElementById("addFeature2").disabled=true;
	var markerPoint2 = new ol.Feature({
	    geometry: new ol.geom.Point([0.3737481481481506, 46.56841490704575]),
	});
	
	var maCouche = carte.mapContent.getLayerById("maCoucheVector");
	maCouche.addFeatures([markerPoint2]);

}

function addFeature3() {
	document.getElementById("addFeature3").disabled=true;
	var markerLigne= new ol.Feature({
	    geometry: new ol.geom.LineString([[0.10779680049086071,45.24686233328316], [0.8356841244345237,44.955707403705695], [1.625961790430499,45.30925267533547], [2.4162394564264753,45.16367521054674]])
	});
    
	var maCouche = carte.mapContent.getLayerById("maCoucheVector");
	maCouche.addFeatures([markerLigne]);

}

function addFeature4() {
	document.getElementById("addFeature4").disabled=true;
	var markerPolygon= new ol.Feature({
	    geometry: new ol.geom.Polygon([	[[1.8546370370370404,47.26352601815686],[1.4919703703703728,46.296414907045744],[2.2475259259259293,46.17552601815686],[1.8546370370370404,47.26352601815686]]]),
	});
    
	var maCouche = carte.mapContent.getLayerById("maCoucheVector");
	maCouche.addFeatures([markerPolygon]);

}

function exportPNG() {
	carte.OL_map.once('postcompose', function(event) {
          var canvas = event.context.canvas;
          if (navigator.msSaveBlob) {
            navigator.msSaveBlob(canvas.msToBlob(), 'maCarteDescartes.png');
          } else {
            canvas.toBlob(function(blob) {
              saveAs(blob, 'maCarteDescartes.png');
            });
          }
        });
	carte.OL_map.renderSync();
}