function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	    
    layerCoucheGenericVector = new Descartes.Layer.GenericVector(coucheGenericVector6.title, coucheGenericVector6.definition, coucheGenericVector6.options);
    contenuCarte.addItem(layerCoucheGenericVector);
     
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
    
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar",
			uiOptions: {
				resultUiParams:{
					div: 'resultat',
					withReturn: true,
					withCsvExport: true
				}
			}
	};
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
    var markerPoint = new ol.Feature({
	    geometry: new ol.geom.Point([1.317688888888891, 45.55597046260131]),
	});
	var markerPoint2 = new ol.Feature({
	    geometry: new ol.geom.Point([0.3737481481481506, 46.56841490704575]),
	});
    
    layerCoucheGenericVector.addFeatures([markerPoint,markerPoint2]);

	
}
