var myFeatures = null;

function chargeEditionCouchesGroupes() {
	
	coucheEditionGenericVector = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [
		                 {	 
		                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
		                		 if (myFeatures === null) {
		                			 initMyFeatures();
		                		 }
		                		 this.addFeatures(myFeatures);
		                	 }

		                 }
		    ],
		    options: {
		        alwaysVisible: false,
		        attributes: {
		            attributesEditable: [
		                {fieldName: 'Nom', label: 'Nom'}
		            ]
		        },
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        format: "image/png"
		    }
		};
	
	coucheEditionGenericVector2 = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [
		                 {	 
		                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
		                		 if (myFeatures === null) {
		                			 myFeatures = [];
		                		 }
		                		 this.addFeatures(myFeatures);
		                		
		                	 }

		                 }
		    ],
		    options: {
		        alwaysVisible: false,
		        attributes: {
		            attributesEditable: [
		                {fieldName: 'Nom', label: 'Nom'}
		            ]
		        },
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        format: "image/png"
		    }
		};
		
	coucheEditionGenericVector3 = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [
		                 {	 
		                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
		                		 if (myFeatures === null) {
		                			 initMyFeatures();
		                		 }
		                		 this.addFeatures(myFeatures);
		                	 }

		                 }
		    ],
		    options: {
		        alwaysVisible: false,
		        attributes: {
		            attributesEditable: [
		                {fieldName: 'Nom', label: 'Nom'}
		            ]
		        },
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        format: "image/png"
		    }
		};
	
	coucheEditionGenericVector4 = {
		    title: "Ma couche GenericVector",
		    type: Descartes.Layer.TYPE_GenericVector,
		    definition: [
		                 {	 
		                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
		                		 if (myFeatures === null) {
		                			 initMyFeatures();
		                		 }
		                		 this.addFeatures(myFeatures);
		                	 }

		                 }
		    ],
		    options: {
		        alwaysVisible: false,
		        attributes: {
		            attributesEditable: [
		                {fieldName: 'Nom', label: 'Nom'}
		            ]
		        },
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null,
		        format: "image/png",
		        hide: true
		    }
		};
	
		coucheBase = {
				title : "Fond de carte",
				type: 0,
				definition: [
					{
						serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
						layerName: "fond_vecteur"
					}
				],
				options: {
					maxScale: 100,
					minScale: 10000001,
					alwaysVisible: false,
					visible: true,
					queryable:false,
					activeToQuery:false,
					sheetable:false,
					opacity: 50,
					opacityMax: 100,
					legend: [],
					metadataURL: null,
					format: "image/png"
				}
			};
		
      groupeFonds = {
	      title: "Fonds cartographiques",
	      options: {
	          opened: true
	      }
	  };

	  groupeEdition = {
	      title: "Mes couches d'édition WFS - objet simple",
	      options: {
	          opened: true
	      }
	  };



}

function initMyFeatures() {
	var markerPoint = new ol.Feature({
		geometry: new ol.geom.Point([1.317688888888891, 45.55597046260131]),
	});
	markerPoint.setId("marker1");
	var markerPoint2 = new ol.Feature({
	    geometry: new ol.geom.Point([0.3737481481481506, 46.56841490704575]),
	});
	markerPoint2.setId("marker2");
	var markerPolygon= new ol.Feature({
		geometry: new ol.geom.Polygon([	[[1.8546370370370404,47.26352601815686],[1.4919703703703728,46.296414907045744],[2.2475259259259293,46.17552601815686],[1.8546370370370404,47.26352601815686]]]),
	});
	markerPolygon.setId("marker3");
	var markerLigne= new ol.Feature({
	    geometry: new ol.geom.LineString([[0.10779680049086071,45.24686233328316], [0.8356841244345237,44.955707403705695], [1.625961790430499,45.30925267533547], [2.4162394564264753,45.16367521054674]])
	});
	markerLigne.setId("marker4");
	myFeatures = [markerPoint,markerPoint2,markerPolygon,markerLigne];
}