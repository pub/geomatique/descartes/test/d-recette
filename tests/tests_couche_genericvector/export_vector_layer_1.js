function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable:true, editInitialItems:true, fixedDisplayOrders:false});
	
	    
    layerCoucheGenericVector = new Descartes.Layer.GenericVector(coucheGenericVector.title, coucheGenericVector.definition, coucheGenericVector.options);
    contenuCarte.addItem(layerCoucheGenericVector);
   
    gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
    contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options), gpFonds);

    var projection = "EPSG:4326";
    var bounds = [-0.615, 41.657, 5.721, 51.993];
    
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
		'map',
		contenuCarte,
		{
			projection: projection,
			displayExtendedOLExtent: true,
			initExtent: bounds,
			maxExtent: bounds,
			maxScale: 100,
			size: [750, 500]
		}
	);
	
	var managerOptions = {
			toolBarDiv: "managerToolBar"
	};

	
	carte.addContentManager(
		'layersTree',
		[
			{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.EXPORT_VECTORLAYER_TOOL}
		],
		managerOptions
	);

	// Affichage de la carte
	carte.show();
	
	var infos =  [{type:"MetricScale", div:'MetricScale'}];	  
	carte.addInfos(infos);
	
	//CONTROLES OPENLAYERS
	carte.addOpenLayersInteractions([
		{type: Descartes.Map.OL_DRAG_PAN}, 
		{type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM} // zoomRoulette, DragPan avec touche ALT et ZoomBox avec la touche SHIFT
	]);
	
	
}
function test () {
    var format1 = new ol.format.GeoJSON();

	console.log(format1.writeFeatures(layerCoucheGenericVector.getFeatures()));

    var format2 = new ol.format.WKT();

	console.log(format2.writeFeatures(layerCoucheGenericVector.getFeatures()));

}
