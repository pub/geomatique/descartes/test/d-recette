var context = {
"map": {
    "type": "Discrete",
    "div": "map",
    "mapParams": {
        "maxExtent": [
            -20037508,
            -20037508,
            20037508,
            20037508
        ],
        "autoSize": true,
        "projection": "EPSG:3857",
        "initExtent": [
            -1971470,
            4938294,
            2683238,
            6963569
        ],
        "minScale": null,
        "maxScale": null,
        "resolutions": [
            156543.03,
            104362.02,
            69574.68000000001,
            46383.12,
            30922.08,
            20614.72,
            13743.146666666667,
            9162.097777777779,
            6108.065185185186,
            4072.0434567901243,
            2714.695637860083,
            1809.797091906722,
            1206.5313946044814,
            804.3542630696543,
            536.2361753797695,
            357.49078358651303,
            238.32718905767535,
            158.8847927051169,
            105.9231951367446,
            70.6154634244964,
            47.076975616330934,
            31.38465041088729,
            20.92310027392486,
            13.948733515949906,
            9.299155677299938,
            6.199437118199959,
            4.132958078799972,
            2.755305385866648,
            1.836870257244432,
            1.224580171496288,
            0.816386780997525,
            0.544257853998350,
            0.362838569332233,
            0.2418923795548223,
            0.1612615863698815,
            0.1075077242465877,
            0.0716718161643918,
            0.0477812107762612,
            0.0318541405175074,
            0.0212360936783383,
            0.0141573957855588,
            0.0094382638570392,
            0.0062921759046928,
            0.0041947839364618,
            0.0027965226243079
        ]
    }
},
"mapContent": {
    "mapContentParams": {
        "editable": true,
        "fctOpacity": true,
        "fctDisplayLegend": true,
        "fctMetadataLink": true,
        "displayMoreInfos": true,
        "displayIconLoading": true
    },
    "mapContentManager": {
        "div": "layersTree",
        "contentTools": [
            {
                "type": "AddGroup"
            },
            {
                "type": "RemoveGroup"
            },
            {
                "type": "RemoveLayer",
                "options": {
                    "title": "Supprimer une couche WMS"
                }
            },
            {
                "type": "ChooseWmsLayers"
            }
        ],
        "options": {
            "toolBarDiv": "managerToolBar",
            "uiOptions": {
                "moreInfosUiParams": {
                    "fctOpacity": true,
                    "fctDisplayLegend": true,
                    "displayOnClickLayerName": true
                },
                "resultUiParams": {
                    "withReturn": true,
                    "withUIExports": true,
                    "withAvancedView": true,
                    "withListResultLayer": true,
                    "withFilterColumns": true,
                    "withResultLayerExport": true
                }
            }
        }
    },
    "items": [
        {
            "id": 1,
            "title": "Ma couche Annotations",
            "type": 14,
            "itemType": "Layer"
        },
        {
            "id": 813767925,
            "itemType": "Layer",
            "title": "Stations_hydro52_l93",
            "type": 0,
            "options": {
                "id": "813767925",
                "visible": true,
                "opacity": 100,
                "minScale": null,
                "maxScale": null,
                "legend": [
                    "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/Stations_hydro52_l93?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=Stations_hydro52_l93&LEGEND_OPTIONS=forceLabels%3Aon"
                ],
                "geometryType": "Point",
                "queryable": true,
                "attributes": {
                    "attributesAlias": []
                }
            },
            "definition": [
                {
                    "serverUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/Stations_hydro52_l93",
                    "serverVersion": "1.3.0",
                    "layerName": "Stations_hydro52_l93",
                    "featureGeometryName": "the_geom",
                    "featureNameSpace": "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
                    "internalProjection": "EPSG:3857",
                    "useBboxSrsProjection": true,
                    "featureServerUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/Stations_hydro52_l93",
                    "featureServerVersion": "1.1.0",
                    "featureName": "Stations_hydro52_l93"
                }
            ]
        },
        {
            "id": 813767924,
            "itemType": "Layer",
            "title": "Points d'eau isolés Métropole",
            "type": 4,
            "options": {
                "id": "813767924",
                "visible": false,
                "opacity": 100,
                "maxScale": null,
                "minScale": null,
                "legend": [
                    "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/public?SERVICE=WMS&VERSION=1.1.0&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=topp%3Astates&LEGEND_OPTIONS=forceLabels%3Aon&SLD_BODY=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CStyledLayerDescriptor+version%3D%221.0.0%22+xsi%3AschemaLocation%3D%22http%3A%2F%2Fwww.opengis.net%2Fsld+StyledLayerDescriptor.xsd%22+xmlns%3D%22http%3A%2F%2Fwww.opengis.net%2Fsld%22+xmlns%3Aogc%3D%22http%3A%2F%2Fwww.opengis.net%2Fogc%22+xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22+xmlns%3Axsi%3D%22http%3A%2F%2Fwww.w3.org%2F2001%2FXMLSchema-instance%22%3E%3CNamedLayer%3E%3CName%3Edefault%3C%2FName%3E%3CUserStyle%3E%3CName%3Edefault%3C%2FName%3E%3CTitle%3Edefault%3C%2FTitle%3E%3CFeatureTypeStyle%3E%3CRule%3E%3CName%2F%3E%3CPointSymbolizer%3E%3CGraphic%3E%3CMark%3E%3CWellKnownName%3Ecircle%3C%2FWellKnownName%3E%3CFill%3E%3CCssParameter+name%3D%22fill%22%3E%23c1c1ff%3C%2FCssParameter%3E%3CCssParameter+name%3D%22fill-opacity%22%3E1%3C%2FCssParameter%3E%3C%2FFill%3E%3C%2FMark%3E%3CSize%3E12%3C%2FSize%3E%3C%2FGraphic%3E%3C%2FPointSymbolizer%3E%3C%2FRule%3E%3C%2FFeatureTypeStyle%3E%3C%2FUserStyle%3E%3C%2FNamedLayer%3E%3C%2FStyledLayerDescriptor%3E"
                ],
                "queryable": true
            },
            "definition": [
                {
                    "serverUrl": "http://services.sandre.eaufrance.fr/geo/zonage",
                    "layerName": "sa:PointEauIsole",
                    "serverVersion": "1.1.0",
                    "featureServerUrl": "http://services.sandre.eaufrance.fr/geo/zonage",
                    "featureServerVersion": "1.1.0",
                    "featureGeometryName": "msGeometry",
                    "featureNameSpace": "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
                    "useBboxSrsProjection": true
                }
            ]
        },
        {
            "id": 813767923,
            "itemType": "Layer",
            "title": "L_POINT_P",
            "type": 0,
            "options": {
                "id": "813767923",
                "visible": true,
                "opacity": 100,
                "minScale": null,
                "maxScale": null,
                "legend": [
                    "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/N_INDUS_PREM_TRANSFO_BOIS_P_R26?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=N_INDUS_PREM_TRANSFO_BOIS_P_R26&LEGEND_OPTIONS=forceLabels%3Aon"
                ],
                "geometryType": "Point",
                "queryable": true,
                "attributes": {
                    "attributesAlias": []
                }
            },
            "definition": [
                {
                    "serverUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/N_INDUS_PREM_TRANSFO_BOIS_P_R26",
                    "serverVersion": "1.3.0",
                    "layerName": "N_INDUS_PREM_TRANSFO_BOIS_P_R26",
                    "featureGeometryName": "the_geom",
                    "featureNameSpace": "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
                    "internalProjection": "EPSG:3857",
                    "useBboxSrsProjection": true,
                    "featureServerUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/N_INDUS_PREM_TRANSFO_BOIS_P_R26",
                    "featureServerVersion": "1.1.0",
                    "featureName": "N_INDUS_PREM_TRANSFO_BOIS_P_R26"
                }
            ]
        },
        {
            "id": 813767922,
            "itemType": "Layer",
            "title": "L_PISTE_SKI_L_074",
            "type": 0,
            "options": {
                "id": "813767922",
                "visible": true,
                "opacity": 100,
                "minScale": null,
                "maxScale": null,
                "legend": [
                    "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_PISTE_SKI_L_074&LEGEND_OPTIONS=forceLabels%3Aon"
                ],
                "geometryType": "Line",
                "queryable": true,
                "attributes": {
                    "attributesAlias": []
                }
            },
            "definition": [
                {
                    "serverUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
                    "serverVersion": "1.3.0",
                    "layerName": "L_PISTE_SKI_L_074",
                    "featureGeometryName": "the_geom",
                    "featureNameSpace": "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
                    "internalProjection": "EPSG:3857",
                    "useBboxSrsProjection": true,
                    "featureServerUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
                    "featureServerVersion": "1.1.0",
                    "featureName": "L_PISTE_SKI_L_074"
                }
            ]
        },
        {
            "id": 813767921,
            "itemType": "Layer",
            "title": "L_ENV_DOMAINE_SKIABLE_S_074",
            "type": 0,
            "options": {
                "id": "813767921",
                "visible": true,
                "opacity": 100,
                "minScale": null,
                "maxScale": null,
                "legend": [
                    "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_ENV_DOMAINE_SKIABLE_S_074&LEGEND_OPTIONS=forceLabels%3Aon"
                ],
                "geometryType": "Polygon",
                "queryable": true,
                "attributes": {
                    "attributesAlias": []
                }
            },
            "definition": [
                {
                    "serverUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
                    "serverVersion": "1.3.0",
                    "layerName": "L_ENV_DOMAINE_SKIABLE_S_074",
                    "featureGeometryName": "the_geom",
                    "featureNameSpace": "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
                    "internalProjection": "EPSG:3857",
                    "useBboxSrsProjection": true,
                    "featureServerUrl": "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
                    "featureServerVersion": "1.1.0",
                    "featureName": "L_ENV_DOMAINE_SKIABLE_S_074"
                }
            ]
        },
        {
            "id": 813767920,
            "itemType": "Layer",
            "title": "Photographies aériennes",
            "type": 3,
            "options": {
                "id": "813767920",
                "visible": false,
                "opacity": 100,
                "minScale": null,
                "maxScale": null,
                "matrixSet": "PM",
                "projection": "EPSG:3857",
                "matrixIds": [
                    "0",
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21"
                ],
                "origins": [
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ],
                    [
                        -20037508.342789248,
                        20037508.342789248
                    ]
                ],
                "resolutions": [
                    156543.033928041,
                    78271.51696402048,
                    39135.758482010235,
                    19567.87924100512,
                    9783.93962050256,
                    4891.96981025128,
                    2445.98490512564,
                    1222.99245256282,
                    611.49622628141,
                    305.7481131407048,
                    152.8740565703525,
                    76.43702828517624,
                    38.21851414258813,
                    19.10925707129406,
                    9.554628535647032,
                    4.777314267823516,
                    2.388657133911758,
                    1.194328566955879,
                    0.5971642834779395,
                    0.2985821417389697,
                    0.1492910708694849,
                    0.0746455354347424
                ],
                "tileSizes": [
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256,
                    256
                ],
                "format": "image/jpeg",
                "queryable": false
            },
            "definition": [
                {
                    "serverUrl": "https://wxs.ign.fr/pratique/geoportail/wmts?",
                    "serverVersion": "1.0.0",
                    "layerName": "ORTHOIMAGERY.ORTHOPHOTOS",
                    "extent": [
                        -19981848.597392607,
                        -12932243.11199203,
                        19981848.597392607,
                        12932243.11199202
                    ],
                    "layerStyles": "normal"
                }
            ]
        }
    ]
},
"editionManager":{
	"configureOptions": {
		 globalEditionMode: false
	}
},
"features": {
	"toolBars": [
	    {
			"div": "toolBar",
			"tools" : [
			            {"type" : Descartes.Map.DRAG_PAN},
						{"type" : Descartes.Map.ZOOM_IN},
						{"type" : Descartes.Map.ZOOM_OUT},
						{"type" : Descartes.Map.INITIAL_EXTENT, 
						 args : [799205.2, 6215857.5, 1078390.1, 6452614.0]
						},
						{"type" : Descartes.Map.MAXIMAL_EXTENT},
						{"type" : Descartes.Map.CENTER_MAP},
						{"type" : Descartes.Map.COORDS_CENTER},
						{"type" : Descartes.Map.NAV_HISTORY},
						{"type" : Descartes.Map.DISTANCE_MEASURE},
						{"type" : Descartes.Map.AREA_MEASURE},
						{"type" : Descartes.Map.TOOLBAR_OPENER, 
					     args : { 
							displayClass: 'defaultOpenerButton',
							title: "Plus d'outils",
							tools: [
								{"type" : Descartes.Map.POINT_SELECTION},
								{"type" : Descartes.Map.POLYGON_SELECTION},
								{"type" : Descartes.Map.RECTANGLE_SELECTION},
								{"type" : Descartes.Map.CIRCLE_SELECTION},
								{"type" : Descartes.Map.POINT_RADIUS_SELECTION},
								{"type" : Descartes.Map.PNG_EXPORT},
								{"type" : Descartes.Map.PDF_EXPORT}
							],
							toolBarOptions: {
								panel:true,
								draggable: {
				                    enable: true,
				                    containment: 'parent'
				                }
				            }
						 }
						}
			],
			"options": {},
			"toolBarId": "toolBar1"
	    }
	],
/*	"editionToolBars": [
	    {
			"div": "editionToolBar",
			"tools" : [
	                    {type: Descartes.Map.EDITION_DRAW_CREATION},
	                    {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
	                    {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
	                    {type: Descartes.Map.TOOLBAR_OPENER, 
					     args : { 
							displayClass: 'defaultOpenerButton',
							title: "Plus d'outils d'édition'",
							tools: [
								{type: Descartes.Map.EDITION_RUBBER_DELETION},
	                            {type: Descartes.Map.EDITION_ATTRIBUTE},
	                            {type: Descartes.Map.EDITION_SAVE},
							],
							toolBarOptions: {
								panel:true,
								draggable: {
				                    enable: true,
				                    containment: 'parent'
				                }
				            }
						 }
						}
			],
			"options": {
				toolBarId: "toolBar1"
			}
	    }
	],*/
	"annotationToolBars": [
	    {
			"div": "annotationToolBar",
			"tools" : [
				  {
		               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
		               args: {
		                   geometryType: Descartes.Layer.POINT_GEOMETRY,
		                   snapping:true
		               }
		           },{
		               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
		               args: {
		                   geometryType: Descartes.Layer.LINE_GEOMETRY,
		                   snapping:true,
		                   autotracing: true
		               }
		           },{
		               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
		               args: {
		                   geometryType: Descartes.Layer.POLYGON_GEOMETRY,
		                   snapping:true,
		                   autotracing: true
		               }
		           },{
		               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
		               args: {
		                   geometryType: Descartes.Tool.Edition.ANNOTATION_RECTANGLE_GEOMETRY
		               }
		           }, {
		               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
		               args: {
		                   geometryType: Descartes.Tool.Edition.ANNOTATION_CIRCLE_GEOMETRY
		               }
		           }, {
		               type: Descartes.Map.EDITION_ARROW_ANNOTATION
		           }, {
		               type: Descartes.Map.EDITION_FREEHAND_ANNOTATION
		           },
		           	{type: Descartes.Map.EDITION_TEXT_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_GLOBAL_MODIFICATION_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_VERTICE_MODIFICATION_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_ADD_TEXT_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_ATTRIBUTE_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_STYLE_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_RUBBER_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_ERASE_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_EXPORT_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_IMPORT_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_SNAPPING_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_GEOLOCATION_SIMPLE_ANNOTATION
					           }, {
					               type: Descartes.Map.EDITION_GEOLOCATION_TRACKING_ANNOTATION
					           }
			],
			"options": {
				toolBarId: "toolBar1"
			}
	    }
	],
	"infos": [
            {
                "type": "MetricScale",
                "div": "MetricScale"
            }
        ],
	    "openlayersFeatures": {
            "interactions": [
                {
                    "type": Descartes.Map.OL_MOUSE_WHEEL_ZOOM
                },
                {
                    "type": Descartes.Map.OL_DRAG_PAN,
                    "args": {
                        condition:ol.events.condition.noModifierKeys/*,
                        kinetic: new ol.Kinetic(-0.01, 0.1, 200)*/
                    }
                }
            ]
        }
}
};