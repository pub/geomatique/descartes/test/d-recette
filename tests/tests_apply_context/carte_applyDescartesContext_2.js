proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
proj4.defs('urn:ogc:def:crs:EPSG::4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargementCarte() {
	 context.editionManager.configureOptions.save = function (json) {
 	    var urlServletBouchon = Descartes.getWebServiceRoot()+"edition";
		if (json.fluxWfst) {
			var elementsForSave = JSON.stringify(json.fluxWfst);
			
			var xhr = new XMLHttpRequest();
		    xhr.open('POST', urlServletBouchon);
		    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		    xhr.onload = function () {
		        if (xhr.status === 200) {
		        	var data = xhr.responseXML;
		       	  	if(!data || !data.documentElement) {
		       	  		data = xhr.responseText;
		       	  	}
		              
		       	  	try{
		       	  		data = JSON.parse(data);
			      	  	json.priv={status:data.status,message:data.message};  
		       	  	}catch (e){
		       	  		json.priv={status:500};
		       	  	}
		       	  
		            json.callback.call(json);
		        } else {
		        	json.priv={status:500,message:xhr.responseText};
		        	json.callback.call(json);
		        }
		    };
		    xhr.send(elementsForSave);
			
		}
	 };
	 Descartes.applyDescartesContext(context);
}
