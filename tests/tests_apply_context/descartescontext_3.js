var context = {
	"map" : {
		"type": "Continuous",
		"div": "map",
		"mapParams": {
			"projection": "EPSG:2154",
			"initExtent": [799205.2, 6215857.5, 1078390.1, 6452614.0],
			"maxExtent": [799205.2, 6215857.5, 1078390.1, 6452614.0],
			"minScale": 2150000,
			"maxScale": 100,
			"size": [600, 400]
		}
	},
	"mapContent": {
		"mapContentParams": {
			"editable": true
		},
		"mapContentManager":{
			"div": "layersTree",
			"contentTools": [
			     			{"type" : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL}
			    		],
			"options": {
				"toolBarDiv": "managerToolBar"
			}
		},
		"items" : [{
				"itemType" : "Layer",
				"title" : "Lieux",
				"type" : Descartes.Layer.TYPE_WMS,
				"options" : {
					"format" : "image/png",
					"legend" : null,
					"metadataURL" : "http://metadataURL.fr",
					"attribution" : "&#169;mon copyright",
					"visible" : true,
					"alwaysVisible" : false,
					"queryable" : false,
					"activeToQuery" : false,
					"sheetable" : false,
					"opacity" : 100,
					"opacityMax" : 100,
					"displayOrder" : 1,
					"addedByUser" : false,
					"id" : "coucheToponymes",
					"maxScale" : null,
					"minScale" : 190000
				},
				"definition" : [{
						"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
						"serverVersion" : null,
						"layerName" : "c_places_Etiquettes",
						"layerStyles" : null,
						"imageServerUrl" : null,
						"imageServerVersion" : null,
						"imageLayerName" : null,
						"imageLayerStyles" : null,
						"featureServerUrl" : null,
						"featureServerVersion" : null,
						"featureName" : null,
						"featureNameSpace" : null,
						"featureGeometryName" : "msGeometry",
						"featureLoaderMode" : null,
						"featureReverseAxisOrientation" : false,
						"featureInternalProjection" : null,
						"crossOrigin" : null,
						"internalProjection" : null,
						"displayProjection" : null
					}
				]
			}, {
				"itemType" : "Group",
				"title" : "Infrastructures",
				"options" : {
					"opened" : true,
					"addedByUser" : false,
					"visible" : null
				},
				"items" : [{
						"itemType" : "Layer",
						"title" : "Parkings",
						"type" : Descartes.Layer.TYPE_WMS,
						"options" : {
							"format" : "image/png",
							"legend" : ["https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
							"metadataURL" : null,
							"attribution" : "&#169;mon copyright",
							"visible" : false,
							"alwaysVisible" : false,
							"queryable" : true,
							"activeToQuery" : true,
							"sheetable" : true,
							"opacity" : 100,
							"opacityMax" : 100,
							"displayOrder" : 2,
							"addedByUser" : false,
							"id" : "coucheParkings",
							"maxScale" : null,
							"minScale" : 390000
						},
						"definition" : [{
								"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
								"serverVersion" : null,
								"layerName" : "c_parkings",
								"layerStyles" : null,
								"imageServerUrl" : null,
								"imageServerVersion" : null,
								"imageLayerName" : null,
								"imageLayerStyles" : null,
								"featureServerUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
								"featureServerVersion" : null,
								"featureName" : "c_parkings",
								"featureNameSpace" : null,
								"featureGeometryName" : "msGeometry",
								"featureLoaderMode" : null,
								"featureReverseAxisOrientation" : false,
								"featureInternalProjection" : null,
								"crossOrigin" : null,
								"internalProjection" : null,
								"displayProjection" : null
							}
						]
					}, {
						"itemType" : "Layer",
						"title" : "Stations essence",
						"type" : Descartes.Layer.TYPE_WMS,
						"options" : {
							"format" : "image/png",
							"legend" : ["https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
							"metadataURL" : null,
							"attribution" : "&#169;mon copyright",
							"visible" : true,
							"alwaysVisible" : false,
							"queryable" : true,
							"activeToQuery" : true,
							"sheetable" : true,
							"opacity" : 100,
							"opacityMax" : 100,
							"displayOrder" : 3,
							"addedByUser" : false,
							"id" : "coucheStations",
							"maxScale" : null,
							"minScale" : 390000
						},
						"definition" : [{
								"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
								"serverVersion" : null,
								"layerName" : "c_stations",
								"layerStyles" : null,
								"imageServerUrl" : null,
								"imageServerVersion" : null,
								"imageLayerName" : null,
								"imageLayerStyles" : null,
								"featureServerUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
								"featureServerVersion" : null,
								"featureName" : "c_stations",
								"featureNameSpace" : null,
								"featureGeometryName" : "msGeometry",
								"featureLoaderMode" : null,
								"featureReverseAxisOrientation" : false,
								"featureInternalProjection" : null,
								"crossOrigin" : null,
								"internalProjection" : null,
								"displayProjection" : null
							}
						]
					}, {
						"itemType" : "Layer",
						"title" : "Routes",
						"type" : Descartes.Layer.TYPE_WMS,
						"options" : {
							"format" : "image/png",
							"legend" : ["https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_roads"],
							"metadataURL" : null,
							"attribution" : "&#169;mon copyright",
							"visible" : true,
							"alwaysVisible" : false,
							"queryable" : false,
							"activeToQuery" : false,
							"sheetable" : false,
							"opacity" : 100,
							"opacityMax" : 100,
							"displayOrder" : 4,
							"addedByUser" : false,
							"id" : "coucheRoutes",
							"maxScale" : null,
							"minScale" : 390000
						},
						"definition" : [{
								"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
								"serverVersion" : null,
								"layerName" : "c_roads",
								"layerStyles" : null,
								"imageServerUrl" : null,
								"imageServerVersion" : null,
								"imageLayerName" : null,
								"imageLayerStyles" : null,
								"featureServerUrl" : null,
								"featureServerVersion" : null,
								"featureName" : null,
								"featureNameSpace" : null,
								"featureGeometryName" : "msGeometry",
								"featureLoaderMode" : null,
								"featureReverseAxisOrientation" : false,
								"featureInternalProjection" : null,
								"crossOrigin" : null,
								"internalProjection" : null,
								"displayProjection" : null
							}
						]
					}, {
						"itemType" : "Layer",
						"title" : "Chemins de fer",
						"type" : Descartes.Layer.TYPE_WMS,
						"options" : {
							"format" : "image/png",
							"legend" : ["https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_railways"],
							"metadataURL" : null,
							"attribution" : "&#169;mon copyright",
							"visible" : false,
							"alwaysVisible" : false,
							"queryable" : false,
							"activeToQuery" : false,
							"sheetable" : false,
							"opacity" : 100,
							"opacityMax" : 100,
							"displayOrder" : 5,
							"addedByUser" : false,
							"id" : "coucheFer",
							"maxScale" : null,
							"minScale" : null
						},
						"definition" : [{
								"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
								"serverVersion" : null,
								"layerName" : "c_railways",
								"layerStyles" : null,
								"imageServerUrl" : null,
								"imageServerVersion" : null,
								"imageLayerName" : null,
								"imageLayerStyles" : null,
								"featureServerUrl" : null,
								"featureServerVersion" : null,
								"featureName" : null,
								"featureNameSpace" : null,
								"featureGeometryName" : "msGeometry",
								"featureLoaderMode" : null,
								"featureReverseAxisOrientation" : false,
								"featureInternalProjection" : null,
								"crossOrigin" : null,
								"internalProjection" : null,
								"displayProjection" : null
							}
						]
					}
				]
			}, {
				"itemType" : "Layer",
				"title" : "Cours d'eau",
				"type" : Descartes.Layer.TYPE_WMS,
				"options" : {
					"format" : "image/png",
					"legend" : null,
					"metadataURL" : null,
					"attribution" : "&#169;mon copyright",
					"visible" : true,
					"alwaysVisible" : false,
					"queryable" : false,
					"activeToQuery" : false,
					"sheetable" : false,
					"opacity" : 100,
					"opacityMax" : 100,
					"displayOrder" : 6,
					"addedByUser" : false,
					"id" : "coucheEau",
					"maxScale" : null,
					"minScale" : null
				},
				"definition" : [{
						"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
						"serverVersion" : null,
						"layerName" : "c_waterways_Valeurs_type",
						"layerStyles" : null,
						"imageServerUrl" : null,
						"imageServerVersion" : null,
						"imageLayerName" : null,
						"imageLayerStyles" : null,
						"featureServerUrl" : null,
						"featureServerVersion" : null,
						"featureName" : null,
						"featureNameSpace" : null,
						"featureGeometryName" : "msGeometry",
						"featureLoaderMode" : null,
						"featureReverseAxisOrientation" : false,
						"featureInternalProjection" : null,
						"crossOrigin" : null,
						"internalProjection" : null,
						"displayProjection" : null
					}
				]
			}, {
				"itemType" : "Layer",
				"title" : "Constructions",
				"type" : Descartes.Layer.TYPE_WMS,
				"options" : {
					"format" : "image/png",
					"legend" : ["https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
					"metadataURL" : null,
					"attribution" : "&#169;mon copyright",
					"visible" : true,
					"alwaysVisible" : false,
					"queryable" : false,
					"activeToQuery" : false,
					"sheetable" : false,
					"opacity" : 100,
					"opacityMax" : 100,
					"displayOrder" : 7,
					"addedByUser" : false,
					"id" : "coucheBati",
					"maxScale" : null,
					"minScale" : 39000
				},
				"definition" : [{
						"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
						"serverVersion" : null,
						"layerName" : "c_buildings",
						"layerStyles" : null,
						"imageServerUrl" : null,
						"imageServerVersion" : null,
						"imageLayerName" : null,
						"imageLayerStyles" : null,
						"featureServerUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
						"featureServerVersion" : null,
						"featureName" : "c_buildings2",
						"featureNameSpace" : null,
						"featureGeometryName" : "msGeometry",
						"featureLoaderMode" : null,
						"featureReverseAxisOrientation" : false,
						"featureInternalProjection" : null,
						"crossOrigin" : null,
						"internalProjection" : null,
						"displayProjection" : null
					}
				]
			}, {
				"itemType" : "Layer",
				"title" : "Espaces naturels",
				"type" : Descartes.Layer.TYPE_WMS,
				"options" : {
					"format" : "image/png",
					"legend" : ["https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
					"metadataURL" : null,
					"attribution" : "&#169;mon copyright",
					"visible" : true,
					"alwaysVisible" : false,
					"queryable" : false,
					"activeToQuery" : false,
					"sheetable" : false,
					"opacity" : 100,
					"opacityMax" : 100,
					"displayOrder" : 8,
					"addedByUser" : false,
					"id" : "coucheNature",
					"maxScale" : null,
					"minScale" : null
				},
				"definition" : [{
						"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?",
						"serverVersion" : null,
						"layerName" : "c_natural_Valeurs_type",
						"layerStyles" : null,
						"imageServerUrl" : null,
						"imageServerVersion" : null,
						"imageLayerName" : null,
						"imageLayerStyles" : null,
						"featureServerUrl" : null,
						"featureServerVersion" : null,
						"featureName" : null,
						"featureNameSpace" : null,
						"featureGeometryName" : "msGeometry",
						"featureLoaderMode" : null,
						"featureReverseAxisOrientation" : false,
						"featureInternalProjection" : null,
						"crossOrigin" : null,
						"internalProjection" : null,
						"displayProjection" : null
					}
				]
			}
		]
	},
	"features": {
		"toolBars": [
		    {
				"div": "toolBar",
				"tools" : [
				            {"type" : Descartes.Map.DRAG_PAN},
							{"type" : Descartes.Map.ZOOM_IN},
							{"type" : Descartes.Map.ZOOM_OUT},
							{"type" : Descartes.Map.INITIAL_EXTENT, 
							 args : [799205.2, 6215857.5, 1078390.1, 6452614.0]
							},
							{"type" : Descartes.Map.MAXIMAL_EXTENT},
							{"type" : Descartes.Map.CENTER_MAP},
							{"type" : Descartes.Map.COORDS_CENTER},
							{"type" : Descartes.Map.NAV_HISTORY},
							{"type" : Descartes.Map.DISTANCE_MEASURE},
							{"type" : Descartes.Map.AREA_MEASURE},
							{"type" : Descartes.Map.POINT_SELECTION},
							{"type" : Descartes.Map.POLYGON_SELECTION},
							{"type" : Descartes.Map.RECTANGLE_SELECTION},
							{"type" : Descartes.Map.CIRCLE_SELECTION},
							{"type" : Descartes.Map.POINT_RADIUS_SELECTION},
							{"type" : Descartes.Map.PNG_EXPORT},
							{"type" : Descartes.Map.PDF_EXPORT}
				],
				"options": {}
		    }
		],
		"infos":[
		         {"type" : Descartes.Map.GRAPHIC_SCALE_INFO, div : null},
		         {"type" : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'},
		         {"type" : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'},
		         {"type" : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'},
		         {"type" : Descartes.Map.LEGEND_INFO, div : 'Legend'},
		         {"type" : Descartes.Map.ATTRIBUTION_INFO, div : null}
		],
		"actions": [{
				"type": Descartes.Map.SCALE_SELECTOR_ACTION,
	            "div": 'ScaleSelector',
	            "options": {
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type":  Descartes.Map.SCALE_CHOOSER_ACTION,
	            "div": 'ScaleChooser',
	            "options": {
	                size: 5,
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type": Descartes.Map.COORDS_INPUT_ACTION,
	            "div": 'CoordinatesInput',
	            "options": {
	                size: 20,
	                label: true,
	                optionsPanel: {
	                    collapsible: false,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type": Descartes.Map.SIZE_SELECTOR_ACTION,
	            "div": 'SizeSelector',
	            "options": {
	                sizeList: [
	                           [450, 300],
	                           [600, 400],
	                           [750, 500],
	                           [900, 600]
	                       ],
	                defaultSize: 1,
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type": Descartes.Map.PRINTER_SETUP_ACTION,
	            "div": 'Pdf',
	            "options": {
	                size: 5,
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }
	     ],
    	"directionalPanPanel":{
	    	"options":{}
	    },
	    "miniMap":{
	    	"resourceUrl": "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type",
	    	"options": {}
	    },
	    "toolTip":{
	    	"div": "ToolTip",
	    	"toolTipLayers": [
	    	    {
	    	        "layerId": "coucheParkings", 
	    	        "fields": ['name']
	    	    },
	    	    {
	    	    	"layerId": "coucheStations", 
	    	    	"fields": ['name']
	    	    }
	    	],
	    	"options": {}
	    },
	    "bookmarksManager":{
	    	"div": "Bookmarks",
	    	"mapName":"exemple-descartes",
	    	"options":{}
	    },
	    "defaultGazetteer":{
	    	"div": "DefaultGazetteer",
	    	"initValue":"93",
	    	"startlevel":Descartes.Action.DefaultGazetteer.DEPARTEMENT,
	    	"options":{}
	    	
	    },
	    "gazetteer":{
	    	"div": "Gazetteer",
	    	"initValue": "93",
	    	"levels": [{
	    		"message": "Choisissez un département",
	    		"error": "Aucun département",
	    		"name": "dept",
	    		"options":{}
	    	}],
	    	"options": {}
	    },
	    "openlayersFeatures":{
	         "controls":[ 
	               {type: Descartes.Map.OL_ZOOM}
	         ],
	         "interactions":[ 
	               {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	     	 ]
		}
	}
};