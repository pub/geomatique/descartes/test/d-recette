proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargementCarte() {
	 var carte = Descartes.applyDescartesContext(context);
	 
    // Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes',{
	  resultUiParams: {
        withCsvExport: true,
        withReturn: true
    }});
	
	var coucheStations = carte.mapContent.getLayerById("coucheBati");
	
	var requeteStation = new Descartes.Request(coucheStations, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);
	
	requeteStation.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteStation);
}
