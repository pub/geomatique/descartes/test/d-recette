var context = {
	"map" : {
		"type": "Continuous",
		"div": "map",
		"mapParams": {
			"projection": "EPSG:2154",
			"initExtent": [799205.2, 6215857.5, 1078390.1, 6452614.0],
			"maxExtent": [799205.2, 6215857.5, 1078390.1, 6452614.0],
			"minScale": 2150000,
			"maxScale": 100,
			"size": [600, 400]
		}
	},
	"mapContent": {
		"mapContentParams": {
			"editable": true
		},
		"mapContentManager":{
			"div": "layersTree",
			"contentTools": [
			     			{"type" : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			    			{"type" : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL}
			    		],
			"options": {
				"toolBarDiv": "managerToolBar"
			}
		},
		"items" : [{
			"itemType" : "Layer",
			"title" : "Ma couche WFS de polygones",
			"type" : Descartes.Layer.EditionLayer.TYPE_WFS,
			"options" : {
				"format" : "image/png",
				"legend" : null,
				"metadataURL" : null,
				"attribution" : "&#169;mon copyright",
				"visible" : true,
				"alwaysVisible" : false,
				"queryable" : false,
				"activeToQuery" : false,
				"sheetable" : false,
				"opacity" : 100,
				"opacityMax" : 100,
				"displayOrder" : 1,
				"addedByUser" : false,
				"id" : "coucheBase",
				"maxScale" : 10000,
				"minScale" : 4000000, 
		        "maxEditionScale": 10000,
		        "minEditionScale": 4000000,
				"geometryType": Descartes.Layer.POLYGON_GEOMETRY
			},
			"definition" : [{
					"serverUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/geoserver/descartes/ows?",
					"serverVersion" : "1.1.0",
					"layerName" : "polygones2154",
					"layerStyles" : null,
					"imageServerUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/geoserver/descartes/ows?",
					"imageServerVersion" : null,
					"imageLayerName" : null,
					"imageLayerStyles" : null,
					"featureServerUrl" : "https://preprod.descartes.din.developpement-durable.gouv.fr/geoserver/descartes/ows?",
					"featureServerVersion" : null,
					"featureName" : null,
					"featureNameSpace" : "descartes",
					"featurePrefix" : "descartes",
					"featureGeometryName" : "polygones_geom",
					"featureLoaderMode" : null,
					"featureReverseAxisOrientation" : false,
					"featureInternalProjection" : null,
					"crossOrigin" : null,
					"internalProjection" : "EPSG:2154"
				}
			]
		},
		{
				"itemType" : "Layer",
				"title" : "Fond de carte",
				"type" : Descartes.Layer.TYPE_WMS,
				"options" : {
					"format" : "image/png",
					"legend" : null,
					"metadataURL" : null,
					"attribution" : "&#169;mon copyright",
					"visible" : true,
					"alwaysVisible" : false,
					"queryable" : false,
					"activeToQuery" : false,
					"sheetable" : false,
					"opacity" : 100,
					"opacityMax" : 100,
					"displayOrder" : 1,
					"addedByUser" : false,
					"id" : "coucheBase",
					"maxScale" : 100,
					"minScale" : 10000001
				},
				"definition" : [{
						"serverUrl" : "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
						"serverVersion" : null,
						"layerName" : "fond_vecteur",
						"layerStyles" : null,
						"imageServerUrl" : null,
						"imageServerVersion" : null,
						"imageLayerName" : null,
						"imageLayerStyles" : null,
						"featureServerUrl" : null,
						"featureServerVersion" : null,
						"featureName" : null,
						"featureNameSpace" : null,
						"featureGeometryName" : "msGeometry",
						"featureLoaderMode" : null,
						"featureReverseAxisOrientation" : false,
						"featureInternalProjection" : null,
						"crossOrigin" : null,
						"internalProjection" : null,
						"displayProjection" : null
					}
				]
			}
		]
	},
	"editionManager":{
		"configureOptions": {
			 autoSave:false, 
			 globalEditionMode: true,
		     save: null // à définir
		}
	},
	"features": {
		"toolBars": [
		    {
				"div": "toolBar",
				"tools" : [
				            {"type" : Descartes.Map.DRAG_PAN},
							{"type" : Descartes.Map.ZOOM_IN},
							{"type" : Descartes.Map.ZOOM_OUT},
							{"type" : Descartes.Map.INITIAL_EXTENT, 
							 args : [799205.2, 6215857.5, 1078390.1, 6452614.0]
							},
							{"type" : Descartes.Map.MAXIMAL_EXTENT},
							{type : Descartes.Map.TOOLBAR_OPENER, 
							     args : { 
									displayClass: 'defaultOpenerButton',
									title: "Plus d'outils1",
									tools: [
										{"type" : Descartes.Map.POINT_SELECTION},
										{"type" : Descartes.Map.POLYGON_SELECTION},
										{"type" : Descartes.Map.RECTANGLE_SELECTION},
										{"type" : Descartes.Map.CIRCLE_SELECTION},
										{"type" : Descartes.Map.POINT_RADIUS_SELECTION},
										{"type" : Descartes.Map.PNG_EXPORT},
										{"type" : Descartes.Map.PDF_EXPORT}   
									]
								}
							},
							{type : Descartes.Map.TOOLBAR_OPENER, 
							     args : { 
									displayClass: 'defaultOpenerButton',
									title: "Plus d'outils2",
									tools: [
										{"type" : Descartes.Map.CENTER_MAP},
										{"type" : Descartes.Map.COORDS_CENTER},
										{"type" : Descartes.Map.NAV_HISTORY},
										{"type" : Descartes.Map.DISTANCE_MEASURE},
										{"type" : Descartes.Map.AREA_MEASURE}
									],
									toolBarOptions: {
						                vertical:true
						            }
								}
							}
							
				],
				"options": {},
				"toolBarId": "toolBar1"
		    }
		],
		"editionToolBars": [
 		    {
 				"div": null,
 				"tools" : [
 				            {type: Descartes.Map.EDITION_DRAW_CREATION},
 				            {type: Descartes.Map.EDITION_GLOBAL_MODIFICATION},
 				            {type: Descartes.Map.EDITION_VERTICE_MODIFICATION},
 				            {type: Descartes.Map.EDITION_RUBBER_DELETION},
 				            {type: Descartes.Map.EDITION_ATTRIBUTE},
 				            {type: Descartes.Map.EDITION_SAVE}
 				],
 				"options": {toolBarId: "toolBar1"}
 		    }
 		],
		"infos":[
		         {"type" : Descartes.Map.GRAPHIC_SCALE_INFO, div : null},
		         {"type" : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'},
		         {"type" : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'},
		         {"type" : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'},
		         {"type" : Descartes.Map.LEGEND_INFO, div : 'Legend'},
		         {"type" : Descartes.Map.ATTRIBUTION_INFO, div : null}
		],
		"actions": [{
				"type": Descartes.Map.SCALE_SELECTOR_ACTION,
	            "div": 'ScaleSelector',
	            "options": {
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type":  Descartes.Map.SCALE_CHOOSER_ACTION,
	            "div": 'ScaleChooser',
	            "options": {
	                size: 5,
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type": Descartes.Map.COORDS_INPUT_ACTION,
	            "div": 'CoordinatesInput',
	            "options": {
	                size: 20,
	                label: true,
	                optionsPanel: {
	                    collapsible: false,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type": Descartes.Map.SIZE_SELECTOR_ACTION,
	            "div": 'SizeSelector',
	            "options": {
	                sizeList: [
	                           [450, 300],
	                           [600, 400],
	                           [750, 500],
	                           [900, 600]
	                       ],
	                defaultSize: 1,
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }, {
	            "type": Descartes.Map.PRINTER_SETUP_ACTION,
	            "div": 'Pdf',
	            "options": {
	                size: 5,
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: false
	                }
	            }
	        }
	     ],
    	"directionalPanPanel":{
	    	"options":{}
	    },
	    "miniMap":{
	    	"resourceUrl": "https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type",
	    	"options": {}
	    },
	    "bookmarksManager":{
	    	"div": "Bookmarks",
	    	"mapName":"exemple-descartes",
	    	"options":{}
	    },
	    "defaultGazetteer":{
	    	"div": "DefaultGazetteer",
	    	"initValue":"93",
	    	"startlevel":Descartes.Action.DefaultGazetteer.DEPARTEMENT,
	    	"options":{}
	    	
	    },
	    "gazetteer":{
	    	"div": "Gazetteer",
	    	"initValue": "93",
	    	"levels": [{
	    		"message": "Choisissez un département",
	    		"error": "Aucun département",
	    		"name": "dept",
	    		"options":{}
	    	}],
	    	"options": {}
	    },
	    "openlayersFeatures":{
	         "controls":[ 
	               {type: Descartes.Map.OL_ZOOM}
	         ],
	         "interactions":[ 
	               {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	     	 ]
		}
	}
};