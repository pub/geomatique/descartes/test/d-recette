var coucheBdxBDorthoWmts, coucheBase;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeCouchesGroupes() {

    var matrixIdsWMTS = [
        'GoogleMapsCompatible:0',
        'GoogleMapsCompatible:1',
        'GoogleMapsCompatible:2',
        'GoogleMapsCompatible:3',
        'GoogleMapsCompatible:4',
        'GoogleMapsCompatible:5',
        'GoogleMapsCompatible:6',
        'GoogleMapsCompatible:7',
        'GoogleMapsCompatible:8',
        'GoogleMapsCompatible:9',
        'GoogleMapsCompatible:10',
        'GoogleMapsCompatible:11'
    ];
   
   var originsWMTS = [
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508]
  ];
            
   var projectionWMTS = new Descartes.Projection('EPSG:3857');
   var extentWMTS = projectionWMTS.getExtent();

   var tileSize = 256;
   var sizeWMTS = ol.extent.getWidth(extentWMTS) / tileSize;

   var resolutionsWMTS = [];

   for (var i = 0; i < 19; i++) {
        resolutionsWMTS[i] = sizeWMTS / Math.pow(2, i);
        //matrixIds[i] = i;
   }
   
   coucheBdxBDorthoWmts = new Descartes.Layer.WMTS(
   		 'couche georef WMTS', 
   		 [
   		  	{
   		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmts?',
   		  		layerName: 'georefmonde:GoogleMapsCompatible'
   		  	}
   		 ],
   		 {
   			 extent: extentWMTS,
   			 matrixSet: "GoogleMapsCompatible",
   			 projection: projectionWMTS,
   			 matrixIds: matrixIdsWMTS,
   			 origins: originsWMTS,
   			 resolutions: resolutionsWMTS,
   			 tileSize: [256, 256],
   			 format: 'image/jpeg',
   			 visible: true,
   			 opacity: 100
   		 }
   );

   defCoucheBdxBDorthoWmts = {
   		 title: 'couche georef WMTS',
   		 type: 3,
   		 definition: [
   		  	{
   		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmts?',
   		  		layerName: 'georefmonde:GoogleMapsCompatible'
   		  	}
   		 ],
   		 options: {
   			 extent: extentWMTS,
   			 matrixSet: "GoogleMapsCompatible",
   			 projection: projectionWMTS,
   			 matrixIds: matrixIdsWMTS,
   			 origins: originsWMTS,
   			 resolutions: resolutionsWMTS,
   			 tileSize: [256, 256],
   			 format: 'image/jpeg',
   			 visible: true,
   			 opacity: 100
   		 }
   };
   	
	coucheBase = new Descartes.Layer.WMS (
		"couche georef WMS",
		[
			{
				serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur"
			}
		],
		{
//			maxScale: 100,
//			minScale: 10000001,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 50,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			format: "image/png"
		}
	);
	
   var matrixIdsWMTSIGN = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"];
   
   var originsWMTSIGN = [
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508]
  ];
            
   var projectionWMTSIGN = new Descartes.Projection('EPSG:3857');
   var extentWMTSIGN = projectionWMTSIGN.getExtent();

   var resolutionsWMTSIGN = [
       156543.03392804103,
       78271.5169640205,
       39135.75848201024,
       19567.879241005125,
       9783.939620502562,
       4891.969810251281,
       2445.9849051256406,
       1222.9924525628203,
       611.4962262814101,
       305.74811314070485,
       152.87405657035254,
       76.43702828517625,
       38.218514142588134,
       19.109257071294063,
       9.554628535647034,
       4.777314267823517,
       2.3886571339117584,
       1.1943285669558792,
       0.5971642834779396,
       0.29858214173896974,
       0.14929107086948493,
       0.07464553543474241
   ] ;


	
    coucheIGN= new Descartes.Layer.WMTS(
            "couche de fond",
            [
                {
                    serverUrl: "https://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wmts?",
                    layerName: "ORTHOIMAGERY.ORTHOPHOTOS",
                    layerStyles: "normal"
                }
            ],
      		 {
      			 extent: extentWMTSIGN,
      			 matrixSet: "PM",
      			 projection: projectionWMTSIGN,
      			 matrixIds: matrixIdsWMTSIGN,
      			 origins: originsWMTSIGN,
      			 resolutions: resolutionsWMTSIGN,
      			 tileSize: [256, 256],
      			 format: 'image/jpeg',
      			 queryable: false,
      			 visible: true,
      			 opacity: 100
      		 }
        );
    defCoucheIGN= {
            title: "couche de fond",
            type: 3,
            definition: [
                {
                    serverUrl: "https://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wmts?",
                    layerName: "ORTHOIMAGERY.ORTHOPHOTOS",
                    layerStyles: "normal"
                }
            ],
      		 options: {
      			 extent: extentWMTSIGN,
      			 matrixSet: "PM",
      			 projection: projectionWMTSIGN,
      			 matrixIds: matrixIdsWMTSIGN,
      			 origins: originsWMTSIGN,
      			 resolutions: resolutionsWMTSIGN,
      			 tileSize: [256, 256],
      			 format: 'image/jpeg',
      			 queryable: false,
      			 visible: true,
      			 opacity: 100
      		 }
        };	
	groupeFonds = new Descartes.Group(
		"Fonds cartographiques",
		{
			opened: true
		}
	);
	
	defGroupeFonds = {
    title: 'Fonds cartographiques',
    options: {
        opened: true
    }
};
}
