proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
var coucheAggregatWFS;
function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();
	// Ajout couches wfs
	contenuCarte.addItem(coucheAggregatWFS);
	coucheAggregatWFS.OL_layers[0].getSource().on('featureloadend', ma_fonction_de_stylage1, this);
	coucheAggregatWFS.OL_layers[1].getSource().on('featureloadend', ma_fonction_de_stylage2, this);

	contenuCarte.addItem(coucheAggregatWFSStyleFunction);
	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
function ma_fonction_de_stylage1(){

	var features=coucheAggregatWFS.OL_layers[0].getSource().getFeatures();

	var style1= {
		    'Point': {
		        pointRadius: 4,
		        graphicName: 'circle',
		        fillColor: 'red',
		        fillOpacity: 1,
		        strokeWidth: 1,
		        strokeOpacity: 1,
		        strokeColor: 'red'
		    }
	};
	
	for (var i=0, len=features.length ; i<len ; i++) {
		 if(features[i].getProperties().name === "Station Total"){
			 var olStyles = Descartes.Symbolizers.getOlStyle(style1);
			 var olStyle = olStyles["Point"];
			 coucheAggregatWFS.OL_layers[0].getSource().getFeatures()[i].setStyle(olStyle);
		 }
	}
}
function ma_fonction_de_stylage2(){

	var features=coucheAggregatWFS.OL_layers[1].getSource().getFeatures();

	var style1= {
		    'Point': {
		        pointRadius: 4,
		        graphicName: 'circle',
		        fillColor: 'blue',
		        fillOpacity: 1,
		        strokeWidth: 1,
		        strokeOpacity: 1,
		        strokeColor: 'blue'
		    }
	};
	
	for (var i=0, len=features.length ; i<len ; i++) {
		var olStyles = Descartes.Symbolizers.getOlStyle(style1);
		var olStyle = olStyles["Point"];
		coucheAggregatWFS.OL_layers[1].getSource().getFeatures()[i].setStyle(olStyle);
	}
}