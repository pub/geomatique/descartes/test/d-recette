proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
    var IGNScanExpressClassique = new Descartes.Layer.WMS(
            "ScanExpress - GEOGRAPHICALGRIDSYSTEMS.PLANIGN",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/r/wms?",
                    layerName: "GEOGRAPHICALGRIDSYSTEMS.PLANIGN",

                }
            ],
            {
    			opacity: 50,
    			opacityMax: 100,
            }
        );


      var IGNOrthoPhotos = new Descartes.Layer.WMS(
            "Photographies aériennes - ORTHOIMAGERY.ORTHOPHOTOS.BDORTHO",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/r/wms?",
                    layerName: "ORTHOIMAGERY.ORTHOPHOTOS.BDORTHO"
                }
            ]
        );

      var  IGNCadastre = new Descartes.Layer.WMS(
            "Cadastre - CADASTRALPARCELS.PARCELS",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/r/wms?",
                    layerName: "CADASTRALPARCELS.PARCELS"
                }
            ]
        );    

	
	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(IGNScanExpressClassique);
    contenuCarte.addItem(IGNCadastre);
    contenuCarte.addItem(IGNOrthoPhotos);
    
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
