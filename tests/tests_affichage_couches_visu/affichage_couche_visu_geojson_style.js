proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

function chargementCarte() {
	
    Descartes.Symbolizers.Descartes_Symbolizers_GEOJSON = {
        'Point': {
            pointRadius: 4, //6
            graphicName: 'circle',
            fillColor: 'red',
            fillOpacity: 1,
            strokeWidth: 1,
            strokeOpacity: 1,
            strokeColor: 'red',
            pointerEvents: 'visiblePainted',
            cursor: 'pointer'
        },
        'Line': {
            strokeWidth: 3,
            strokeOpacity: 1,
            strokeColor: 'red',
            strokeDashstyle: 'solide',
            pointerEvents: 'visiblePainted',
            cursor: 'pointer'
        },
        'MultiPoint': {
            pointRadius: 4, //6
            graphicName: 'circle',
            fillColor: 'red',
            fillOpacity: 1,
            strokeWidth: 1,
            strokeOpacity: 1,
            strokeColor: 'red',
            pointerEvents: 'visiblePainted',
            cursor: 'pointer'
        },
        'MultiLine': {
            strokeWidth: 3,
            strokeOpacity: 1,
            strokeColor: 'red',
            strokeDashstyle: 'solide',
            pointerEvents: 'visiblePainted',
            cursor: 'pointer'
        },
        'Polygon': {
            strokeWidth: 2, //1
            strokeOpacity: 1, //1
            strokeColor: 'red',
            fillColor: 'red',
            fillOpacity: 0.3,
            pointerEvents: 'visiblePainted',
            cursor: 'pointer'
        },
        'MultiPolygon': {
            strokeWidth: 2, //1
            strokeOpacity: 1, //1
            strokeColor: 'red',
            fillColor: 'red',
            fillOpacity: 0.3,
            pointerEvents: 'visiblePainted',
            cursor: 'pointer'
        }
    };
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();
	// Ajout couches geojosn
	contenuCarte.addItem(couchePaysGeoJson);
	contenuCarte.addItem(couchePaysGeoJson2);
	contenuCarte.addItem(couchePaysGeoJsonStyle);
	
    var projection = 'EPSG:4326';
    var bounds = [-5.8, 42.3, 8.13, 52.1];
    
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
