
function chargementCarte() {
	
    var IGNBDCARTOReg = new Descartes.Layer.WFS(
            "BDCARTO_BDD_WLD_WGS84G:region",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wfs?",
                    layerName: "BDCARTO_BDD_WLD_WGS84G:region",
                    useBboxSrsProjection: true
                }
            ]
        );

    var IGNBDTOPOGare = new Descartes.Layer.WFS(
            "BDTOPO_BDD_WLD_WGS84G:gare",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wfs?",
                    layerName: "BDTOPO_BDD_WLD_WGS84G:gare",
                    useBboxSrsProjection: true

                }
            ]
        );
    

	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(IGNBDTOPOGare);
    contenuCarte.addItem(IGNBDCARTOReg);
    
	var bounds = [-800086,5055726,1150172,6772047];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

	var items = carte.mapContent.serialize();
    //console.log(items);
	//console.log(JSON.stringify(items));
	//console.log(JSON.parse(items));   
	carte.mapContent.removeItemByIndex("2",false);
	carte.mapContent.removeItemByIndex("1",false);
	carte.mapContent.removeItemByIndex("0",false);
	alert(JSON.stringify(items));
	carte.mapContentManager.populate(items);
	
}
