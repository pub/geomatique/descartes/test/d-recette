
function chargementCarte() {

    var matrixIdsWMTS = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"];
   
   var originsWMTS = [
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508]
  ];
            
   var projectionWMTS = new Descartes.Projection('EPSG:3857');
   var extentWMTS = projectionWMTS.getExtent();

   var resolutionsWMTS = [
       156543.03392804103,
       78271.5169640205,
       39135.75848201024,
       19567.879241005125,
       9783.939620502562,
       4891.969810251281,
       2445.9849051256406,
       1222.9924525628203,
       611.4962262814101,
       305.74811314070485,
       152.87405657035254,
       76.43702828517625,
       38.218514142588134,
       19.109257071294063,
       9.554628535647034,
       4.777314267823517,
       2.3886571339117584,
       1.1943285669558792,
       0.5971642834779396,
       0.29858214173896974,
       0.14929107086948493,
       0.07464553543474241
   ] ;


	
    var IGN= new Descartes.Layer.WMTS(
            "BDCARTO_BDD_WLD_WGS84G:region",
            [
                {
                    serverUrl: "https://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wmts?",
                    layerName: "ORTHOIMAGERY.ORTHOPHOTOS",
                    layerStyles: "normal"
                }
            ],
      		 {
      			 extent: extentWMTS,
      			 matrixSet: "PM",
      			 projection: projectionWMTS,
      			 matrixIds: matrixIdsWMTS,
      			 origins: originsWMTS,
      			 resolutions: resolutionsWMTS,
      			 tileSize: [256, 256],
      			 format: 'image/jpeg',
      			 visible: true,
      			 opacity: 100
      		 }
        );
	
	var contenuCarte = new Descartes.MapContent();
	
    contenuCarte.addItem(IGN);
	
	var bounds = [-20037508, -20037508, 20037508, 20037508];
	var initExtent = [-880269, 5201305, 1286588, 6611497];
    var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initExtent,
					maxExtent: bounds,
					//maxScale: 100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
