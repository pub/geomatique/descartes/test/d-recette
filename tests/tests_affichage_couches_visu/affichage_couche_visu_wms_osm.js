proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	var coucheOSM = new Descartes.Layer.WMS(
			"couche OSM", 
			[
				{
					serverUrl: "https://tile.geobretagne.fr/osm/service/wms?",
					layerName: "osm:google"
				}
				],
			{
				maxScale: null,
				//minScale: 390000,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:true,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				attribution: "&#169;mon copyright"
			}
		);
	
	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(coucheOSM);
    
	//var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
