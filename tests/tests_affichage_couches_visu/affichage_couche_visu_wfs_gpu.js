
function chargementCarte() {
	
    var GPUprescriptionpct = new Descartes.Layer.WFS(
            "GPU - Prescriptions ponctuelles",
            [
                {
                    serverUrl: "http://wxs-gpu.mongeoportail.ign.fr/externe/39wtxmgtn23okfbbs1al2lz3/wfs",
                    layerName: "wfs_du:prescription_pct",
                    internalProjection: "urn:ogc:def:crs:EPSG::4326",
                    featureReverseAxisOrientation: true
                }
            ],
            {
            	queryable:false
            }
        );
    
    var GPUscot = new Descartes.Layer.WFS(
            "GPU - Schéma de cohérence territorial",
            [
                {
                    serverUrl: "http://wxs-gpu.mongeoportail.ign.fr/externe/39wtxmgtn23okfbbs1al2lz3/wfs",
                    layerName: "wfs_scot:scot",
                    internalProjection: "urn:ogc:def:crs:EPSG::4326",
                    featureReverseAxisOrientation: true
                }
            ],
            {
            	queryable:false
            }
        );
	
	var couche4326 = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://vmap0.tiles.osgeo.org/wms/vmap0",
					layerName: "basic",
					featureServerUrl: null,
					featureName: null
				}
			],
			options: {
				//maxScale: 100, // ex  100
				//minScale: 50000,	// ex 500000
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				//displayOrder:2
			}
		};
    
	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(GPUprescriptionpct);
    contenuCarte.addItem(GPUscot);
    contenuCarte.addItem(new Descartes.Layer.WMS(couche4326.title, couche4326.definition, couche4326.options));
    
	var bounds = [-6.22, 39.46, 9.14, 53.4];
	var initBounds = [4.02, 42.60, 7.91, 45.37];
	var projection = 'EPSG:4326';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initBounds,
					maxExtent: bounds,
					minScale:15000000,
					size: [600, 400],
					displayExtendedOLExtent: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initBounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});

	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

}
