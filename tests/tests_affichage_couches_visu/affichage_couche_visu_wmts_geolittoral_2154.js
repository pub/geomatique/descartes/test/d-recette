proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();

	var coucheBase = {
		title : "Couche WMS Géoref",
		type: 0,
		definition: [
			{
				serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur"
			}
		],
		options: {
			maxScale: 100,
			minScale: 10000001,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [],
			metadataURL: null,
			format: "image/png"
		}
	};

 var matrixIdsWMTSGeoLittoral = [
         'EPSG2154_256px:0',
         'EPSG2154_256px:1',
         'EPSG2154_256px:2',
         'EPSG2154_256px:3',
         'EPSG2154_256px:4',
         'EPSG2154_256px:5',
         'EPSG2154_256px:6',
         'EPSG2154_256px:7',
         'EPSG2154_256px:8',
         'EPSG2154_256px:9',
         'EPSG2154_256px:10',
         'EPSG2154_256px:11'
     ];
    
    var originsWMTSGeoLittoral = [
       [70000, 7207600.0],
       [70000, 7310000.0],
       [70000, 7182000.0],
       [70000, 7182000.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130800.0],
       [70000, 7130160.0],
       [70000, 7130032.0],
       [70000, 7130032.0]
   ];
             
    var projectionWMTSGeoLittoral = new Descartes.Projection('EPSG:2154');
    //var extentWMTSGeoLittoral = projectionWMTSGeoLittoral.getExtent();
    var extentWMTSGeoLittoral = [70000, 6030000, 1270000, 7130000];
    //var extentWMTSGeoLittoral = [-101991.9, 6023917.0, 1528303.1, 7110780.4];

  var resolutionsWMTSGeoLittoral = [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5];

   var coucheGeoRefWmts1 = new Descartes.Layer.WMTS(
    		 'Couche WMTS Géoref', 
    		 [
    		  	{
    		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmts?',
    		  		layerName: 'georef:plan2154'
    		  	}
    		 ],
    		 {
    			 extent: extentWMTSGeoLittoral,
    			 matrixSet: "EPSG2154_256px",
    			 projection: projectionWMTSGeoLittoral,
    			 matrixIds: matrixIdsWMTSGeoLittoral,
    			 origins: originsWMTSGeoLittoral,
    			 resolutions: resolutionsWMTSGeoLittoral,
    			 tileSize: [256, 256],
    			 format: 'image/png',
    			 opacity: 100,
			 queryable: false
    		 }
    );

   var coucheGeoLittoralWmts1 = new Descartes.Layer.WMTS(
    		 'ortholittorale:v2en2154', 
    		 [
    		  	{
    		  		serverUrl: 'http://geolittoral.din.developpement-durable.gouv.fr/cache/service/wmts',
    		  		layerName: 'ortholittorale:v2en2154'
    		  	}
    		 ],
    		 {
    			 extent: extentWMTSGeoLittoral,
    			 matrixSet: "EPSG2154_256px",
    			 projection: projectionWMTSGeoLittoral,
    			 matrixIds: matrixIdsWMTSGeoLittoral,
    			 origins: originsWMTSGeoLittoral,
    			 resolutions: resolutionsWMTSGeoLittoral,
    			 tileSize: [256, 256],
    			 format: 'image/jpeg',
    			 opacity: 50,
			 queryable: false
    		 }
    );

   var coucheGeoLittoralWmts2 = new Descartes.Layer.WMTS(
    		 'ortholittorale:v2IRCen2154', 
    		 [
    		  	{
    		  		serverUrl: 'http://geolittoral.din.developpement-durable.gouv.fr/cache/service/wmts',
    		  		layerName: 'ortholittorale:v2IRCen2154'
    		  	}
    		 ],
    		 {
    			 extent: extentWMTSGeoLittoral,
    			 matrixSet: "EPSG2154_256px",
    			 projection: projectionWMTSGeoLittoral,
    			 matrixIds: matrixIdsWMTSGeoLittoral,
    			 origins: originsWMTSGeoLittoral,
    			 resolutions: resolutionsWMTSGeoLittoral,
    			 tileSize: [256, 256],
    			 format: 'image/jpeg',
    			 opacity: 50,
			 queryable: false
    		 }
    );

   var coucheGeoLittoralWmts3 = new Descartes.Layer.WMTS(
    		 'ortholittorale:v1en2154', 
    		 [
    		  	{
    		  		serverUrl: 'http://geolittoral.din.developpement-durable.gouv.fr/cache/service/wmts',
    		  		layerName: 'ortholittorale:v1en2154'
    		  	}
    		 ],
    		 {
    			 extent: extentWMTSGeoLittoral,
    			 matrixSet: "EPSG2154_256px",
    			 projection: projectionWMTSGeoLittoral,
    			 matrixIds: matrixIdsWMTSGeoLittoral,
    			 origins: originsWMTSGeoLittoral,
    			 resolutions: resolutionsWMTSGeoLittoral,
    			 tileSize: [256, 256],
    			 format: 'image/jpeg',
    			 opacity: 50,
			 queryable: false
    		 }
    );
	
	var contenuCarte = new Descartes.MapContent({editable: true});

	contenuCarte.addItem(coucheGeoLittoralWmts1);
	contenuCarte.addItem(coucheGeoLittoralWmts2);
	contenuCarte.addItem(coucheGeoLittoralWmts3);

	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(coucheGeoRefWmts1,gpFonds);
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
	//var bounds = [70000, 6030000, 1270000, 7130000];

        var projection = 'EPSG:2154';
       
        // Construction de la carte
	var carte = new Descartes.Map.DiscreteScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					resolutions: [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5],
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});

        carte.addOpenLayersControls([
	  {type: Descartes.Map.OL_SCALE_LINE}
        ]);

   	carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	]);  

}

