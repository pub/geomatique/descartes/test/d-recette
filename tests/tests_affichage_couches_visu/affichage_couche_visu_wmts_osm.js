proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {

	
	 var matrixIdsWMTS = [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12',
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19'
        
    ];
	                            
   var originsWMTS = [
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244],
      [-20037508.342789244, 20037508.342789244]
   ];
	                                     
   var projectionWMTS = new Descartes.Projection('EPSG:3857');

   var extentWMTS = [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244];
  
   var resolutionsWMTS = [156543.03392804097, 78271.51696402048, 39135.75848201024, 19567.87924100512, 9783.93962050256, 4891.96981025128, 2445.98490512564, 1222.99245256282, 611.49622628141, 305.748113140705, 152.8740565703525, 76.43702828517625, 38.21851414258813, 19.109257071294063, 9.554628535647032, 4.777314267823516, 2.388657133911758, 1.194328566955879, 0.5971642834779395, 0.29858214173896974];

  var OSM = new Descartes.Layer.WMTS(
   		 'Couche OSM', 
   		 [
   		  	{
   		  		serverUrl: 'http://tile.geobretagne.fr/osm/service/wmts?',
   		  		layerName: 'osm:google'
   		  	}
   		 ],
   		 {
   			 extent: extentWMTS,
   			 matrixSet: "PM",
   			 projection: projectionWMTS,
   			 matrixIds: matrixIdsWMTS,
   			 origins: originsWMTS,
   			 resolutions: resolutionsWMTS,
   			 tileSize: [256, 256],
   			 format: 'image/png',
   			 opacity: 100,
			 queryable: false
   		 }
   );
	
	var contenuCarte = new Descartes.MapContent();
	
    contenuCarte.addItem(OSM);
	
	//var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];	
  //var bounds = [569000,6444000,586000,6455000];
  //	var initExtent = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
	var bounds = [-20037508, -20037508, 20037508, 20037508];
	var initExtent = [-880269, 5201305, 1286588, 6611497];   
      var projection = 'EPSG:3857';

    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initExtent,
					maxExtent: bounds,
					//maxScale: 100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
