proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();

	var coucheBase = {
		title : "Couche WMS Géoref",
		type: 0,
		definition: [
			{
				serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur",
				internalProjection: "EPSG:2154"
			}
		],
		options: {
			maxScale: 100,
			minScale: 14000001,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [],
			metadataURL: null,
			format: "image/png"
		}
	};

	    var matrixIdsWMTSGeoLittoral = [
		 'GoogleMapsCompatible:0',
		 'GoogleMapsCompatible:1',
		 'GoogleMapsCompatible:2',
		 'GoogleMapsCompatible:3',
		 'GoogleMapsCompatible:4',
		 'GoogleMapsCompatible:5',
		 'GoogleMapsCompatible:6',
		 'GoogleMapsCompatible:7',
		 'GoogleMapsCompatible:8',
		 'GoogleMapsCompatible:9',
		 'GoogleMapsCompatible:10',
		 'GoogleMapsCompatible:11',
		 'GoogleMapsCompatible:12',
		 'GoogleMapsCompatible:13',
		 'GoogleMapsCompatible:14',
		 'GoogleMapsCompatible:15',
		 'GoogleMapsCompatible:16',
		 'GoogleMapsCompatible:17',
		 'GoogleMapsCompatible:18'
	     ];
	    
	    var originsWMTSGeoLittoral = [
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508],
	       [-20037508.342789248, 20037508]
	   ];
		     
	    var projectionWMTSGeoLittoral = new Descartes.Projection('EPSG:3857');
	    var extentWMTSGeoLittoral = projectionWMTSGeoLittoral.getExtent();

	    var tileSizeGeoLittoral = 256;
	    var sizeWMTSGeoLittoral = ol.extent.getWidth(extentWMTSGeoLittoral) / tileSizeGeoLittoral;

	    var resolutionsWMTSGeoLittoral = [];

	    for (var i = 0; i < 19; i++) {
		 resolutionsWMTSGeoLittoral[i] = sizeWMTSGeoLittoral / Math.pow(2, i);
		 //matrixIds[i] = i;
	    }

	    var coucheGeoLittoralWmts1 = new Descartes.Layer.WMTS(
	    		 'ortholittorale:v2enGoogleMapsCompatible', 
	    		 [
	    		  	{
	    		  		serverUrl: 'http://geolittoral.recette.application.i2/cache/service/wmts?',
	    		  		layerName: 'ortholittorale:v2enGoogleMapsCompatible'
	    		  	}
	    		 ],
	    		 {
	    			 extent: extentWMTSGeoLittoral,
	    			 matrixSet: "GoogleMapsCompatible",
	    			 projection: projectionWMTSGeoLittoral,
	    			 matrixIds: matrixIdsWMTSGeoLittoral,
	    			 origins: originsWMTSGeoLittoral,
	    			 resolutions: resolutionsWMTSGeoLittoral,
	    			 tileSize: [256, 256],
	    			 format: 'image/jpeg',
	    			 visible: true,
	    			 opacity: 50,
				 queryable: false
	    		 }
	    );

	    var coucheGeoLittoralWmts2 = new Descartes.Layer.WMTS(
	    		 'ortholittorale:v2IRCenGoogleMapsCompatible', 
	    		 [
	    		  	{
	    		  		serverUrl: 'http://geolittoral.recette.application.i2/cache/service/wmts?',
	    		  		layerName: 'ortholittorale:v2IRCenGoogleMapsCompatible'
	    		  	}
	    		 ],
	    		 {
	    			 extent: extentWMTSGeoLittoral,
	    			 matrixSet: "GoogleMapsCompatible",
	    			 projection: projectionWMTSGeoLittoral,
	    			 matrixIds: matrixIdsWMTSGeoLittoral,
	    			 origins: originsWMTSGeoLittoral,
	    			 resolutions: resolutionsWMTSGeoLittoral,
	    			 tileSize: [256, 256],
	    			 format: 'image/jpeg',
	    			 visible: true,
	    			 opacity: 50,
				 queryable: false
	    		 }
	    );

	    var coucheGeoLittoralWmts3 = new Descartes.Layer.WMTS(
	    		 'ortholittorale:v1enGoogleMapsCompatible', 
	    		 [
	    		  	{
	    		  		serverUrl: 'http://geolittoral.recette.application.i2/cache/service/wmts?',
	    		  		layerName: 'ortholittorale:v1enGoogleMapsCompatible'
	    		  	}
	    		 ],
	    		 {
	    			 extent: extentWMTSGeoLittoral,
	    			 matrixSet: "GoogleMapsCompatible",
	    			 projection: projectionWMTSGeoLittoral,
	    			 matrixIds: matrixIdsWMTSGeoLittoral,
	    			 origins: originsWMTSGeoLittoral,
	    			 resolutions: resolutionsWMTSGeoLittoral,
	    			 tileSize: [256, 256],
	    			 format: 'image/jpeg',
	    			 visible: true,
	    			 opacity: 50,
				 queryable: false
	    		 }
	    );

        coucheBdxBDorthoWmts.title ="Couche WMTS Géoref";
        coucheBdxBDorthoWmts.queryable= false;

	var contenuCarte = new Descartes.MapContent();
	
	contenuCarte.addItem(coucheGeoLittoralWmts1);
	contenuCarte.addItem(coucheGeoLittoralWmts2);
	contenuCarte.addItem(coucheGeoLittoralWmts3);
        var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(coucheBdxBDorthoWmts,gpFonds);
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);;

	var bounds = [-20037508, -20037508, 20037508, 20037508];
	var initExtent = [-880269, 5201305, 1286588, 6611497];
        var projection = 'EPSG:3857';
        // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
		contenuCarte,
		{
			projection: projection,
			initExtent: initExtent,
			maxExtent: bounds,
			//maxScale: 100,
			size: [600, 400]
		}
	);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initExtent});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});

        carte.addOpenLayersControls([
	  {type: Descartes.Map.OL_SCALE_LINE}
        ]);

   	carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	]); 
}
