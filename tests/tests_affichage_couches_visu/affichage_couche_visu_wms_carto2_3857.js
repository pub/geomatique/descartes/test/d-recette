
function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});

	var serveur = "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?";

	coucheNature = new Descartes.Layer.WMS(
		"Espaces naturels", 
		[
			{
				serverUrl: serveur,
				layerName: "c_natural_Valeurs_type"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheBati = new Descartes.Layer.WMS(
		"Constructions", 
		[
			{
				serverUrl: serveur,
				layerName: "c_buildings",
				featureServerUrl: serveur,
				featureName: "c_buildings2"
			}
			],
		{
			maxScale: null,
			minScale: 39000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheRoutes = new Descartes.Layer.WMS(
		"Routes", 
		[
			{
				serverUrl: serveur,
				layerName: "c_roads"			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_roads"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheFer = new Descartes.Layer.WMS(
		"Chemins de fer", 
		[
			{
				serverUrl: serveur,
				layerName: "c_railways"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_railways"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheEau = new Descartes.Layer.WMS(
		"Cours d'eau", 
		[
			{
				serverUrl: serveur,
				layerName: "c_waterways_Valeurs_type"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheToponymes = new Descartes.Layer.WMS(
		"Lieux", 
		[
			{
				serverUrl: serveur,
				layerName: "c_places_Etiquettes"			}
			],
		{
			maxScale: null,
			minScale: 190000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: "http://metadataURL.fr",
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);
	
	coucheStations = new Descartes.Layer.WMS(
		"Stations essence", 
		[
			{
				serverUrl: serveur,
				layerName: "c_stations",
			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheParkings = new Descartes.Layer.WMS(
		"Parkings", 
		[
			{
				serverUrl: serveur,
				layerName: "c_parkings",
			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);	
	
	contenuCarte.addItem(coucheParkings);
	contenuCarte.addItem(coucheStations);
	contenuCarte.addItem(coucheRoutes);
	contenuCarte.addItem(coucheFer);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(coucheBdxBDorthoWmts,gpFonds);


	var bounds = [-800086,5055726,1150172,6772047];
	var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
