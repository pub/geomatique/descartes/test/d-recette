
function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();
	
	// Ajout couches WMTS
	contenuCarte.addItem(coucheWmtsKO);
	contenuCarte.addItem(coucheBdxBDorthoWmts);
	
	var bounds = [-20037508, -20037508, 20037508, 20037508];
	var initExtent = [-880269, 5201305, 1286588, 6611497];
    var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initExtent,
					maxExtent: bounds,
					//maxScale: 100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
