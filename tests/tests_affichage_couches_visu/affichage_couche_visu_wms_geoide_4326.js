proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
      
    var GeoideCatalogue = new Descartes.Layer.WMS(
            "Geo-IDE Catalogue",
            [
                {
                    serverUrl: "https://ogc.geo-ide.developpement-durable.gouv.fr/wxs?map=/opt/data/carto/geoide-catalogue/1.4/org_38120/f0d52f4c-5eb2-416e-8fca-8d9a4c44f96f.internet.map",
                    layerName: "L_PPRI_BOURBEUSE_090",
                    featureServerUrl: "https://ogc.geo-ide.developpement-durable.gouv.fr/wxs?map=/opt/data/carto/geoide-catalogue/1.4/org_38120/f0d52f4c-5eb2-416e-8fca-8d9a4c44f96f.internet.map",
                    featureName: "L_PPRI_BOURBEUSE_090",
                    featureGeometryName:"geometry",
                    featureServerVersion:"1.1.0",
                    featureReverseAxisOrientation:true

                }
            ],
            {
            	queryable:true, //ATTENTION reponse en urn:ogc:def:crs:EPSG::4326 (coord inverse) donc "localize" non dispo
            	sheetable:true,
            	legend: ["https://ogc.geo-ide.developpement-durable.gouv.fr/wxs?map=/opt/data/carto/geoide-catalogue/1.4/org_38120/f0d52f4c-5eb2-416e-8fca-8d9a4c44f96f.internet.map&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&layer=L_PPRI_BOURBEUSE_090&format=image/png&STYLE=inspire_common:DEFAULT"]
            }
        );
    
    var matrixIdsWMTSGeoLittoral = [
                                    'EPSG2154_256px:0',
        'EPSG2154_256px:1',
        'EPSG2154_256px:2',
        'EPSG2154_256px:3',
        'EPSG2154_256px:4',
        'EPSG2154_256px:5',
        'EPSG2154_256px:6',
        'EPSG2154_256px:7',
        'EPSG2154_256px:8',
        'EPSG2154_256px:9',
        'EPSG2154_256px:10',
        'EPSG2154_256px:11'
    ];
   
   var originsWMTSGeoLittoral = [
      [70000, 7207600.0],
      [70000, 7310000.0],
      [70000, 7182000.0],
      [70000, 7182000.0],
      [70000, 7130800.0],
      [70000, 7130800.0],
      [70000, 7130800.0],
      [70000, 7130800.0],
      [70000, 7130800.0],
      [70000, 7130160.0],
      [70000, 7130032.0],
      [70000, 7130032.0]
  ];
            
   var projectionWMTSGeoLittoral = new Descartes.Projection('EPSG:2154');
   //var extentWMTSGeoLittoral = projectionWMTSGeoLittoral.getExtent();
   var extentWMTSGeoLittoral = [70000, 6030000, 1270000, 7130000];
   //var extentWMTSGeoLittoral = [-101991.9, 6023917.0, 1528303.1, 7110780.4];

 var resolutionsWMTSGeoLittoral = [2300, 1000, 500, 250, 100, 50, 25, 10, 5, 2.5, 1, 0.5];

  var coucheGeoRefWmts1 = new Descartes.Layer.WMTS(
   		 'Couche WMTS Géoref', 
   		 [
   		  	{
   		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmts?',
   		  		layerName: 'georef:plan2154'
   		  	}
   		 ],
   		 {
   			 extent: extentWMTSGeoLittoral,
   			 matrixSet: "EPSG2154_256px",
   			 projection: projectionWMTSGeoLittoral,
   			 matrixIds: matrixIdsWMTSGeoLittoral,
   			 origins: originsWMTSGeoLittoral,
   			 resolutions: resolutionsWMTSGeoLittoral,
   			 tileSize: [256, 256],
   			 format: 'image/png',
   			 opacity: 100,
		 queryable: false
   		 }
   );

	
	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(GeoideCatalogue);
    //contenuCarte.addItem(coucheGeoRefWmts1);
    
	//var initBounds = [205462,6020516,215246,6030300];
	//var bounds = [-800086,5055726,1150172,6772047];
	//var initBounds = [151000,5637000,176000,5651000];
	//var projection = 'EPSG:3857';
	
    //var initBounds = [569000,6444000,586000,6455000];
	//var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    //var projection = 'EPSG:2154';
    
    
	//var initBounds = [-6.22, 39.46, 9.14, 53.4];
	var initBounds = [6.90534, 47.5433, 7.02141, 47.7086];
	var bounds = [-6.22, 39.46, 9.14, 53.4];
	var projection = 'EPSG:4326';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initBounds,
					//initExtent: bounds,
					maxExtent: bounds,
					minScale:15000000,
					size: [600, 400],
					displayExtendedOLExtent: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initBounds});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	]);
}
