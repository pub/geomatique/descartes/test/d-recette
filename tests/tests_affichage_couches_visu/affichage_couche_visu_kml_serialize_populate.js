proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	// Ajout couche KML
	contenuCarte.addItem(coucheReservesNaturellesRegionalesKML);
	contenuCarte.addItem(couchePointsKML);
	contenuCarte.addItem(coucheLinesKML);
    
    var projection = 'EPSG:4326';
    var bounds = [-5.8, 42.3, 8.13, 52.1];
    
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	var items = carte.mapContent.serialize();
    //console.log(items);
	//console.log(JSON.stringify(items));
	//console.log(JSON.parse(items));   
	carte.mapContent.removeItemByIndex("2",false);
	carte.mapContent.removeItemByIndex("1",false);
	carte.mapContent.removeItemByIndex("0",false);
	alert(JSON.stringify(items));
	carte.mapContentManager.populate(items);
}
