proj4.defs('EPSG:4326', "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

function chargementCarte() {
		
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	couche4326 = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://vmap0.tiles.osgeo.org/wms/vmap0",
					layerName: "basic",
					featureServerUrl: null,
					featureName: null
				}
			],
			options: {
				//maxScale: 100, // ex  100
				//minScale: 50000,	// ex 500000
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				//displayOrder:2
			}
		};
	
	
	couchePointsKML.OL_layers[0].getSource().on('featureloadend', ma_fonction_de_stylage, this);
	
	
	// Ajout couche KML
	contenuCarte.addItem(couchePointsKML);
	contenuCarte.addItem(new Descartes.Layer.WMS(couche4326.title, couche4326.definition, couche4326.options));
    
    var projection = 'EPSG:4326';
    var bounds = [-5.8, 42.3, 8.13, 52.1];
    
    
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}

function ma_fonction_de_stylage(){
	
	var styles=[];
	var style1= {
	        graphicName: 'circle',
			fillColor: "#11b234",
			fillOpacity: 0.4,
			hoverFillColor: "white",
			hoverFillOpacity: 0.8,
			strokeColor: "#11b234",
			strokeOpacity: 1,
			strokeWidth: 1,
			strokeLinecap: "round",
			strokeDashstyle: "solid",
			hoverStrokeColor: "red",
			hoverStrokeOpacity: 1,
			hoverStrokeWidth: 0.2,
			pointRadius: 6,
			hoverPointRadius: 1,
			hoverPointUnit: "%",
			pointerEvents: "visiblePainted",
			cursor: "pointer",
			fontColor: "#000000",
			labelAlign: "cm",
			labelOutlineColor: "white",
			labelOutlineWidth: 3
	};
	var style2= {
	        graphicName: 'circle',
			fillColor: "#11b234",
			fillOpacity: 0.4,
			hoverFillColor: "white",
			hoverFillOpacity: 0.8,
			strokeColor: "#11b234",
			strokeOpacity: 1,
			strokeWidth: 1,
			strokeLinecap: "round",
			strokeDashstyle: "solid",
			hoverStrokeColor: "red",
			hoverStrokeOpacity: 1,
			hoverStrokeWidth: 0.2,
			pointRadius: 10,
			hoverPointRadius: 1,
			hoverPointUnit: "%",
			pointerEvents: "visiblePainted",
			cursor: "pointer",
			fontColor: "#000000",
			labelAlign: "cm",
			labelOutlineColor: "white",
			labelOutlineWidth: 3
	};
	var style3= {
	        graphicName: 'circle',
			fillColor: "#11b234",
			fillOpacity: 0.4,
			hoverFillColor: "white",
			hoverFillOpacity: 0.8,
			strokeColor: "#11b234",
			strokeOpacity: 1,
			strokeWidth: 1,
			strokeLinecap: "round",
			strokeDashstyle: "solid",
			hoverStrokeColor: "red",
			hoverStrokeOpacity: 1,
			hoverStrokeWidth: 0.2,
			pointRadius: 14,
			hoverPointRadius: 1,
			hoverPointUnit: "%",
			pointerEvents: "visiblePainted",
			cursor: "pointer",
			fontColor: "#000000",
			labelAlign: "cm",
			labelOutlineColor: "white",
			labelOutlineWidth: 3
	};
	
	styles.push({'Point':style1});
	styles.push({'Point':style2});
	styles.push({'Point':style3});
	setStyles(couchePointsKML,styles,'Point');
}

function setStyles(couche,styles,typeGeom){
	var features=initAttributesForTest(couche);
	for (var i=0, len=features.length ; i<len ; i++) {
		var feature = features[i];
		if (feature.attributes && feature.attributes.d_attrib_1 && feature.attributes.d_attrib_1.value===1){
            var olStyles = Descartes.Symbolizers.getOlStyle(styles[0]);
            var olStyle = olStyles[typeGeom];
            couche.OL_layers[0].getSource().getFeatures()[i].setStyle(olStyle);
		} else if (feature.attributes && feature.attributes.d_attrib_1 && feature.attributes.d_attrib_1.value===2){
            var olStyles = Descartes.Symbolizers.getOlStyle(styles[1]);
            var olStyle = olStyles[typeGeom];
            couche.OL_layers[0].getSource().getFeatures()[i].setStyle(olStyle);
		} else if (feature.attributes && feature.attributes.d_attrib_1 && feature.attributes.d_attrib_1.value===3){
            var olStyles = Descartes.Symbolizers.getOlStyle(styles[2]);
            var olStyle = olStyles[typeGeom];
            couche.OL_layers[0].getSource().getFeatures()[i].setStyle(olStyle);
		}
	}
}

function initAttributesForTest(couche){

	var features=couche.OL_layers[0].getSource().getFeatures();
	for (var i=0, len=features.length ; i<len ; i++) {
		var val=Math.floor((Math.random() * 3) + 1);
		features[i].attributes={d_attrib_1:{value:val}};
	}
	return features;
}