proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();
	
	var couche = new Descartes.Layer.WMS(
	          'Courbes de niveaux - Style wms: noirgris', 
	          [
	              {
	                  serverUrl : 'http://georef.e2.rie.gouv.fr/cartes/mapserv?',
	                  layerName : 'courbes',
	                  layerStyles: 'noirgris'
	                	  
	              }
	          ], {
	                maxScale         : 5000, 
	                minScale         : 100001,
	                alwaysVisible    : false,
	                visible          : true,
	                queryable        : false,
	                activeToQuery    : false,
	                sheetable        : false,
	                opacity          : 100,
	                opacityMax       : 100,
	                legend           : null,
	                metadataURL      : null,
	                format           : 'image/png'
	            }
	        );
	
	var couche2 = new Descartes.Layer.WMS(
	          'Courbes de niveaux - Style wms: marron', 
	          [
	              {
	                  serverUrl : 'http://georef.e2.rie.gouv.fr/cartes/mapserv?',
	                  layerName : 'courbes',
	                  layerStyles: 'marron'
	                	  
	              }
	          ], {
	                maxScale         : 5000, 
	                minScale         : 100001,
	                alwaysVisible    : false,
	                visible          : true,
	                queryable        : false,
	                activeToQuery    : false,
	                sheetable        : false,
	                opacity          : 100,
	                opacityMax       : 100,
	                legend           : null,
	                metadataURL      : null,
	                format           : 'image/png'
	            }
	        );
	
	contenuCarte.addItem(couche);
	contenuCarte.addItem(couche2);
	
	//var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
	var bounds = [941110, 6332510, 948350, 6337330];

    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:90000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
