proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

    var coucheLycesWFS = new Descartes.Layer.WFS('Lycee (WFS 1.0.0)', [{
        serverUrl: 'https://ows.region-bretagne.fr/geoserver/rb/wfs',
        layerName: 'lycee',
        featurePrefix: 'rb',
        featureGeometryName: 'geom',
        serverVersion: '1.0.0'
    }],{
        useProxy: false,
    });
	
	contenuCarte.addItem(coucheLycesWFS);	
	
    var coucheLycesWFS2 = new Descartes.Layer.WFS('Lycee (WFS 1.1.0)', [{
        serverUrl: 'https://ows.region-bretagne.fr/geoserver/rb/wfs',
        layerName: 'lycee',
        featurePrefix: 'rb',
        featureGeometryName: 'geom',
        serverVersion: '1.1.0'
    }],{
        useProxy: false,
    });
	
	contenuCarte.addItem(coucheLycesWFS2);	
	
    var coucheLycesWFS3 = new Descartes.Layer.WFS('Lycee (WFS 2.0.0)', [{
        serverUrl: 'https://ows.region-bretagne.fr/geoserver/rb/wfs',
        layerName: 'lycee',
        featurePrefix: 'rb',
        featureGeometryName: 'geom',
        serverVersion: '2.0.0'
    }],{
        useProxy: false,
    });
	contenuCarte.addItem(coucheLycesWFS3);	
    
	var coucheLycesWFS4 = new Descartes.Layer.WFS(
			"couche carto2 (WFS 1.0.0)", 
			[
				{
					serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560/ows?",
					layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
					serverVersion: "1.0.0",
					featureNameSpace:"org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
					featureGeometryName: "the_geom"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100
			}
		);
	contenuCarte.addItem(coucheLycesWFS4);

	var coucheLycesWFS5 = new Descartes.Layer.WFS(
			"couche carto2 (WFS 1.1.0)", 
			[
				{
					serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560/ows?",
					layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
					serverVersion: "1.1.0",
					featureNameSpace:"org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
					featureGeometryName: "the_geom"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100
			}
		);
	contenuCarte.addItem(coucheLycesWFS5);
	
	var coucheLycesWFS6 = new Descartes.Layer.WFS(
			"couche carto2 (WFS 2.0.0)", 
			[
				{
					serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560/ows?",
					layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
					serverVersion: "2.0.0",
					featureNameSpace:"org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
					featureGeometryName: "the_geom"
				}
			],
			{
				maxScale: null,
				minScale: null,
				alwaysVisible: false,
				visible: true,
				queryable:true,
				activeToQuery:true,
				sheetable:false,
				opacity: 100,
				opacityMax: 100
			}
		);
	contenuCarte.addItem(coucheLycesWFS6);
	
	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
