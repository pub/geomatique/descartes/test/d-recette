proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
    Descartes.Symbolizers.Descartes_Symbolizers_WFS = {
            'Point': {
                pointRadius: 4, //6
                graphicName: 'circle',
                fillColor: 'red',
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: 'red',
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            },
            'Line': {
                strokeWidth: 3,
                strokeOpacity: 1,
                strokeColor: 'red',
                strokeDashstyle: 'solide',
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            },
            'MultiPoint': {
                pointRadius: 4, //6
                graphicName: 'circle',
                fillColor: 'red',
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: 'red',
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            },
            'MultiLine': {
                strokeWidth: 3,
                strokeOpacity: 1,
                strokeColor: 'red',
                strokeDashstyle: 'solide',
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            },
            'Polygon': {
                strokeWidth: 2, //1
                strokeOpacity: 1, //1
                strokeColor: 'red',
                fillColor: 'red',
                fillOpacity: 0.3,
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            },
            'MultiPolygon': {
                strokeWidth: 2, //1
                strokeOpacity: 1, //1
                strokeColor: 'red',
                fillColor: 'red',
                fillOpacity: 0.3,
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            }
        };
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();
	// Ajout couches wfs
	contenuCarte.addItem(coucheNatureWfs);
	contenuCarte.addItem(coucheEauWfs);
	contenuCarte.addItem(coucheWfsStyle);
	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	var items = carte.mapContent.serialize();
    //console.log(items);
	//console.log(JSON.stringify(items));
	//console.log(JSON.parse(items));   
	carte.mapContent.removeItemByIndex("2",false);
	carte.mapContent.removeItemByIndex("1",false);
	carte.mapContent.removeItemByIndex("0",false);
	alert(JSON.stringify(items));
	carte.mapContentManager.populate(items);
}
