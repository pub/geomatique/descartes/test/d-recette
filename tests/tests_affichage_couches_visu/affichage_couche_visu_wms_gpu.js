
function chargementCarte() {
	chargeCouchesGroupes();
    var GPUzonessecteurs = new Descartes.Layer.WMS(
            "GPU - Zones secteurs",
            [
                {
                    serverUrl: "https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v",
                    layerName: "zone_secteur",

                }
            ],
            {
            	minScale:90000,
            	maxScale:100,
            	queryable:false,
    			legend: ["https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=zone_secteur"]
            }
        );
      
    var GPUscot = new Descartes.Layer.WMS(
            "GPU - Schéma de Cohérence Territoriale",
            [
                {
                    serverUrl: "https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v",
                    layerName: "scot",

                }
            ],
            {
            	queryable:false,
            	legend: ["https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=scot"]
            }
        );
    
    var GPUmunicipality = new Descartes.Layer.WMS(
            "GPU - Vue d'ensemble (PLUi, PLU, POS, CC, PSMV, RNU)",
            [
                {
                    serverUrl: "https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v",
                    layerName: "municipality,document",

                }
            ],
            {
            	queryable:false,
            	legend: ["https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=municipality","https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=document"]
            }
        );
    
    var GPUreserves = new Descartes.Layer.WMS(
            "GPU - Réserves et parcs - Servitude d'Utilité Publique de catégories AC3 et EL10",
            [
                {
                    serverUrl: "https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v",
                    layerName: "reserves",

                }
            ],
            {
            	minScale:90000,
            	maxScale:100,
            	queryable:false,
            	legend: ["https://wxs-gpu.mongeoportail.ign.fr/externe/vkd1evhid6jdj5h4hkhyzjto/wms/v?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=reserves"]
            }
        ); 
    
    var matrixIdsWMTS = [
                         'GoogleMapsCompatible:0',
                         'GoogleMapsCompatible:1',
                         'GoogleMapsCompatible:2',
                         'GoogleMapsCompatible:3',
                         'GoogleMapsCompatible:4',
                         'GoogleMapsCompatible:5',
                         'GoogleMapsCompatible:6',
                         'GoogleMapsCompatible:7',
                         'GoogleMapsCompatible:8',
                         'GoogleMapsCompatible:9',
                         'GoogleMapsCompatible:10',
                         'GoogleMapsCompatible:11',
                         'GoogleMapsCompatible:12',
                         'GoogleMapsCompatible:13',
                         'GoogleMapsCompatible:14',
                         'GoogleMapsCompatible:15',
                         'GoogleMapsCompatible:16',
                         'GoogleMapsCompatible:17',
                         'GoogleMapsCompatible:18'
                     ];
                    
                    var originsWMTS = [
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508],
                       [-20037508.342789248, 20037508]
                   ];
                             
                    var projectionWMTS = new Descartes.Projection('EPSG:3857');
                    var extentWMTS = projectionWMTS.getExtent();

                    var tileSize = 256;
                    var sizeWMTS = ol.extent.getWidth(extentWMTS) / tileSize;

                    var resolutionsWMTS = [];

                    for (var i = 0; i < 19; i++) {
                         resolutionsWMTS[i] = sizeWMTS / Math.pow(2, i);
                         //matrixIds[i] = i;
                    }
    var coucheBdxBDorthoWmts = new Descartes.Layer.WMTS(
      		 'Couche de fond GéoRef', 
      		 [
      		  	{
      		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmts?',
      		  		layerName: 'georefmonde:GoogleMapsCompatible'
      		  	}
      		 ],
      		 {
      			 queryable:false,
            	 extent: extentWMTS,
      			 matrixSet: "GoogleMapsCompatible",
      			 projection: projectionWMTS,
      			 matrixIds: matrixIdsWMTS,
      			 origins: originsWMTS,
      			 resolutions: resolutionsWMTS,
      			 tileSize: [256, 256],
      			 format: 'image/jpeg',
      			 visible: true,
      			 opacity: 100,
            	 maxScale:90000
      		 }
      );
	
	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(GPUmunicipality);
    contenuCarte.addItem(GPUscot);
    contenuCarte.addItem(GPUzonessecteurs);
    contenuCarte.addItem(GPUreserves);
    contenuCarte.addItem(coucheBdxBDorthoWmts);
    
	//var initBounds = [205462,6020516,215246,6030300];
	var bounds = [-800086,5055726,1150172,6772047];
	var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					//initExtent: initBounds,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:15000000,
					size: [600, 400],
					displayExtendedOLExtent: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initBounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	//carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	]);
}
