proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
    var IGNBDCARTOReg = new Descartes.Layer.WFS(
            "BDCARTO_BDD_WLD_WGS84G:region",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wfs?",
                    layerName: "BDCARTO_BDD_WLD_WGS84G:region",
                    internalProjection: "urn:ogc:def:crs:EPSG::4326",
                    featureReverseAxisOrientation: true

                }
            ]
        );

    var IGNBDTOPOGare = new Descartes.Layer.WFS(
            "BDTOPO_BDD_WLD_WGS84G:gare",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wfs?",
                    layerName: "BDTOPO_BDD_WLD_WGS84G:gare",
                    internalProjection: "urn:ogc:def:crs:EPSG::4326",
                    featureReverseAxisOrientation: true

                }
            ]
        );
	
	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(IGNBDTOPOGare);
    contenuCarte.addItem(IGNBDCARTOReg);
    
	var bounds = [-6.22, 39.46, 9.14, 53.4];

	var projection = 'EPSG:4326';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

}
