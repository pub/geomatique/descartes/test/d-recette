proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
    var matrixIdsWMTS = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"];
   
   var originsWMTS = [
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508],
      [-20037508, 20037508]
  ];
            
   var projectionWMTS = new Descartes.Projection('EPSG:3857');
   var extentWMTS = projectionWMTS.getExtent();

   var resolutionsWMTS = [
       156543.03392804103,
       78271.5169640205,
       39135.75848201024,
       19567.879241005125,
       9783.939620502562,
       4891.969810251281,
       2445.9849051256406,
       1222.9924525628203,
       611.4962262814101,
       305.74811314070485,
       152.87405657035254,
       76.43702828517625,
       38.218514142588134,
       19.109257071294063,
       9.554628535647034,
       4.777314267823517,
       2.3886571339117584,
       1.1943285669558792,
       0.5971642834779396,
       0.29858214173896974,
       0.14929107086948493,
       0.07464553543474241
   ] ;


	
    var IGN= new Descartes.Layer.WMTS(
            "Photographies aériennes (PM - EPSG:3857)",
            [
                {
                    serverUrl: "https://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wmts?",
                    layerName: "ORTHOIMAGERY.ORTHOPHOTOS",
                    layerStyles: "normal"
                }
            ],
      		 {
      			 extent: extentWMTS,
      			 matrixSet: "PM",
      			 projection: projectionWMTS,
      			 matrixIds: matrixIdsWMTS,
      			 origins: originsWMTS,
      			 resolutions: resolutionsWMTS,
      			 tileSize: [256, 256],
      			 format: 'image/jpeg',
      			 visible: true,
      			 opacity: 100
      		 }
        );



    var matrixIdsWMTS2 = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21"];
   
   var originsWMTS2 = [
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000],
      [0, 12000000]
  ];
            
   var projectionWMTS2 = new Descartes.Projection('EPSG:2154');
   var extentWMTS2 = projectionWMTS2.getExtent();

   var resolutionsWMTS2 = [
       104879.2245498940,
       52277.5323537905,
       26135.4870785954,
       13066.8913818000,
       6533.2286041135,
       3266.5595244627,
       1633.2660045974,
       816.6295549860,
       408.3139146768,
       204.1567415109,
       102.0783167832,
       51.0391448966,
       25.5195690743,
       12.7597836936,
       6.3798916360,
       3.1899457653,
       1.5949728695,
       0.7974864315,
       0.3987432149,
       0.1993716073,
       0.0996858037,
       0.0498429018
   ] ;
	
    var IGN2= new Descartes.Layer.WMTS(
            "SCAN25 Topographique L93 (LAMB93 - EPSG:2154)",
            [
                {
                    serverUrl: "https://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wmts?",
                    layerName: "GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN25TOPO.L93",
                    layerStyles: "normal"
                }
            ],
      		 {
      			 extent: extentWMTS2,
      			 matrixSet: "LAMB93",
      			 projection: projectionWMTS2,
      			 matrixIds: matrixIdsWMTS2,
      			 origins: originsWMTS2,
      			 resolutions: resolutionsWMTS2,
      			 tileSize: [256, 256],
      			 format: 'image/jpeg',
      			 visible: true,
      			 opacity: 100
      		 }
        );
	
	var contenuCarte = new Descartes.MapContent();
	
    contenuCarte.addItem(IGN2);
    contenuCarte.addItem(IGN);
	
	var bounds = [-20037508, -20037508, 20037508, 20037508];
	var initExtent = [465770.92878885584650561, 5331196.19765195623040199, 652766.07225444202776998, 5458880.10618572682142258];
    var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initExtent,
					maxExtent: bounds,
					//maxScale: 100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
}
