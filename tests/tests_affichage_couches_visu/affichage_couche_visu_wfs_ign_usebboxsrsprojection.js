
function chargementCarte() {
	
    var IGNBDCARTOReg = new Descartes.Layer.WFS(
            "BDCARTO_BDD_WLD_WGS84G:region",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wfs?",
                    layerName: "BDCARTO_BDD_WLD_WGS84G:region",
                    useBboxSrsProjection: true
                }
            ]
        );

    var IGNBDTOPOGare = new Descartes.Layer.WFS(
            "BDTOPO_BDD_WLD_WGS84G:gare",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wfs?",
                    layerName: "BDTOPO_BDD_WLD_WGS84G:gare",
                    useBboxSrsProjection: true

                }
            ]
        );
    
    var IGNAOCVITICOLES = new Descartes.Layer.WFS(
            "AOC-VITICOLES:aire_parcellaire",
            [
                {
                    serverUrl: "http://wxs.ign.fr/iqqdkoa235g0mf1gr3si0162/geoportail/wfs?",
                    layerName: "AOC-VITICOLES:aire_parcellaire",
                    useBboxSrsProjection: true

                }
            ],
            {
    			maxScale: null,
    			minScale: 100000,
            }
        );
	
	var contenuCarte = new Descartes.MapContent();

    contenuCarte.addItem(IGNAOCVITICOLES);
    contenuCarte.addItem(IGNBDTOPOGare);
    contenuCarte.addItem(IGNBDCARTOReg);
    
	var bounds = [-800086,5055726,1150172,6772047];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();

}
