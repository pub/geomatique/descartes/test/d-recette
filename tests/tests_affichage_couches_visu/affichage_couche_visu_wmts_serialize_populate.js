
function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();
	
	// Ajout couches WMTS
	contenuCarte.addItem(coucheBdxBDorthoWmts);
	
	var bounds = [-20037508, -20037508, 20037508, 20037508];
	var initExtent = [-880269, 5201305, 1286588, 6611497];
    var projection = 'EPSG:3857';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initExtent,
					maxExtent: bounds,
					//maxScale: 100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	var items = carte.mapContent.serialize();
    //console.log(items);
	//console.log(JSON.stringify(items));
	//console.log(JSON.parse(items));   
	carte.mapContent.removeItemByIndex("0",false);
	alert(JSON.stringify(items));
	carte.mapContentManager.populate(items);
}
