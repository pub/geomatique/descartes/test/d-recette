proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	contenuCarte.addItem(groupeFonds);
 //   contenuCarte.addItem(coucheBase,groupeFonds);
//	contenuCarte.addItem(coucheBdxBDorthoWmts,groupeFonds);
    contenuCarte.addItem(new Descartes.Layer.OSM("couche OSM"), groupeFonds);
    //contenuCarte.addItem(coucheIGN,groupeFonds);


	var bounds = [-20037508,-20037508,20037508,20037508];
	var initbounds = [-800086,5055726,1150172,6772047];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
//					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar', null, {vertical:true});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Tool.ExtensionGeolocationSimple});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Tool.ExtensionGeolocationTracking});

//	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});
//	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_SIMPLE});
//	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_TRACKING});
//	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_SIMPLE, args : {enableHighAccuracy: true}});
//	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_TRACKING, args : {enableHighAccuracy: true}});	
//	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_SIMPLE, args : {enableHighAccuracy: true, maximumAge: 0}});
//	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_TRACKING, args : {enableHighAccuracy: true, maximumAge: 0}});
    carte.addContentManager("layersTree");
	
	// Affichage de la carte
	carte.show();
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},  
         {type: Descartes.Map.OL_PINCH_ZOOM}
		]);
		
		
		
/*		
	 var geolocation = new ol.Geolocation({
    	trackingOptions: {
    		  enableHighAccuracy: true,
        },
        projection: carte.OL_map.getView().getProjection()
      });

      function el(id) {
        return document.getElementById(id);
      }

      el('track').addEventListener('change', function() {
        geolocation.setTracking(this.checked);
      });

      // update the HTML page when the position changes.
      geolocation.on('change', function() {
        el('accuracy').innerText = geolocation.getAccuracy() + ' [m]';
        el('altitude').innerText = geolocation.getAltitude() + ' [m]';
        el('altitudeAccuracy').innerText = geolocation.getAltitudeAccuracy() + ' [m]';
        el('heading').innerText = geolocation.getHeading() + ' [rad]';
        el('speed').innerText = geolocation.getSpeed() + ' [m/s]';
      });

      // handle geolocation error.
      geolocation.on('error', function(error) {
        var info = document.getElementById('info');
        info.innerHTML = error.message;
        info.style.display = '';
      });

      var accuracyFeature = new ol.Feature();
      geolocation.on('change:accuracyGeometry', function() {
        accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
      });

      var positionFeature = new ol.Feature();
      positionFeature.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
          radius: 6,
          fill: new ol.style.Fill({
            color: '#3399CC'
          }),
          stroke: new ol.style.Stroke({
            color: '#fff',
            width: 2
          })
        })
      }));

      geolocation.on('change:position', function() {
        var coordinates = geolocation.getPosition();
        positionFeature.setGeometry(coordinates ?
          new ol.geom.Point(coordinates) : null);
      });

      new ol.layer.Vector({
        map: carte.OL_map,
        source: new ol.source.Vector({
          features: [accuracyFeature, positionFeature]
        })
      });	*/
	
}