Descartes.Tool.ExtensionGeolocationSimple = Descartes.Utils.Class(Descartes.Tool, {

    active: false,

    geolocationLayer: null,

    trackingOptions: {},

	initialize: function(options) {
   		Descartes.Tool.prototype.initialize.apply(this, arguments);
	},

    isActive: function () {
        return this.active;
    },

    activate: function () {
        if (this.isActive()) {
            return false;
        }
        this.active = true;
        
        var that = this;
        
        this.geolocation = new ol.Geolocation({
    	  trackingOptions: {
    	  	  enableHighAccuracy: true,
          },
          projection: this.olMap.getView().getProjection()
        });

        this.geolocation.setTracking(true);

	    this.geolocation.on('change', function() {
		  if (that.formDialog) {
	        document.getElementById('accuracy').innerText = that.geolocation.getAccuracy() + ' [m]';
	        document.getElementById('altitude').innerText = that.geolocation.getAltitude() + ' [m]';
	        document.getElementById('altitudeAccuracy').innerText = that.geolocation.getAltitudeAccuracy() + ' [m]';
		    document.getElementById('heading').innerText = that.geolocation.getHeading() + ' [rad]';
	        document.getElementById('speed').innerText = that.geolocation.getSpeed() + ' [m/s]';
	      }
	    });

        this.geolocation.on('error', function(error) {
          if (that.formDialog) {
            var info = document.getElementById('geolocationinfo');
            info.innerHTML = error.message;
            info.style.display = '';
          }
        });

        var accuracyFeature = new ol.Feature();
        this.geolocation.on('change:accuracyGeometry', function() {
          accuracyFeature.setGeometry(that.geolocation.getAccuracyGeometry());
        });

        var positionFeature = new ol.Feature();
        positionFeature.setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 2
            })
          })
        }));

        this.geolocation.on('change:position', function() {
          var coordinates = that.geolocation.getPosition();
          positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
          that.olMap.getView().setCenter(positionFeature.getGeometry().getCoordinates());
        });

        this.geolocationLayer = new ol.layer.Vector({
          source: new ol.source.Vector({
            features: [accuracyFeature, positionFeature]
          })
        });	
        this.olMap.addLayer(this.geolocationLayer);

        this.createModalDialog();

        this.events.triggerEvent('activate', this);
        this.updateElement();
        Descartes._activeClickToolTip = false;
        return true;
    },
    
    close: function () {
        this.formDialog = null;
    },
    createModalDialog: function () {
        var close = this.close.bind(this);
        var content = "<div id=\"geolocationinfo\" style=\"display: none;\"></div>" + 
        "<div id=\"DescartesInfosGeolocationSimple\" class=\"DescartesDialogContent2\">" +
        "position précision : <code id=\"accuracy\"></code>&nbsp;&nbsp;" +
        "</br>altitude : <code id=\"altitude\"></code>&nbsp;&nbsp;" +
        "</br>altitude précision : <code id=\"altitudeAccuracy\"></code>&nbsp;&nbsp;" +
        "</br>angle : <code id=\"heading\"></code>&nbsp;&nbsp;" +
        "</br>vitesse : <code id=\"speed\"></code>" +
        "</div>";
        var formDialogOptions = {
                id: 'DescartesInfosGeolocationSimple_dialog',
                title: "Géolocalisation",
                size: 'modal-lg',
                sendLabel: 'Fermer',
                content: content
            };
        this.formDialog = new Descartes.UI.FormDialog(formDialogOptions);

        this.formDialog.open(close,close);
        
        var formSelector = '#' + this.formDialog.id + '_formDialog';

        var formDialogDiv = $(formSelector);
        formDialogDiv.parent().parent()[0].className = formDialogDiv.parent().parent()[0].className + ' geolocationsimpledialog';
    },
    
    /**
     * Methode: deactivate
     * Désactive le bouton
     */
    deactivate: function () {
        if (!this.isActive()) {
            return false;
        }
        this.active = false;
        
      	this.geolocation.setTracking(false);
      	this.olMap.removeLayer(this.geolocationLayer);

        if (this.formDialog) {
		   this.formDialog.close();
		}
		
		this.updateElement();
        Descartes._activeClickToolTip = true;
        return true;

    },
	
	CLASS_NAME: "Descartes.Tool.ExtensionGeolocationSimple"
});
