proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable:true, fctDisplayLegend: true});

	var defcouche1 = {
		title : "L_ENV_DOMAINE_SKIABLE_S_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_ENV_DOMAINE_SKIABLE_S_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				featureServerVersion: "1.1.0",
				layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_ENV_DOMAINE_SKIABLE_S_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
		}
	};

	var defcouche2 = {
		title : "L_PISTE_SKI_L_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_PISTE_SKI_L_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				featureServerVersion: "1.1.0",
				layerName: "L_PISTE_SKI_L_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_PISTE_SKI_L_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.LINE_GEOMETRY
		}
	};

    var couche1 = new Descartes.Layer.WMS(defcouche1.title, defcouche1.definition, defcouche1.options);
    var couche2 = new Descartes.Layer.WMS(defcouche2.title, defcouche2.definition, defcouche2.options);

    contenuCarte.addItem(couche2);
    contenuCarte.addItem(couche1);

	contenuCarte.addItem(groupeFonds);
//  contenuCarte.addItem(coucheBase,groupeFonds);
//	contenuCarte.addItem(coucheBdxBDorthoWmts,groupeFonds);
    contenuCarte.addItem(coucheIGN,groupeFonds);


	//var bounds = [-20037508,-20037508,20037508,20037508];
	var bounds = [-800086,5055726,1150172,6772047];
	var initbounds = [683697, 5738429, 804314, 5809973];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
//					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar', null, {vertical:true});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});
		carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
        args: {
        	persist:true, 
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withListResultLayer: true,
                withFilterColumns: true,
                withResultLayerExport: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
        args: {
        	persist:true, 
        	infoRadius: true,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withListResultLayer: true,
                withFilterColumns: true,
                withResultLayerExport: true
            },
            resultLayerParams: {
                display: true
            }
        }});
    	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	configHalo: false,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withListResultLayer: true,
                withFilterColumns: true,
                withResultLayerExport: true
            },
            resultLayerParams: {
                display: true
            }
      }});
      carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	configHalo: false,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withListResultLayer: true,
                withFilterColumns: true,
                withResultLayerExport: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_BUFFER_HALO_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	configHalo: false,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withListResultLayer: true,
                withFilterColumns: true,
                withResultLayerExport: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.LINE_BUFFER_HALO_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	configHalo: false,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withListResultLayer: true,
                withFilterColumns: true,
                withResultLayerExport: true
            },
            resultLayerParams: {
                display: true
            }
        }});

    var managerOptions = {
	    toolBarDiv: "managerToolBar",
        uiOptions: {
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withListResultLayer: true,
                withFilterColumns: true,
                withResultLayerExport: true
            }
        }
    };
    
   var contentTools = [
        {type: "AddGroup", options: null}
    ];
    
    //var contentTools = null;
    carte.addContentManager("layersTree", contentTools, managerOptions);
	
	// Affichage de la carte
	carte.show();
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}}   
	]);
	
		// Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes',{
		withChooseExtent: true,
		chooseExtentGazetteerConfig: {
	       startLevel: 2,
           initValue: "74"
        },
        resultUiParams: {
            withReturn: true,
            withUIExports: true,
            withAvancedView: true,
            withListResultLayer: true,
            withFilterColumns: true,
            withResultLayerExport: true
        }});
	var requeteStation = new Descartes.Request(couche2, "Filtrer la couche L_PISTE_SKI_L_074", Descartes.Layer.LINE_GEOMETRY);
	var critereType = new Descartes.RequestMember(
		"Sélectionner une catégorie",
		"CATEGORIE",
		"==",
		["N","R","B","V"],
		true
	);
	
	requeteStation.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteStation);
}