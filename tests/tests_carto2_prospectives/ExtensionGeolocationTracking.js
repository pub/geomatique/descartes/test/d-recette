Descartes.Tool.ExtensionGeolocationTracking = Descartes.Utils.Class(Descartes.Tool, {

    active: false,

    geolocationLayer: null,

    trackingOptions: {},

	initialize: function(options) {
   		Descartes.Tool.prototype.initialize.apply(this, arguments);
	},

    isActive: function () {
        return this.active;
    },

    activate: function () {
        if (this.isActive()) {
            return false;
        }
        this.active = true;
        
        var that = this;
        
        this.geolocation = new ol.Geolocation({
    	  trackingOptions: {
    	  	  enableHighAccuracy: true,
          },
          projection: this.olMap.getView().getProjection()
        });

        this.geolocation.setTracking(true);

	    /*this.geolocation.on('change', function() {
		  
	    });*/

        /*this.geolocation.on('error', function(error) {
          
        });*/

        var positionFeature = new ol.Feature();
        positionFeature.setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 2
            })
          })
        }));

        this.geolocation.on('change:position', function() {
          var coordinates = that.geolocation.getPosition();
          positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
          that.olMap.getView().setCenter(positionFeature.getGeometry().getCoordinates());
          var currentPosition = positionFeature.clone();
          currentPosition.setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            radius: 2,
            fill: new ol.style.Fill({
              color: 'black'
            })
          })
        }));
		  that.positionListGeolocationLayer.getSource().addFeature(currentPosition);
        });

        this.geolocationLayer = new ol.layer.Vector({
          source: new ol.source.Vector({
            features: [positionFeature]
          })
        });	
        this.olMap.addLayer(this.geolocationLayer);

		this.positionListGeolocationLayer = new ol.layer.Vector({
          source: new ol.source.Vector()
        });
        this.olMap.addLayer(this.positionListGeolocationLayer);

        this.events.triggerEvent('activate', this);
        this.updateElement();
        Descartes._activeClickToolTip = false;
        return true;
    },
    
    
    /**
     * Methode: deactivate
     * Désactive le bouton
     */
    deactivate: function () {
        if (!this.isActive()) {
            return false;
        }
        this.active = false;
        
      	this.geolocation.setTracking(false);
      	this.olMap.removeLayer(this.geolocationLayer);

        this.olMap.removeLayer(this.positionListGeolocationLayer);

		this.updateElement();
        Descartes._activeClickToolTip = true;
        return true;

    },
	
	CLASS_NAME: "Descartes.Tool.ExtensionGeolocationTracking"
});

