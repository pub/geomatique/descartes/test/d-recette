proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

	var defcouche1 = {
		title : "L_ENV_DOMAINE_SKIABLE_S_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_ENV_DOMAINE_SKIABLE_S_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				featureServerVersion: "1.1.0",
				layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_ENV_DOMAINE_SKIABLE_S_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
		}
	};

	var defcouche2 = {
		title : "L_PISTE_SKI_L_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_PISTE_SKI_L_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				featureServerVersion: "1.1.0",
				layerName: "L_PISTE_SKI_L_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_PISTE_SKI_L_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
		}
	};
var context = {};

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	// constitution de l'objet JSON de contexte
	var contextFile = (new Descartes.Url(this.location.href)).getParamValue("context");
	
	if (contextFile !== "") {
		// contexte sauvegardé
		var xhr = new XMLHttpRequest();
        xhr.open('GET', Descartes.CONTEXT_MANAGER_SERVER+"?" + contextFile);
        xhr.onload = function () {
            if (xhr.status === 200) {
            	getContext(xhr);
            } else {
            	stopMap();
            }
        };
        xhr.send();
	
	} else {
		context.bbox = {xMin:683697, yMin:5738429, xMax:804314, yMax:5809973};

		context.items = [];
	
		context.items[0] = Descartes.Utils.extend(defcouche2, {itemType:"Layer"});
		context.items[1] = Descartes.Utils.extend(defcouche1, {itemType:"Layer"});
		context.items[2] = Descartes.Utils.extend(defGroupeFonds, {itemType:"Group", items:[]});
		context.items[2].items[0] = Descartes.Utils.extend(defCoucheIGN, {itemType:"Layer"});
		doMap();
	}
}
	
function doMap(){	
	
	var contenuCarte = new Descartes.MapContent({editable: true});

    contenuCarte.populate(context.items);

	//var bounds = [-20037508,-20037508,20037508,20037508];
	var bounds = [-800086,5055726,1150172,6772047];
	//var initbounds = [683697, 5738429, 804314, 5809973];
	var initbounds = [context.bbox.xMin, context.bbox.yMin, context.bbox.xMax, context.bbox.yMax];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
//					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar', null, {vertical:true});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.SHARE_LINK_MAP});
	
    var managerOptions = {
        uiOptions: {
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withFilterColumns: true
            }
        }
    };
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	
	// Affichage de la carte
	carte.show();
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}}   
	]);
	
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes');

}

function getContext(transport) {
	
	var response = transport.responseText;
		if (response.charAt(0) !== "{") {
			//alert("La carte est disponible jusqu'au " + response.substring(0, response.indexOf("{")));
			response = response.substring(response.indexOf("{"));
		}
	context = JSON.parse(response);
	doMap();
	
}

function stopMap() {
	alert("Impossible de recharger la carte: problème lors du chargement du fichier de contexte");
}