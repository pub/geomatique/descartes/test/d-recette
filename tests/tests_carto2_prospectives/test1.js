proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent();

	contenuCarte.addItem(groupeFonds);
    contenuCarte.addItem(coucheBase,groupeFonds);
//	contenuCarte.addItem(coucheBdxBDorthoWmts,groupeFonds);
    contenuCarte.addItem(new Descartes.Layer.OSM("couche OSM"), groupeFonds);
    contenuCarte.addItem(coucheIGN,groupeFonds);


	var bounds = [-20037508,-20037508,20037508,20037508];
	var initbounds = [-800086,5055726,1150172,6772047];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
//					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar', null, {vertical:true});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_SIMPLE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.GEOLOCATION_TRACKING});
    carte.addContentManager("layersTree");
	
	// Affichage de la carte
	carte.show();
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},  
         {type: Descartes.Map.OL_PINCH_ZOOM}
		]);
	
}