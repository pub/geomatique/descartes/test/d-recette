proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();
	
    Descartes.EditionManager.configure({
	    globalEditionMode: false
    });
	
	var contenuCarte = new Descartes.MapContent();

    var coucheAnnotations = {
	    title: "Annotations",
	    type: Descartes.Layer.EditionLayer.TYPE_Annotations
	};



    var couche1 = new Descartes.Layer.EditionLayer.Annotations(coucheAnnotations.title, coucheAnnotations.definition, coucheAnnotations.options);

    contenuCarte.addItem(couche1);

	contenuCarte.addItem(groupeFonds);
//  contenuCarte.addItem(coucheBase,groupeFonds);
	contenuCarte.addItem(coucheBdxBDorthoWmts,groupeFonds);
//    contenuCarte.addItem(coucheIGN,groupeFonds);


	var bounds = [-20037508,-20037508,20037508,20037508];
	var initbounds = [-800086,5055726,1150172,6772047];
	//var initbounds = [683697, 5738429, 804314, 5809973];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
//					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar', null, {vertical:true});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});

    carte.addAnnotationToolBar(null, [ {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Layer.POINT_GEOMETRY,
                   snapping:true
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Layer.LINE_GEOMETRY,
                   snapping:true,
                   autotracing: true
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Layer.POLYGON_GEOMETRY,
                   snapping:true,
                   autotracing: true
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Tool.Edition.ANNOTATION_RECTANGLE_GEOMETRY
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Tool.Edition.ANNOTATION_CIRCLE_GEOMETRY
               }
           }, {
               type: Descartes.Map.EDITION_ARROW_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_FREEHAND_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_TEXT_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_GLOBAL_MODIFICATION_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_VERTICE_MODIFICATION_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_ADD_TEXT_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_ATTRIBUTE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_STYLE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_RUBBER_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_ERASE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_EXPORT_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_IMPORT_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_SNAPPING_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_GEOLOCATION_SIMPLE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_GEOLOCATION_TRACKING_ANNOTATION
           }
       ],
       {
    	   toolBar: toolsBar,// barre d'outils d'édition imbriquée dans la barre principale
   	       //openerTool: true,
   	       vertical: true
   	   } 
    );


    var managerOptions = {
        uiOptions: {
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withFilterColumns: true
            }
        }
    };
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	
	// Affichage de la carte
	carte.show();
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}}   
	]);
	
}