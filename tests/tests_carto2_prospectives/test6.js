proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();
	
    Descartes.EditionManager.configure({
	    globalEditionMode: false
    });
	
	var contenuCarte = new Descartes.MapContent();

    var defcoucheAnnotations = {
	    title: "Annotations",
	    type: Descartes.Layer.EditionLayer.TYPE_Annotations
	};
	var defcouche1 = {
		title : "L_ENV_DOMAINE_SKIABLE_S_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_ENV_DOMAINE_SKIABLE_S_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				featureServerVersion: "1.1.0",
				useBboxSrsProjection: true,
				//featureInternalProjection: "EPSG:2154",
				layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_ENV_DOMAINE_SKIABLE_S_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_ENV_DOMAINE_SKIABLE_S_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
		}
	};

	var defcouche2 = {
		title : "L_PISTE_SKI_L_074", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "L_PISTE_SKI_L_074",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				featureServerVersion: "1.1.0",
				//featureInternalProjection: "EPSG:2154",
				useBboxSrsProjection: true,
				layerName: "L_PISTE_SKI_L_074",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/L_PISTE_SKI_L_074?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=L_PISTE_SKI_L_074&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POLYGON_GEOMETRY
		}
	};

	var defcouche3 = {
		title : "L_POINT_P", 
		type: 0,
		definition: [
		  {
				featureGeometryName: "the_geom",
				featureName: "Stations_hydro52_l93",
				featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featurePrefix: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/Stations_hydro52_l93",
				featureServerVersion: "1.1.0",
				//featureInternalProjection: "EPSG:2154",
				useBboxSrsProjection: true,
				layerName: "Stations_hydro52_l93",
				serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/Stations_hydro52_l93",
				serverVersion: "1.3.0"
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/N_INDUS_PREM_TRANSFO_BOIS_P_R26?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=N_INDUS_PREM_TRANSFO_BOIS_P_R26&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POINT_GEOMETRY
		}
	};
	
	var defcouche4 = {
		title : "Points d'eau ", 
		type: 4,
		definition: [
		  {
				//featureGeometryName: "the_geom",
				//featureName: "Stations_hydro52_l93",
				//featureNameSpace: "org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
				//featureServerUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/Stations_hydro52_l93",
				//featureServerVersion: "1.1.0",
				//featureInternalProjection: "EPSG:2154",
				//useBboxSrsProjection: true,
				layerName: "sa:PointEauIsole",
				serverUrl: "http://services.sandre.eaufrance.fr/geo/zonage",
				serverVersion: "1.1.0",
				displayMaxFeatures: 1000
		  }
		],
		options:{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: ["https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/f0443c65-01aa-417b-bde2-f0bdc9393560/N_INDUS_PREM_TRANSFO_BOIS_P_R26?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=N_INDUS_PREM_TRANSFO_BOIS_P_R26&LEGEND_OPTIONS=forceLabels%3Aon"],
			format: "image/png",
			geometryType: Descartes.Layer.POINT_GEOMETRY,
			symbolizers: {
            'Point': {
                pointRadius: 2, //6
                graphicName: 'circle',
                fillColor: '#ABE192',
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: '#ABE192',
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            },
            'MultiPoint': {
                pointRadius: 2, //6
                graphicName: 'circle',
                fillColor: '#ABE192',
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: '#ABE192',
                pointerEvents: 'visiblePainted',
                cursor: 'pointer'
            }
          }
		}
	};
    var couche1 = new Descartes.Layer.EditionLayer.Annotations(defcoucheAnnotations.title, defcoucheAnnotations.definition, defcoucheAnnotations.options);
    var couche3 = new Descartes.Layer.WMS(defcouche1.title, defcouche1.definition, defcouche1.options);
    var couche2 = new Descartes.Layer.WMS(defcouche2.title, defcouche2.definition, defcouche2.options);
    var couche4 = new Descartes.Layer.WMS(defcouche3.title, defcouche3.definition, defcouche3.options);
    var couche5 = new Descartes.Layer.WFS(defcouche4.title, defcouche4.definition, defcouche4.options);


    contenuCarte.addItem(couche1);
    contenuCarte.addItem(couche5);
    contenuCarte.addItem(couche4);
    contenuCarte.addItem(couche2);
    contenuCarte.addItem(couche3);
	contenuCarte.addItem(groupeFonds);
//  contenuCarte.addItem(coucheBase,groupeFonds);
	contenuCarte.addItem(coucheBdxBDorthoWmts,groupeFonds);
//    contenuCarte.addItem(coucheIGN,groupeFonds);


	var bounds = [-20037508,-20037508,20037508,20037508];
	var initbounds = [-800086,5055726,1150172,6772047];
	//var initbounds = [683697, 5738429, 804314, 5809973];
	var projection = 'EPSG:3857';
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
//					minScale:2150000,
					autoScreenSize: true
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar', null, {vertical:true});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});

    carte.addAnnotationToolBar(null, [ {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Layer.POINT_GEOMETRY,
                   snapping:true
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Layer.LINE_GEOMETRY,
                   snapping:true,
                   autotracing: true
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Layer.POLYGON_GEOMETRY,
                   snapping:true,
                   autotracing: true
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Tool.Edition.ANNOTATION_RECTANGLE_GEOMETRY,
                   snapping:true
               }
           }, {
               type: Descartes.Map.EDITION_DRAW_ANNOTATION,
               args: {
                   geometryType: Descartes.Tool.Edition.ANNOTATION_CIRCLE_GEOMETRY,
                   snapping:true
               }
           }, {
               type: Descartes.Map.EDITION_ARROW_ANNOTATION,
               args: {
                   snapping:true
               }
           }, {
               type: Descartes.Map.EDITION_FREEHAND_ANNOTATION,
               args: {
                   snapping:true,
                   autotracing: true
               }
           }, {
               type: Descartes.Map.EDITION_TEXT_ANNOTATION,
               args: {
                   snapping:true
               }
           }, {
               type: Descartes.Map.EDITION_GLOBAL_MODIFICATION_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_VERTICE_MODIFICATION_ANNOTATION,
               args: {
                   snapping:true
               }
           }, {
               type: Descartes.Map.EDITION_ADD_TEXT_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_ATTRIBUTE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_STYLE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_RUBBER_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_ERASE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_EXPORT_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_IMPORT_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_SNAPPING_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_GEOLOCATION_SIMPLE_ANNOTATION
           }, {
               type: Descartes.Map.EDITION_GEOLOCATION_TRACKING_ANNOTATION
           }
       ],
       {
    	   toolBar: toolsBar,// barre d'outils d'édition imbriquée dans la barre principale
   	       openerTool: true,
	       title: "Outils d'annotations",
		   panel:true,
		   draggable: {
            enable: true,
            containment: 'parent'
           },
           resizable:true
   	       //vertical: true
   	   } 
    );


    var managerOptions = {
        uiOptions: {
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withFilterColumns: true
            }
        }
    };
	
	carte.addContentManager('layersTree', null, managerOptions);
	
	
	// Affichage de la carte
	carte.show();
	
    carte.addOpenLayersInteractions([
         {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM},
         {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}}   
	]);
	
	// Ajout des select infos-bulle
    var selectToolTipLayers = [{layer: Descartes.AnnotationsLayer, fields: ['nom', 'description']}];
    carte.addSelectToolTip(selectToolTipLayers);
	
}