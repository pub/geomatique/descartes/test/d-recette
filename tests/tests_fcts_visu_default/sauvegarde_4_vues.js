proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

var context = {};
function chargementCarte() {
	chargeCouchesGroupesContext();
	
	// constitution de l'objet JSON de contexte
	var contextFile = (new Descartes.Url(this.location.href)).getParamValue("context");
	
	if (contextFile !== "") {
		// contexte sauvegardé
		var xhr = new XMLHttpRequest();
        xhr.open('GET', Descartes.CONTEXT_MANAGER_SERVER+"?" + contextFile);
        xhr.onload = function () {
            if (xhr.status === 200) {
            	getContext(xhr);
            } else {
            	stopMap();
            }
        };
        xhr.send();
	
	} else {
		context.bbox = {xMin:799205.2, yMin:6215857.5, xMax:1078390.1, yMax:6452614.0};

		context.size = {w:600 , h:400};
		context.items = [];
	
		context.items[0] = Descartes.Utils.extend(coucheToponymes, {itemType:"Layer"});
		context.items[1] = Descartes.Utils.extend(groupeInfras, {itemType:"Group", items:[]});
		context.items[1].items[0] = Descartes.Utils.extend(coucheParkings, {itemType:"Layer"});
		context.items[1].items[1] = Descartes.Utils.extend(coucheStations, {itemType:"Layer"});
		context.items[1].items[2] = Descartes.Utils.extend(coucheRoutes, {itemType:"Layer"});
		context.items[1].items[3] = Descartes.Utils.extend(coucheFer, {itemType:"Layer"});
		context.items[2] = Descartes.Utils.extend(coucheEau, {itemType:"Layer"});
		context.items[3] = Descartes.Utils.extend(coucheBati, {itemType:"Layer"});
		context.items[4] = Descartes.Utils.extend(coucheNature, {itemType:"Layer"});
		doMap();
	}
}
	
function doMap(){
	
	var contenuCarte = new Descartes.MapContent();

	contenuCarte.populate(context.items);
	
	var maxBounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
	var initBounds = [context.bbox.xMin, context.bbox.yMin, context.bbox.xMax, context.bbox.yMax];
	var projection = 'EPSG:2154';
	// Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initBounds,
					maxExtent: maxBounds,
					minScale:2150000,
					maxScale:100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initBounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});
	
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");
	
	// Ajout du gestionnaire de requete
	var laCoucheBati = carte.mapContent.getLayerById("coucheBati");
	var gestionnaireRequetes = carte.addRequestManager('Requetes');
	var requeteBati = new Descartes.Request(laCoucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);
	
	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	
	// Ajout du gestionnaire d'info-bulle
	var laCoucheParkings = carte.mapContent.getLayerById("coucheParkings");
	var laCoucheStations = carte.mapContent.getLayerById("coucheStations");
	carte.addToolTip('ToolTip', [
		{layer: laCoucheParkings, fields: ['name']},
		{layer: laCoucheStations, fields: ['name']}
	]);
	
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes');
}

function getContext(transport) {
	
	var response = transport.responseText;
		if (response.charAt(0) !== "{") {
			//alert("La carte est disponible jusqu'au " + response.substring(0, response.indexOf("{")));
			response = response.substring(response.indexOf("{"));
		}
	context = JSON.parse(response);
	doMap();
	
}

function stopMap() {
	alert("Impossible de recharger la carte: problème lors du chargement du fichier de contexte");
}