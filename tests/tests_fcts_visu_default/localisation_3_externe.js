proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupes();
	
	
		coucheBase = {
		title : "habillage metropole",
		type: 0,
		definition: [
			{
				serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur"
			}
		],
		options: {
			maxScale: 100,
			minScale: 10000001,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 50,
			opacityMax: 100,
			legend: [],
			metadataURL: null,
			format: "image/png"
		}
	};
	
	groupeFonds = {
		title: "Fonds cartographiques",
		options : {
			opened: true
		}
	};
	
	
	var contenuCarte = new Descartes.MapContent();
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
    var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

	var initbounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initbounds,
					maxExtent: bounds,
					minScale:2150000,
					maxScale:100,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initbounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	
	carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});

	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
	carte.addMiniMap("https://preprod.descartes.din.developpement-durable.gouv.fr/mapserver?LAYERS=c_natural_Valeurs_type");
	
	// Ajout du gestionnaire de requete
	var gestionnaireRequetes = carte.addRequestManager('Requetes');
	var requeteBati = new Descartes.Request(coucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
	var critereType = new Descartes.RequestMember(
					"Sélectionner le type",
					"type",
					"==",
					["chateau", "batiment public", "ecole", "eglise"],
					true
				);
	
	requeteBati.addMember(critereType);
	gestionnaireRequetes.addRequest(requeteBati);
	
	// Ajout du gestionnaire d'info-bulle
	carte.addToolTip('ToolTip', [
  		{layer: coucheParkings, fields: ['name']},
  		{layer: coucheStations, fields: ['name']}
  	]);
  	
  	/*carte.addToolTip('ToolTip', [ // pour intranet
   		{layer: coucheParkings, fields: ['name']},
   		{layer: coucheStations, fields: ['name']}
   	]);*/

	
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes');
	
	// Ajout du gestionnaire de localisation rapide
	/*var urlService = Descartes.getApplicationRoot()+"localize";
	var gazetteerService = new Descartes.GazetteerService(urlService, "niv", "projection", "parent", [{"key":"niveau", "value": 1}]);
	var niveauChoixDpt = new Descartes.GazetteerLevel("Choisissez un département", "Aucun département");
	carte.addGazetteer('Gazetteer', "93", [niveauChoixDpt],{service:gazetteerService});
	*/
		// Ajout du gestionnaire de localisation rapide
	var urlService = Descartes.getWebServiceRoot() +"/localize";
	var gazetteerService = new Descartes.GazetteerService(urlService, "niveau", "projection", "parent", [{"key":"niveauBase", "value": 0}]);
	var niveauChoix1 = new Descartes.GazetteerLevel("Choisissez un élément de niveau 1", "Aucun élément");
	var niveauChoix2 = new Descartes.GazetteerLevel("Choisissez un élément de niveau 2", "Aucun élément");
	carte.addGazetteer('Gazetteer', "", [niveauChoix1, niveauChoix2],{service:gazetteerService});
}
