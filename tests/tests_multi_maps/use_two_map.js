proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupesMS();
	chargeCouchesGroupesQS();
	
	var contenuCarte = new Descartes.MapContent();
	
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	
	var contenuCarte2 = new Descartes.MapContent();
	contenuCarte2.addItem(coucheToponymesQS);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte2.addItem(groupeInfrasQS);
	// Ajout de couches au groupe
	contenuCarte2.addItem(coucheParkingsQS, groupeInfrasQS);
	contenuCarte2.addItem(coucheStationsQS, groupeInfrasQS);
	contenuCarte2.addItem(coucheRoutesQS, groupeInfrasQS);
	contenuCarte2.addItem(coucheFerQS, groupeInfrasQS);
	// Ajout des autres couches
	contenuCarte2.addItem(coucheEauQS);
	contenuCarte2.addItem(coucheBatiQS);
	contenuCarte2.addItem(coucheNatureQS);
	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					maxScale:100,
					size: [600, 400]
				}
			);
	
	var carte2 = new Descartes.Map.ContinuousScalesMap(
			'map2',
			contenuCarte2,
			{
				projection: projection,
				initExtent: bounds,
				maxExtent: bounds,
				minScale:2150000,
				maxScale:100,
				size: [600, 400]
			}
		);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	
	//carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	carte2.show();
    
    carte2.OL_map.setView(carte.OL_map.getView());
}
