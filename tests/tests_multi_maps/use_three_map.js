proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	chargeCouchesGroupesMS();
	chargeCouchesGroupesGS();
	chargeCouchesGroupesQS();	
	
	var contenuCarte = new Descartes.MapContent();
	
	contenuCarte.addItem(coucheToponymes);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte.addItem(groupeInfras);
	// Ajout de couches au groupe
	contenuCarte.addItem(coucheParkings, groupeInfras);
	contenuCarte.addItem(coucheStations, groupeInfras);
	contenuCarte.addItem(coucheRoutes, groupeInfras);
	contenuCarte.addItem(coucheFer, groupeInfras);
	// Ajout des autres couches
	contenuCarte.addItem(coucheEau);
	contenuCarte.addItem(coucheBati);
	contenuCarte.addItem(coucheNature);
	
	var contenuCarte2 = new Descartes.MapContent();
	contenuCarte2.addItem(coucheToponymesGS);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte2.addItem(groupeInfrasGS);
	// Ajout de couches au groupe
	contenuCarte2.addItem(coucheParkingsGS, groupeInfrasGS);
	contenuCarte2.addItem(coucheStationsGS, groupeInfrasGS);
	contenuCarte2.addItem(coucheRoutesGS, groupeInfrasGS);
	contenuCarte2.addItem(coucheFerGS, groupeInfrasGS);
	// Ajout des autres couches
	contenuCarte2.addItem(coucheEauGS);
	contenuCarte2.addItem(coucheBatiGS);
	contenuCarte2.addItem(coucheNatureGS);

	var contenuCarte3 = new Descartes.MapContent();
	contenuCarte3.addItem(coucheToponymesQS);
	// Ajout d'un groupe de couches au contenu de la carte
	contenuCarte3.addItem(groupeInfrasQS);
	// Ajout de couches au groupe
	contenuCarte3.addItem(coucheParkingsQS, groupeInfrasQS);
	contenuCarte3.addItem(coucheStationsQS, groupeInfrasQS);
	contenuCarte3.addItem(coucheRoutesQS, groupeInfrasQS);
	contenuCarte3.addItem(coucheFerQS, groupeInfrasQS);
	// Ajout des autres couches
	contenuCarte3.addItem(coucheEauQS);
	contenuCarte3.addItem(coucheBatiQS);
	contenuCarte3.addItem(coucheNatureQS);
	
	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					maxScale:100,
					size: [600, 400]
				}
			);
	
	var carte2 = new Descartes.Map.ContinuousScalesMap(
			'map2',
			contenuCarte2,
			{
				projection: projection,
				initExtent: bounds,
				maxExtent: bounds,
				minScale:2150000,
				maxScale:100,
				size: [600, 400]
			}
		);
	
	var carte3 = new Descartes.Map.ContinuousScalesMap(
		'map3',
		contenuCarte3,
		{
			projection: projection,
			initExtent: bounds,
			maxExtent: bounds,
			minScale:2150000,
			maxScale:100,
			size: [600, 400]
		}
	);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	
	//carte.addContentManager('layersTree');
	
	// Affichage de la carte
	carte.show();
	carte2.show();
    carte3.show();
    
    carte2.OL_map.setView(carte.OL_map.getView());
    carte3.OL_map.setView(carte.OL_map.getView());
}
