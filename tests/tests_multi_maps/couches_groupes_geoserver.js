var coucheNatureGS, coucheBatiGS, coucheRoutesGS, coucheFerGS, coucheEauGS, coucheToponymesGS, coucheParkingsGS, coucheStationsGS, groupeInfrasGS;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeCouchesGroupesGS() {
	var serveur = "https://preprod.descartes.din.developpement-durable.gouv.fr/geoserver/descartes/ows?";

	coucheNatureGS = new Descartes.Layer.WMS(
		"Espaces naturels", 
		[
			{
				serverUrl: serveur,
				layerName: "c_natural_Valeurs_type"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheBatiGS = new Descartes.Layer.WMS(
		"Constructions", 
		[
			{
				serverUrl: serveur,
				layerName: "c_buildings",
				featureServerUrl: serveur,
				featureName: "c_buildings2",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		{
			maxScale: null,
			minScale: 39000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheRoutesGS = new Descartes.Layer.WMS(
		"Routes", 
		[
			{
				serverUrl: serveur,
				layerName: "c_roads"			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_roads"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheFerGS = new Descartes.Layer.WMS(
		"Chemins de fer", 
		[
			{
				serverUrl: serveur,
				layerName: "c_railways"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_railways"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheEauGS = new Descartes.Layer.WMS(
		"Cours d'eau", 
		[
			{
				serverUrl: serveur,
				layerName: "c_waterways_Valeurs_type"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheToponymesGS = new Descartes.Layer.WMS(
		"Lieux", 
		[
			{
				serverUrl: serveur,
				layerName: "c_places_Etiquettes"			}
			],
		{
			maxScale: null,
			minScale: 190000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: null,
			metadataURL: "http://metadataURL.fr",
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);
	
	coucheStationsGS = new Descartes.Layer.WMS(
		"Stations essence", 
		[
			{
				serverUrl: serveur,
				layerName: "c_stations",
				featureServerUrl: serveur,
				featureName: "c_stations",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheParkingsGS = new Descartes.Layer.WMS(
		"Parkings", 
		[
			{
				serverUrl: serveur,
				layerName: "c_parkings",
				featureServerUrl: serveur,
				featureName: "c_parkings",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_parkings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	groupeInfrasGS = new Descartes.Group(
		"Infrastructures", 
		{opened : false}
	);
}
