proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

function chargementCarte() {
	
	chargeCouchesGroupes();
	
	var contenuCarte = new Descartes.MapContent({editable: true});
	var gpFonds = contenuCarte.addItem(new Descartes.Group(groupeFonds.title, groupeFonds.options));
	contenuCarte.addItem(new Descartes.Layer.WMS(coucheBase.title, coucheBase.definition, coucheBase.options),gpFonds);

	var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
    var projection = 'EPSG:2154';
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: bounds,
					maxExtent: bounds,
					minScale:2150000,
					size: [600, 400]
				}
			);
	
	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_IN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.ZOOM_OUT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : bounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CENTER_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.COORDS_CENTER});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.NAV_HISTORY});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DISTANCE_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.AREA_MEASURE});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PNG_EXPORT});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.PDF_EXPORT});
	
	carte.addContentManager(
		'layersTree',
		[
			{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
			{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
			{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL}
		],
		{
			toolBarDiv: "managerToolBar"
		}
	);
	
	// Affichage de la carte
	carte.show();
	
	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition'});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend'});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});
	
	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector'});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser'});
	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput'});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf'});
	
	// Ajout asssitant localisation à l'adresse
    carte.addAction({type: Descartes.Action.LocalisationAdresse, div: 'localisationAdresse',
    	 options : {optionsPanel: {collapsible: true,collapsed: true,panelCss: 'bg-secondary'}}});
	
	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
	
	// Ajout de la minicarte
	carte.addMiniMap("http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geocat_metr.map&LAYERS=raster_geosignal",{open:true});
	
	// Ajout du gestionnaire de localisation rapide
	carte.addDefaultGazetteer('Gazetteer', "93", Descartes.Action.DefaultGazetteer.DEPARTEMENT);
}
