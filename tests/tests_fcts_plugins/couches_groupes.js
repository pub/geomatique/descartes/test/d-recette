var coucheBase, groupeFonds, coucheBdxBDorthoWmts, couche4326;

Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)

function chargeCouchesGroupes() {
	coucheBase = {
		title : "habillage metropole",
		type: 0,
		definition: [
			{
				serverUrl: "http://georef.e2.rie.gouv.fr/cartes/mapserv?",
				layerName: "fond_vecteur"
			}
		],
		options: {
			maxScale: 100,
			minScale: 10000001,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 50,
			opacityMax: 100,
			legend: [],
			metadataURL: null,
			format: "image/png"
		}
	};
	
	groupeFonds = {
		title: "Fonds cartographiques",
		options : {
			opened: true
		}
	};
	
    var matrixIdsWMTS = [
        'GoogleMapsCompatible:0',
        'GoogleMapsCompatible:1',
        'GoogleMapsCompatible:2',
        'GoogleMapsCompatible:3',
        'GoogleMapsCompatible:4',
        'GoogleMapsCompatible:5',
        'GoogleMapsCompatible:6',
        'GoogleMapsCompatible:7',
        'GoogleMapsCompatible:8',
        'GoogleMapsCompatible:9',
        'GoogleMapsCompatible:10',
        'GoogleMapsCompatible:11'
    ];
   
   var originsWMTS = [
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508],
      [-20037508.342789248, 20037508]
  ];
            
   var projectionWMTS = new Descartes.Projection('EPSG:3857');
   var extentWMTS = projectionWMTS.getExtent();

   var tileSize = 256;
   var sizeWMTS = ol.extent.getWidth(extentWMTS) / tileSize;

   var resolutionsWMTS = [];

   for (var i = 0; i < 19; i++) {
        resolutionsWMTS[i] = sizeWMTS / Math.pow(2, i);
        //matrixIds[i] = i;
   }

   coucheBdxBDorthoWmts = new Descartes.Layer.WMTS(
   		 'couche georef WMTS', 
   		 [
   		  	{
   		  		serverUrl: 'http://georef.e2.rie.gouv.fr/cache/service/wmts?',
   		  		layerName: 'georefmonde:GoogleMapsCompatible'
   		  	}
   		 ],
   		 {
   			 extent: extentWMTS,
   			 matrixSet: "GoogleMapsCompatible",
   			 projection: projectionWMTS,
   			 matrixIds: matrixIdsWMTS,
   			 origins: originsWMTS,
   			 resolutions: resolutionsWMTS,
   			 tileSize: [256, 256],
   			 format: 'image/jpeg',
   			 visible: true,
   			 opacity: 100
   		 }
   );
   
	couche4326 = {
			title : "Fond de carte",
			type: 0,
			definition: [
				{
					serverUrl: "http://vmap0.tiles.osgeo.org/wms/vmap0",
					layerName: "basic",
					featureServerUrl: null,
					featureName: null
				}
			],
			options: {
				maxScale: 100, // ex  100
				minScale: 50000,	// ex 500000
				alwaysVisible: false,
				visible: true,
				queryable:false,
				activeToQuery:false,
				sheetable:false,
				opacity: 100,
				opacityMax: 100,
				legend: null,
				metadataURL: null,
				format: "image/png",
				//displayOrder:2
			}
		};
	
	
}
