# D-Recette, ensemble des tests fonctionnels

Ce module regroupe l'ensemble des tests fonctionnels de Descartes.
 
## Accès aux tests

https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette
 